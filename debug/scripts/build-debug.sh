#!/bin/bash

prog=$(basename $0)
script_dir=$(dirname $0)
BASE_DIR=$( cd $script_dir/../..; echo $PWD )

cat <<EOF
Building and uploading debug container only. Separate from regular build.
EOF

cd $BASE_DIR

set -a

source .env/_common

# Official builds should not have parameters
VCS_BRANCH_ENV_FILE=$BASE_DIR/.env/empty
: > $VCS_BRANCH_ENV_FILE

set +a

#export DEBUG_VERSION=1.$(date +%s)
export DEBUG_VERSION=latest

pushd debug > /dev/null
docker-compose build
popd > /dev/null

# Upload the image
image_name=debug
local_image_name=ranqx/ranqx-loan/debug
image_tag=latest
local_tagged_image_name=$local_image_name:$image_tag

repository_root=727040197320.dkr.ecr.ap-southeast-2.amazonaws.com/ranqx/lo/
remote_image_name=$repository_root$image_name:$image_tag

echo "Pushing $remote_image_name"

docker tag $local_tagged_image_name $remote_image_name
docker push $remote_image_name
