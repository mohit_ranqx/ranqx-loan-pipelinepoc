# Debug Container

This is a simple container which can be quickly and easily deployed in order to
perform debugging activities, like figuring out networking and context
execution questions, without affecting the rest of the application. It is not
used in automated deployments, nor in production.
