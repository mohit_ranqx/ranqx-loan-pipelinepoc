#!/usr/bin/env bash -euo pipefail

docker system prune

docker stop $(docker ps -aq)
docker rm $(docker ps -aq)

docker volume ls | grep -v DRIVER | awk '{print "docker volume rm "$2}' | sh
