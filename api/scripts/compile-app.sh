#!/usr/bin/env bash

# NOTE: the latest version of the code is assumed to already be in the folder

# The user is usually www-data but it needs to be able to be overridden for local builds
# e.g. setting the user to ubuntu allows the application_builder_run to update bind mount data

USER=${1-"www-data"}
FOLDER=/var/www

# TODO: remove create user script line below after 19/06/2019
# test -f /usr/local/bin/bind-mount-create-user.sh && /usr/local/bin/bind-mount-create-user.sh || true

echo "=== Compile ${0} ==="

echo "Running as ${USER}"

pushd $FOLDER
touch .env
chown $USER .env

chown -R $USER $FOLDER/var
chmod -R ugo+rX $FOLDER
chmod -R ugo+rwX $FOLDER/var

su -l $USER -s /bin/bash -c "cd $FOLDER &&
    composer install &&
    find ${FOLDER}/var -type d -exec chmod 777 {} \; &&
    find ${FOLDER}/var -type f -exec chmod 666 {} \;"

cp /usr/local/bin/init-app.sh $FOLDER/bin

ls -al $FOLDER
ls -al $FOLDER/bin

echo "======================="
