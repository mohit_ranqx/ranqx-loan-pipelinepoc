#!/usr/bin/env bash

# Arguments:
#   $1 Ignored (used to be user to run as)
#   $2 APP_ENV, usually "prod" or "dev"
#   $3 Xero Queue Name
#   $4 Org Queue Name

echo "=== App Init ${0} ${1} ${2} ${3} ${4} ==="

# Run as the correct user: owner of the html folder
FOLDER=/var/www
OWNER=$( stat -c '%U:%G' $FOLDER )

if [[ "$OWNER" != "UNKNOWN:UNKNOWN" ]]; then
    USER=$( echo "$OWNER" | cut -d : -f 1 )
    echo "$OWNER is found, running as ${USER}"
else
    USER=www-build
    NEW_UID=$( stat -c '%u' $FOLDER )
    NEW_GID=$( stat -c '%g' $FOLDER )

    echo "Creating ${USER} ${NEW_UID}:${NEW_GID}"
    groupadd --gid $NEW_GID $USER
    useradd --shell /bin/bash --uid $NEW_UID --gid $NEW_GID $USER --home-dir $FOLDER

    stat -c '%U:%G' $FOLDER
fi

echo "Running as ${USER}"

su_run_console() {
    ccmd="php bin/console $1 --no-interaction --no-debug $2"
    echo "-----------------"
    echo "--running command: $ccmd"
    su --preserve-environment -l $USER -s /bin/bash -c "cd $FOLDER && $ccmd"
}

echo "Waiting until database connection is successful"
# This is useful in test scenarios when waiting for a docker-compose MySQL to start.
while ! su_run_console "doctrine:query:sql" "'select 1 from dual'" ; do
    echo "Database connection failed, sleeping and trying again"
    sleep 10
done

su_run_console "cache:clear"
su_run_console "doctrine:cache:clear-metadata"
su_run_console "doctrine:migrations:migrate"


echo "Set up seed in ${2} environment"

su --preserve-environment -l $USER -s /bin/bash -c "cd $FOLDER &&
    php bin/console app:ranqx:scorecard:seed --no-debug"

# For test environments, set up the queues
if [[ "${3}" =~ ^test_ ]] ; then
    echo "Set up queue in ${2} environment"

    for ((i=3;i<=$#;i++))
    do
      echo "Setting ${!i}"
      su_run_console "app:ranqx:queue:sqs:create" "${!i}"
    done
fi

load_referer_whitelist() {
    if [[ "$REFERER_WHITELIST" =~ ^null:// ]] ; then
        echo "Not Loading Referer Whitelist"
        return 0
    fi

    echo "Loading REFERER_WHITELIST=$REFERER_WHITELIST"

    local whitelist_file=$(mktemp /var/ranqx-init-tmp/referer_whitelist_init.XXXXXXXX)

    aws s3 cp $REFERER_WHITELIST $whitelist_file

    echo "Loading whitelist: $whitelist_file"
    cat $whitelist_file
    echo

    su_run_console "app:ranqx:load:whitelist" "$whitelist_file"
    ret_code=$?

    rm -f $whitelist_file
    if [[ $ret_code == 0 ]] ; then
        echo "Whitelist Loading Suceeded"
    else
        echo "Whitelist Loading Failed"
    fi

    return $ret_code
}

load_nzbn_data() {
    if [[ "$NZBN_DATA" =~ ^null:// ]] ; then
        echo "Not Loading NZBN Data"
        return 0
    fi

    echo "Loading NZBN_DATA=$NZBN_DATA"

    jsondatadir=$(mktemp -d /var/ranqx-init-tmp/nzbninit.XXXXXXXX)
    echo "$jsondatadir"
    df -h

    if [[ "$NZBN_DATA" =~ ^file:// ]] ; then
        local filename=${NZBN_DATA#file://}

        cp $FOLDER/$filename $jsondatadir

    elif [[ "$NZBN_DATA" =~ ^s3:// ]] ; then
        # Temporary - process files 1 at a time
        downloaddir=$(mktemp -d /var/ranqx-init-tmp/nzbninit.XXXXXXXX)
        echo "Downlading NZBN data from an S3 bucket"
        aws s3 sync $NZBN_DATA $downloaddir
        echo "Fetched Data"
        ls -lh $downloaddir
        pushd $downloaddir > /dev/null
        for zipfile in *.zip ; do
            mv $zipfile $jsondatadir

            pushd $jsondatadir > /dev/null
            unzip $zipfile
            rm $zipfile
            chmod -R ugo+rX $jsondatadir
            echo "Loading single NZBN Data file"
            ls -lha
            su_run_console "app:ranqx:load:nzbn:jsondir" "$jsondatadir"
            rm -f *
            popd > /dev/null

        done
        popd > /dev/null
        rm -rf "$jsondatadir" "$downloaddir"
        return 0

    elif [[ "$NZBN_DATA" =~ ^s3:// ]] ; then
        echo "Downlading NZBN data from an S3 bucket"
        aws s3 sync $NZBN_DATA $jsondatadir
        echo "Fetched Data"
        ls -lh $jsondatadir
        pushd $jsondatadir > /dev/null
        for zipfile in *.zip ; do
            unzip $zipfile
            rm $zipfile
        done
        popd > /dev/null

    else
        echo "NZBN_DATA value not a recognised format"
        return 1
    fi

    echo "NZBN data prepared in $jsondatadir"
    chmod -R go+rX "$jsondatadir"
    ls -la "$jsondatadir"

    su_run_console "app:ranqx:load:nzbn:jsondir" "$jsondatadir"
    ret_code=$?

    rm -rf "$jsondatadir"
    return $ret_code
}

# Load Referer Whitelist data if the variable is set and not empty
if [[ -v REFERER_WHITELIST ]] && [[ -n $(echo $REFERER_WHITELIST) ]] ; then
    load_referer_whitelist || echo "****Referer Whitelist load failed"
fi

# Load NZBN data if the variable is set and not empty
if [[ -v NZBN_DATA ]] && [[ -n $(echo $NZBN_DATA) ]] ; then
    load_nzbn_data || echo "****NZBN data load failed"
fi

echo "======================="
