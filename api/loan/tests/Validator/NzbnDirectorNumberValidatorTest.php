<?php declare(strict_types=1);

namespace App\Tests\Validator;

use PHPUnit\Framework\TestCase;
use App\Validator\NzbnDirectorNumberValidator;
use Symfony\Component\Validator\Validation;

class NzbnDirectorNumberValidatorTest extends TestCase
{

    private $validator;
    public function setUp()
    {
        $this->validator = Validation::createValidatorBuilder()
            ->addMethodMapping('loadValidatorMetadata')
            ->getValidator();
    }
    public function testValidate()
    {
        $v = new NzbnDirectorNumberValidator("2222", $this->validator);
        $errors = $v->validate();
        $this->assertNotEmpty($errors);
    }

    public function testValidate1()
    {
        $v = new NzbnDirectorNumberValidator('', $this->validator);
        $errors = $v->validate();
        $this->assertNotEmpty($errors);
    }

    public function testValidate3()
    {
        $v = new NzbnDirectorNumberValidator('9429037441074', $this->validator);
        $errors = $v->validate();
        $this->assertEmpty($errors);
    }
}