<?php declare(strict_types = 1);


namespace App\Tests\DataInterface;

use App\DataInterface\Equifax;
use App\DTO\CreateApplicant;
use App\Entity\Applicant;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Bundle\FrameworkBundle\Client;
use Doctrine\Common\Persistence\ObjectManager;
use App\DataInterface\RanqxDataHandler;
use App\Kernel;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class RanqxDataHandlerTest extends WebTestCase
{
    /** @var  Application $application */
    protected static $application;

    /** @var  Client $client */
    protected $client;

    protected $equifax;

    protected $params;


    /** @var  ObjectManager $entityManager */
    protected $entityManager;

    public function setUp()
    {
        $env = $_ENV['APP_ENV'] ?? $_SERVER['APP_ENV'] && getenv('APP_ENV');
        $this->client = static::createClient([
            'environment' => $env,
            'debug'       => false
        ]);
        $this->entityManager = self::$container->get('doctrine.orm.entity_manager');

        $apps = $this->entityManager->getRepository(Applicant::class)->findBy(['nzbnNumber' => '1111111111111']);
        foreach ($apps as $app) {
            $this->entityManager->remove($app);
        }
        $this->entityManager->flush();

        $bag = $this->getMockBuilder(ParameterBagInterface::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->getMock();

        $eq = ['base' => 'https://apiconnect.test.equifax.co.nz',
            'key' => '12b88e41-407e-4a12-a13f-24cc4c2c1ff3',
            'secret' => '06413694-27f8-42dd-b1f0-e6b3b66c1c0e',
            'tokenUrl' => '/auth/oauth/v2/token'];
        $map = array(
            array('equifax', $eq)
        );

        $bag->expects($this->any())
            ->method('get')
            ->will($this->returnValueMap($map));

        $this->params = new ParameterBag([]);
        $this->equifax = new Equifax($this->params);

        parent::setUp();
    }

    protected static function getKernelClass()
    {
        return Kernel::class;
    }

    protected static function runCommand($command)
    {
        $command = sprintf('%s --quiet', $command);
        return self::getApplication()->run(new StringInput($command));
    }

    protected static function getApplication()
    {
        if (null === self::$application) {
            $client = static::createClient();

            self::$application = new Application($client->getKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }

    public function testXeroData()
    {
        self::runCommand('doctrine:fixtures:load --append --no-interaction --group=AppFixtures');
        /** @var Applicant $app ***/
        $app = $this->entityManager->getRepository(Applicant::class)->findOneBy(['nzbnNumber' => '1111111111111']);
        if ($app) {
            $handler = new RanqxDataHandler($this->entityManager, $this->equifax, $this->params);
            $data = $handler->xeroData($app->getOrganisation()->getId());
            $this->assertEquals(24, $data['total']);
            //$this->assertEquals(24, \count($data));
        } else {
            $this->assertEquals(1, 2, 'could not load applicant');
        }

        $apps = $this->entityManager->getRepository(Applicant::class)->findBy(['nzbnNumber' => '1111111111111']);
        foreach ($apps as $app) {
            $this->entityManager->remove($app);
        }
        $this->entityManager->flush();
    }

    public function testNumberOfNzbn()
    {
        $app['businessName'] = 'ranqx';
        $app['email']	= 'anru.chen@ranqx.com';
        $app['firstName'] = 'anru';
        $app['isCustomer'] = 'no';
        $app['lastName'] = 'chen';
        $app['loanAmount']	= 55000;
        $app['loanReason']	= 'fg';
        $app['phone'] =	'02041355177';
        $app['nzbnNumber'] =	'1111111111111'; //13 1s
        $applicant = new CreateApplicant($app);
        $at = $applicant->build();
        $at->setCreated(new \DateTime("-4 month"));

        $this->entityManager->persist($at);

        $app['businessName'] = 'ranqx';
        $app['email']	= 'anru.chen@ranqx.com';
        $app['firstName'] = 'anru';
        $app['isCustomer'] = 'no';
        $app['lastName'] = 'chen';
        $app['loanAmount']	= 55000;
        $app['loanReason']	= 'fg';
        $app['phone'] =	'02041355177';
        $app['nzbnNumber'] =	'1111111111111'; //13 1s
        $applicant = new CreateApplicant($app);
        $at = $applicant->build();
        $at->setCreated(new \DateTime("-3 month"));

        $this->entityManager->persist($at);

        $this->entityManager->flush();

        $this->client->request('GET', '/ranqx/nzbn/number?nzbn=1111111111111', [], [], [
            'HTTP_USER_AGENT' => 'MySuperBrowser/1.0',
            'accept'=>'application/json'
        ]);

        $date = new \DateTime('-6 month');
        $handler = new RanqxDataHandler($this->entityManager, new Equifax($this->params), $this->params);
        $count = $handler->numberOf('1111111111111', $date);
        $this->assertEquals(2, $count);

        $apps = $this->entityManager->getRepository(Applicant::class)->findBy(['nzbnNumber' => '1111111111111']);
        foreach ($apps as $app) {
            $this->entityManager->remove($app);
        }
        $this->entityManager->flush();
    }

    public function testNumberOfNzbn1()
    {
        $app['businessName'] = 'ranqx';
        $app['email']	= 'anru.chen@ranqx.com';
        $app['firstName'] = 'anru';
        $app['isCustomer'] = 'no';
        $app['lastName'] = 'chen';
        $app['loanAmount']	= 55000;
        $app['loanReason']	= 'fg';
        $app['phone'] =	'02041355177';
        $app['nzbnNumber'] =	'1111111111111'; //13 1s
        $applicant = new CreateApplicant($app);
        $at = $applicant->build();
        $at->setCreated(new \DateTime("-4 month"));

        $this->entityManager->persist($at);

        $app['businessName'] = 'ranqx';
        $app['email']	= 'anru.chen@ranqx.com';
        $app['firstName'] = 'anru';
        $app['isCustomer'] = 'no';
        $app['lastName'] = 'chen';
        $app['loanAmount']	= 55000;
        $app['loanReason']	= 'fg';
        $app['phone'] =	'02041355177';
        $app['nzbnNumber'] =	'1111111111111'; //13 1s
        $applicant = new CreateApplicant($app);
        $at = $applicant->build();
        $at->setCreated(new \DateTime("-1 year"));

        $this->entityManager->persist($at);

        $this->entityManager->flush();

        $date = new \DateTime('-6 month');
        $handler = new RanqxDataHandler($this->entityManager, new Equifax($this->params), $this->params);
        $count = $handler->numberOf('1111111111111', $date);

        $this->assertEquals(1, $count);

        $apps = $this->entityManager->getRepository(Applicant::class)->findBy(['nzbnNumber' => '1111111111111']);
        foreach ($apps as $app) {
            $this->entityManager->remove($app);
        }
        $this->entityManager->flush();
    }

    public function testNumberOfNzbn2()
    {
        $app['businessName'] = 'ranqx';
        $app['email']	= 'anru.chen@ranqx.com';
        $app['firstName'] = 'anru';
        $app['isCustomer'] = 'no';
        $app['lastName'] = 'chen';
        $app['loanAmount']	= 55000;
        $app['loanReason']	= 'fg';
        $app['phone'] =	'02041355177';
        $app['nzbnNumber'] =	'1111111111111'; //13 1s
        $applicant = new CreateApplicant($app);
        $at = $applicant->build();
        $at->setCreated(new \DateTime("-14 month"));

        $this->entityManager->persist($at);

        $app['businessName'] = 'ranqx';
        $app['email']	= 'anru.chen@ranqx.com';
        $app['firstName'] = 'anru';
        $app['isCustomer'] = 'no';
        $app['lastName'] = 'chen';
        $app['loanAmount']	= 55000;
        $app['loanReason']	= 'fg';
        $app['phone'] =	'02041355177';
        $app['nzbnNumber'] =	'1111111111111'; //13 1s
        $applicant = new CreateApplicant($app);
        $at = $applicant->build();
        $at->setCreated(new \DateTime("-1 year"));

        $this->entityManager->persist($at);

        $this->entityManager->flush();

        $date = new \DateTime('-6 month');
        $handler = new RanqxDataHandler($this->entityManager, new Equifax($this->params), $this->params);
        $count = $handler->numberOf('1111111111111', $date);
        $this->assertEquals(0, $count);

        $apps = $this->entityManager->getRepository(Applicant::class)->findBy(['nzbnNumber' => '1111111111111']);
        foreach ($apps as $app) {
            $this->entityManager->remove($app);
        }
        $this->entityManager->flush();
    }

    public function testOldestData()
    {
        self::runCommand('doctrine:fixtures:load --append --no-interaction --group=OldestDataFixtures');
        /** @var Applicant $app ***/
        $app = $this->entityManager->getRepository(Applicant::class)->findOneBy(['nzbnNumber' => '1111111111111']);
        //dump($app);
        $handler = new RanqxDataHandler($this->entityManager, $this->equifax, $this->params);
        if ($app) {
            $data = $handler->oldestData((int)$app->getId());
            $period = $data['period'];
            $this->assertEquals('2018-08-02 03:41:20', $period->format('Y-m-d H:i:s'));
        } else {
            $this->assertEquals(24, 1, 'Cannot find Applicant.');
        }

        $apps = $this->entityManager->getRepository(Applicant::class)->findBy(['nzbnNumber' => '1111111111111']);
        foreach ($apps as $app) {
            $this->entityManager->remove($app);
        }
        $this->entityManager->flush();
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        $apps = $this->entityManager->getRepository(Applicant::class)->findBy(['nzbnNumber' => '1111111111111']);
        foreach ($apps as $app) {
            $this->entityManager->remove($app);
        }
        $this->entityManager->flush();

        parent::tearDown();
        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}