<?php declare(strict_types = 1);


namespace App\Tests\DataInterface;


use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\DataInterface\Equifax;

class EquifaxTest extends TestCase
{
    protected $equifax;
    public function setUp()
    {
        $bag = $this->getMockBuilder(ParameterBagInterface::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->getMock();

        $eq = ['base' => 'https://apiconnect.test.equifax.co.nz',
            'key' => '12b88e41-407e-4a12-a13f-24cc4c2c1ff3',
            'secret' => '06413694-27f8-42dd-b1f0-e6b3b66c1c0e',
            'tokenUrl' => '/auth/oauth/v2/token'];
        $map = array(
            array('equifax', $eq)
        );

        $bag->expects($this->any())
            ->method('get')
            ->will($this->returnValueMap($map));


        $this->equifax = new Equifax($bag);
    }

    public function testGetBaseUri()
    {
        $this->assertEquals('https://apiconnect.test.equifax.co.nz', $this->equifax->getBaseUri());
    }

    public function testGetOrganisationNumber()
    {
        $token  = $this->equifax->getAccessToken();
        //$orgId = $this->equifax->getOrganisationNumber($token, '1234123412345');
        //var_dump($orgId);
    }
}