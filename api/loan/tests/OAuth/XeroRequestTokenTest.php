<?php


namespace App\Tests\OAuth;
use PHPUnit\Framework\TestCase;
use App\OAuth\XeroRequestToken;

class XeroRequestTokenTest extends TestCase
{
    private $tokenStr;
    public function setUp()
    {
        $this->tokenStr = 'oauth_token=hdk48Djdsa&oauth_token_secret=xyz4992k83j47x0b&oauth_callback_confirmed=true';
    }
    public function testToken()
    {
        $token = new XeroRequestToken($this->tokenStr);
        $value = $token->token();
        $expected = 'hdk48Djdsa';
        $this->assertEquals($value, $expected);
    }

    public function testSecret()
    {
        $token = new XeroRequestToken($this->tokenStr);
        $value = $token->secret();
        $expected = 'xyz4992k83j47x0b';
        $this->assertEquals($value, $expected);
    }

    public function testTokenEmpty()
    {
        $token = new XeroRequestToken('');
        $value = $token->token();
        $this->assertEquals($value, '');
    }

    public function testSecretEmpty()
    {
        $token = new XeroRequestToken('');
        $value = $token->secret();
        $this->assertEquals($value, '');
    }
}