<?php


namespace App\Tests\OAuth;

use PHPUnit\Framework\TestCase;
use App\OAuth\OAuthFixedNonceTime;
class OAuthFixedNonceTimeTest extends TestCase
{
    public function testRun()
    {
        $once = new OAuthFixedNonceTime();
        $result = $once->run();
        $result2 = $once->run();
        $this->assertEquals($result, $result2);
    }

    public function testRunMultipleTime()
    {
        $once = new OAuthFixedNonceTime();
        $result = $once->run();
        $result2 = $once->run();
        $this->assertEquals($result, $result2);

        $result3 = $once->run();
        $this->assertEquals($result, $result3);
        $this->assertEquals($result2, $result3);
    }

    public function testTime()
    {
        $once = new OAuthFixedNonceTime();
        $this->assertEquals( $once->getTime(), $once->getTime());
    }

    public function testNonce()
    {
        $once = new OAuthFixedNonceTime();
        $this->assertEquals( $once->getNonce(), $once->getNonce());
    }

    public function testRunThenTime()
    {
        $once = new OAuthFixedNonceTime();
        $result = $once->run();
        $time = $once->getTime();
        $this->assertEquals($result['time'], $time);
    }

    public function testRunThenNonce()
    {
        $once = new OAuthFixedNonceTime();
        $result = $once->run();
        $nonce = $once->getNonce();
        $this->assertEquals($result['nonce'], $nonce);
    }

    public function testRunAndNonceAndTime()
    {
        $once = new OAuthFixedNonceTime();
        $result = $once->run();
        $nonce = $once->getNonce();
        $time  = $once->getTime();
        $this->assertEquals($result['nonce'], $nonce);
        $this->assertEquals($result['time'], $time);
    }

    public function testTimeThenRun()
    {
        $once = new OAuthFixedNonceTime();
        $time = $once->getTime();
        $result = $once->run();
        $this->assertEquals($result['time'], $time);
    }

    public function testNonceThenRun()
    {
        $once = new OAuthFixedNonceTime();
        $n = $once->getNonce();
        $result = $once->run();
        $this->assertEquals($result['nonce'], $n);
    }
}