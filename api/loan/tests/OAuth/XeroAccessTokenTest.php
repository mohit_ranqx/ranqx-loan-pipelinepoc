<?php


namespace App\Tests\OAuth;
use PHPUnit\Framework\TestCase;
use App\OAuth\XeroAccessToken;

class XeroAccessTokenTest extends TestCase
{
    private $accessTokenStr;
    public function setUp()
    {
        $this->accessTokenStr = 'oauth_token=VMELVG0K07OWEQKSMSQRFKKWO63SMJ&oauth_token_secret=MJLX9FHES9BFMX6VZLWVLXBIMYFS0U';
        $this->accessTokenStr .='&oauth_expires_in=1800&oauth_session_handle=M723JS90ORQBY8SDIDU8';
        $this->accessTokenStr .= '&oauth_authorization_expires_in=315360000&xero_org_muid=KWGgdLkBU4wPb3FsGvMiDW';
    }

    public function testToken()
    {
        $acc = new XeroAccessToken($this->accessTokenStr);
        $value = $acc->token();
        $expected = 'VMELVG0K07OWEQKSMSQRFKKWO63SMJ';
        $this->assertEquals($value, $expected);
    }

    public function testSecret()
    {
        $acc = new XeroAccessToken($this->accessTokenStr);
        $value = $acc->secret();
        $expected = 'MJLX9FHES9BFMX6VZLWVLXBIMYFS0U';
        $this->assertEquals($value, $expected);
    }

    public function testTokenEmpty()
    {
        $acc = new XeroAccessToken('');
        $value = $acc->token();
        $this->assertEquals($value, '');
    }

    public function testSecretEmpty()
    {
        $acc = new XeroAccessToken('');
        $value = $acc->secret();
        $this->assertEquals($value, '');
    }
}