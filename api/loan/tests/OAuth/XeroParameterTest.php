<?php


namespace App\Tests\OAuth;
use PHPUnit\Framework\TestCase;

class XeroParameterTest extends TestCase
{
    /* @var XeroParameterDummy $para ***/
    protected $para;
    protected function setUp()
    {
        $this->para = new XeroParameterDummy();
    }

    public function testParametersKeyInOrder(): void
    {
        //parameters returns a sorted array;
        $result = $this->para->parameters();
        //first one should be oauth_callback
        $value = array_splice($result, 0, 1);
        $this->assertArrayHasKey('oauth_callback', $value);

        //second key should be oauth_consumer_key
        $value = array_splice($result, 0, 1);
        $this->assertArrayHasKey('oauth_consumer_key', $value);

        //3rd one is oauth_nonce
        $value = array_splice($result, 0, 1);
        $this->assertArrayHasKey('oauth_nonce', $value);

        //4th one is oauth_signature_method
        $value = array_splice($result, 0, 1);
        $this->assertArrayHasKey('oauth_signature_method', $value);

        //5th one is oauth_timestamp
        $value = array_splice($result, 0, 1);
        $this->assertArrayHasKey('oauth_timestamp', $value);

        //6th one is oauth_version
        $value = array_splice($result, 0, 1);
        $this->assertArrayHasKey('oauth_version', $value);
    }
}