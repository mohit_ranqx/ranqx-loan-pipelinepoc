<?php


namespace App\Tests\OAuth;
use App\OAuth\OAuth1ConfigInterface;
use App\OAuth\OAuthFixedNonceTime;

class XeroParameterDummy implements OAuth1ConfigInterface
{
    /* @var OAuthFixedNonceTime $nonceTime ***/
    private $nonceTime;
    private $extra;

    public function __construct()
    {
        $this->nonceTime = new OAuthFixedNonceTime();
        $this->extra = [];
    }

    public function parameters(): array
    {
        $temp['oauth_callback'] = $this->callback();
        $temp['oauth_consumer_key'] = $this->consumerKey();
        $temp['oauth_nonce'] = $this->getNonce();
        $temp['oauth_signature_method'] = $this->algo();
        $temp['oauth_timestamp'] = $this->getTime();
        $temp['oauth_version'] = $this->oauthVersion();

        ksort($temp);
        return $temp;
    }

    public function callback(): string
    {
        return 'https://localhost.ranqx.com/xero/success';
    }

    public function getNonce(): string
    {
        return 'abcde';
    }

    public function getTime(): int
    {
        return 123456;
    }

    public function algo(): string
    {
        return 'RSA-SHA1';
    }

    public function consumerKey(): string
    {
        return '123456';
    }

    public function oauthVersion(): string
    {
        return '1.0';
    }

    public function requestTokenUri(): string
    {
        return 'https://api.xero.com/oauth/RequestToken';
    }

    public function authorizationUri(): string
    {
        return 'https://api.xero.com/oauth/Authorize';
    }

    public function accessTokenUri(): string
    {
        return 'https://api.xero.com/oauth/AccessToken';
    }

    public function getPublicKey(): string
    {
        return '1';
    }

    public function getPrivateKey(): string
    {
        return '1';
    }

    public function addExtra($key, $value): void
    {
        $this->extra['key'] = $value;
    }

    public function apiUri(): string
    {
        return 'https://api.xero.com/api.xro/2.0/';
    }


}