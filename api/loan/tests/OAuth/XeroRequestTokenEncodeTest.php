<?php


namespace App\Tests\OAuth;

use PHPUnit\Framework\TestCase;
use App\OAuth\XeroRequestTokenEncode;

class XeroRequestTokenEncodeTest extends TestCase
{

    public function testFirstEncode()
    {
        $enc = new XeroRequestTokenEncode(new XeroParameterDummy());
        $value = $enc->encodeFirst();
        $expected = \rawurlencode('GET') . '&';
        $this->assertEquals($expected, $value);
    }

    public function testSecondEncode()
    {
        $enc = new XeroRequestTokenEncode(new XeroParameterDummy());
        $value = $enc->encodeSecond();
        $expected = \rawurlencode('https://api.xero.com/oauth/RequestToken') . '&';
        $this->assertEquals($expected, $value);
    }

    public function testBaseEncode()
    {
        $p = new XeroParameterDummy();
        $enc = new XeroRequestTokenEncode($p);
        $value = $enc->encodeBase();
        $expected = \rawurlencode('oauth_callback').'='.\rawurlencode($p->callback());
        $expected .= '&'.\rawurlencode('oauth_consumer_key').'='.\rawurlencode($p->consumerKey());
        $expected .= '&'.\rawurlencode('oauth_nonce').'='.\rawurlencode($p->getNonce());
        $expected .= '&'.\rawurlencode('oauth_signature_method').'='.\rawurlencode($p->algo());
        $expected .= '&'.\rawurlencode('oauth_timestamp').'='.\rawurlencode((string)$p->getTime());
        $expected .= '&'.\rawurlencode('oauth_version').'='.\rawurlencode($p->oauthVersion());
        $this->assertEquals($expected, $value);
    }
}