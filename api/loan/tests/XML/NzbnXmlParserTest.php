<?php declare(strict_types = 1);


namespace App\Tests\XML;

use PHPUnit\Framework\TestCase;
use App\XML\NzbnXmlParser;

class NzbnXmlParserTest extends TestCase
{
    public function testParse()
    {
        $process = function ($companies) {
            foreach ($companies as $company) {
                $this->assertEquals('9429041155967', $company['nzbn']);
                $this->assertEquals('ACCOUNTING METRICS (NZ) LIMITED', $company['name']);
                $this->assertEquals('5072341', $company['company_number']);
                $this->assertEquals('LTD', $company['source_register']);
                $this->assertEquals(1, $company['status']);
                $this->assertEquals('New Zealand', $company['country_of_origin']);
                $this->assertEquals('2014-03-24 16:51:59', $company['registration_date']);
            }
        };
        $parser = new NzbnXmlParser(__DIR__.'/nzbnxml1.xml', $process);
        $parser->parse();
    }

    public function testParse1()
    {
        //testing country
        $process = function ($companies) {
            foreach ($companies as $company) {
                $this->assertEquals('9429041155967', $company['nzbn']);
                $this->assertEquals('ACCOUNTING METRICS (NZ) LIMITED', $company['name']);
                $this->assertEquals('5072341', $company['company_number']);
                $this->assertEquals('ASIC', $company['source_register']);
                $this->assertEquals(1, $company['status']);
                $this->assertEquals('Australia', $company['country_of_origin']);
                $this->assertEquals('2015-03-24 16:51:59', $company['registration_date']);
            }
        };
        $parser = new NzbnXmlParser(__DIR__.'/nzbnxml2.xml', $process);
        $parser->parse();
    }

    public function testParse2()
    {
        //testing country of origin
        $process = function ($companies) {
            foreach ($companies as $company) {
                $this->assertEquals('9429041155967', $company['nzbn']);
                $this->assertEquals('ACCOUNTING METRICS (NZ) LIMITED', $company['name']);
                $this->assertEquals('5072341', $company['company_number']);
                $this->assertEquals('NON_ASIC', $company['source_register']);
                $this->assertEquals(1, $company['status']);
                $this->assertEquals('VIRGIN ISLANDS, BRITISH', $company['country_of_origin']);
                $this->assertEquals('2014-09-24 16:51:59', $company['registration_date']);
            }
        };
        $parser = new NzbnXmlParser(__DIR__.'/nzbnxml3.xml', $process);
        $parser->parse();
    }

    public function testParse3()
    {
        //testing status
        $process = function ($companies) {
            foreach ($companies as $company) {
                $this->assertEquals('9429041155967', $company['nzbn']);
                $this->assertEquals('ACCOUNTING METRICS (NZ) LIMITED', $company['name']);
                $this->assertEquals('5072341', $company['company_number']);
                $this->assertEquals('ASIC', $company['source_register']);
                $this->assertEquals(0, $company['status']);
                $this->assertEquals('Australia', $company['country_of_origin']);
                $this->assertEquals('2014-03-24 16:51:59', $company['registration_date']);
            }
        };
        $parser = new NzbnXmlParser(__DIR__.'/nzbnxml4.xml', $process);
        $parser->parse();
    }

    public function testParse4()
    {
        //testing status
        $process = function ($companies) {
            foreach ($companies as $company) {
                $this->assertEquals('9429046740205', $company['nzbn']);
                $this->assertEquals('PARSONS RODDICK & CO LIMITED', $company['name']);
                $this->assertEquals('6816740', $company['company_number']);
                $this->assertEquals('LTD', $company['source_register']);
                $this->assertEquals(1, $company['status']);
                $this->assertEquals('New Zealand', $company['country_of_origin']);
                $this->assertEquals('2018-04-24 09:46:19', $company['registration_date']);
            }
        };
        $parser = new NzbnXmlParser(__DIR__.'/nzbnxml5.xml', $process);
        $parser->parse();
    }

}
