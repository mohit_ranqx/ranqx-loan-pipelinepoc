<?php declare(strict_types=1);


namespace App\Tests\DTO;

use PHPUnit\Framework\TestCase;
use App\DTO\CreateDirectorFromJson;

class CreateDirectorFromJsonTest extends TestCase
{
    public function testNumberOfDirectors()
    {
        $json = file_get_contents(__DIR__ . '/test.json');
        $director = new CreateDirectorFromJson($json);
        $r = $director->toArray();
        $this->assertCount(2, $r);
    }

    public function testNumberOfDirectors2()
    {
        $json = file_get_contents(__DIR__ . '/test1.json');
        $director = new CreateDirectorFromJson($json);
        $r = $director->toArray();
        $this->assertCount(1, $r);
    }

    public function testNameOfDirector()
    {
        $json = file_get_contents(__DIR__ . '/test1.json');
        $director = new CreateDirectorFromJson($json);
        $r = $director->toArray();
        $this->assertEquals('Snjezana', $r[0]['firstName']);
    }

    public function testTitleOfDirector()
    {
        $json = file_get_contents(__DIR__ . '/test1.json');
        $director = new CreateDirectorFromJson($json);
        $r = $director->toArray();
        $this->assertEquals(null, $r[0]['title']);
    }
}