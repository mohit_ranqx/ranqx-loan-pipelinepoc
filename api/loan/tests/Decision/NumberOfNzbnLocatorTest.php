<?php declare(strict_types=1);


namespace App\Tests\Decision;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use App\Decision\NumberOfNzbn;
use App\Decision\NumberOfNzbnLocator;
use PHPUnit\Framework\TestCase;

class NumberOfNzbnLocatorTest extends TestCase
{
    public function testBadData(): void
    {
        $dataSrc['nzbn'] = 'test';
        $obj = new NumberOfNzbnLocator();
        $this->expectException(KiwiBankDataSrcBadException::class);
        $obj->findData($dataSrc);
    }

    public function testMissingData(): void
    {
        $obj = new NumberOfNzbnLocator();
        $this->expectException(KiwiBankDataSrcNoExistException::class);
        $obj->findData([]);
    }

    public function testGoodData(): void
    {
        $dataSrc['nzbn'] = '2';
        $obj = new NumberOfNzbnLocator();
        $result = $obj->findData($dataSrc);
        $this->assertInstanceOf(NumberOfNzbn::class, $result);
    }

    public function testBadExceptionData(): void
    {
        try {
            $dataSrc['nzbn'] = 'test';
            $obj = new NumberOfNzbnLocator();
            $obj->findData($dataSrc);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcBadException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new NumberOfNzbn('test');
            $actual = $obj->dataIsBad();
            $this->assertEquals($expected, $actual);
        }
    }

    public function testMissingExceptionData(): void
    {
        try {
            $obj = new NumberOfNzbnLocator();
            $obj->findData([]);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcNoExistException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new NumberOfNzbn(null);
            $actual = $obj->dataMissing();
            $this->assertEquals($expected, $actual);
        }
    }
}