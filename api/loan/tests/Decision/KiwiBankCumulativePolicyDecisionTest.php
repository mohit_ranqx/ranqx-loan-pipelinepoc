<?php


namespace App\Tests\Decision;


use App\Decision\KiwiBankCumulativePolicyDecision;
use App\Decision\Policy;
use PHPUnit\Framework\TestCase;

class KiwiBankCumulativePolicyDecisionTest extends TestCase
{
    public function testCumulativePolicyDecision()
    {
        $rule = new KiwiBankCumulativePolicyDecision([]);
        $actual = $rule->make();
        $this->assertEquals(Policy::REFER, $actual);
    }

    public function testCumWithData()
    {
        $json = file_get_contents(__DIR__.'/result.json');
        $src = \json_decode($json, true);
        $rule = new KiwiBankCumulativePolicyDecision($src);
        $actual = $rule->make();
        $this->assertEquals(Policy::DECLINE, $actual);
    }

    public function testCumWithRefer()
    {
        $json = file_get_contents(__DIR__.'/result_refer.json');
        $src = \json_decode($json, true);
        $rule = new KiwiBankCumulativePolicyDecision($src);
        $actual = $rule->make();
        $this->assertEquals(Policy::REFER, $actual);
    }

    public function testCumWithPass()
    {
        $json = file_get_contents(__DIR__.'/result_pass.json');
        $src = \json_decode($json, true);
        $rule = new KiwiBankCumulativePolicyDecision($src);
        $actual = $rule->make();
        $this->assertEquals(Policy::PASS, $actual);
    }
}