<?php declare(strict_types = 1);


namespace App\Tests\Decision;

use App\Decision\BadBureauScore;
use App\Decision\BadBureauScoreLocator;
use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use PHPUnit\Framework\TestCase;

class BadBureauScoreLocatorTest extends TestCase
{
    public function testBadData(): void
    {
        $dataSrc['equiFax']['decision']['score'][0]['score'] = 'test';
        $locator = new BadBureauScoreLocator();
        $this->expectException(KiwiBankDataSrcBadException::class);
        $locator->findData($dataSrc);
    }

    public function testMissingData(): void
    {
        $locator = new BadBureauScoreLocator();
        $this->expectException(KiwiBankDataSrcNoExistException::class);
        $locator->findData([]);
    }

    public function testGoodData(): void
    {
        $locator = new BadBureauScoreLocator();
        $dataSrc['equiFax']['decision']['score'][0]['score'] = '234';
        $r = $locator->findData($dataSrc);
        $this->assertInstanceOf(BadBureauScore::class, $r);
    }

    public function testBadExceptionData(): void
    {
        try {
            $dataSrc['equiFax']['decision']['score'][0]['score'] = 'test';
            $obj = new BadBureauScoreLocator();
            $obj->findData($dataSrc);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcBadException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new BadBureauScore('test');
            $actual = $obj->dataIsBad();
            $this->assertEquals($expected, $actual);
        }
    }

    public function testMissingExceptionData(): void
    {
        try {
            $obj = new BadBureauScoreLocator();
            $obj->findData([]);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcNoExistException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new BadBureauScore(null);
            $actual = $obj->dataMissing();
            $this->assertEquals($expected, $actual);
        }
    }
}