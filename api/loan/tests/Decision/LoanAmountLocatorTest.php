<?php declare(strict_types=1);


namespace App\Tests\Decision;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use App\Decision\LoanAmount;
use App\Decision\LoanAmountLocator;
use PHPUnit\Framework\TestCase;

class LoanAmountLocatorTest extends TestCase
{
    public function testBadData(): void
    {
        $dataSrc['loanAmount'] = 'test';
        $obj = new LoanAmountLocator();
        $this->expectException(KiwiBankDataSrcBadException::class);
        $obj->findData($dataSrc);
    }

    public function testMissingData(): void
    {
        $obj = new LoanAmountLocator();
        $this->expectException(KiwiBankDataSrcNoExistException::class);
        $obj->findData([]);
    }

    public function testGoodData(): void
    {
        $dataSrc['loanAmount'] = '10000';
        $obj = new LoanAmountLocator();
        $result = $obj->findData($dataSrc);
        $this->assertInstanceOf(LoanAmount::class, $result);
    }

    public function testBadExceptionData(): void
    {
        try {
            $dataSrc['loanAmount'] = 'test';
            $obj = new LoanAmountLocator();
            $obj->findData($dataSrc);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcBadException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new LoanAmount('test');
            $actual = $obj->dataIsBad();
            $this->assertEquals($expected, $actual);
        }
    }

    public function testMissingExceptionData(): void
    {
        try {
            $obj = new LoanAmountLocator();
            $obj->findData([]);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcNoExistException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new LoanAmount(null);
            $actual = $obj->dataMissing();
            $this->assertEquals($expected, $actual);
        }
    }
}