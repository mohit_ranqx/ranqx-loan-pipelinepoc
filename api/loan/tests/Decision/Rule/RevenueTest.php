<?php declare(strict_types = 1);


namespace App\Tests\Decision\Rule;

use PHPUnit\Framework\TestCase;
use App\Decision\Rule\Revenue;

class RevenueTest extends TestCase
{
    public function testDecision()
    {
        $rule = new Revenue(5000);
        $answer = $rule->accept(50000);
        $this->assertFalse($answer);
    }

    public function testDecision2()
    {
        $rule = new Revenue(5000);
        $answer = $rule->accept(50);
        $this->assertTrue($answer);
    }
}