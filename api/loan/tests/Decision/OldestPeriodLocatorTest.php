<?php declare(strict_types=1);


namespace App\Tests\Decision;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use App\Decision\NumberOfNzbn;
use App\Decision\NumberOfNzbnLocator;
use App\Decision\OldestPeriod;
use App\Decision\OldestPeriodLocator;
use PHPUnit\Framework\TestCase;

class OldestPeriodLocatorTest extends TestCase
{
    public function testMissingData(): void
    {
        $obj = new OldestPeriodLocator();
        $this->expectException(KiwiBankDataSrcNoExistException::class);
        $obj->findData([]);
    }

    public function testGoodData(): void
    {
        $dataSrc['xeroData'] = [];
        $obj = new OldestPeriodLocator();
        $this->expectException(KiwiBankDataSrcNoExistException::class);
        $result = $obj->findData($dataSrc);
        //$this->assertInstanceOf(OldestPeriod::class, $result);
    }

    public function testMissingExceptionData(): void
    {
        try {
            $obj = new OldestPeriodLocator();
            $obj->findData([]);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcNoExistException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new OldestPeriod([]);
            $actual = $obj->dataMissing();
            $this->assertEquals($expected, $actual);
        }
    }
}