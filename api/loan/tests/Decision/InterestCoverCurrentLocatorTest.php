<?php declare(strict_types=1);


namespace App\Tests\Decision;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use App\Decision\InterestCoverCurrent;
use App\Decision\InterestCoverCurrentLocator;
use PHPUnit\Framework\TestCase;

class InterestCoverCurrentLocatorTest extends TestCase
{
    public function testBadData(): void
    {
        $dataSrc['calFinanceCurrentRolling'][0]['interest_cover_rolling'] = 'test';
        $obj = new InterestCoverCurrentLocator();
        $this->expectException(KiwiBankDataSrcBadException::class);
        $obj->findData($dataSrc);
    }

    public function testMissingData(): void
    {
        $obj = new InterestCoverCurrentLocator();
        $this->expectException(KiwiBankDataSrcNoExistException::class);
        $obj->findData([]);
    }

    public function testGoodData(): void
    {
        $dataSrc['calFinanceCurrentRolling'][0]['interest_cover_rolling'] = '0.98';
        $obj = new InterestCoverCurrentLocator();
        $result = $obj->findData($dataSrc);
        $this->assertInstanceOf(InterestCoverCurrent::class, $result);
    }

    public function testBadExceptionData(): void
    {
        try {
            $dataSrc['calFinanceCurrentRolling'][0]['interest_cover_rolling'] = 'test';
            $obj = new InterestCoverCurrentLocator();
            $obj->findData($dataSrc);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcBadException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new InterestCoverCurrent('test');
            $actual = $obj->dataIsBad();
            $this->assertEquals($expected, $actual);
        }
    }

    public function testMissingExceptionData(): void
    {
        try {
            $obj = new InterestCoverCurrentLocator();
            $obj->findData([]);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcNoExistException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new InterestCoverCurrent(null);
            $actual = $obj->dataMissing();
            $this->assertEquals($expected, $actual);
        }
    }
}