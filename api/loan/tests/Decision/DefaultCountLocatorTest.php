<?php declare(strict_types=1);


namespace App\Tests\Decision;

use App\Decision\DefaultCount;
use App\Decision\DefaultCountLocator;
use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use PHPUnit\Framework\TestCase;

class DefaultCountLocatorTest extends TestCase
{
    public function testBadData(): void
    {
        $dataSrc['equiFax']['decision']['summary']['defaultsCount'] = 'test';
        $obj = new DefaultCountLocator();
        $this->expectException(KiwiBankDataSrcBadException::class);
        $obj->findData($dataSrc);
    }

    public function testMissingData(): void
    {
        $obj = new DefaultCountLocator();
        $this->expectException(KiwiBankDataSrcNoExistException::class);
        $obj->findData([]);
    }

    public function testGoodData(): void
    {
        $dataSrc['equiFax']['decision']['summary']['defaultsCount'] = '0';
        $obj = new DefaultCountLocator();
        $result = $obj->findData($dataSrc);
        $this->assertInstanceOf(DefaultCount::class, $result);
    }

    public function testBadExceptionData(): void
    {
        try {
            $dataSrc['equiFax']['decision']['summary']['defaultsCount'] = 'test';
            $obj = new DefaultCountLocator();
            $obj->findData($dataSrc);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcBadException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new DefaultCount('test');
            $actual = $obj->dataIsBad();
            $this->assertEquals($expected, $actual);
        }
    }

    public function testMissingExceptionData(): void
    {
        try {
            $obj = new DefaultCountLocator();
            $obj->findData([]);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcNoExistException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new DefaultCount(null);
            $actual = $obj->dataMissing();
            $this->assertEquals($expected, $actual);
        }
    }
}