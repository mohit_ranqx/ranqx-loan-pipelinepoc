<?php declare(strict_types=1);


namespace App\Tests\Decision;

use App\DTO\CreateApplicant;
use App\Kernel;
use App\Message\ApplicantExceptionMessage;
use App\Message\SetDecisionExceptionMessage;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DecisionFlagTest
 * @package App\Tests\Decision
 */
class DecisionFlagTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager|object
     */
    private $entityManager;
    /**
     * @var object|\Symfony\Component\Messenger\TraceableMessageBus
     */
    private $bus;

    /**
     *
     */
    public function setUp()
    {
        $env = $_ENV['APP_ENV'] ?? $_SERVER['APP_ENV'] && getenv('APP_ENV');
        $this->client = static::createClient([
            'environment' => $env,
            'debug'       => false
        ]);

        $this->entityManager = self::$container->get('doctrine.orm.entity_manager');
        //message_bus
        $this->bus = self::$container->get('message_bus');
    }

    /**
     * @return string
     */
    protected static function getKernelClass()
    {
        return Kernel::class;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Throwable
     */
    public function testFlag()
    {
        $app['businessName'] = 'ranqx';
        $app['email']	= 'anru.chen@ranqx.com';
        $app['firstName'] = 'anru';
        $app['isCustomer'] = 'no';
        $app['lastName'] = 'chen';
        $app['loanAmount']	= 55000;
        $app['loanReason']	= 'fg';
        $app['phone'] =	'02041355177';
        $app['nzbnNumber'] =	'1111111111112'; //13 1s
        $applicant = new CreateApplicant($app);
        $at = $applicant->build();
        $at->setCreated(new \DateTime("-4 month"));

        $this->entityManager->persist($at);
        $this->entityManager->flush();

        $this->bus->dispatch(new SetDecisionExceptionMessage($at->getId()));
        $this->entityManager->flush();

        $this->assertTrue($at->getDecision());
        $this->entityManager->remove($at);
        $this->entityManager->flush();

    }
}