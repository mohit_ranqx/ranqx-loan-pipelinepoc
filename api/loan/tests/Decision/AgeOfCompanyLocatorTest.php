<?php declare(strict_types = 1);


namespace App\Tests\Decision;

use App\Decision\AgeOfCompany;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use PHPUnit\Framework\TestCase;
use App\Decision\AgeOfCompanyLocator;

class AgeOfCompanyLocatorTest extends TestCase
{
    public function testDataMissing(): void
    {
        $obj = new AgeOfCompanyLocator();
        $this->expectException(KiwiBankDataSrcNoExistException::class);
        $obj->findData([]);
    }

    public function testGoodData(): void
    {
        $obj = new AgeOfCompanyLocator();
        $dataSrc['equiFax']['decision']['companyDetails']['ageOfCompany'] = '2019-08-08';
        $r = $obj->findData($dataSrc);
        $this->assertInstanceOf(AgeOfCompany::class, $r);
    }


    public function testMissingExceptionData(): void
    {
        try {
            $obj = new AgeOfCompanyLocator();
            $obj->findData([]);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcNoExistException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new AgeOfCompany('');
            $actual = $obj->dataMissing();
            $this->assertEquals($expected, $actual);
        }
    }
}