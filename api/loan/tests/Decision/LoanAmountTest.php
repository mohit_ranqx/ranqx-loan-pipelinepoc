<?php declare(strict_types=1);


namespace App\Tests\Decision;

use App\Decision\LoanAmount;
use App\Decision\Policy;
use PHPUnit\Framework\TestCase;

class LoanAmountTest extends TestCase
{
    public function testPolicyRule(): void
    {
        $rule = new LoanAmount(50000);
        $expected = $rule->policyRule();
        $this->assertEquals(Policy::PASS, $expected);
    }

    public function testPolicyRule2(): void
    {
        $rule = new LoanAmount(50001);
        $expected = $rule->policyRule();
        $this->assertEquals(Policy::REFER, $expected);
    }
}