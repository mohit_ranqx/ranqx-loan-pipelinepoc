<?php declare(strict_types=1);


namespace App\Tests\Decision;

use App\Decision\DebtRatioCurrent;
use App\Decision\DebtRatioLocator;
use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use PHPUnit\Framework\TestCase;

class DebtRatioLocatorTest extends TestCase
{
    public function testBadData(): void
    {
        $dataSrc['calFinanceCurrentMonth'][0]['debt_ratio_current'] = 'test';
        $obj = new DebtRatioLocator();
        $this->expectException(KiwiBankDataSrcBadException::class);
        $obj->findData($dataSrc);
    }

    public function testMissingData(): void
    {
        $obj = new DebtRatioLocator();
        $this->expectException(KiwiBankDataSrcNoExistException::class);
        $obj->findData([]);
    }

    public function testGoodData(): void
    {
        $obj = new DebtRatioLocator();
        $dataSrc['calFinanceCurrentMonth'][0]['debt_ratio_current'] = '34';
        $r = $obj->findData($dataSrc);
        $this->assertInstanceOf(DebtRatioCurrent::class, $r);
    }

    public function testBadExceptionData(): void
    {
        try {
            $dataSrc['calFinanceCurrentMonth'][0]['debt_ratio_current'] = 'test';
            $obj = new DebtRatioLocator();
            $obj->findData($dataSrc);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcBadException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new DebtRatioCurrent('test');
            $actual = $obj->dataIsBad();
            $this->assertEquals($expected, $actual);
        }
    }

    public function testMissingExceptionData(): void
    {
        try {
            $obj = new DebtRatioLocator();
            $obj->findData([]);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcNoExistException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new DebtRatioCurrent(null);
            $actual = $obj->dataMissing();
            $this->assertEquals($expected, $actual);
        }
    }
}