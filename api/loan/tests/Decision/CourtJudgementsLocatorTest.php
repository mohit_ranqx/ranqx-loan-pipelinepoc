<?php declare(strict_types = 1);


namespace App\Tests\Decision;

use App\Decision\BadBureauScore;
use App\Decision\BadBureauScoreLocator;
use App\Decision\CourtJudgements;
use App\Decision\CourtJudgementsLocator;
use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use PHPUnit\Framework\TestCase;

class CourtJudgementsLocatorTest extends TestCase
{
    public function testBadValue(): void
    {
        $obj = new CourtJudgementsLocator();
        $dataSrc['equiFax']['decision']['summary']['judgementsCount'] = 'test';
        $this->expectException(KiwiBankDataSrcBadException::class);
        $obj->findData($dataSrc);
    }

    public function testMissingValue(): void
    {
        $obj = new CourtJudgementsLocator();
        $this->expectException(KiwiBankDataSrcNoExistException::class);
        $obj->findData([]);
    }

    public function testGoodValue(): void
    {
        $obj = new CourtJudgementsLocator();
        $dataSrc['equiFax']['decision']['summary']['judgementsCount'] = '8';
        $r = $obj->findData($dataSrc);
        $this->assertInstanceOf(CourtJudgements::class, $r);
    }

    public function testBadExceptionData(): void
    {
        try {
            $dataSrc['equiFax']['decision']['summary']['judgementsCount'] = 'test';
            $obj = new CourtJudgementsLocator();
            $obj->findData($dataSrc);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcBadException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new CourtJudgements('test');
            $actual = $obj->dataIsBad();
            $this->assertEquals($expected, $actual);
        }
    }

    public function testMissingExceptionData(): void
    {
        try {
            $obj = new CourtJudgementsLocator();
            $obj->findData([]);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcNoExistException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new CourtJudgements(null);
            $actual = $obj->dataMissing();
            $this->assertEquals($expected, $actual);
        }
    }
}