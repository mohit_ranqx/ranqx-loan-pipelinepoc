<?php


namespace App\Tests\Decision;

use App\Decision\BadBureauScore350;
use App\Decision\BadBureauScore350Locator;
use App\Decision\EnquiryLast5Years;
use App\Decision\EnquiryLast5YearsLocator;
use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use PHPUnit\Framework\TestCase;

class BadBureauScore350LocatorTest extends TestCase
{
    public function testBadData(): void
    {
        $dataSrc['equiFax']['decision']['score'][0]['score'] = 'test';
        $locator = new BadBureauScore350Locator();
        $this->expectException(KiwiBankDataSrcBadException::class);
        $locator->findData($dataSrc);
    }

    public function testMissingData(): void
    {
        $locator = new BadBureauScore350Locator();
        $this->expectException(KiwiBankDataSrcNoExistException::class);
        $locator->findData([]);
    }

    public function testGoodData(): void
    {
        $locator = new BadBureauScore350Locator();
        $dataSrc['equiFax']['decision']['score'][0]['score'] = '111';
        $r = $locator->findData($dataSrc);
        $this->assertInstanceOf(BadBureauScore350::class, $r);
    }

    public function testBadExceptionData(): void
    {
        try {
            $dataSrc['equiFax']['decision']['score'][0]['score'] = 'test';
            $obj = new BadBureauScore350Locator();
            $obj->findData($dataSrc);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcBadException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new BadBureauScore350('test');
            $actual = $obj->dataIsBad();
            $this->assertEquals($expected, $actual);
        }
    }

    public function testMissingExceptionData(): void
    {
        try {
            $obj = new BadBureauScore350Locator();
            $obj->findData([]);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcNoExistException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new BadBureauScore350(null);
            $actual = $obj->dataMissing();
            $this->assertEquals($expected, $actual);
        }
    }
}