<?php declare(strict_types=1);


namespace App\Tests\Decision;

use App\Decision\EnquiryLast5Years;
use App\Decision\EnquiryLast5YearsLocator;
use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use PHPUnit\Framework\TestCase;

class EnquiryLast5YearsLocatorTest extends TestCase
{
    public function testBadData(): void
    {
        $dataSrc['equiFax']['decision']['enquiryHistory']['enquiryTotalsLast5Years']['count'] = 'test';
        $obj = new EnquiryLast5YearsLocator();
        $this->expectException(KiwiBankDataSrcBadException::class);
        $obj->findData($dataSrc);
    }

    public function testMissingData(): void
    {
        $obj = new EnquiryLast5YearsLocator();
        $this->expectException(KiwiBankDataSrcNoExistException::class);
        $obj->findData([]);
    }

    public function testGoodData(): void
    {
        $dataSrc['equiFax']['decision']['enquiryHistory']['enquiryTotalsLast5Years']['count'] = '1';
        $obj = new EnquiryLast5YearsLocator();
        $result = $obj->findData($dataSrc);
        $this->assertInstanceOf(EnquiryLast5Years::class, $result);
    }

    public function testBadExceptionData(): void
    {
        try {
            $dataSrc['equiFax']['decision']['enquiryHistory']['enquiryTotalsLast5Years']['count'] = 'test';
            $obj = new EnquiryLast5YearsLocator();
            $obj->findData($dataSrc);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcBadException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new EnquiryLast5Years('test');
            $actual = $obj->dataIsBad();
            $this->assertEquals($expected, $actual);
        }
    }

    public function testMissingExceptionData(): void
    {
        try {
            $obj = new EnquiryLast5YearsLocator();
            $obj->findData([]);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcNoExistException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new EnquiryLast5Years(null);
            $actual = $obj->dataMissing();
            $this->assertEquals($expected, $actual);
        }
    }
}