<?php


namespace App\Tests\Decision;

use App\Decision\DebtorConcentration;
use App\Decision\DebtorConcentration025;
use App\Decision\Policy;
use PHPUnit\Framework\TestCase;

class DebtorConcentration025Test extends TestCase
{
    public function testPolicy(): void
    {
        $rule = new DebtorConcentration025([]);
        $actual = $rule->policyRule();
        $this->assertEquals(Policy::REFER, $actual);

        $id = $rule->toArray();
        $this->assertEquals('DC02', $id['RefId']);
        $this->assertEquals('KB_US_PR_007', $id['InternalId']);
        $this->assertEquals('', $id['RuleValue']);
        $this->assertEquals('Debtor Concentration has no data', $id['reason']);
    }

    public function testPolicy1(): void
    {
        $rule = new DebtorConcentration025(['data' => 'no']);
        $actual = $rule->policyRule();
        $this->assertEquals(Policy::REFER, $actual);

        $id = $rule->toArray();

        $this->assertEquals('DC02', $id['RefId']);
        $this->assertEquals('KB_US_PR_007', $id['InternalId']);
        $this->assertEquals('bad data', $id['RuleValue']);
        $this->assertEquals('Debtor Concentration has no valid data.', $id['reason']);
    }

    /**
     *
     */
    public function testPolicy2(): void
    {
        $rule = new DebtorConcentration025(['debtorConcentration' => '0.998877']);
        $actual = $rule->policyRule();
        $this->assertEquals(Policy::REFER, $actual);

        $id = $rule->toArray();

        $this->assertEquals('DC02', $id['RefId']);
        $this->assertEquals('KB_US_PR_007', $id['InternalId']);
        $this->assertEquals('bad data', $id['RuleValue']);
        $this->assertEquals('Debtor Concentration has no valid data.', $id['reason']);
    }

    /**
     *
     */
    public function testPolicy3(): void
    {
        $rule = new DebtorConcentration025(['debtorConcentration' => '0.998877', 'data' => 'no']);
        $actual = $rule->policyRule();
        $this->assertEquals(Policy::REFER, $actual);

        $id = $rule->toArray();
        $this->assertEquals('DC02', $id['RefId']);
        $this->assertEquals('KB_US_PR_007', $id['InternalId']);
        $this->assertEquals('no', $id['RuleValue']);
        $this->assertEquals('There are no debtor invoices recorded', $id['reason']);
    }

    /**
     *
     */
    public function testPolicy4(): void
    {
        $rule = new DebtorConcentration025(['debtorConcentration' => '0.0', 'data' => 'yes']);
        $actual = $rule->policyRule();
        $this->assertEquals(Policy::PASS, $actual);
        //The total value of all invoices is zero
        $id = $rule->toArray();
        $this->assertEquals('DC02', $id['RefId']);
        $this->assertEquals('KB_US_PR_007', $id['InternalId']);
        $this->assertEquals('0.0', $id['RuleValue']);
        $this->assertEquals('Invoice total < 0.25', $id['reason']);
    }

    /**
     *
     */
    public function testPolicy5(): void
    {
        $rule = new DebtorConcentration025(['debtorConcentration' => '0.323456', 'data' => 'yes']);
        $actual = $rule->policyRule();
        $this->assertEquals(Policy::REFER, $actual);

        $id = $rule->toArray();
        $this->assertEquals('DC02', $id['RefId']);
        $this->assertEquals('KB_US_PR_007', $id['InternalId']);
        $this->assertEquals('0.323456', $id['RuleValue']);
        $this->assertEquals('Company\'s Revenue by Debtor >=0.25', $id['reason']);
    }
}