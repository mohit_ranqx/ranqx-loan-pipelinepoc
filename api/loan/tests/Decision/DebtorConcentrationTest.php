<?php declare(strict_types = 1);


namespace App\Tests\Decision;

use App\Decision\DebtorConcentration;
use App\Decision\Policy;
use PHPUnit\Framework\TestCase;

/**
 * Class DebtorConcentrationTest
 * @package App\Tests\Decision
 */
class DebtorConcentrationTest extends TestCase
{
    /**
     *
     */
    public function testPolicy()
    {
        $rule = new DebtorConcentration([]);
        $actual = $rule->policyRule();
        $this->assertEquals(Policy::REFER, $actual);

        $id = $rule->toArray();
        $this->assertEquals('DC03', $id['RefId']);
        $this->assertEquals('KB_US_PR_018', $id['InternalId']);
        $this->assertEquals('', $id['RuleValue']);
    }

    /**
     *
     */
    public function testPolicy1()
    {
        $rule = new DebtorConcentration(['data' => 'no']);
        $actual = $rule->policyRule();
        $this->assertEquals(Policy::REFER, $actual);

        $id = $rule->toArray();

        $this->assertEquals('DC03', $id['RefId']);
        $this->assertEquals('KB_US_PR_018', $id['InternalId']);
        $this->assertEquals('', $id['RuleValue']);
        $this->assertEquals('Debtor Concentration has no valid data.', $id['reason']);
    }

    /**
     *
     */
    public function testPolicy2()
    {
        $rule = new DebtorConcentration(['debtorConcentration' => '0.998877']);
        $actual = $rule->policyRule();
        $this->assertEquals(Policy::REFER, $actual);

        $id = $rule->toArray();

        $this->assertEquals('DC03', $id['RefId']);
        $this->assertEquals('KB_US_PR_018', $id['InternalId']);
        $this->assertEquals('', $id['RuleValue']);
        $this->assertEquals('Debtor Concentration has no valid data.', $id['reason']);
    }

    /**
     *
     */
    public function testPolicy3()
    {
        $rule = new DebtorConcentration(['debtorConcentration' => '0.998877', 'data' => 'no']);
        $actual = $rule->policyRule();
        $this->assertEquals(Policy::REFER, $actual);

        $id = $rule->toArray();
        $this->assertEquals('DC03', $id['RefId']);
        $this->assertEquals('KB_US_PR_018', $id['InternalId']);
        $this->assertEquals('no', $id['RuleValue']);
        $this->assertEquals('There are no debtor invoices recorded', $id['reason']);
    }

    /**
     *
     */
    public function testPolicy4()
    {
        $rule = new DebtorConcentration(['debtorConcentration' => '0.0', 'data' => 'yes']);
        $actual = $rule->policyRule();
        $this->assertEquals(Policy::REFER, $actual);
        //The total value of all invoices is zero
        $id = $rule->toArray();
        $this->assertEquals('DC03', $id['RefId']);
        $this->assertEquals('KB_US_PR_018', $id['InternalId']);
        $this->assertEquals('0.0', $id['RuleValue']);
        $this->assertEquals('The total value of all invoices is zero', $id['reason']);
    }

    /**
     *
     */
    public function testPolicy5()
    {
        $rule = new DebtorConcentration(['debtorConcentration' => '0.123456', 'data' => 'yes']);
        $actual = $rule->policyRule();
        $this->assertEquals(Policy::PASS, $actual);

        $id = $rule->toArray();
        $this->assertEquals('DC03', $id['RefId']);
        $this->assertEquals('KB_US_PR_018', $id['InternalId']);
        $this->assertEquals('0.123456', $id['RuleValue']);
        $this->assertEquals('Invoice total > $0', $id['reason']);
    }
}