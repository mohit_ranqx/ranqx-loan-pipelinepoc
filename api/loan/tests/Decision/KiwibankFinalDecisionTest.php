<?php


namespace App\Tests\Decision;


use App\Decision\KiwibankFinalDecision;
use App\Decision\Policy;
use PHPUnit\Framework\TestCase;

class KiwibankFinalDecisionTest extends TestCase
{
    public function testDecsion1()
    {
        $rule = new KiwibankFinalDecision(Policy::DECLINE, Policy::DECLINE);
        $actual = $rule->make();
        $this->assertEquals(Policy::DECLINE, $actual);
    }

    public function testDecsion2()
    {
        $rule = new KiwibankFinalDecision(Policy::DECLINE, Policy::REFER);
        $actual = $rule->make();
        $this->assertEquals(Policy::DECLINE, $actual);
    }

    public function testDecsion3()
    {
        $rule = new KiwibankFinalDecision(Policy::DECLINE, Policy::APPROVE);
        $actual = $rule->make();
        $this->assertEquals(Policy::DECLINE, $actual);
    }

    public function testDecsion5()
    {
        $rule = new KiwibankFinalDecision(Policy::REFER, Policy::DECLINE);
        $actual = $rule->make();
        $this->assertEquals(Policy::DECLINE, $actual);
    }

    public function testDecsion6()
    {
        $rule = new KiwibankFinalDecision(Policy::REFER, Policy::REFER);
        $actual = $rule->make();
        $this->assertEquals(Policy::REFER, $actual);
    }

    public function testDecsion7()
    {
        $rule = new KiwibankFinalDecision(Policy::REFER, Policy::APPROVE);
        $actual = $rule->make();
        $this->assertEquals(Policy::REFER, $actual);
    }

    public function testDecsion8()
    {
        $rule = new KiwibankFinalDecision(Policy::PASS, Policy::DECLINE);
        $actual = $rule->make();
        $this->assertEquals(Policy::DECLINE, $actual);
    }

    public function testDecsion9()
    {
        $rule = new KiwibankFinalDecision(Policy::PASS, Policy::REFER);
        $actual = $rule->make();
        $this->assertEquals(Policy::REFER, $actual);
    }

    public function testDecsion10()
    {
        $rule = new KiwibankFinalDecision(Policy::PASS, Policy::APPROVE);
        $actual = $rule->make();
        $this->assertEquals(Policy::APPROVE, $actual);
    }
}