<?php declare(strict_types=1);


namespace App\Tests\Decision;

use App\Decision\DebtRatioCurrent;
use App\Decision\DebtRatioLocator;
use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use App\Decision\ExcessiveRecentEnquiries;
use App\Decision\ExcessiveRecentEnquiriesLocator;
use PHPUnit\Framework\TestCase;

class ExcessiveRecentEnquiriesLocatorTest extends TestCase
{
    public function testBadData(): void
    {
        $dataSrc['equiFax']['decision']['enquiryHistory']['totalEnquires'] = 'test';
        $obj = new ExcessiveRecentEnquiriesLocator();
        $this->expectException(KiwiBankDataSrcBadException::class);
        $obj->findData($dataSrc);
    }

    public function testMissingData(): void
    {
        $obj = new ExcessiveRecentEnquiriesLocator();
        $this->expectException(KiwiBankDataSrcNoExistException::class);
        $obj->findData([]);
    }

    public function testGoodData(): void
    {
        $dataSrc['equiFax']['decision']['enquiryHistory']['totalEnquires'] = '1';
        $obj = new ExcessiveRecentEnquiriesLocator();
        $result = $obj->findData($dataSrc);
        $this->assertInstanceOf(ExcessiveRecentEnquiries::class, $result);
    }

    public function testBadExceptionData(): void
    {
        try {
            $dataSrc['equiFax']['decision']['enquiryHistory']['totalEnquires'] = 'test';
            $obj = new ExcessiveRecentEnquiriesLocator();
            $obj->findData($dataSrc);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcBadException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new ExcessiveRecentEnquiries('test');
            $actual = $obj->dataIsBad();
            $this->assertEquals($expected, $actual);
        }
    }

    public function testMissingExceptionData(): void
    {
        try {
            $obj = new ExcessiveRecentEnquiriesLocator();
            $obj->findData([]);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcNoExistException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new ExcessiveRecentEnquiries(null);
            $actual = $obj->dataMissing();
            $this->assertEquals($expected, $actual);
        }
    }
}