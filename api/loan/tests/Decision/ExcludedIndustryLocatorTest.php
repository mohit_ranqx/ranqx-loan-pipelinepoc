<?php declare(strict_types=1);


namespace App\Tests\Decision;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use App\Decision\ExcessiveRecentEnquiries;
use App\Decision\ExcessiveRecentEnquiriesLocator;
use App\Decision\ExcludedIndustry;
use App\Decision\ExcludedIndustryLocator;
use PHPUnit\Framework\TestCase;

class ExcludedIndustryLocatorTest extends TestCase
{
    public function testMissingData(): void
    {
        $obj = new ExcludedIndustryLocator();
        $this->expectException(KiwiBankDataSrcNoExistException::class);
        $obj->findData([]);
    }

    public function testGoodData(): void
    {
        $obj = new ExcludedIndustryLocator();
        $dataSrc['equiFax']['decision']['companyDetails']['industryCode'] = 'as4567';
        $r = $obj->findData($dataSrc);
        $this->assertInstanceOf(ExcludedIndustry::class, $r);
    }

    public function testMissingExceptionData(): void
    {
        try {
            $obj = new ExcludedIndustryLocator();
            $obj->findData([]);
            $this->assertEquals(1, 0);
        } catch (KiwiBankDataSrcNoExistException $e) {
            $expected = $e->getPolicyInformation();
            $obj = new ExcludedIndustry('');
            $actual = $obj->dataMissing();
            $this->assertEquals($expected, $actual);
        }
    }
}