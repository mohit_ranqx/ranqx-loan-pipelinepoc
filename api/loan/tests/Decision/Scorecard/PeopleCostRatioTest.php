<?php


namespace App\Tests\Decision\Scorecard;


use App\Decision\Scorecard\PeopleCostRatio;
use PHPUnit\Framework\TestCase;

class PeopleCostRatioTest extends TestCase
{
    public function testValueIsMissing()
    {
        $rule = new PeopleCostRatio(null);
        $actual = $rule->evaluate();
        $this->assertEquals(17, $actual);
    }

    public function testSmaller()
    {
        $rule = new PeopleCostRatio(0.0000001);
        $actual = $rule->evaluate();
        $this->assertEquals(23, $actual);
    }

    public function testInBetween()
    {
        $rule = new PeopleCostRatio(0.000001);
        $actual = $rule->evaluate();
        $this->assertEquals(0, $actual);

        $rule = new PeopleCostRatio(0.0000011);
        $actual = $rule->evaluate();
        $this->assertEquals(0, $actual);

        $rule = new PeopleCostRatio(0.12);
        $actual = $rule->evaluate();
        $this->assertEquals(0, $actual);
    }

    public function testInBetween1()
    {
        $rule = new PeopleCostRatio(0.125);
        $actual = $rule->evaluate();
        $this->assertEquals(30, $actual);

        $rule = new PeopleCostRatio(0.20);
        $actual = $rule->evaluate();
        $this->assertEquals(30, $actual);
    }

    public function testGreate()
    {
        $rule = new PeopleCostRatio(0.25);
        $actual = $rule->evaluate();
        $this->assertEquals(46, $actual);

        $rule = new PeopleCostRatio(0.255);
        $actual = $rule->evaluate();
        $this->assertEquals(46, $actual);
    }
}