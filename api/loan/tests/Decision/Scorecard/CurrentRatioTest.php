<?php


namespace App\Tests\Decision\Scorecard;


use App\Decision\Scorecard\CurrentRatio;
use PHPUnit\Framework\TestCase;

class CurrentRatioTest extends TestCase
{
    public function testValueMissing()
    {
        $rule = new CurrentRatio(null);
        $actual = $rule->evaluate();
        $this->assertEquals(11, $actual);
    }

    public function testSmallerThan1()
    {
        $rule = new CurrentRatio(0.99);
        $actual = $rule->evaluate();
        $this->assertEquals(18, $actual);

        $rule = new CurrentRatio(0.9);
        $actual = $rule->evaluate();
        $this->assertEquals(18, $actual);

        $rule = new CurrentRatio(0.5);
        $actual = $rule->evaluate();
        $this->assertEquals(18, $actual);

        $rule = new CurrentRatio(-8);
        $actual = $rule->evaluate();
        $this->assertEquals(18, $actual);
    }

    public function testInBetween()
    {
        //1 <= current_ratio < 4.5
        $rule = new CurrentRatio(1.0);
        $actual = $rule->evaluate();
        $this->assertEquals(26, $actual);

        $rule = new CurrentRatio(1.9);
        $actual = $rule->evaluate();
        $this->assertEquals(26, $actual);

        $rule = new CurrentRatio(3.5);
        $actual = $rule->evaluate();
        $this->assertEquals(26, $actual);
    }

    public function testGreaterThan()
    {
        $rule = new CurrentRatio(4.5);
        $actual = $rule->evaluate();
        $this->assertEquals(74, $actual);

        $rule = new CurrentRatio(4.6);
        $actual = $rule->evaluate();
        $this->assertEquals(74, $actual);

        $rule = new CurrentRatio(5);
        $actual = $rule->evaluate();
        $this->assertEquals(74, $actual);

        $rule = new CurrentRatio(4.50009);
        $actual = $rule->evaluate();
        $this->assertEquals(74, $actual);
    }
}