<?php


namespace App\Tests\Decision\Scorecard;


use App\Decision\Scorecard\RevenueGrowth;
use PHPUnit\Framework\TestCase;

class RevenueGrowthTest extends TestCase
{
    public function testValueIsMissing()
    {
        $rule = new RevenueGrowth(null);
        $actual = $rule->evaluate();
        $this->assertEquals(12, $actual);
    }

    public function testLessThanZero()
    {
        $rule = new RevenueGrowth(-0.76);
        $actual = $rule->evaluate();
        $this->assertEquals(21, $actual);
    }

    public function testInBetween()
    {
        $rule = new RevenueGrowth(0);
        $actual = $rule->evaluate();
        $this->assertEquals(32, $actual);

        $rule = new RevenueGrowth(0.15);
        $actual = $rule->evaluate();
        $this->assertEquals(32, $actual);
    }

    public function testInBetween2()
    {
        $rule = new RevenueGrowth(0.25);
        $actual = $rule->evaluate();
        $this->assertEquals(51, $actual);

        $rule = new RevenueGrowth(0.34);
        $actual = $rule->evaluate();
        $this->assertEquals(51, $actual);
    }

    public function testGreate()
    {
        $rule = new RevenueGrowth(0.55);
        $actual = $rule->evaluate();
        $this->assertEquals(31, $actual);

        $rule = new RevenueGrowth(0.56);
        $actual = $rule->evaluate();
        $this->assertEquals(31, $actual);
    }
}