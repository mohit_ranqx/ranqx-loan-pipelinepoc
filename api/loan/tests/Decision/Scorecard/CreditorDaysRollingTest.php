<?php


namespace App\Tests\Decision\Scorecard;


use App\Decision\Scorecard\CreditorDaysRolling;
use PHPUnit\Framework\TestCase;

class CreditorDaysRollingTest extends TestCase
{
    public function testValueIsMissing()
    {
        $rule = new CreditorDaysRolling(null);
        $actual = $rule->evaluate();
        $this->assertEquals(27, $actual);
    }

    public function testSmallerThan()
    {
        $rule = new CreditorDaysRolling(20);
        $actual = $rule->evaluate();
        $this->assertEquals(35, $actual);

        $rule = new CreditorDaysRolling(0);
        $actual = $rule->evaluate();
        $this->assertEquals(35, $actual);

        $rule = new CreditorDaysRolling(-98);
        $actual = $rule->evaluate();
        $this->assertEquals(35, $actual);
    }

    public function testInBetween()
    {
        $rule = new CreditorDaysRolling(30);
        $actual = $rule->evaluate();
        $this->assertEquals(23, $actual);

        $rule = new CreditorDaysRolling(31);
        $actual = $rule->evaluate();
        $this->assertEquals(23, $actual);

        $rule = new CreditorDaysRolling(240);
        $actual = $rule->evaluate();
        $this->assertEquals(23, $actual);
    }

    public function testGreaterThan()
    {
        $rule = new CreditorDaysRolling(300);
        $actual = $rule->evaluate();
        $this->assertEquals(-44, $actual);

        $rule = new CreditorDaysRolling(301);
        $actual = $rule->evaluate();
        $this->assertEquals(-44, $actual);
    }
}