<?php


namespace App\Tests\Decision\Scorecard;


use App\Decision\Scorecard\ExpenseCoverRatioRolling;
use PHPUnit\Framework\TestCase;

class ExpenseCoverRatioRollingTest extends TestCase
{
    public function testValusIsMissing()
    {
        $rule = new ExpenseCoverRatioRolling(null);
        $actual = $rule->evaluate();
        $this->assertEquals(13, $actual);
    }

    public function testSmaller()
    {
        $rule = new ExpenseCoverRatioRolling(0.1);
        $actual = $rule->evaluate();
        $this->assertEquals(35, $actual);
    }

    public function testGreate()
    {
        $rule = new ExpenseCoverRatioRolling(0.2);
        $actual = $rule->evaluate();
        $this->assertEquals(4, $actual);

        $rule = new ExpenseCoverRatioRolling(0.3);
        $actual = $rule->evaluate();
        $this->assertEquals(4, $actual);
    }
}