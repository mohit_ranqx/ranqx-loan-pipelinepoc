<?php


namespace App\Tests\Decision\Scorecard;


use App\Decision\Scorecard\BureauFileAge;
use PHPUnit\Framework\TestCase;

class BureauFileAgeTest extends TestCase
{
    public function testValueIsMissing()
    {
        $rule = new BureauFileAge(null);
        $actual = $rule->evaluate();
        $this->assertEquals(-39, $actual);
    }

    public function testSmaller()
    {
        $rule = new BureauFileAge('6 years 8 months');
        $actual = $rule->evaluate();
        $this->assertEquals(12, $actual);
    }

    public function testGreate()
    {
        $rule = new BureauFileAge('12 years 8 months');
        $actual = $rule->evaluate();
        $this->assertEquals(59, $actual);
    }
}