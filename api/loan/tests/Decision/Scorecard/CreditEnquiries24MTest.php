<?php


namespace App\Tests\Decision\Scorecard;


use App\Decision\Scorecard\CreditEnquiries24M;
use PHPUnit\Framework\TestCase;

class CreditEnquiries24MTest extends TestCase
{
    public function testValueIsMissing()
    {
        $rule = new CreditEnquiries24M(null);
        $actual = $rule->evaluate();
        $this->assertEquals(-38, $actual);
    }

    public function testSmaller()
    {
        $rule = new CreditEnquiries24M(1.0000);
        $actual = $rule->evaluate();
        $this->assertEquals(42, $actual);

        $rule = new CreditEnquiries24M(0);
        $actual = $rule->evaluate();
        $this->assertEquals(42, $actual);

        $rule = new CreditEnquiries24M(-9);
        $actual = $rule->evaluate();
        $this->assertEquals(42, $actual);
    }

    public function testInBetween()
    {
        $rule = new CreditEnquiries24M(1.00001);
        $actual = $rule->evaluate();
        $this->assertEquals(29, $actual);

        $rule = new CreditEnquiries24M(3.00001);
        $actual = $rule->evaluate();
        $this->assertEquals(29, $actual);

        $rule = new CreditEnquiries24M(4.000);
        $actual = $rule->evaluate();
        $this->assertEquals(29, $actual);
    }

    public function testGreate()
    {
        $rule = new CreditEnquiries24M(4.00001);
        $actual = $rule->evaluate();
        $this->assertEquals(0, $actual);

        $rule = new CreditEnquiries24M(4.000011);
        $actual = $rule->evaluate();
        $this->assertEquals(0, $actual);

        $rule = new CreditEnquiries24M(5.000011);
        $actual = $rule->evaluate();
        $this->assertEquals(0, $actual);
    }
}