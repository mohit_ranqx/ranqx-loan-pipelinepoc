<?php


namespace App\Tests\Decision\Scorecard;


use App\Decision\Scorecard\KBCustomDefensiveIntervalRatio;
use App\Decision\Scorecard\KBCustomExpenseCoverRatio;
use PHPUnit\Framework\TestCase;

class KBCustomDefensiveIntervalRatioTest extends TestCase
{
    public function testValueIsMissing()
    {
        $rule = new KBCustomDefensiveIntervalRatio(null);
        $actual = $rule->evaluate();
        $this->assertEquals(14, $actual);
    }

    public function testSmallValue()
    {
        $rule = new KBCustomDefensiveIntervalRatio(20);
        $actual = $rule->evaluate();
        $this->assertEquals(9, $actual);
    }

    public function testBetweenValue()
    {
        $rule = new KBCustomDefensiveIntervalRatio(21);
        $actual = $rule->evaluate();
        $this->assertEquals(55, $actual);
    }

    public function testBetween1Value()
    {
        $rule = new KBCustomDefensiveIntervalRatio(89);
        $actual = $rule->evaluate();
        $this->assertEquals(55, $actual);
    }

    public function testBigValue()
    {
        $rule = new KBCustomDefensiveIntervalRatio(90);
        $actual = $rule->evaluate();
        $this->assertEquals(18, $actual);
    }

    public function testBigValue2()
    {
        $rule = new KBCustomDefensiveIntervalRatio(99);
        $actual = $rule->evaluate();
        $this->assertEquals(18, $actual);
    }
}