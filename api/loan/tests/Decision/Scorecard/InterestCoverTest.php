<?php


namespace App\Tests\Decision\Scorecard;


use App\Decision\Scorecard\InterestCover;
use PHPUnit\Framework\TestCase;

class InterestCoverTest extends TestCase
{
    public function testValueIsMissing()
    {
        $rule = new InterestCover(null);
        $actual = $rule->evaluate();
        $this->assertEquals(27, $actual);
    }

    public function testSmallValue()
    {
        $rule = new InterestCover(0.0000001);
        $actual = $rule->evaluate();
        $this->assertEquals(8, $actual);
    }

    public function testSmallValue2()
    {
        //0.000001 <= interest_cover < 50
        $rule = new InterestCover(0.0000011);
        $actual = $rule->evaluate();
        $this->assertEquals(20, $actual);

        $rule = new InterestCover(0.000001);
        $actual = $rule->evaluate();
        $this->assertEquals(20, $actual);

        $rule = new InterestCover(40);
        $actual = $rule->evaluate();
        $this->assertEquals(20, $actual);
    }

    public function testGreate()
    {
        $rule = new InterestCover(50);
        $actual = $rule->evaluate();
        $this->assertEquals(57, $actual);

        $rule = new InterestCover(50.09);
        $actual = $rule->evaluate();
        $this->assertEquals(57, $actual);

        $rule = new InterestCover(60);
        $actual = $rule->evaluate();
        $this->assertEquals(57, $actual);
    }
}