<?php declare(strict_types=1);


namespace App\Tests\Decision\Scorecard;

use App\Decision\Scorecard\InterestCover;
use App\Decision\Scorecard\KBCustomExpenseCoverRatio;
use PHPUnit\Framework\TestCase;

class KBCustomExpenseCoverRatioTest extends TestCase
{
    public function testValueIsMissing()
    {
        $rule = new KBCustomExpenseCoverRatio(null);
        $actual = $rule->evaluate();
        $this->assertEquals(13, $actual);
    }

    public function testSmallValue()
    {
        $rule = new KBCustomExpenseCoverRatio(0.0000001);
        $actual = $rule->evaluate();
        $this->assertEquals(35, $actual);
    }

    public function testBigValue()
    {
        $rule = new KBCustomExpenseCoverRatio(0.2);
        $actual = $rule->evaluate();
        $this->assertEquals(4, $actual);
    }

    public function testBig2Value()
    {
        $rule = new KBCustomExpenseCoverRatio(0.21);
        $actual = $rule->evaluate();
        $this->assertEquals(4, $actual);
    }
}