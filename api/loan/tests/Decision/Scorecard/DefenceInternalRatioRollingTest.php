<?php


namespace App\Tests\Decision\Scorecard;


use App\Decision\Scorecard\DefenceInternalRatioRolling;
use PHPUnit\Framework\TestCase;

class DefenceInternalRatioRollingTest extends TestCase
{
    public function testValueIsMissing()
    {
        $rule = new DefenceInternalRatioRolling(null);
        $actual = $rule->evaluate();
        $this->assertEquals(14, $actual);
    }

    public function testSmaller()
    {
        $rule = new DefenceInternalRatioRolling(20);
        $actual = $rule->evaluate();
        $this->assertEquals(9, $actual);

        $rule = new DefenceInternalRatioRolling(0);
        $actual = $rule->evaluate();
        $this->assertEquals(9, $actual);

        $rule = new DefenceInternalRatioRolling(-5);
        $actual = $rule->evaluate();
        $this->assertEquals(9, $actual);

        $rule = new DefenceInternalRatioRolling(10);
        $actual = $rule->evaluate();
        $this->assertEquals(9, $actual);
    }

    public function testInBetween()
    {
        $rule = new DefenceInternalRatioRolling(21);
        $actual = $rule->evaluate();
        $this->assertEquals(55, $actual);

        $rule = new DefenceInternalRatioRolling(45);
        $actual = $rule->evaluate();
        $this->assertEquals(55, $actual);

        $rule = new DefenceInternalRatioRolling(89);
        $actual = $rule->evaluate();
        $this->assertEquals(55, $actual);
    }

    public function testGreater()
    {
        $rule = new DefenceInternalRatioRolling(90);
        $actual = $rule->evaluate();
        $this->assertEquals(18, $actual);

        $rule = new DefenceInternalRatioRolling(98);
        $actual = $rule->evaluate();
        $this->assertEquals(18, $actual);
    }
}