<?php declare(strict_types = 1);

namespace App\Tests\Service;

use App\Service\PrivateKeyCrypt;
use PHPUnit\Framework\TestCase;
use InvalidArgumentException;
use App\TypeTrait\XeroKey;
use Symfony\Component\Dotenv\Dotenv;

class PrivateKeyCryptTest extends TestCase
{
    use XeroKey;

    public function test__construct()
    {
        $this->expectException(InvalidArgumentException::class);
        new PrivateKeyCrypt('123', '123');
    }

    public function testDecode()
    {
        //pri_for_test.pem
        $pri = new PrivateKeyCrypt($this->privateKey(), $this->publicKey());
        $base64En = $pri->encode('12');

        $value = $pri->decode($base64En);
        $this->assertEquals('12', $value);
    }
}
