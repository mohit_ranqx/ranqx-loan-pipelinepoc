<?php declare(strict_types=1);

namespace App\Tests\Service;

use PHPUnit\Framework\TestCase;
use App\Service\EquiFaxDataQuery;

/**
 * Class EquiFaxDataQueryTest
 * @package App\Tests\Service
 */
class EquiFaxDataQueryTest extends TestCase
{
    /**
     * @var mixed
     */
    private $json;
    /**
     * @var mixed
     */
    private $bad;

    /**
     * @var mixed
     */
    private $alt;

    /**
     *
     */
    public function setUp()
    {
        $json = file_get_contents(__DIR__ . '/equiFax.json');
        $this->json = \json_decode($json, true);

        $json = file_get_contents(__DIR__ . '/equiFaxBad.json');
        $this->bad = \json_decode($json, true);

        $json = file_get_contents(__DIR__ . '/equiFaxAlt.json');
        $this->alt = \json_decode($json, true);
    }

    /**
     * json file contains company name.
     */
    public function testCompanyName(): void
    {
        $obj = new EquiFaxDataQuery($this->json);
        $actual = $obj->companyName();
        $expected = 'SOFTWARE 400 LIMITED';
        $this->assertEquals($expected, $actual);
    }

    /**
     * json file has no company name field.
     */
    public function testCompanyNameBad(): void
    {
        $obj = new EquiFaxDataQuery($this->bad);
        $actual = $obj->companyName();
        $expected = '';
        $this->assertEquals($expected, $actual);
    }
    /**
     * organisationNumber
     * ***/
    public function testNZCN(): void
    {
        $obj = new EquiFaxDataQuery($this->json);
        $actual = $obj->NZCN();
        $expected = '487967';
        $this->assertEquals($expected, $actual);
    }

    /**
     * contains no organisationNumber
     * ***/
    public function testNZCNBad(): void
    {
        $obj = new EquiFaxDataQuery($this->bad);
        $actual = $obj->NZCN();
        $expected = '';
        $this->assertEquals($expected, $actual);
    }

    //ANZSIC
    /**
     * industry code
     * ****/
    public function testANZSIC(): void
    {
        $obj = new EquiFaxDataQuery($this->json);
        $actual = $obj->ANZSIC();
        $expected = '111';
        $this->assertEquals($expected, $actual);
    }
    /**
     * contains no industry code
     * ****/
    public function testANZSICBad(): void
    {
        $obj = new EquiFaxDataQuery($this->bad);
        $actual = $obj->ANZSIC();
        $expected = '';
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testRegistedAddress(): void
    {
        $obj = new EquiFaxDataQuery($this->json);
        $actual = $obj->registedAddress();
        $expected = 'Level 8, 120 Albert Street Auckland Central Auckland 1010';
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testBusinessAddress(): void
    {
        $obj = new EquiFaxDataQuery($this->json);
        $actual = $obj->businessAddress();
        $expected = '10 Hawke Crescent Beachlands Auckland 2018';
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testPostalAddressIsRegistedAddress(): void
    {
        $obj = new EquiFaxDataQuery($this->json);
        $actual = $obj->postalAddress();
        $expected = 'Level 8, 120 Albert Street Auckland Central Auckland 1010';
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testPostalAddress(): void
    {
        $obj = new EquiFaxDataQuery($this->alt);
        $actual = $obj->postalAddress();
        $expected = '10 Victoria street, auckland';
        $this->assertEquals($expected, $actual);
    }

    /**
     * @throws \Exception
     */
    public function testAgeOfBusiness(): void
    {
        $obj = new EquiFaxDataQuery($this->json);
        $actual = $obj->ageOfBusiness();
        $expected = '18-10-1990';
        $this->assertEquals($expected, $actual);
    }

    /**
     * @throws \Exception
     */
    public function testAgeOfBusinessAlt(): void
    {
        $obj = new EquiFaxDataQuery($this->alt);
        $actual = $obj->ageOfBusiness();
        $expected = '18-10-2001';
        $this->assertEquals($expected, $actual);
    }

    /**
     * @throws \Exception
     */
    public function testAgeOfBusinessBad(): void
    {
        $this->expectException(\Exception::class);
        $obj = new EquiFaxDataQuery($this->bad);
        $obj->ageOfBusiness();
    }


    public function testAgeOfBureauFile(): void
    {
        $obj = new EquiFaxDataQuery($this->json);
        $actual = $obj->ageOfBureauFile();
        $expected = '17 years 11 months';
        $this->assertEquals($expected, $actual);
    }

    public function testAgeOfBureauFileBad(): void
    {
        $obj = new EquiFaxDataQuery($this->bad);
        $actual = $obj->ageOfBureauFile();
        $expected = '';
        $this->assertEquals($expected, $actual);
    }
}