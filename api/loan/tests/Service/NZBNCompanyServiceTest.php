<?php declare(strict_types=1);


namespace App\Tests\Service;

use App\DTO\CreateNZBNCompanyFromJson;
use App\Service\NZBNCompanyService;
use PHPUnit\Framework\TestCase;

class NZBNCompanyServiceTest extends TestCase
{
    public function testStatus(): void
    {
        $json = file_get_contents(__DIR__ . '/test.json');
        $obj = new CreateNZBNCompanyFromJson($json);

        $service = new NZBNCompanyService($obj->toNZBNCompany());
        $this->assertEquals(true, $service->isActive());
        $this->assertEquals(false, $service->excludedIndustry());
        $this->assertEquals(false, $service->excluded());
    }

    public function testExcludedIndustry(): void
    {
        $json = file_get_contents(__DIR__ . '/test.json');
        $obj = new CreateNZBNCompanyFromJson($json);

        $service = new NZBNCompanyService($obj->toNZBNCompany());
        $this->assertEquals(false, $service->excludedIndustry());
        $this->assertEquals(true, $service->isActive());
        $this->assertEquals(false, $service->excluded());
    }

    public function testStatus1(): void
    {
        $json = file_get_contents(__DIR__ . '/test1.json');
        $obj = new CreateNZBNCompanyFromJson($json);

        $service = new NZBNCompanyService($obj->toNZBNCompany());
        $this->assertEquals(false, $service->isActive());
        $this->assertEquals(true, $service->excluded());
    }

    public function testNzbnExclude():void
    {
        $json = file_get_contents(__DIR__ . '/test2.json');
        $obj = new CreateNZBNCompanyFromJson($json);

        $service = new NZBNCompanyService($obj->toNZBNCompany());
        $this->assertEquals(true, $service->excludedIndustry());
        $this->assertEquals(true, $service->excluded());
    }

    public function testNzbnExclude1():void
    {
        $json = file_get_contents(__DIR__ . '/test3.json');
        $obj = new CreateNZBNCompanyFromJson($json);

        $service = new NZBNCompanyService($obj->toNZBNCompany());
        $this->assertEquals(true, $service->excludedIndustry());
        $this->assertEquals(true, $service->excluded());
    }

    public function testNzbnExclude2():void
    {
        $json = file_get_contents(__DIR__ . '/test4.json');
        $obj = new CreateNZBNCompanyFromJson($json);

        $service = new NZBNCompanyService($obj->toNZBNCompany());
        $this->assertEquals(true, $service->excludedIndustry());

        $this->assertEquals(true, $service->excluded());
    }

    public function testNzbnExclude3(): void
    {
        $json = file_get_contents(__DIR__ . '/test5.json');
        $obj = new CreateNZBNCompanyFromJson($json);

        $service = new NZBNCompanyService($obj->toNZBNCompany());

        $this->assertEquals(true, $service->excludeForeignCompanyAndNonLtd());
        $this->assertEquals(true, $service->excluded());

    }
}