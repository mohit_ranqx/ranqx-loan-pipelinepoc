<?php


namespace App\Tests\Xero;
use App\OAuth\XeroAccessToken;
use App\Tests\OAuth\XeroParameterDummy;
use App\Xero\XeroAPIEncode;
use PHPUnit\Framework\TestCase;

class XeroAPIAccountEncodeTest extends TestCase
{
    private $tokenStr;
    public function setUp()
    {
        $this->tokenStr = 'oauth_token=hdk48Djdsa&oauth_token_secret=xyz4992k83j47x0b&oauth_callback_confirmed=true';
    }

    public function testFirst()
    {
        $config = new XeroParameterDummy();
        $payload = new AccountPayLoadDummy($config, new XeroAccessToken($this->tokenStr));

        $acc = new XeroAPIEncode($config, $payload, 'Accounts');
        $value = $acc->encodeFirst();
        $expected = \rawurlencode('GET') . '&';
        $this->assertEquals($expected, $value);
    }

    public function testSecond()
    {
        $config = new XeroParameterDummy();
        $payload = new AccountPayLoadDummy($config, new XeroAccessToken($this->tokenStr));

        $acc = new XeroAPIEncode($config, $payload, 'Accounts');
        $value = $acc->encodeSecond();

        $expected = \rawurlencode($config->apiUri().'Accounts') . '&';
        $this->assertEquals($expected, $value);
    }

    public function testBase()
    {
        $config = new XeroParameterDummy();
        $token = new XeroAccessToken($this->tokenStr);
        $payload = new AccountPayLoadDummy($config, $token);

        $acc = new XeroAPIEncode($config, $payload, 'Accounts');
        $value = $acc->encodeBase();

        //this string as base string;
        //oauth_consumer_key=123456&oauth_nonce=abcde&oauth_signature_method=RSA-SHA1&oauth_timestamp=123456&oauth_token=hdk48Djdsa&oauth_version=1.0

        $expected = \rawurlencode('oauth_consumer_key').'='.\rawurlencode($config->consumerKey());
        $expected .= '&'.\rawurlencode('oauth_nonce').'='.\rawurlencode($config->getNonce());
        $expected .= '&'.\rawurlencode('oauth_signature_method').'='.\rawurlencode($config->algo());
        $expected .= '&'.\rawurlencode('oauth_timestamp').'='.\rawurlencode((string)$config->getTime());
        $expected .= '&'.\rawurlencode('oauth_token').'='.\rawurlencode($token->token());
        $expected .= '&'.\rawurlencode('oauth_version').'='.\rawurlencode($config->oauthVersion());
        $this->assertEquals($expected, $value);

    }
}