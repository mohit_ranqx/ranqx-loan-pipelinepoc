<?php


namespace App\Tests\Xero;

use App\OAuth\OAuth1ConfigInterface;
use App\Xero\SignaturePayloadInterface;
use App\OAuth\XeroAccessToken;

class AccountPayLoadDummy implements SignaturePayloadInterface
{
    private $config;
    private $extra;
    private $token;
    public function __construct(OAuth1ConfigInterface $config, XeroAccessToken $token)
    {
        $this->config = $config;
        $this->token = $token;
        $this->extra = [];
    }

    public function payload(): array
    {
        $temp['oauth_consumer_key'] = $this->config->consumerKey();
        $temp['oauth_nonce'] = $this->config->getNonce();
        $temp['oauth_signature_method'] = $this->config->algo();
        $temp['oauth_timestamp'] = $this->config->getTime();
        $temp['oauth_version'] = $this->config->oauthVersion();
        $temp['oauth_token'] = $this->token->token();
        $temp = array_merge($temp, $this->extra);
        ksort($temp);
        return $temp;
    }

    public function addExtra($key, $value): void
    {
        $this->extra[$key] = $value;
    }
}
