<?php


namespace App\Tests\Xero;

use App\Xero\SignaturePayloadInterface;
use App\OAuth\OAuth1ConfigInterface;

class RequestTokenPayLoadDummy implements SignaturePayloadInterface
{
    private $config;
    private $extra;
    public function __construct(OAuth1ConfigInterface $config)
    {
        $this->config = $config;
    }

    public function payload(): array
    {
        $temp['oauth_consumer_key'] = $this->config->consumerKey();
        $temp['oauth_nonce'] = $this->config->getNonce();
        $temp['oauth_signature_method'] = $this->config->algo();
        $temp['oauth_timestamp'] = $this->config->getTime();
        $temp['oauth_version'] = $this->config->oauthVersion();
        $temp = array_merge($temp, $this->extra);
        ksort($temp);
        return $temp;
    }

    public function addExtra($key, $value)
    {
        $this->extra[$key] = $value;
    }
}
