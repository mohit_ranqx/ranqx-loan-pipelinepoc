<?php declare(strict_types = 1);

namespace App\Tests\SQS;

use App\Message\XeroAccessTokenMessage;
use App\SQS\XeroSenderFIFOParameter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Envelope;
use App\OAuth\XeroAccessToken;

class XeroSenderFIFOParameterTest extends TestCase
{
    /**
     * @dataProvider provider
     */
    public function testParametersUrl($data)
    {
        $param = $this->createMock(ParameterBagInterface::class);
        $param->expects($this->once())->method('get')
            ->with('ranqx')->willReturn($data);

        $mess = new XeroAccessTokenMessage(new XeroAccessToken('test'), 13);
        $xeroPara = new XeroSenderFIFOParameter($param, new Envelope($mess));
        $result = $xeroPara->parameters();
        $this->assertEquals($result['QueueUrl'], 'test');
    }

    /**
     * @dataProvider provider
     */
    public function testParametersMessageBody($data)
    {
        $param = $this->createMock(ParameterBagInterface::class);
        $param->expects($this->once())->method('get')
            ->with('ranqx')->willReturn($data);

        $mess = new XeroAccessTokenMessage(new XeroAccessToken('test'), 13);
        $xeroPara = new XeroSenderFIFOParameter($param, new Envelope($mess));
        $result = $xeroPara->parameters();
        $this->assertEquals($result['MessageBody'], \json_encode(['token'=>'test', 'appId' => 13]));
    }

    /**
     * @dataProvider provider
     */
    public function testParametersMessageGroupId($data): void
    {
        $param = $this->createMock(ParameterBagInterface::class);
        $param->expects($this->once())->method('get')
            ->with('ranqx')->willReturn($data);

        $mess = new XeroAccessTokenMessage(new XeroAccessToken('test'), 13);
        $xeroPara = new XeroSenderFIFOParameter($param, new Envelope($mess));
        $result = $xeroPara->parameters();
        $this->assertEquals($result['MessageGroupId'], 'xero');
    }

    public function provider(): array
    {
        $data =[[
            array( 'aws' =>
                array( 'queue' =>
                    array( 'xero' =>
                        array( 'dsn' => 'test', 'groupId' => 'xero' ),
                        'calculus' =>
                            array( 'dsn' => 'test', 'groupId' => 'calculus' )
                    )
                )
        )]];
        return $data;
    }
}
