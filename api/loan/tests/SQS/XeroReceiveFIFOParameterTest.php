<?php declare(strict_types = 1);

namespace App\Tests\SQS;

use App\SQS\XeroReceiveFIFOParameter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class XeroReceiveFIFOParameterTest extends TestCase
{

    /**
     * @dataProvider provider
     */
    public function testParametersURL($data)
    {
        $param = $this->createMock(ParameterBagInterface::class);
        $param->expects($this->once())->method('get')
            ->with('ranqx')->willReturn($data);

        $xeroPara = new XeroReceiveFIFOParameter($param);
        $result = $xeroPara->parameters();
        $this->assertEquals($result['QueueUrl'], 'test');
    }

    public function provider(): array
    {
        $data =[[
            array( 'aws' =>
                array( 'queue' =>
                    array( 'xero' =>
                        array( 'dsn' => 'test', 'groupId' => 'xero' ),
                        'calculus' =>
                            array( 'dsn' => 'test', 'groupId' => 'calculus' )
                    )
                )
            )]];
        return $data;
    }
}
