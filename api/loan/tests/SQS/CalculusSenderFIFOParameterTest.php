<?php declare(strict_types = 1);

namespace App\Tests\SQS;

use App\Message\XeroAccessTokenMessage;
use App\OAuth\XeroAccessToken;
use App\SQS\CalculusSenderFIFOParameter;
use App\SQS\XeroSenderFIFOParameter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Envelope;

class CalculusSenderFIFOParameterTest extends TestCase
{
    /**
     * @dataProvider provider
     */
    public function testParametersURL($data)
    {
        $param = $this->createMock(ParameterBagInterface::class);
        $param->expects($this->once())->method('get')
            ->with('ranqx')->willReturn($data);

        $mess = new XeroAccessTokenMessage(new XeroAccessToken('test'), 13);
        $xeroPara = new CalculusSenderFIFOParameter($param, new Envelope($mess));
        $result = $xeroPara->parameters();
        $this->assertEquals($result['QueueUrl'], 'test');
    }

    /**
     * @dataProvider provider
     */
    public function testParametersMessageBody($data)
    {
        $param = $this->createMock(ParameterBagInterface::class);
        $param->expects($this->once())->method('get')
            ->with('ranqx')->willReturn($data);

        $mess = new XeroAccessTokenMessage(new XeroAccessToken('test'), 13);
        $xeroPara = new CalculusSenderFIFOParameter($param, new Envelope($mess));
        $result = $xeroPara->parameters();
        $this->assertEquals($result['MessageBody'], \json_encode(['token' => 'test', 'appId' => 13]));
    }

    /**
     * @dataProvider provider
     */
    public function testParametersMessageGroupId($data)
    {
        $param = $this->createMock(ParameterBagInterface::class);
        $param->expects($this->once())->method('get')
            ->with('ranqx')->willReturn($data);

        $mess = new XeroAccessTokenMessage(new XeroAccessToken('test'), 13);
        $xeroPara = new CalculusSenderFIFOParameter($param, new Envelope($mess));
        $result = $xeroPara->parameters();
        $this->assertEquals($result['MessageGroupId'], 'calculus');
    }

    public function provider(): array
    {
        $data =[[
            array( 'aws' =>
                array( 'queue' =>
                    array( 'xero' =>
                        array( 'dsn' => 'test', 'groupId' => 'xero' ),
                        'calculus' =>
                            array( 'dsn' => 'test', 'groupId' => 'calculus' )
                    )
                )
            )]];
        return $data;
    }
}
