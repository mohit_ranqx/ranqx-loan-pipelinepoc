<?php declare(strict_types = 1);

namespace App\Tests\SQS;

use App\SQS\CalculusReveiveFIFOParameter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class CalculusReveiveFIFOParameterTest extends TestCase
{

    /**
     * @dataProvider provider
     */
    public function testParametersURL($data)
    {
        $param = $this->createMock(ParameterBagInterface::class);
        $param->expects($this->once())->method('get')
            ->with('ranqx')->willReturn($data);

        $calPara = new CalculusReveiveFIFOParameter($param);
        $result = $calPara->parameters();
        $this->assertEquals($result['QueueUrl'], 'test_calculus');
    }

    public function provider(): array
    {
        $data =[[
            array( 'aws' =>
                array( 'queue' =>
                    array( 'xero' =>
                        array( 'dsn' => 'test_xero', 'groupId' => 'xero' ),
                        'calculus' =>
                            array( 'dsn' => 'test_calculus', 'groupId' => 'calculus' )
                    )
                )
            )]];
        return $data;
    }
}
