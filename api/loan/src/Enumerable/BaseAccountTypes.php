<?php

namespace App\Enumerable;

use ReflectionClass;
use ReflectionException;

/**
 * Class BaseAccountTypes
 * @package App\Enumerable
 */
abstract class BaseAccountTypes
{
    /**
     *
     */
    public const RAW_DATA = 'RAW DATA';


    /**
     * @param $type
     * @return bool
     * @throws ReflectionException
     */
    final public static function isValidType($type): bool
    {
        $allTypes =
            (new ReflectionClass(IncomeStatementAccountTypes::class))->getConstants()
            + (new ReflectionClass(BalanceSheetAccountTypes::class))->getConstants();

        return in_array($type, $allTypes);
    }


    /**
     * @param $column
     * @return bool
     * @throws ReflectionException
     */
    final public static function isIncomeStatement($column)
    {
        return IncomeStatementAccountTypes::isValidColumn($column);
    }


    /**
     * @param $column
     * @return bool
     * @throws ReflectionException
     */
    final public static function isValidColumn($column): bool
    {
        return \in_array($column, (new ReflectionClass(static::class))->getConstants());
    }


    /**
     * @param $column
     * @return bool
     * @throws ReflectionException
     */
    final public static function isBalanceSheet($column): bool
    {
        return BalanceSheetAccountTypes::isValidColumn($column);
    }
}
