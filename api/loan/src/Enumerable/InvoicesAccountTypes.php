<?php

namespace App\Enumerable;

/**
 * Class InvoicesAccountTypes
 * @package App\Enumerable
 */


class InvoicesAccountTypes extends BaseAccountTypes
{

    public const TYPE = 'ACCREC';
    public const STATUS = 'AUTHORISED';
    //public const STATUS = 'SUBMITTED';
    //public const STATUS3 = 'PAID';


        /**
        * Conenct to api https://api.xero.com/api.xro/2.0/Invoices?
        * Where=Type=="ACCREC"
        * and Status=="AUTHORISED"
        * and Status=="SUBMITTED"
        * and Status=="PAID"
        * and Date >= DateTime(2019,05,01) and Date < DateTime(2019,06,30)&page=2
        **/



    /**
     * @param string $invoiceType
     * @return bool
     */
    public static function isInvoiceType($invoiceType):bool
    {
        return $invoiceType === 'ACCREC';
    }

    /**
     * @param string $invoiceStatus
     * @return bool
     */
    public static function isInvoiceStatus($invoiceStatus):bool
    {
        return ($invoiceStatus === 'AUTHORISED' || $invoiceStatus === 'SUBMITTED' || $invoiceStatus === 'PAID');
    }
}