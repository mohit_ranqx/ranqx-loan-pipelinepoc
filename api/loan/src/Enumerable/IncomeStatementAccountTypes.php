<?php
namespace App\Enumerable;

class IncomeStatementAccountTypes extends BaseAccountTypes
{
    public const LEGACY_COGS = 'COGS';
    public const LEGACY_DEPRECIATION = 'DEPRECIATION';
    public const LEGACY_INTEREST = 'INTEREST';
    public const LEGACY_LABOUR = 'LABOUR COSTS';
    public const LEGACY_NONCASH = 'NON CASH';
    public const LEGACY_OTHER = 'OTHER COSTS';
    public const LEGACY_RENT = 'RENTAL';
    public const LEGACY_REVENUE = 'REVENUE';

    public const REVENUE = 'IS-REV';
    public const COSTOFGOODS = 'IS-COG';
    public const PEOPLE = 'IS-PPL';
    public const PEOPLE_OTHER = 'IS-PPLO';
    public const RENT = 'IS-RNT';
    public const OPERATINGCOSTS = 'IS-OPC';
    public const OTHER = 'IS-OTR';
    public const OTHER_INTEREST = 'IS-OIN';
    public const OTHER_TAX = 'IS-OTX';
    public const OTHER_DEPRECIATION = 'IS-ODN';
    public const OTHER_EXCLUSIONS = 'IS-OEX';

    /**
     * @param string $accountType
     * @return bool
     */
    public static function isRevenue($accountType):bool
    {
        return $accountType === 'IS-REV';
    }

    /**
     * @param string $accountType
     * @return bool
     */
    public static function isCostOfGoods($accountType):bool
    {
        return $accountType === 'IS-COG';
    }

    /**
     * @param string $accountType
     * @return bool
     */
    public static function isPeople($accountType): bool
    {
        return strpos($accountType, 'IS-PPL') === 0;
    }

    /**
     * @param string $accountType
     * @return bool
     */
    public static function isRent($accountType): bool
    {
        return $accountType === 'IS-RNT';
    }

    /**
     * @param string $accountType
     * @return bool
     */
    public static function isOperatingCost($accountType): bool
    {
        return $accountType === 'IS-OPC';
    }

    /**
     * @param string $accountType
     * @return bool
     */
    public static function isOther($accountType): bool
    {
        return !self::isOperatingCost($accountType) && strpos($accountType, 'IS-O') === 0;
    }
}
