<?php

namespace App\Enumerable;

/**
 * Class BalanceSheetAccountTypes
 * @package App\Enumerable
 */
class BalanceSheetAccountTypes extends BaseAccountTypes
{
    public const ASSETS_CURRENT_BANK = 'A-CB';
    public const ASSETS_CURRENT_DEBTORS = 'A-CD';
    public const ASSETS_CURRENT_OTHER = 'A-CO';
    public const ASSETS_CURRENT_STOCK = 'A-CS';
    public const ASSETS_FIXED = 'A-F';
    public const ASSETS_OTHER = 'A-O';
    public const EQUITY_CAPITAL = 'E-C';
    public const EQUITY_EARNINGS = 'E-E';
    public const LIABILITIES_CURRENT_BANK = 'L-CB';
    public const LIABILITIES_CURRENT_CREDITORS = 'L-CC';
    public const LIABILITIES_CURRENT_OTHER = 'L-CO';
    public const LIABILITIES_OTHER = 'L-O';

    /**
     * @param string | null $accountType
     * @return bool
     */
    public static function isLiability($accountType): bool
    {
        return $accountType !== null && strpos($accountType, 'L-') === 0;
    }

    /**
     * @param string|null $accountType
     * @return bool
     */
    public static function isAsset($accountType): bool
    {
        return $accountType !== null && strpos($accountType, 'A-') === 0;
    }

    /**
     * @param string|null $accountType
     * @return bool
     */
    public static function isEquity($accountType): bool
    {
        return $accountType !== null && strpos($accountType, 'E-') === 0;
    }
}
