<?php declare(strict_types = 1);


namespace App\DataInterface;

/**
 * Interface IEquifax
 * @package App\DataInterface
 */
interface IEquifax
{
    /**
     * @return array
     */
    public function industryCode() : array;
}