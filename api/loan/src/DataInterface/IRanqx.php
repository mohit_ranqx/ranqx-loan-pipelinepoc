<?php declare(strict_types = 1);


namespace App\DataInterface;

/**
 * Interface IRanqx
 * @package App\DataInterface
 */
interface IRanqx
{
    /**
     * @param string $nzbn
     * @param \DateTime $date
     * @return int
     */
    public function numberOf(string $nzbn, \DateTime $date) : int;
    public function calculationData(int $organisationId): array;
    public function xeroData(int $organisationId): array;
}
