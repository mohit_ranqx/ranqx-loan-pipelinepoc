<?php declare(strict_types = 1);


namespace App\DataInterface;

use GuzzleHttp\Exception\GuzzleException;
use RuntimeException;
use function json_decode;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class Equifax
 * @package App\DataInterface
 */
class Equifax
{
    /**
     * @var ParameterBagInterface
     */
    private $para;
    /**
     * @var Client
     */
    private $http;

    /**
     * @var string
     */
    private $token;

    /**
     * Equifax constructor.
     * @param ParameterBagInterface $para
     */
    public function __construct(ParameterBagInterface $para)
    {
        $this->para = $para;
        $this->http = new Client();
    }

    /**
     * @return mixed
     * @throws GuzzleException
     */
    public function getAccessToken()
    {
        if ($this->token) return $this->token;

        $url = $this->makeUri();
        $body = $this->makeBody();
        $res = $this->http->request('POST', $url, [
            'headers' => ['Content-Type' =>'application/x-www-form-urlencoded'],
            'body' => $body
        ]);

        if ($res->getStatusCode() !== 200) {
            throw new HttpException($res->getStatusCode(), $res->getBody()->getContents());
        }

        $content = $res->getBody()->getContents();
        $data = json_decode($content, true);
        $this->token = $data['access_token'];
        return $data['access_token'];
    }

    /**
     * @return string
     */
    protected function makeBody() : string
    {
        $ranqx = $this->para->get('equifax');
        $body = 'grant_type=client_credentials';
        $body .= '&client_id='.$ranqx['key'];
        $body .= '&client_secret='.$ranqx['secret'];
        return $body;
    }

    /**
     * @param string $token
     * @return array
     */
    protected function makeTokenHeader(string $token): array
    {
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];
        return $headers;
    }

    /**
     * @return string
     */
    protected function makeUri(): string
    {
        $ranqx = $this->para->get('equifax');
        return $ranqx['base'] . $ranqx['tokenUrl'];
    }

    /**
     * @return string
     */
    public function getBaseUri(): string
    {
        $ranqx = $this->para->get('equifax');
        return $ranqx['base'];
    }


    /**
     * @param string $accessToken
     * @param string $nzbn
     * @return array
     * @throws GuzzleException
     */
    public function getOrganisation(string $accessToken, string $nzbn): array
    {
        $headers = $this->makeTokenHeader($accessToken);

        $base = $this->getBaseUri() . '/commercial/organisations/v1?term='.$nzbn;
        $res = $this->http->request('GET', $base, ['headers' => $headers, 'verify' => false]);

        if ($res->getStatusCode() === 200) {
            $data = json_decode($res->getBody()->getContents(), true);
            return $data['data']['organisations'][0] ?? [];
        }
        throw new HttpException($res->getStatusCode(), $res->getBody()->getContents());
    }

    /**
     * @param array $links
     * @param string $name
     * @return string
     */
    private function findLink(array $links, string $name): string
    {
        foreach ($links as $link) {
            if ($link['reportType'] === $name) {
                return $link['href'];
            }
        }
        return '';
    }


    /**
     * @param string $accessToken
     * @param array $links
     * @return array|mixed
     * @throws GuzzleException
     */
    public function score(string $accessToken, array $links)
    {
        $url = $this->findLink($links, 'score');
        $data = $this->getUrlData( $accessToken, $url);
        return $data['data']['body']['scores'] ?? [];
    }


    /**
     * @param string $accessToken
     * @param array $links
     * @return array
     * @throws GuzzleException
     */
    public function enquiryHistory(string $accessToken, array $links): array
    {
        $url = $this->findLink($links, 'enquiryHistory');
        $data = $this->getUrlData( $accessToken, $url);
        return $data;
    }


    /**
     * @param string $accessToken
     * @param array $links
     * @return array
     * @throws GuzzleException
     */
    public function management(string $accessToken, array $links) : array
    {
        $url = $this->findLink($links, 'management');
        $details = $this->getUrlData( $accessToken, $url);
        return $details;
    }


    /**
     * @param string $accessToken
     * @param array $links
     * @return array
     * @throws GuzzleException
     */
    public function creditActivitySummary(string $accessToken, array $links) : array
    {
        $url = $this->findLink($links, 'creditActivitySummary');
        $details = $this->getUrlData( $accessToken, $url);
        return $details;
    }


    /**
     * @param string $accessToken
     * @param array $links
     * @return array
     * @throws GuzzleException
     */
    public function companyDetails(string $accessToken, array $links): array
    {
        $url = $this->findLink($links, 'organisationDetails');
        $details = $this->getUrlData( $accessToken, $url);
        return $details;
    }


    /**
     * @param string $accessToken
     * @param string $nzbn
     * @return mixed
     * @throws GuzzleException
     */
    public function directors(string $accessToken, string $nzbn)
    {
        $url = $this->getBaseUri() . '/commercial/organisations/v1/directors/'.$nzbn;
        return $this->getUrlData($accessToken, $url);
    }

    public function getStandardReportPDF(string $accessToken, string $url): string
    {
        sleep(30);
        $headers = $this->makeTokenHeader($accessToken);
        $headers = array_merge($headers, ['Accept' => 'application/pdf']);
        $res = $this->http->request('GET', $url,
            ['headers' => $headers]
        );

        $code = $res->getStatusCode();
        if ($code >= 200 && $code < 400) {
            return $res->getBody()->getContents();
        }
        throw new HttpException($res->getStatusCode(), $res->getBody()->getContents());
    }


    /**
     * @param string $accessToken
     * @param array $org
     * @return array
     * @throws GuzzleException
     */
    public function getStandardReportDetailData(string $accessToken, array $org) : array
    {
        $url = $this->getBaseUri() . '/reporting/requests/v1';
        $data = $this->getStandardReportData($accessToken, $org, $url);
        $reportLink = $data['data']['_links']['reports']['href'];
        return $this->getUrlData($accessToken, $reportLink);
    }

    /**
     * @param string $accessToken
     * @param array $org
     * @param string $url
     * @return mixed
     * @throws GuzzleException
     */
    protected function getStandardReportData(string $accessToken, array $org, string $url)
    {
        $data['reportType'] = 'standard';
        $data['reportParameters'] = [
            'nzbn' => $org['nzbn'],
            'organisationNumber' => $org['organisationNumber'],
            'organisationName' => $org['businessName'],
            'enquiryType' => 'CA',
            'accountType' => 'OC',
            'amount' => '1000'
        ];

        $headers = $this->makeTokenHeader($accessToken);
        $res = $this->http->request('POST', $url,
            [
                'headers' => $headers
                ,'json' => $data]
        );

        $code = $res->getStatusCode();
        if ($code >= 200 && $code < 400) {
            return json_decode($res->getBody()->getContents(), true);
        }
        throw new HttpException($res->getStatusCode(), $res->getBody()->getContents());
    }

    /**
     * @param string $accessToken
     * @param string $url
     * @return mixed
     * @throws GuzzleException
     */
    public function getUrlData(string $accessToken, string $url, $headersOverride = [])
    {
        try {
            $headers = $this->makeTokenHeader($accessToken);
            $headers = array_merge($headers, $headersOverride);
            $res = $this->http->request('GET', $url, ['headers' => $headers]);

            $code = $res->getStatusCode();
            if ($code >= 200 && $code < 400) {
                return json_decode($res->getBody()->getContents(), true);
            }
            throw new HttpException($res->getStatusCode(), $res->getBody()->getContents());
        } catch (ServerException $e) {
            $response = $e->getResponse();
            if ($response) {
                $responseBodyAsString = $response->getBody()->getContents();
                throw new HttpException(500, $responseBodyAsString);
            }
            throw new HttpException(500, $e->getMessage());
        }
    }
}
