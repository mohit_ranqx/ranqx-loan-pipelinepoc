<?php declare(strict_types = 1);


namespace App\DataInterface;

use App\Entity\Applicant;
use App\Entity\ApplicantException;
use App\Entity\CalculateFinanceCurrentMonth;
use App\Entity\CalculateFinanceMonth;
use App\Entity\CalculateFinanceYearly;
use App\Entity\FinanceInvoice;
use App\Entity\FinanceMonth;
use App\Entity\Organisation;
use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class RanqxDataHandler
 * @package App\DataInterface
 *
 * https://ranqxltd.atlassian.net/browse/LO-252
 */
class RanqxDataHandler implements IRanqx
{
    /**
     * @var ObjectManager
     */
    private $entityManager;

    /**
     * @var Equifax
     */
    private $equifax;

    /**
     * @var ParameterBagInterface
     */
    private $params;


    /**
     * RanqxDataHandler constructor.
     * @param ObjectManager $entityManager
     * @param Equifax $equifax
     * @param ParameterBagInterface $bag
     */
    public function __construct(ObjectManager $entityManager, Equifax $equifax, ParameterBagInterface $bag)
    {
        $this->entityManager = $entityManager;
        $this->equifax = $equifax;
        $this->params = $bag;
    }

    /**
     * @param string $nzbn
     * @param DateTime $date
     * @return int
     *
     * use case 2
     * loan applications have been made by the same company within the past 6 calendar months. Multiple applications
     * KB_US_PR_002
     */
    public function numberOf(string $nzbn, DateTime $date): int
    {
        $qb = $this->entityManager->getRepository(Applicant::class)->numberOfNzbn($nzbn, $date);
        $count = 0;
        try {
            $count = $qb->getQuery()->getSingleScalarResult();
        } catch (Exception $e) {
            $count = 0;
        } finally {
            return (int)$count;
        }
    }

    /**
     * @param int $organisationId
     * @return array
     */
    public function calculationData(?int $organisationId): array
    {
        if ($organisationId) {
            $org = $this->entityManager->getRepository(Organisation::class)->find($organisationId);
            if ($org) {
                $qb = $this->entityManager->getRepository(CalculateFinanceCurrentMonth::class)
                    ->findByOrg($org);
                return $qb->getQuery()->getArrayResult();
            }
        }
        return [];
    }

    /**
     * @param int $organisationId
     * @return array
     * KB_US_PR_003
     */
    public function xeroData(int $organisationId): array
    {
        $org = $this->entityManager->getRepository(Organisation::class)->find($organisationId);
        if ($org) {
            $qb = $this->entityManager->getRepository(FinanceMonth::class)->findOrg($org);
            $results = $qb->getQuery()->getArrayResult();

            //if any one column container all zero, then set all zero to true
            $allZero = 'false';
            foreach ($results as $item) {
                if ($this->allZero($item)) {
                    $allZero = 'true';
                    break;
                }
            }

            return ['total' => count($results),
                'allZero' => $allZero,
                'oldest' => $results[0]];
        }

        return ['total' => 0, 'ordest' => []];
    }

    /**
     * @param $items
     * @return bool
     */
    private function allZero($items)
    {
        $sum = 0.0;
        foreach ($items as $key => $value) {
            if ($key !== 'period' && $key !== 'staffNumber') {
                if (is_numeric($value)) {
                    $sum += $value;
                }
            }
        }

        return ($sum - 0.0) <= 0.0001;//equals to zero
    }

    /**
     * @param int $applicantId
     * @return array
     *
     * Case 3: oldest period data
     * get olderst period data from finance month table, when Applicant id is been supplied.
     * KB_US_PR_003
     */
    public function oldestData(int $applicantId): array
    {
        /** @var Applicant | null $app**/
        $app = $this->entityManager->getRepository(Applicant::class)->find($applicantId);
        if ($app) {
            /** @var Organisation | null $org **/
            $org = $app->getOrganisation();
            if ($org) {
                $qb = $this->entityManager->getRepository(FinanceMonth::class)->lastData($org);
                $temp = $qb->getQuery()->getArrayResult();
                return $temp[0] ?? [];
            }
        }

        return [];
    }

    /**
     * @param int $organisationId
     * @return array
     *
     * Case 4: most recent monthly value from the calculate_finance_month_current
     * KB_US_PR_005
     */
    public function mostRecentMonthlyCalulationValue(int $organisationId) : array
    {
        $org = $this->entityManager->getRepository(Organisation::class)->find($organisationId);
        if ($org) {
            $qb = $this->entityManager->getRepository(CalculateFinanceCurrentMonth::class)
                ->findFirstRevenueRoll($org);
            $temp = $qb->getQuery()->getArrayResult();
            return $temp[0] ?? [];
        }

        return [];
    }

    /**
     * @param int $organisationId
     * @return array
     */
    public function calFinanceYearly(?int $organisationId): array
    {
        if ($organisationId) {
            $org = $this->entityManager->getRepository(Organisation::class)->find($organisationId);
            if ($org) {
                $qb = $this->entityManager->getRepository(CalculateFinanceYearly::class)
                    ->findFirstRevenueRoll($org);
                return $qb->getQuery()->getArrayResult();
            }
        }
        return [];
    }

    /**
     * @param int $organisationId
     * @return array
     */
    public function calFinanceRolling(?int $organisationId): array
    {
        if ($organisationId) {
            $org = $this->entityManager->getRepository(Organisation::class)->find($organisationId);
            if ($org) {
                $qb = $this->entityManager->getRepository(CalculateFinanceMonth::class)
                    ->findFirstRevenueRoll($org);
                return $qb->getQuery()->getArrayResult();
            }
        }
        return [];
    }

    /**
     * @param int $organisationId
     * @return int|mixed
     */
    public function loanAmount(?int $organisationId)
    {
        if ($organisationId) {
            /** @var Organisation|null $org * */
            $org = $this->entityManager->getRepository(Organisation::class)->find($organisationId);
            if ($org) {
                $app = $org->getApplicant();
                if ($app) {
                    return $app->getLoanAmount();
                }
            }
        }
        return 0;
    }

    /**
     * @param int|null $organisationId
     * @return array
     */
    public function debtorConcentration(?int $organisationId)
    {
        if ($organisationId) {
            /** @var Organisation|null $org * */
            $org = $this->entityManager->getRepository(Organisation::class)->find($organisationId);
            if ($org) {

                /** @var FinanceInvoice|null $fi * */
                $fi = $this->entityManager->getRepository(FinanceInvoice::class)
                    ->findOneBy(['organisation_id' => $org->getId()]);
                if ($fi) {
                    return [
                        'debtorConcentration' => $fi->getDebtorConcentration(),
                        'data' => $fi->getInvoiceDataReturned()
                    ];
                }
            }
        }
        return [
            'debtorConcentration' => null,
            'data' => 'no'
        ];
    }


    /**
     * @param int|null $organisationId
     * @param int $appId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function allData(?int $organisationId, int $appId) : array
    {
        /** @var Applicant|null $app ***/
        $app = $this->entityManager->getRepository(Applicant::class)->find($appId);

        $result = [];
        $nzbnNumber = '';
        $xeroData = [];
        $equiFax = [];

        try {
            if ($app) {
                $nzbn = $app->getNzbnNumber();
                if ($nzbn !== '' && $nzbn !== null) {
                    $date = new \DateTime($this->getTimeFrameForNzbnNumber());
                    $nzbnNumber = $this->numberOf($nzbn, $date);
                    $equiFax = $this->equiFaxData($nzbn);
                }
            }

            if ($organisationId) {
                $xeroData = $this->xeroData($organisationId);
            }

            return [
                'nzbn' => $nzbnNumber,
                'xeroData' => $xeroData,
                'calFinanceYearly' => $this->calFinanceYearly($organisationId),
                'calFinanceCurrentMonth' => $this->calculationData($organisationId),
                'calFinanceCurrentRolling' => $this->calFinanceRolling($organisationId),
                'debtorConcentration' => $this->debtorConcentration($organisationId),
                'loanAmount' => $this->loanAmount($organisationId),
                'equiFax' => $equiFax,

            ];
        } catch (\Exception $e) {
            $result = [
                'exception' => $e->getMessage(),
                'nzbn' => $nzbnNumber,
                'xeroData' => $xeroData,
                'calFinanceYearly' => $this->calFinanceYearly($organisationId),
                'calFinanceCurrentMonth' => $this->calculationData($organisationId),
                'calFinanceCurrentRolling' => $this->calFinanceRolling($organisationId),
                'loanAmount' => $this->loanAmount($organisationId),
                'debtorConcentration' => $this->debtorConcentration($organisationId),
                'equiFax' => $equiFax
            ];
        }
        return $result;
    }

    /**
     * @param string $nzbn
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function equiFaxData(string $nzbn): array
    {
        $handler = new EquifaxHandler($this->equifax, $nzbn);
        return $handler->allData();
    }

    /**
     * @return string
     */
    private function getTimeFrameForNzbnNumber(): string
    {
        $decision = $this->params->get('decision');
        $bank = $decision['bank'];
        return $decision['conditions'][$bank]['multiple_applications_time_frame'];
    }
}
