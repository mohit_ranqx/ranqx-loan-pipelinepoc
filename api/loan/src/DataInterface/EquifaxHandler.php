<?php declare(strict_types = 1);


namespace App\DataInterface;

use \GuzzleHttp\Exception\GuzzleException;

/**
 * Class EquifaxHandler
 * @package App\DataInterface
 */
class EquifaxHandler
{
    /**
     * @var Equifax
     */
    private $src;
    /**
     * @var string
     */
    private $nzbn;

    /**
     * EquifaxHandler constructor.
     * @param Equifax $src
     *
     *
     * @param string $nzbn
     */
    public function __construct(Equifax $src, string $nzbn)
    {
        $this->src = $src;
        $this->nzbn = $nzbn;
    }

    /**
     * @return array
     * @throws GuzzleException
     */
    public function allData(): array
    {
        $result = [];
        $code = [];
        $score = [];
        $org = [];
        $directors = [];
        $details = [];
        $creditActivitySummary = [];
        $management = [];
        $pdf = '';
        $enquiryHistory = [];
        try {
            $accessToken = $this->src->getAccessToken();
            $org = $this->src->getOrganisation($accessToken, $this->nzbn);
            $directors = $this->src->getUrlData($accessToken, $org['_links']['directorDetails']['href']);

            $reports = $this->src->getStandardReportDetailData($accessToken, $org);
            $pdf = $this->src->getStandardReportPDF($accessToken, $reports['_links']['self']['href']);
            $details = $this->src->companyDetails($accessToken, $reports['data']['related']);
            $score = $this->src->score($accessToken, $reports['data']['related']);
            $enquiryHistory = $this->src->enquiryHistory($accessToken, $reports['data']['related']);
            $creditActivitySummary = $this->src->creditActivitySummary($accessToken, $reports['data']['related']);
            $management = $this->src->management($accessToken, $reports['data']['related']);

            //save $org
            //save $details

            $code = [
                'industryCode' => $details['data']['body']['businessDetails']['industry']['code'] ?? '',
                'ageOfCompany' => $details['data']['body']['businessDetails']['registrationDate'] ?? '',
            ];

            return [
                'decision' => ['companyDetails' => $code,
                    'score' => $score,
                    'enquiryHistory' => $this->getEnquireHistoryForDecision($enquiryHistory),
                    'summary' => $reports['data']['body']['summary'] ?? []],

                'db' => [
                    'org' => $org,
                    'directors' => $directors,
                    'companyDetails' => $details,
                    'creditActivitySummary' => $creditActivitySummary,
                    'enquiryHistory' => $enquiryHistory,
                    'management' => $management,
                    'pdf' => $pdf
                ]
            ];
        } catch (\Exception $e) {
            $result = [
                'exception' => $e->getMessage(),
                'decision' => ['companyDetails' => $code,
                    'score' => $score,
                    'enquiryHistory' => $this->getEnquireHistoryForDecision($enquiryHistory),
                    'summary' => $reports['data']['body']['summary'] ?? []],

                'db' => [
                    'org' => $org,
                    'directors' => $directors,
                    'companyDetails' => $details,
                    'creditActivitySummary' => $creditActivitySummary,
                    'enquiryHistory' => $enquiryHistory,
                    'management' => $management,
                    'pdf' => $pdf
                ]
            ];
        }

        return $result;
    }

    /**
     * @param array $data
     * @return array
     */
    private function getEnquireHistoryForDecision(array $data): array
    {
        if (!$data) return [];

        $recentEnquires = $data['data']['body']['enquiries'] ?? [];
        $excessiveEnquiries = $data['data']['body']['enquiryTotalsLast5Years'] ?? [];

        $total = ['count' => 0, 'sum' => 0, 'average' => 0];
        foreach ($excessiveEnquiries as $enquiry) {
            $total['count'] += $enquiry['count'];
            $total['sum'] += $enquiry['valueSum'];
            $total['average'] += $enquiry['valueAverage'];
        }

        return ['totalEnquires' => $this->lastMonths($recentEnquires),
            'credit_enquiries_24M' => $this->lastMonths($recentEnquires, $last = '-24 months'),
            'enquiryTotalsLast5Years' => $total];
    }

    /**
     * @param array $data
     * @param string $last
     * @return int
     * @throws \Exception
     */
    private function lastMonths(array $data, string $last = '-3 months'): int
    {
        $date = (new \DateTime())->modify($last);
        $c = 0;
        foreach ($data as $item)
        {
            $itemDate = new \DateTime($item['date']);
            if ($itemDate >= $date) {
                ++$c;
            }
        }
        return $c;
    }
}