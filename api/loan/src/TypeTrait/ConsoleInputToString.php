<?php declare(strict_types = 1);


namespace App\TypeTrait;

use RuntimeException;
use Symfony\Component\Console\Input\InputInterface;

trait ConsoleInputToString
{
    public function toString(InputInterface $input, string $argumentName, string $defaultValue): string
    {
        $chunk = $input->getArgument($argumentName);
        if (is_array($chunk)) {
            throw new RuntimeException($argumentName. ' argument should not be array.');
        }

        if ($chunk === null) {
            $chunk = $defaultValue;
        }

        return $chunk;
    }
}
