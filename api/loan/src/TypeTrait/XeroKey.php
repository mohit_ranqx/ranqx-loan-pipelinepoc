<?php declare(strict_types=1);

namespace App\TypeTrait;

trait XeroKey
{
    public function publicKey()
    {
        $p = $_ENV['XERO_PARTNER_PUBLIC_KEY'] ?? ( $_SERVER['XERO_PARTNER_PUBLIC_KEY'] ?? getenv('XERO_PARTNER_PUBLIC_KEY'));

        $key = "-----BEGIN CERTIFICATE-----\n";
        $key .= $this->validKey($p);
        $key .= "\n-----END CERTIFICATE-----";
        return $key;
    }

    public function privateKey()
    {
        $p = $_ENV['XERO_PARTNER_PRIVATE_KEY'] ?? ( $_SERVER['XERO_PARTNER_PRIVATE_KEY'] ?? getenv('XERO_PARTNER_PRIVATE_KEY'));
        $key = "-----BEGIN RSA PRIVATE KEY-----\n";
        $key .= $this->validKey($p);
        $key .= "\n-----END RSA PRIVATE KEY-----";
        return $key;
    }

    private function validKey($key)
    {
        $search = ["\r\n","\n"];
        $replace = ['',''];
        $str = str_replace($search, $replace, $key);
        return wordwrap($str, 64, "\n", true);
    }
}