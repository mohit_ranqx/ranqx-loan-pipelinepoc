<?php


namespace App\TypeTrait;


trait EnvEmail
{
    public function getEnvEmails()
    {
        $from = $_ENV['EMAIL_FROM_ADDRESS'] ?? ( $_SERVER['EMAIL_FROM_ADDRESS'] ?? getenv('EMAIL_FROM_ADDRESS'));
        $bcc = $_ENV['EMAIL_BCC_ADDRESSES'] ?? ( $_SERVER['EMAIL_BCC_ADDRESSES'] ?? getenv('EMAIL_BCC_ADDRESSES'));
        $override = $_ENV['EMAIL_OVERRIDE_ADDRESS'] ?? ( $_SERVER['EMAIL_OVERRIDE_ADDRESS'] ?? getenv('EMAIL_OVERRIDE_ADDRESS'));
        return [
            'from' => $from,
            'bcc' => $bcc,
            'override' => $override
        ];
    }

    public function isOverrideExist()
    {
        $emails = $this->getEnvEmails();
        return trim($emails['override']) !== '<>';
    }

    public function bccToArray()
    {
        $emails = ($this->getEnvEmails())['bcc'];
        if ($emails !== '') {
            return explode(',', $emails);
        }
        return [];
    }
}