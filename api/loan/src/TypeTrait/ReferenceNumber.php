<?php declare(strict_types=1);


namespace App\TypeTrait;

use App\Entity\Applicant;

/**
 * Trait ReferenceNumber
 * @package App\TypeTrait
 */
trait ReferenceNumber
{
    /**
     * @param int $appId
     * @return string
     */
    public function KBRefNumber(?int $appId): string
    {
        if ($appId === null) {
            return '1000000';
        }

        return (string)(1000000 + $appId);
    }
}