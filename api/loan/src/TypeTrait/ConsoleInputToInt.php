<?php declare(strict_types = 1);


namespace App\TypeTrait;

use RuntimeException;
use Symfony\Component\Console\Input\InputInterface;

trait ConsoleInputToInt
{
    public function toInt(InputInterface $input, string $argumentName, int $defaultValue): int
    {
        //getArgument returns string|string[]|null
        $chunk = $input->getArgument($argumentName);
        if (is_array($chunk)) {
            throw new RuntimeException($argumentName . ' argument should not be array.');
        }

        if ($chunk === null) {
            $chunk = $defaultValue;
        }

        return (int)$chunk;
    }
}
