<?php declare(strict_types=1);

namespace App\TypeTrait;

/**
 * Trait InputToFloat
 * @package App\TypeTrait
 */
trait InputToFloat
{
    /**
     * @param mixed $value
     * @return float|null
     */
    public function toFloat($value): ?float
    {
        if ($value !== null && is_numeric($value)) {
            return (float)$value;
        }

        return null;
    }
}