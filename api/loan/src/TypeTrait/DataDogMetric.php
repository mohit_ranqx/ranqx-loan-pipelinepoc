<?php declare(strict_types=1);


namespace App\TypeTrait;

use DataDog\DogStatsd;

trait DataDogMetric
{
    public function metric($name, $sampleRate = 1.0, $tags = null, $value = 1): void
    {
        $statsd = new DogStatsd();
        $statsd->increment($name, $sampleRate, $tags, $value);
    }

    public function event($name, $value = []): void
    {
        $statsd = new DogStatsd();
        $statsd->event($name, $value);
    }
}