<?php declare(strict_types=1);

namespace App\TypeTrait;

trait Utc2NZ
{
    /**
     * @param \DateTime $convertDT
     * @return \DateTime
     * @throws \Exception
     */
    public function convert2NZ(\DateTime $convertDT): \DateTime
    {
        $nzTimezone = new \DateTimeZone('Pacific/Auckland');
        $utcTimezone = new \DateTimeZone('UTC');
        $temp = new \DateTime($convertDT->format('Y-m-d H:i:s'), $utcTimezone);
        $temp->setTimezone($nzTimezone);
        $appCreatedDate = $temp->setTimezone($nzTimezone);
        $appCreatedDate->format('Y-m-d H:i:s');

        return new \DateTime($appCreatedDate->format('Y-m-d H:i:s'), $nzTimezone);
    }
}