<?php declare(strict_types=1);


namespace App\Command;

use App\TypeTrait\ConsoleInputToString;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;

class LoadNZBNJsonFromDirCommand extends Command
{
    use ConsoleInputToString;

    /**
     * @var string
     */
    protected static $defaultName = 'app:ranqx:load:nzbn:jsondir';

    protected function configure()
    {
        $this
            ->setDescription('Load NZBN json files that inside a directory into db table.')
            ->addArgument('filedir', InputArgument::REQUIRED, '')
            ->setHelp('app:ranqx:load:nzbn:jsondir <nzbn json dir>');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dir = $this->toString($input, 'filedir', '');
        if ($dir) {
            foreach (new \DirectoryIterator($dir) as $fileInfo) {
                if ($fileInfo->isDot()) {
                    continue;
                }
                if ($fileInfo->getExtension() === 'json') {
                    $path = $fileInfo->getPathname();
                    $command = $this->getApplication()->find('app:ranqx:load:nzbn:json');

                    $arguments = [
                        'command' => 'app:ranqx:load:nzbn:json',
                        'file'    => $path
                    ];

                    $jsonLoader = new ArrayInput($arguments);
                    $command->run($jsonLoader, $output);
                }
            }
        } else {
            $output->writeln('Directory not exists: ' . $dir);
            exit(1);
        }
    }
}