<?php declare(strict_types=1);


namespace App\Command;

use App\DataInterface\Equifax;
use App\DataInterface\RanqxDataHandler;
use App\Entity\Applicant;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use \Psr\Log\LoggerInterface;
use App\TypeTrait\ConsoleInputToInt;

class DecisionDataMonitoringCommand extends Command
{
    use ConsoleInputToInt;
    protected static $defaultName = 'app:ranqx:decision:data:monitoring';
    protected $entityManager;
    protected $params;
    protected $logger;

    public function __construct(EntityManagerInterface $entityManager,
                                ParameterBagInterface $bag, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->params = $bag;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('An Exception happened, use this command to download data again.')
            ->addArgument('appId', InputArgument::REQUIRED, 'applicant ID')
            ->setHelp('An Exception happened, use this command to re download data.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $appId = $this->toInt($input, 'appId', 0);
        /** @var Applicant | null $app**/
        $app = $this->entityManager->getRepository(Applicant::class)
            ->find($appId);
        if ($app) {
            $org = $app->getOrganisation();

            if ($org) {
                $dataSrc = $this->getDecisionData($org->getId(), $appId);

                while (isset($dataSrc['equiFax']['exception'])) {
                    $this->logger->critical($dataSrc['equiFax']['exception']);
                    sleep(60 * 10);//sleep 10 minutes
                    $dataSrc = $this->getDecisionData($org->getId(), $appId);
                }
            } else {
                $output->writeln('org is null.');
            }
        }
    }

    public function getDecisionData(int $orgId, int $appId)
    {
        $handler = new RanqxDataHandler($this->entityManager, new Equifax($this->params), $this->params);
        $data = $handler->allData($orgId, $appId);
        if (empty($data)) {
            $data = json_encode(new \stdClass);
        }

        return $data;
    }
}