<?php declare(strict_types = 1);

namespace App\Command;

use Aws\Ses\SesClient;
use Html2Text\Html2Text;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Twig\Environment;
use Twig\Error;

/**
 * Class EmailCommand
 * @package App\Command
 */
class EmailCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:ranqx:email';

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var SesClient
     */
    private $mailer;

    /**
     * EmailCommand constructor.
     * @param SesClient $mailer
     * @param Environment $twig
     */
    public function __construct(
        SesClient $mailer,
        Environment $twig
    ) {
        parent::__construct();

        $this->twig = $twig;
        $this->mailer = $mailer;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws Error\LoaderError
     * @throws Error\RuntimeError
     * @throws Error\SyntaxError
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $projectDir = '/var/www';
        $from = 'no-reply@ranqx.com';

        $mail = new PHPMailer();

        $mail->CharSet = 'UTF-8';

        $mail->setFrom($from);
        $mail->Subject = 'Thank you for applying for a Kiwibank Business Overdraft';

        // Todo: use traits
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbEmailHeaderOpt1.jpg', 'kbHeader', 'kbEmailHeaderOpt1.jpg');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbLogo.png', 'kbLogo', 'kbLogo.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbShield.png', 'kbShield', 'kbShield.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbComputer.png', 'kbComputer', 'kbComputer.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbTwitter.png', 'kbTwitter', 'kbTwitter.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbPoint.png', 'kbPoint', 'kbPoint.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbFB.png', 'kbFB', 'kbFB.png');

        // Render HTMl content
        $htmlBody = $this->twig->render('\kb\forApplicant.html.twig',
            [
                'referenceNumber' => '12345678',
                'emailAddress' => 'test@ranqx.com',
                'firstName' => 'Test',
                'companyName' => 'Test Company',
                'decision' => 'APPROVE',
            ]);

        $html2TextConverter = new Html2Text($htmlBody);

        $mail->Body = $htmlBody;
        $mail->AltBody = $html2TextConverter->getText();

        $mail->IsHTML(true);

        $mail->addReplyTo($from);
        //$mail->addAddress('devtest@ranqx.com');
        $mail->addAddress('lloyd.mason@ranqx.com');

        $mail->preSend();
        $message = $mail->getSentMIMEMessage();
        $result = $this->mailer->sendRawEmail([
            'RawMessage' => [
                'Data' => $message
            ]
        ]);
    }

}