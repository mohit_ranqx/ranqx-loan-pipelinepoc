<?php declare(strict_types = 1);


namespace App\Command;

use Aws\Sqs\SqsClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Console\Input\InputArgument;
use Aws\Exception\AwsException;

class AwsQueueRemoveCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:ranqx:queue:sqs:remove';

    protected $sqs;

    protected $param;

    public function __construct(SqsClient $sqs, ParameterBagInterface $param)
    {
        $this->sqs = $sqs;
        $this->param = $param;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Remove a queue')
            ->addArgument('queuename', InputArgument::REQUIRED, 'Sqs queue name')
            ->setHelp('This command allows you to remove a sqs queue');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('queuename');

        try {
            $ranqx = $this->param->get('ranqx');
            $prefix = $ranqx['aws']['queueprefix'];

            $result = $this->sqs->deleteQueue([
                'QueueUrl' => $prefix.$name
            ]);

        } catch (AwsException $e) {
            //$output->writeln($e->getMessage());
        }
    }
}