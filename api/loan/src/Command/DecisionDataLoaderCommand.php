<?php declare(strict_types=1);


namespace App\Command;

use App\DataInterface\Equifax;
use App\DataInterface\RanqxDataHandler;
use App\Entity\Applicant;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class DecisionDataLoaderCommand
 * @package App\Command
 */
class DecisionDataLoaderCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:ranqx:decision:data:load';
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;
    /**
     * @var ParameterBagInterface
     */
    protected $params;

    /**
     * DecisionDataLoaderCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param ParameterBagInterface $bag
     */
    public function __construct(EntityManagerInterface $entityManager, ParameterBagInterface $bag)
    {
        $this->entityManager = $entityManager;
        $this->params = $bag;
        parent::__construct();
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('An Exception happened, use this command to download data again.')
            ->addArgument('appId', InputArgument::REQUIRED, 'applicant ID')
            ->setHelp('An Exception happened, use this command to re download data.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $appId = $input->getArgument('appId');
        /** @var Applicant | null $app**/
        $app = $this->entityManager->getRepository(Applicant::class)
            ->find($appId);

        if ($app) {
            $org = $app->getOrganisation();
            $oid = null;
            /** @var int $aid*/
            $aid = $app->getId();
            if ($org) {
                $oid = $org->getId();
            }
            /** @var array $dataSrc**/
            $dataSrc = $this->getDecisionData($oid, $aid);

            $dataSrc['bankData'] = [
                'loanType' => 'What is loan type',
                'bank' => 'Kiwi Bank',
                'applicantId' => $appId,
                'orgId' => $oid
            ];

            $pdf = $dataSrc['equiFax']['db']['pdf'] ?? '';
            if (isset($dataSrc['equiFax']['db']['pdf'])) {
                unset($dataSrc['equiFax']['db']['pdf']);
            }
            $app->setEquiFaxRaw($dataSrc);
            $app->setPdf($pdf);
            $app->setDecision(true);
        }
        $this->entityManager->flush();
    }

    /**
     * @param int|null $orgId
     * @param int $appId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getDecisionData(?int $orgId, int $appId): array
    {
        $handler = new RanqxDataHandler($this->entityManager, new Equifax($this->params), $this->params);
        return $handler->allData($orgId, $appId);
    }
}