<?php declare(strict_types = 1);


namespace App\Command;

use App\XML\NzbnXmlParser;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use App\TypeTrait\ConsoleInputToInt;
use App\TypeTrait\ConsoleInputToString;

/**
 * Class LoadNZBNAsCSVCommand
 * @package App\Command
 */
class LoadNZBNAsCSVCommand extends Command
{
    use ConsoleInputToInt;
    use ConsoleInputToString;
    /**
     * @var string
     */
    protected static $defaultName = 'app:ranqx:load:nzbn:csv';

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var int
     */
    protected $chunk;

    /**
     * @var int
     */
    protected $sqlchunk;

    /**
     * LoadNZBNAsCSVCommand constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setDescription('Load NZBN json file into csv files.')
            ->addArgument('file', InputArgument::REQUIRED, '')
            ->addArgument('outfile', InputArgument::REQUIRED, '')
            ->addArgument('chunk', InputArgument::OPTIONAL, 'How many bytes to read at one block.')
            ->addArgument('sqlchunk', InputArgument::OPTIONAL, 'How many insertion rows send to DB.')
            ->setHelp('ranqx:app:load-nzbn-json <nzbn xml here')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $this->toString($input, 'file', '');
        $outfile = $this->toString($input,'outfile', '');
        $this->chunk = $this->toInt($input,'chunk', 1024*1024);
        $this->sqlchunk = $this->toInt($input,'sqlchunk', 8192);


        $csv = fopen($outfile, 'w');
        if (!$csv) {
            throw new \RuntimeException('could not open file. '. $outfile);
        }

        $progressBar = new ProgressBar($output);
        $progressBar->start();
        $str = 'name,nzbn,company_number,source_register, created, status, country_of_origin';
        fwrite($csv, $str."\n");

        $listener = function ($companies) use ($progressBar, $csv) {
            $this->write($companies, $csv);
            $progressBar->advance();
        };
        //How many rows one can insert with one statement depends on the max_allowed_packet
        $xml = new NzbnXmlParser($name, $listener, $this->sqlchunk, $this->chunk);
        $xml->parse();

        $progressBar->advance();
        $progressBar->finish();
        fclose($csv);
    }

    /**
     * @param array $companyInfor
     * @param resource $fp
     * @throws \Exception
     */
    protected function write($companyInfor, $fp) : void
    {
        $str = '';
        $conn = $this->entityManager->getConnection();
        foreach ($companyInfor as $data) {
            $str .=  $conn->quote($data['name']) . ',';
            $str .=  $conn->quote($data['nzbn']) . ',';

            $str .=  $conn->quote($data['company_number']) . ',';
            $str .= $conn->quote($data['source_register']) . ', ';
            $str .= $conn->quote((new DateTime())->format('Y-m-d H:i:s')) . ',';
            $str .= $conn->quote($data['status']) . ',';
            $str .= $conn->quote($data['country_of_origin']) . "\n";
        }
        fwrite($fp, $str);
    }
}
