<?php declare(strict_types = 1);


namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\TypeTrait\ConsoleInputToInt;
use App\TypeTrait\ConsoleInputToString;
use Symfony\Component\Yaml\Yaml;

/**
 * Class LoadWhiteListCommand
 * @package App\Command
 *
 * Usage: php bin/console app:ranqx:load:whitelist
 */
class LoadWhiteListCommand extends Command
{
    use ConsoleInputToInt;
    use ConsoleInputToString;
    /**
     * @var string
     */
    protected static $defaultName = 'app:ranqx:load:whitelist';

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * LoadNZBNJsonCommand constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('whitelist', InputArgument::REQUIRED, 'Location of the yaml whitelist');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->entityManager->getConnection()->query("TRUNCATE tenant")->execute();

        $whitelist = Yaml::parseFile($input->getArgument('whitelist'));
        $tenantRecords = $whitelist['whitelist'];

        foreach ($tenantRecords as $key => $tenantRecord) {
            $tenant = new User();
            $tenant->setName($key);
            $tenant->setOrigin($tenantRecord['origin']);
            $tenant->setReferer($tenantRecord['referer']);
            $tenant->setType($tenantRecord['type']);
            $tenant->setActive(1);
            $tenant->setCreatedAt(new \DateTime());
            $tenant->setUpdatedAt(new \DateTime());
            $this->entityManager->persist($tenant);
        }

        $this->entityManager->flush();

    }

}
