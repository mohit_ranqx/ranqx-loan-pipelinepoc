<?php declare(strict_types = 1);


namespace App\Command;

use App\Decision\DecisionDataLocatorChain;
use App\Decision\Scorecard\ScorecardDataLocatorChain;
use App\Entity\Applicant;
use App\Entity\DecisionPolicyResult;
use App\Message\ApplicantExceptionMessage;
use App\Message\RanqxExceptionMessage;
use App\PayLoad\PayLoadActiveField;
use App\Service\EquiFaxDataQuery;
use App\Service\S3RanqxClient;
use Aws\Ses\SesClient;
use Doctrine\ORM\EntityManagerInterface;
use Html2Text\Html2Text;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Decision\Scorecard\KiwibankCumulativeScorecardDecision;
use App\Decision\KiwiBankCumulativePolicyDecision;
use App\Decision\Scorecard\KiwiBankScorecardRecord;
use App\Decision\Record\KiwiBankPolicyRuleRecord;
use App\Entity\ScorecardSeed;
use App\Decision\KiwibankFinalDecision;
use Symfony\Component\Messenger\MessageBusInterface;
use Twig\Environment;
use App\TypeTrait\EnvEmail;
use Psr\Log\LoggerInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use App\PayLoad\PayLoadApplicationDetails;
use App\PayLoad\PayLoadPolicyRuleResult;
use App\PayLoad\PayLoadFileCreation;
use App\DataInterface\RanqxDataHandler;
use \GuzzleHttp\Exception\GuzzleException;
use \DateTime;
use \Doctrine\DBAL\DBALException;

/**
 * Class DecisionCommand
 * @package App\Command
 */
class DecisionCommand extends Command
{
    use EnvEmail;
    /**
     * @var string
     */
    protected static $defaultName = 'app:ranqx:decision:make';
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    /**
     * @var ParameterBagInterface
     */
    private $param;
    /**
     * @var DecisionDataLocatorChain
     */
    private $locators;
    /**
     * @var ScorecardDataLocatorChain
     */
    private $scoreLocatorChain;
    /**
     * @var SesClient
     */
    private $mailer;
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var S3RanqxClient
     */
    private $client;

    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @var RanqxDataHandler
     */
    private $ranqxHandler;


    /**
     * DecisionCommand constructor.
     * @param DecisionDataLocatorChain $locatorChain
     * @param ScorecardDataLocatorChain $scoreLocatorChain
     * @param EntityManagerInterface $objectManager
     * @param ParameterBagInterface $bag
     * @param SesClient $mailer
     * @param Environment $twig
     * @param LoggerInterface $logger
     * @param S3RanqxClient $client
     * @param MessageBusInterface $bus
     * @param RanqxDataHandler $handler
     */
    public function __construct(
        DecisionDataLocatorChain $locatorChain,
        ScorecardDataLocatorChain $scoreLocatorChain,
        EntityManagerInterface $objectManager,
        ParameterBagInterface $bag,
        SesClient $mailer,
        Environment $twig,
        LoggerInterface $logger,
        S3RanqxClient $client,
        MessageBusInterface $bus,
        RanqxDataHandler $handler
    ) {
        $this->objectManager = $objectManager;
        $this->param = $bag;
        $this->locators = $locatorChain;
        $this->scoreLocatorChain = $scoreLocatorChain;
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->logger = $logger;
        $this->client = $client;
        $this->bus = $bus;
        $this->ranqxHandler = $handler;
        parent::__construct();
    }

    /**
     *
     */
    protected function configure(): void
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Make decision.')
            ->setHelp('Make decision.');
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            /** @var Applicant|null $app *** */
            $app = $this->objectManager->getRepository(Applicant::class)
                ->findOneBy(['decision' => true], ['updated' => 'asc']);
            $log = null;
            //no Applicant exists , that an valid case.
            if ($app) {
                $app->setDecision(false);
                /** @var int appId */
                $appId = $app->getId();

                if ($app->getOrganisation() === null) {
                    $dataSrc = $this->ranqxHandler->allData(null, $appId);
                    $pdf = $dataSrc['equiFax']['db']['pdf'] ?? '';

                    if (isset($dataSrc['equiFax']['db']['pdf'])) {
                        unset($dataSrc['equiFax']['db']['pdf']);
                    }

                    $app->setEquiFaxRaw($dataSrc);
                    $app->setPdf($pdf);
                } else {
                    $dataSrc = $app->getEquiFaxRaw();
                }

                $app->setUpdated(new DateTime());

                if (isset($dataSrc['equiFax']['exception'])) {
                    $this->bus->dispatch(
                        new ApplicantExceptionMessage(
                            $appId,
                            \json_encode($dataSrc['equiFax']['exception'])
                        )
                    );
                    $this->logger->emergency('Decision engine exception ' . $dataSrc['equiFax']['exception']);
                }

                if (empty($dataSrc['equiFax'])) {
                    $this->bus->dispatch(new ApplicantExceptionMessage($appId, 'equiFax is empty'));
                    $this->logger->emergency('equiFax is empty');
                }

                if (!isset($dataSrc['bankData'])) {
                    $this->bus->dispatch(
                        new ApplicantExceptionMessage(
                            $appId,
                            'bankData is empty, probably xero connection was failed.'
                        )
                    );
                    $this->logger->emergency('bankData is empty, probably xero connection was failed.');
                }

                $record = new KiwiBankPolicyRuleRecord($dataSrc, $this->locators, $this->param);
                $records = $record->policyResult();
                $log = $this->logging($appId, $records);

                $cd = new KiwiBankCumulativePolicyDecision($records);
                $cumulativePolicyDecision = $cd->make();
                $log->setCumulativePolicyDecision($cd->resultToString($cumulativePolicyDecision));

                //score card
                //bank name comes from decsion.yaml file.
                $decision = $this->param->get('decision');
                /** @var ScorecardSeed|null $seed ** */
                $seed = $this->objectManager->getRepository(ScorecardSeed::class)
                    ->findOneBy(['name' => $decision['bank']]);

                if ($seed) {
                    $card = new KiwiBankScorecardRecord($seed->getValue(), $dataSrc, $this->scoreLocatorChain);
                    $cardResult = $card->result();
                    $log->setScorecard($cardResult);

                    $cardDecision = new KiwibankCumulativeScorecardDecision($cardResult);
                    $cumulativeScorecard = $cardDecision->make();
                    $result = $cardDecision->resultToString($cumulativeScorecard);
                    $log->setCumulativeScorecard($result);
                    //final decision
                    $fd = new KiwibankFinalDecision($cumulativePolicyDecision, $cumulativeScorecard);
                    $log->setFinalDecision($fd->resultToString($fd->make()));
                    $app->setDecision(false);
                    $this->objectManager->persist($log);

                    try {
                        $this->payload($app, $records, $cardResult, $fd->resultToString($fd->make()));
                        $this->sendEmail($log, $app);
                    } catch (\Exception $e) {
                        $this->bus->dispatch(new ApplicantExceptionMessage($appId, $e->getTraceAsString()));
                        $this->logger->critical('Decision engine email/payload exception ' . $e->getMessage());
                    }
                } else {
                    $this->logger->critical('Decision engine seed data missing');
                    $this->bus->dispatch(new ApplicantExceptionMessage($appId, 'Decision engine seed data missing'));
                    exit(1);
                }
            }
        } catch (\Exception $e) {
            $this->bus->dispatch(new RanqxExceptionMessage($e->getMessage()));
            $this->logger->emergency($e->getMessage());
        } finally {
            $this->objectManager->flush();
            sleep(10);//for supervisor queue,if quit too fast, supervisor will enter fatal status
        }
    }

    /**
     * @param int $appId
     * @param array $records
     * @return DecisionPolicyResult
     */
    private function logging(int $appId, array $records): DecisionPolicyResult
    {
        /** @var DecisionPolicyResult|null $result ***/
        $result = $this->objectManager->getRepository(DecisionPolicyResult::class)
            ->findOneBy(['applicantId' => $appId]);
        if (!$result) {
            $result = new DecisionPolicyResult();
        }

        $result->setApplicantId($appId);
        $result->setResult($records);
        return $result;
    }


    /**
     * @param Applicant $app
     * @param array $records
     * @param array $cardResult
     * @param string $finalDecision
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws DBALException
     */
    private function payload(Applicant $app, array $records, array $cardResult, string $finalDecision):void
    {
        $equi = new EquiFaxDataQuery($app->getEquiFaxRaw());
        $details = new PayLoadApplicationDetails($this->objectManager, $app, $equi);
        $payLoadPolicyRuleResult = new PayLoadPolicyRuleResult($records);
        $activeFields = new PayLoadActiveField($this->objectManager, $app, $equi, $this->logger);
        $html = $this->generatePayLoadHtml($details, $payLoadPolicyRuleResult, $activeFields, $finalDecision);

        $file = new PayLoadFileCreation(
            $this->objectManager,
            $this->client,
            $this->logger
        );
        $dataDecision['scoreCard'] = $cardResult;
        $dataDecision['payLoadDecisionData'] = $records;

        $file->createPayLoadFiles($dataDecision, $html, $finalDecision, $app);
        if ($app->getPdf() !== null && is_resource($app->getPdf())) {
            rewind($app->getPdf());
        }
        $file->createPayLoadFiles($dataDecision, $html, $finalDecision, $app, 'credit');
    }


    /**
     * @param PayLoadApplicationDetails $applicationDetails
     * @param PayLoadPolicyRuleResult $rule
     * @param PayLoadActiveField $fields
     * @param string $finalDecision
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function generatePayLoadHtml(
        PayLoadApplicationDetails $applicationDetails,
        PayLoadPolicyRuleResult $rule,
        PayLoadActiveField $fields,
        string $finalDecision
    ): string {
        $params = [
            'applicationDetails' => $applicationDetails->toArray(),
            'rules' => $rule->toArray(),
            'fields' => $fields,
            'finalDecision' => $finalDecision
        ];
        return $this->twig->render('\payload\forPayLoad_v2.html.twig', $params);
    }

    /**
     * @param DecisionPolicyResult $result
     * @param Applicant $app
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    protected function sendEmail(DecisionPolicyResult $result, Applicant $app): void
    {
        $projectDir = $this->param->get('project_dir');

        $envEmails = $this->getEnvEmails();

        $mail = new PHPMailer();

        $mail->CharSet = 'UTF-8';
        $mail->setFrom($envEmails['from']);
        $mail->Subject = 'An update on your Kiwibank Business Overdraft Application';

        $mail->addEmbeddedImage($projectDir . '/assets/images/kbEmailHeaderOpt1.jpg', 'kbHeader', 'kbEmailHeaderOpt1.jpg');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbLogo.png', 'kbLogo', 'kbLogo.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbShield.png', 'kbShield', 'kbShield.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbComputer.png', 'kbComputer', 'kbComputer.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbTwitter.png', 'kbTwitter', 'kbTwitter.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbPoint.png', 'kbPoint', 'kbPoint.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbFB.png', 'kbFB', 'kbFB.png');
        // Render HTMl content
        $htmlBody = $this->twig->render(
            '\kb\forApplicantDecision.html.twig',
            [
                'referenceNumber' => $app->getReferenceNumber(),
                'emailAddress' => $app->getEmail(),
                'firstName' => $app->getFirstName(),
                'companyName' => $app->getBusinessName(),
                'decision' => $result->getFinalDecision(),
            ]
        );

        $html2TextConverter = new Html2Text($htmlBody);

        $mail->Body = $htmlBody;
        $mail->AltBody = $html2TextConverter->getText();

        $mail->IsHTML(true);

        foreach ($this->bccToArray() as $bcc) {
            $mail->addBCC($bcc);
        }
        
        $recipientEmails = [$app->getEmail()];
        if ($this->isOverrideExist()) {
            $recipientEmails = [$envEmails['override']];
        }

        foreach ($recipientEmails as $recipientEmail) {
            $mail->addAddress($recipientEmail);
        }
        $mail->addReplyTo($envEmails['from']);

        $mail->preSend();
        $message = $mail->getSentMIMEMessage();
        $this->mailer->sendRawEmail([
            'RawMessage' => [
                'Data' => $message
            ]
        ]);
    }
}