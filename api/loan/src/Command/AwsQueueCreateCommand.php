<?php declare(strict_types = 1);


namespace App\Command;

use Aws\Sqs\SqsClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Console\Input\InputArgument;
use Aws\Exception\AwsException;

class AwsQueueCreateCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:ranqx:queue:sqs:create';

    protected $sqs;

    public function __construct(SqsClient $sqs)
    {
        $this->sqs = $sqs;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Creates a new sqs queue, if the queue not exist, create it.')
            ->addArgument('queuename', InputArgument::REQUIRED, 'Sqs queue name')
            ->setHelp('This command allows you to create a sqs queue');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('queuename');

        try {
            $this->sqs->getQueueUrl([
                'QueueName' => $name
            ]);
            //queue exits, do nothing
        } catch (AwsException $e) {
            //queue is not exist
            //TODO: adding message to DataDog
            try {
                $this->sqs->createQueue([
                    'Attributes' => ['FifoQueue' => 'true'],
                    'QueueName' => $name
                ]);
                //TODO: adding message to DataDog
            } catch (AwsException $e) {
                //TODO: adding message to DataDog
                $output->writeln($e->getMessage());
            }
        }
    }
}