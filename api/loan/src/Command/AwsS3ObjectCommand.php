<?php declare(strict_types = 1);

namespace App\Command;


use Symfony\Component\Console\Command\Command;
use App\Service\S3RanqxClient;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use \Psr\Log\LoggerInterface;
use App\TypeTrait\ConsoleInputToString;

/**
 * Class AwsS3ObjectCommand
 * @package App\Command
 */
class AwsS3ObjectCommand extends Command
{
    use ConsoleInputToString;
    /**
     * @var string
     */
    protected static $defaultName = 'app:ranqx:s3:upload';

    /**
     * @var S3RanqxClient
     */
    private $client;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AwsS3ObjectCommand constructor.
     * @param S3RanqxClient $client
     * @param LoggerInterface $logger
     */
    public function __construct(S3RanqxClient $client,LoggerInterface $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
        parent::__construct();
    }

    /**
     *
     */
    protected function configure(): void
    {
        $this
            ->setDescription('upload a file to s3 bucket')
            ->addArgument('file', InputArgument::REQUIRED, 'file name, use absolute file path.')
            ->setHelp('Upload a file to s3 bucket');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $this->toString($input, 'file', '');
        if (!file_exists($name)) {
            $this->logger->critical($name . ' does not exists.');
            exit(1);
        }

        $this->client->upload($name);
    }

}