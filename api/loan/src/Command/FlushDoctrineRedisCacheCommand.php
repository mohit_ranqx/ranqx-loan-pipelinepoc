<?php declare(strict_types=1);


namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class FlushDoctrineRedisCacheCommand
 * @package App\Command
 */
class FlushDoctrineRedisCacheCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:ranqx:doctrine:redis:flushdb';

    /**
     * @var
     */
    protected $pool;

    /**
     * @var ParameterBagInterface
     */
    protected $bag;

    /**
     * FlushDoctrineRedisCacheCommand constructor.
     * @param ParameterBagInterface $bag
     */
    public function __construct(ParameterBagInterface $bag)
    {
        $this->bag = $bag;
        parent::__construct();
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Flush doctrine redis cache')
            ->setHelp('Flush doctrine redis cache');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $redis = new \Redis();
        $redis->connect(
            $this->bag->get('health_redis_host'),
            (int)$this->bag->get('health_redis_port'));
        $redis->flushDB();
    }
}