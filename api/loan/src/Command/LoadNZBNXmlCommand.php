<?php declare(strict_types = 1);


namespace App\Command;

use DateTime;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use App\XML\NzbnXmlParser;
use App\TypeTrait\ConsoleInputToInt;
use App\TypeTrait\ConsoleInputToString;

/**
 * Class LoadNZBNXmlCommand
 * @package App\Command
 */
class LoadNZBNXmlCommand extends Command
{
    use ConsoleInputToInt;
    use ConsoleInputToString;
    /**
     * @var string
     */
    protected static $defaultName = 'app:ranqx:load:nzbn:xml';

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var int
     */
    protected $chunk;

    /**
     * @var int
     */
    protected $sqlchunk;

    /**
     * LoadNZBNJsonCommand constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    /**
     *
     */
    protected function configure(): void
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Load NZBN xml file into db table.')
            ->addArgument('file', InputArgument::REQUIRED, 'xml file')
            ->addArgument('chunk', InputArgument::OPTIONAL, 'How many bytes to read at one block.')
            ->addArgument('sqlchunk', InputArgument::OPTIONAL, 'How many insertion rows send to DB.')
            ->setHelp('app:ranqx:load:nzbn:xml <nzbn xml file here> [chunk] [sqlchunk]')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $this->toString($input, 'file', '');
        $this->chunk = $this->toInt($input, 'chunk', 1024*1024);
        $this->sqlchunk = $this->toInt($input, 'sqlchunk', 8192);

        $progressBar = new ProgressBar($output);
        $progressBar->start();

        $listener = function ($companies) use ($progressBar) {
            $this->sql($companies);
            $progressBar->advance();
        };
        //How many rows one can insert with one statement depends on the max_allowed_packet
        $xml = new NzbnXmlParser($name, $listener, $this->sqlchunk, $this->chunk);
        $xml->parse();
        $progressBar->finish();
    }

    /**
     * @param array $companyInfor
     * @throws DBALException
     */
    protected function sql(array $companyInfor): void
    {
        $header = 'INSERT INTO `nzbncompany` (`name`,`nzbn`,`company_number`,
                               `source_register`, `created`,`status`,`country_of_origin`,`registration_date`) VALUES ';
        $query = '';
        $conn = $this->entityManager->getConnection();
        foreach ($companyInfor as $data) {
            $sql = '(';

            $sql .=  $conn->quote($data['name']) . ',';
            $sql .= $conn->quote($data['nzbn']) . ',';

            $sql .= $conn->quote($data['company_number']) . ',';
            $sql .= $conn->quote($data['source_register']) . ', ';
            $sql .= $conn->quote((new DateTime())->format('Y-m-d H:i:s')) . ',';
            $sql .= $data['status'] . ',';
            $sql .= $conn->quote($data['country_of_origin']) .', ';
            $sql .= $conn->quote($data['registration_date']);
            $sql .= '),';

            $query .= $sql;
        }

        $query = rtrim($query, ',');
        $query = $header . $query ;
        $query .= ' ON DUPLICATE KEY UPDATE name=values(name), nzbn=values(nzbn),';
        $query .= ' company_number=values(company_number), ';
        $query .= ' source_register=values(source_register), ';
        $query .= ' created=values(created), ';
        $query .= ' status=values(status), ';
        $query .= ' country_of_origin=values(country_of_origin), ';
        $query .= ' registration_date=values(registration_date)';

        $conn = $this->entityManager->getConnection();
        $conn->exec($query);
    }
}
