<?php declare(strict_types = 1);


namespace App\Command;

use App\DTO\CreateNZBNCompanyFromJson;
use App\DTO\CreateDirectorFromJson;
use App\Service\NZBNCompanyService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use App\TypeTrait\ConsoleInputToInt;
use App\TypeTrait\ConsoleInputToString;

/**
 * Class LoadNZBNJsonCommand
 * @package App\Command
 *
 * Usage: php bin/console app:ranqx:load:nzbn:json "f:\my_auto_vm\php7\20181112_3.json"
 */
class LoadNZBNJsonCommand extends Command
{
    use ConsoleInputToInt;
    use ConsoleInputToString;
    /**
     * @var string
     */
    protected static $defaultName = 'app:ranqx:load:nzbn:json';

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * LoadNZBNJsonCommand constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setDescription('Load NZBN json file into db table.')
            ->addArgument('file', InputArgument::REQUIRED, '')
            ->setHelp('app:ranqx:load:nzbn:json <nzbn json file here>')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $this->toString($input, 'file', '');
        $fp = fopen($name, 'rb');
        if (!$fp) {
            die('Could not open '.$fp);
        }

        $progressBar = new ProgressBar($output);
        $progressBar->start();
        $c = 0;
        $sql = '';
        $conn = $this->entityManager->getConnection();
        while (($line = fgets($fp)) !== false) {
            if (preg_match("%nzbn%is", $line)) {
                //we hit company record
                $data = json_decode(rtrim(trim($line), ','), true);
                if (!$data) {
                    //NZBN json file last line containes 3 extra line
                    $lineagain = substr(rtrim(trim($line), ','), 0, -3);
                    $data = json_decode($lineagain, true);
                }

                if ($data) {
                    $jdata = json_encode($data);
                    if ($jdata === false) {
                        $jdata = '';
                    }
                    $obj = new CreateNZBNCompanyFromJson($jdata);
                    $nzbn = $obj->toNZBNCompany();
                    $service = new NZBNCompanyService($nzbn);
                    if ($service->excluded()) {
                        continue;
                    }

                    $directorActor = new CreateDirectorFromJson($jdata);
                    $directors = $directorActor->toArray();

                    $sql .= '(';

                    $sql .=  $conn->quote($nzbn->getName()) . ',';
                    $sql .= $conn->quote($nzbn->getNzbn()) . ',';

                    $sql .= $conn->quote($nzbn->getCompanyNumber()) . ',';
                    $sql .= $conn->quote($nzbn->getSourceRegister()) . ', ';
                    $sql .= $conn->quote((new DateTime())->format('Y-m-d H:i:s')) . ',';
                    $sql .= $nzbn->getStatus() . ',';
                    $sql .= $conn->quote($nzbn->getCountryOfOrigin()) .', ';
                    $sql .= $conn->quote($nzbn->getRegistrationDate()->format('Y-m-d')) . ', ';
                    $sql .= $conn->quote($nzbn->getClassificationCode()) . ', ';
                    $sql .= $conn->quote($nzbn->getClassificationDescription()) . ', ';
                    $sql .= $conn->quote(json_encode($directors));
                    $sql .= '),';

                    ++$c;
                    if ($c === 2048) {
                        $this->sql($sql);
                        $sql = '';
                        $c = 0;
                        $progressBar->advance();
                    }
                } else {
                    $output->writeln('data is not an valid json');
                    $output->writeln($line);
                }
            }
        }

        if ($sql !== '') {
            $this->sql($sql);
        }

        $this->entityManager->flush();
        $progressBar->advance();
        $progressBar->finish();
        fclose($fp);
    }

    protected function sql($sql)
    {
        $header = 'INSERT INTO `nzbncompany` (`name`,`nzbn`,`company_number`,
                               `source_register`, `created`,`status`,`country_of_origin`,`registration_date`,
                               `classification_code`, `classification_description`
                               ,`json_directors`) VALUES ';

        $sql = rtrim($sql, ',');
        $query = $header . $sql ;
        $query .= ' ON DUPLICATE KEY UPDATE name=values(name), nzbn=values(nzbn),';
        $query .= ' company_number=values(company_number), ';
        $query .= ' source_register=values(source_register), ';
        $query .= ' created=values(created), ';
        $query .= ' status=values(status), ';
        $query .= ' country_of_origin=values(country_of_origin), ';
        $query .= ' registration_date=values(registration_date), ';
        $query .= ' classification_code=values(classification_code), ';
        $query .= ' classification_description=values(classification_description), ';
        $query .= ' json_directors=values(json_directors)';

        $conn = $this->entityManager->getConnection();
        $conn->exec($query);
    }
}
