<?php declare(strict_types = 1);


namespace App\XML;

use function strtoupper;

/**
 * Class NzbnXmlParser
 * @package App\XML
 */
class NzbnXmlParser
{
    /**
     * @var string
     */
    private $xmlFileName;
    /**
     * @var int
     */
    private $maxRowsToReturn;
    /**
     * @var callable
     */
    private $listener;
    /**
     * @var int
     */
    private $buff;

    /**
     * NzbnXmlParser constructor.
     * @param string $xmlFileName
     * @param callable $listener
     * @param int $maxRowsToReturn
     * @param int $readBuff
     */
    public function __construct(string $xmlFileName, callable $listener, $maxRowsToReturn = 8192, $readBuff=1048576)
    {
        //xml parse end tag function can not handle Generator, so use call back instead.
        $this->xmlFileName = $xmlFileName;
        $this->maxRowsToReturn = $maxRowsToReturn;
        $this->listener = $listener;
        $this->buff = $readBuff;
    }

    /**
     *
     */
    public function parse()
    {
        $fp = fopen($this->xmlFileName, 'rb');
        if (!$fp) {
            die('Unable to open xml file. ' . $this->xmlFileName);
        }

        $state = "entity_start";
        $companyInfo = [];
        $counter = 1;

        $sax = xml_parser_create();
        xml_parser_set_option($sax, XML_OPTION_CASE_FOLDING, false);
        xml_parser_set_option($sax, XML_OPTION_SKIP_WHITE, true);

        xml_set_element_handler($sax, function ($sax, $tag, $attr) use (&$state, &$companyInfo,&$counter) {
            if ($tag === 'N8:Entity') {
                $state = "entity_start";
                $companyInfo[$counter]['nzbn'] = $attr['PartyID'];
            }

            if ($tag === 'N2:OrganisationName' && $state === 'entity_start') {
                $state = 'entity_orgname';
            }

            if ($tag === 'N2:NameElement' && $state === 'entity_orgname') {
                $state = "entity_name";
            }

            if ($tag === 'N5:Identifiers') {
                $state = 'entity_identifiers';
            }

            if ($tag === 'N5:Identifier' && $state === 'entity_identifiers') {
                if ($attr['N5:Type'] === "CompanyID") {
                    $state = 'entity_identifier';
                }
            }

            if ($tag === 'N5:IdentifierElement' && $state === 'entity_identifier') {
                $state = 'entity_id';
            }

            if ($tag === 'N5:OrganisationInfo') {
                $companyInfo[$counter]['source_register'] = $attr['N5:Type'];
                $companyInfo[$counter]['status'] = 0;
                if ($attr['N5:Status'] ==='Registered') {
                    $companyInfo[$counter]['status'] = 1;
                }
                $companyInfo[$counter]['country_of_origin'] = 'New Zealand';
                if (isset($attr['N8:CountryOfOrigin'])) {
                    $companyInfo[$counter]['country_of_origin'] = trim($attr['N8:CountryOfOrigin']);
                } elseif (strtoupper($attr['N5:Type']) === 'ASIC') {
                    $companyInfo[$counter]['country_of_origin'] = 'Australia';
                }
            }

            if ($tag === 'N5:Events') {
                $state = 'entity_events';
            }

            if ($tag === 'N5:Event' && $state === 'entity_events') {
                if ($attr['N5:Type'] === 'Date of registration') {
                    $state = 'entity_reg_date';
                }
            }

        }, function ($sax, $tag) use (&$state, &$companyInfo,&$counter) {
            if ($tag === 'N8:Entity') {
                $state = "entity_end";
                if ($counter === $this->maxRowsToReturn) {
                    $this->fire($companyInfo);
                    $counter = 1;
                    $companyInfo = [];
                }
                ++$counter;
            }

            if ($tag === 'N2:NameElement' && $state === 'entity_name') {
                $state = 'entity_name_out';
            }

            if ($tag === 'N2:OrganisationName') {
                $state = 'entity_orgname_out';
            }

            if ($tag === 'N5:IdentifierElement' && $state === 'entity_id') {
                $state = 'entity_id_out';
            }

            if ($tag === 'N5:Identifiers') {
                $state = 'entity_identifiers_out';
            }

            if ($tag === 'N5:Identifier' && $state === 'entity_identifier') {
                $state = 'entity_identifier_out';
            }

            if ($tag === 'N5:Events') {
                $state = 'entity_events_out';
            }

            if ($tag === 'N5:Event' && $state === 'entity_reg_date') {
                $state = 'entity_reg_date_out';
            }
        });

        xml_set_character_data_handler($sax, function ($sax, $data) use (&$state, &$companyInfo,&$counter) {
            if ($state === 'entity_name') {

                if (isset($companyInfo[$counter]['name'])) {
                    $companyInfo[$counter]['name'] .= ' ' . trim($data);
                } else {
                    $companyInfo[$counter]['name'] = trim($data);
                }
            }

            if ($state === 'entity_id') {
                $companyInfo[$counter]['company_number'] = trim($data);
            }
            if ($state === 'entity_reg_date') {
                $companyInfo[$counter]['registration_date'] = trim($data);
            }
        });

        while ($data = fread($fp, $this->buff)) {
            if (!xml_parse($sax, $data, feof($fp))) {
                xml_parser_free($sax);
                die(sprintf(
                    "XML error: %s at line %d",
                    xml_error_string(xml_get_error_code($sax)),
                    xml_get_current_line_number($sax)
                ));
            }
        }

        if (!empty($companyInfo)) {
            $this->fire($companyInfo);
        }

        xml_parser_free($sax);

        fclose($fp);
    }

    /**
     * @param array $companies
     */
    public function fire(array $companies)
    {
        call_user_func($this->listener, $companies);
    }
}
