<?php declare(strict_types = 1);


namespace App\OAuth\Consumer;

use App\OAuth\OAuthFixedNonceTime;
use App\OAuth\XeroOAuthSignRSASHA1;
use App\OAuth\XeroOAuth1Config;
use App\OAuth\XeroAccessTokenEncode;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\OAuth\XeroAccessToken;

/**
 * Class OAuth1ThirdLeg
 * @package App\OAuth\Consumer
 */
class OAuth1ThirdLeg
{
    /**
     * @var ParameterBagInterface
     */
    private $parames;
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * OAuth1ThirdLeg constructor.
     * @param ParameterBagInterface $params
     * @param UrlGeneratorInterface $router
     */
    public function __construct(ParameterBagInterface $params, UrlGeneratorInterface $router)
    {
        $this->parames = $params;
        $this->router = $router;
    }

    /**
     * @param string $oauth_token
     * @param string $oauth_verifier
     * @return XeroAccessToken
     * @throws GuzzleException
     */
    public function accessToken(string $oauth_token, string $oauth_verifier, $appId): XeroAccessToken
    {
        $para = new XeroOAuth1Config($this->parames, $this->router, new OAuthFixedNonceTime());
        $para->addExtra('oauth_token', $oauth_token);
        $para->addExtra('oauth_verifier', $oauth_verifier);
        $para->addExtra('appId' , $appId);
        $sign = new XeroOAuthSignRSASHA1(
            $para,
            new XeroAccessTokenEncode($para)
        );

        $query = $sign->sign();

        $client = new Client();
        $res = $client->request('GET', $para->accessTokenUri().'?'.$query);
        return new XeroAccessToken($res->getBody()->getContents());
    }
}
