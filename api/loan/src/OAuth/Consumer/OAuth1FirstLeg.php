<?php declare(strict_types = 1);


namespace App\OAuth\Consumer;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use GuzzleHttp\Client;
use App\OAuth\XeroOAuthSignRSASHA1;
use App\OAuth\XeroRequestToken;
use App\OAuth\OAuthFixedNonceTime;
use App\OAuth\XeroOAuth1Config;
use App\OAuth\XeroRequestTokenEncode;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class OAuth1FirstLeg
 *
 * @package App\OAuth\Consumer
 */
class OAuth1FirstLeg
{
    /**
     * @var ParameterBagInterface
     */
    private $parames;
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    private $appId;

    /**
     * OAuth1FirstLeg constructor.
     * @param ParameterBagInterface $params
     * @param UrlGeneratorInterface $router
     * @param $appId
     */
    public function __construct(ParameterBagInterface $params, UrlGeneratorInterface $router, $appId)
    {
        $this->parames = $params;
        $this->router = $router;
        $this->appId = $appId;
    }


    /**
     * @return XeroRequestToken
     * @throws GuzzleException
     */
    public function requestToken(): XeroRequestToken
    {
        $para = new XeroOAuth1Config($this->parames, $this->router, new OAuthFixedNonceTime());
        $para->addExtra('appId' , $this->appId);
        $sign = new XeroOAuthSignRSASHA1(
            $para,
            new XeroRequestTokenEncode($para)
        );

        $query = $sign->sign();
        $client = new Client();

        $res = $client->request('GET', $this->parames->get('xero.request_token_url').'?'.$query);

        return new XeroRequestToken($res->getBody()->getContents());
    }
}
