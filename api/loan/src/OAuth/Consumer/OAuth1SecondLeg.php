<?php declare(strict_types = 1);


namespace App\OAuth\Consumer;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\OAuth\XeroRequestToken;

/**
 * Class OAuth1SecondLeg
 * @package App\OAuth\Consumer
 */
class OAuth1SecondLeg
{
    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * OAuth1SecondLeg constructor.
     * @param ParameterBagInterface $params
     */
    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @param XeroRequestToken $token
     * @return string
     */
    public function authorization(XeroRequestToken $token)
    {
        //generate url
        return $this->params->get('xero.authorization_url').'?oauth_token='.$token->token().'&redirectOnError=true';
    }
}
