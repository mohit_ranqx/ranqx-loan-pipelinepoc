<?php declare(strict_types=1);


namespace App\OAuth\Consumer;

use App\Entity\Applicant;
use App\Entity\FinanceInvoice;
use App\Entity\FinanceMonth;
use App\Entity\Organisation;
use App\Message\ApplicantExceptionMessage;
use App\OAuth\XeroAccessToken;
use App\OAuth\XeroRequestToken;
use App\Xero\OrganisationData;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Xero\XeroImport;
use App\Map\XeroRanqxMap;
use App\Map\RanqxMapAccount;
use App\Map\RanqxMapBalanceSheet;
use App\Xero\MergeData;
use App\Map\BaseMap;
use Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class Xero
 * @package App\OAuth\Consumer
 */
class Xero
{
    /**
     * @var ParameterBagInterface
     */
    private $parames;
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * Xero constructor.
     * @param ParameterBagInterface $params
     * @param UrlGeneratorInterface $router
     */
    public function __construct(ParameterBagInterface $params, UrlGeneratorInterface $router)
    {
        $this->parames = $params;
        $this->router = $router;
    }
    //first leg

    /**
     * @param $appId
     * @return XeroRequestToken
     * @throws GuzzleException
     */
    public function requestToken($appId): XeroRequestToken
    {
        $requestToken = new OAuth1FirstLeg($this->parames, $this->router, $appId);
        return $requestToken->requestToken();
    }

    //second leg

    /**
     * @param XeroRequestToken $firstLegToken
     * @return string
     */
    public function authorization(XeroRequestToken $firstLegToken): string
    {
        $authorize = new OAuth1SecondLeg($this->parames);
        return $authorize->authorization($firstLegToken);
    }

    //third leg

    /**
     * @param string $oauth_token
     * @param string $oauth_verifier
     * @param $appId
     * @return XeroAccessToken
     * @throws GuzzleException
     */
    public function accessToken(string $oauth_token, string $oauth_verifier, $appId): XeroAccessToken
    {
        $access = new OAuth1ThirdLeg($this->parames, $this->router);
        return $access->accessToken($oauth_token, $oauth_verifier, $appId);
    }

    public function xeroOrg(XeroAccessToken $token) : Organisation
    {
        $import = new XeroImport($this->parames);
        $orgData = $import->organisation($token);
        $map = new OrganisationData($orgData);
        return $map->toEntity();
    }

    public function xeroInvoice(XeroAccessToken $token, Organisation $organisation): FinanceInvoice
    {
        $import = new XeroImport($this->parames);
        $from = new \DateTime(date('Y-m-01 00:00:00', strtotime('-3 months')));
        $to = new \DateTime(date('Y-m-t 23:59:59', strtotime('-2 months')));

        $res = $import->loadInvoice($from, $to, $token);
        return $this->saveInvoice($res, $from, $to, $organisation);
    }



    public function import(XeroAccessToken $token, EntityManagerInterface $orm, Organisation $organisation): Organisation
    {
        $import = new XeroImport($this->parames);
        $acc = $import->accounts($token);
        $accResult = (new RanqxMapAccount($acc))->toArray();

        $to = null;
        $from = null;


        for ($i = 0; $i < 2; $i++) {
            //last of last month
            $to = (new DateTime())->modify('-' . $i . ' year')
                ->modify('last day of last month')->setTime(23, 59, 59)
                ->modify('last day of last month')->setTime(23, 59, 59);

            $from = (new DateTime($to->format('Y-m-d')))->modify('first day of this month');

            $balance = $import->loadBalanceSheets($from, $to, $token);
            $profit = $import->loadProfitAndLoss($from, $to, $token);

            $balanceData = new RanqxMapBalanceSheet($balance);
            $months = $balanceData->months();

            $data = new MergeData(
                $accResult,
                $balanceData,
                new RanqxMapBalanceSheet($profit)
            );
            //get monthe index;
            foreach ($months as $index => $month) {
                $period = new \DateTime($month['Value']);
                $result = $data->merge((int)$index);

                $finalResult = XeroRanqxMap::getFinancialDetails($result, $accResult);
                $financeMonth = new FinanceMonth();
                $financeMonth->setOrganisation($organisation);
                $financeMonth
                    ->setStaffNumber(1)
                    ->setPeriod($period)
                    ->setCurrency($organisation->getBaseCurrency());

                $financeMonth = BaseMap::mapFinance(
                    $financeMonth,
                    $finalResult
                );

                $orm->persist($financeMonth);
            }
        }

        return $organisation;
    }

    private function saveInvoice(ResponseInterface $res, DateTime $from, DateTime $to, Organisation $org)
    {
        $xml = json_decode(json_encode((array) simplexml_load_string($res->getBody()->getContents())), true);
        $currency = 'NZD';
        $debtorCon = [];
        $financeSubTotalInvoices = 0.0;
        $count = 0;
        $dataInvoice = [];
        $debtorConcentration = 0.0;
        $goodData = 'Yes';
        if (array_key_exists('Invoices', $xml)) {
            $xmlData = $xml['Invoices']['Invoice'] ?? $xml['Invoices'];

            foreach ($xmlData as $key => $invoice) {
                $invoiceStatus=$invoice['Status'] ?? $xmlData['Status'] ?? '';
                $invoiceSubTotal = $invoice['SubTotal'] ?? $xmlData['SubTotal'] ?? '';
                $invoiceCurrenceyRate = $invoice['CurrencyRate'] ?? $xmlData['CurrencyRate'] ?? '';

                if ($invoiceStatus === 'AUTHORISED' || $invoiceStatus === 'SUBMITTED' || $invoiceStatus === 'PAID') {
                    $contactID = $invoice['Contact']['ContactID'] ?? $xmlData['Contact']['ContactID'];
                    $subTotal = $invoiceSubTotal;
                    $debtorCon[] = array('contactID' => $contactID, 'subTotal' => $subTotal);
                    $financeSubTotalInvoices += $invoiceSubTotal;
                    $count++;
                    $currencyRate = $invoiceCurrenceyRate ?? '1.0000000';


                    $dataInvoice[] = array(
                        "InvoiceData" =>
                            array(
                                "ContactID" => $invoice['Contact']['ContactID'] ?? $xmlData['Contact']['ContactID'],
                                "Name" => $invoice['Contact']['Name'] ?? $xmlData['Contact']['Name'],
                                "Date" => $invoice['Date'] ?? $xmlData['Date'] ?? '',
                                "DueDate" => $invoice['DueDate'] ?? $xmlData['DueDate'] ?? '',
                                "Status" => $invoice['Status'] ?? $xmlData['Status'] ?? '',
                                "LineAmountTypes" => $invoice['LineAmountTypes'] ?? $xmlData['LineAmountTypes'] ?? '',
                                "SubTotal" => $invoice['SubTotal'] ?? $xmlData['SubTotal'] ?? '',
                                "TotalTax" => $invoice['TotalTax'] ?? $xmlData['TotalTax'] ?? '',
                                "Total" => $invoice['Total'] ?? $xmlData['Total'] ?? '',
                                "UpdatedDateUTC" => $invoice['UpdatedDateUTC'] ?? $xmlData['UpdatedDateUTC'] ?? '',
                                "CurrencyCode" => $invoice['CurrencyCode'] ?? $xmlData['CurrencyCode'] ?? '',
                                "Type" => $invoice['Type'] ?? $xmlData['Type'] ?? '',
                                "InvoiceID" => $invoice['InvoiceID'] ?? $xmlData['InvoiceID'] ?? '',
                                "InvoiceNumber" => $invoice['InvoiceNumber'] ?? $xmlData['InvoiceNumber'] ?? '',
                                "AmountDue" => $invoice['AmountDue'] ?? $xmlData['AmountDue'] ?? '',
                                "AmountPaid" => $invoice['AmountPaid'] ?? $xmlData['AmountPaid'] ?? '',
                                "AmountCredited" => $invoice['AmountCredited'] ?? $xmlData['AmountCredited'] ?? '',
                                "CurrencyRate" => $currencyRate,
                                "HasAttachments" => $invoice['HasAttachments'] ?? $xmlData['HasAttachments'] ?? '',
                                "HasErrors" => $invoice['HasErrors'] ?? $xmlData['HasErrors'] ?? '',
                            )
                    );
                }
            }

            $mhDataInvoice = json_encode($dataInvoice);

            $debtorConcat = array_reduce($debtorCon, function ($a, $b) {
                isset($a[$b['contactID']]) ? $a[$b['contactID']]['subTotal'] += $b['subTotal'] : $a[$b['contactID']] = $b;
                return $a;
            });

            $invoiceMax=0;
            foreach ($debtorConcat as $i => $b) {
                if ($invoiceMax <= $b['subTotal']) { $invoiceMax = $b['subTotal']; }
            }

            if ($financeSubTotalInvoices!=0 && $financeSubTotalInvoices!='' && $financeSubTotalInvoices!='nan') {
                $debtorConcentration = ($invoiceMax / $financeSubTotalInvoices);
            } else {
                $debtorConcentration = 0.0;
            }
        } else {
            $debtorConcentration = NULL;
            $mhDataInvoice = '[]';
            $goodData = 'NO';
        }


        $calI = new FinanceInvoice();
        $calI->setOrganisationId($org->getId());
        $calI->setCurrency($currency);

        if ($goodData=='Yes') { $calI->setDebtorConcentration(number_format((float)$debtorConcentration, 7, '.', '')); }

        $calI->setInvoiceDataReturned($goodData);
        $calI->setPeriodStart($from);
        $calI->setPeriodEnd($to);
        $calI->setRawData($mhDataInvoice);

        return $calI;
    }
}
