<?php declare(strict_types = 1);


namespace App\OAuth\Exception;

use Exception;
use Throwable;

class TokenRejectedException extends Exception
{
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        if (empty($message)) {
            $message = 'There was a problem connecting to your accounting system. [Token Rejected]';
        }

        parent::__construct($message, $code, $previous);
    }
}
