<?php declare(strict_types = 1);


namespace App\OAuth\Exception;

use Exception;
use Throwable;

class IntegrationLimitExceededException extends Exception
{
    public function __construct($message = "", $code = 500, Throwable $previous = null)
    {
        if (empty($message)) {
            $message = 'We have reached an integration import limit, please try again after a few minutes.';
        }

        parent::__construct($message, $code, $previous);
    }
}
