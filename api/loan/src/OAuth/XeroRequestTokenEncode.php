<?php declare(strict_types = 1);


namespace App\OAuth;

use function rawurlencode;

/**
 * Class XeroRequestTokenEncode
 * @package App\OAuth
 */
class XeroRequestTokenEncode implements XeroRequestTokenEncodeInterface
{
    /**
     * @var OAuth1ConfigInterface
     */
    private $parames;

    /**
     * XeroRequestTokenEncode constructor.
     * @param OAuth1ConfigInterface $parames
     */
    public function __construct(OAuth1ConfigInterface $parames)
    {
        $this->parames = $parames;
    }

    /**
     * @return string
     */
    public function encodeFirst(): string
    {
        return rawurlencode('GET') . '&';
    }

    /**
     * @return string
     */
    public function encodeSecond(): string
    {
        return rawurlencode($this->parames->requestTokenUri()) . '&';
    }

    /**
     * @return string
     */
    public function encodeBase(): string
    {
        $result = $this->parames->parameters();//payload for signature
        $temp = [];
        foreach ($result as $key => $value) {
            $temp[] = rawurlencode($key) . '=' . rawurlencode((string)$value);
        }
        return implode('&', $temp);
    }
}
