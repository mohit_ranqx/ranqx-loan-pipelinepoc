<?php declare(strict_types = 1);


namespace App\OAuth;

/**
 * Class XeroRequestToken
 * @package App\OAuth
 */
class XeroRequestToken
{
    /**
     * @var string
     */
    private $requestTokenStr;

    /**
     * XeroRequestToken constructor.
     * @param string $requestTokenStr
     */
    public function __construct(string $requestTokenStr)
    {
        //$requestTokenStr look like below
        //oauth_token=hdk48Djdsa&oauth_token_secret=xyz4992k83j47x0b&oauth_callback_confirmed=true
        $this->requestTokenStr = $requestTokenStr;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        parse_str($this->requestTokenStr, $result);
        return $result;
    }

    /**
     * @return string
     */
    public function token()
    {
        $r = $this->toArray();
        return $r['oauth_token'] ?? '';
    }

    /**
     * @return string
     */
    public function secret()
    {
        $r = $this->toArray();
        return $r['oauth_token_secret'] ?? '';
    }
}
