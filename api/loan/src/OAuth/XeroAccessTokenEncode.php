<?php declare(strict_types = 1);


namespace App\OAuth;

use function rawurlencode;

class XeroAccessTokenEncode implements XeroRequestTokenEncodeInterface
{
    private $parames;

    public function __construct(OAuth1ConfigInterface $parames)
    {
        $this->parames = $parames;
    }

    public function encodeFirst(): string
    {
        return rawurlencode('GET') . '&';
    }

    public function encodeSecond(): string
    {
        return rawurlencode($this->parames->accessTokenUri()) . '&';
    }

    public function encodeBase(): string
    {
        $result = $this->parames->parameters();

        $temp = [];
        foreach ($result as $key => $value) {
            $temp[] = rawurlencode($key) . '=' . rawurlencode((string)$value);
        }
        return implode('&', $temp);
    }
}
