<?php declare(strict_types = 1);

namespace App\OAuth;

/**
 * Interface OAuthSignInterface
 * @package App\OAuth
 */
interface OAuthSignInterface
{
    /**
     * @return string
     */
    public function sign() : string;
}
