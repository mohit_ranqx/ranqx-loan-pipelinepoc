<?php declare(strict_types = 1);

namespace App\OAuth;

/**
 * Interface NonceTime
 * @package App\OAuth
 */
interface NonceTime
{
    /**
     * @return string
     */
    public function getNonce() : string;

    /**
     * @return int
     */
    public function getTime() : int;
}
