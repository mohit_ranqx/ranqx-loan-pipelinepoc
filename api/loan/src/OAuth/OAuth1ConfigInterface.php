<?php declare(strict_types = 1);

namespace App\OAuth;

/**
 * Interface OAuth1ConfigInterface
 * @package App\OAuth
 */
interface OAuth1ConfigInterface
{
    /**
     * @return array
     */
    public function parameters(): array;

    /**
     * @return string
     */
    public function callback(): string;

    /**
     * @return string
     */
    public function getNonce(): string;

    /**
     * @return int
     */
    public function getTime(): int;

    /**
     * @return string
     */
    public function algo() : string;

    /**
     * @return string
     */
    public function consumerKey(): string;

    /**
     * @return string
     */
    public function oauthVersion(): string;

    /**
     * @return string
     */
    public function requestTokenUri(): string;

    /**
     * @return string
     */
    public function authorizationUri(): string;

    /**
     * @return string
     */
    public function accessTokenUri(): string;

    /**
     * @return string
     */
    public function getPublicKey(): string;

    /**
     * @return string
     */
    public function getPrivateKey(): string;

    /**
     * @param string $key
     * @param string $value
     */
    public function addExtra(string $key, string $value): void;

    /**
     * @return string
     */
    public function apiUri(): string;
}
