<?php declare(strict_types = 1);


namespace App\OAuth;

use InvalidArgumentException;
use function rawurlencode;
use App\TypeTrait\XeroKey;

/**
 * Class XeroOAuthSignRSASHA1
 * @package App\OAuth
 */
class XeroOAuthSignRSASHA1 implements OAuthSignInterface
{
    use XeroKey;
    /**
     * @var OAuth1ConfigInterface
     */
    private $parames;
    /**
     * @var XeroRequestTokenEncodeInterface
     */
    private $enc;

    /**
     * XeroOAuthSignRSASHA1 constructor.
     * @param OAuth1ConfigInterface $params
     * @param XeroRequestTokenEncodeInterface $enc
     */
    public function __construct(OAuth1ConfigInterface $params, XeroRequestTokenEncodeInterface $enc)
    {
        $this->parames = $params;
        $this->enc = $enc;
    }

    /**
     * @return string
     */
    public function sign() : string
    {
        $sig = $this->encode();
        $query = $this->parames->parameters();
        $query['oauth_signature'] = $sig;

        ksort($query);

        $temp = [];
        foreach ($query as $key => $value) {
            $temp[] = rawurlencode($key) . '=' . rawurlencode((string)$value);
        }
        return implode('&', $temp);
    }

    /**
     * @return string
     */
    private function encode() : string
    {
        $pf = openssl_pkey_get_public($this->publicKey());
        if (!$pf) {
            throw new InvalidArgumentException('unable to access public key file.');
        }

        $pri = openssl_pkey_get_private($this->privateKey());
        if (!$pri) {
            throw new InvalidArgumentException('unable to access private key file.');
        }
        $sig = $this->enc->encodeFirst(). $this->enc->encodeSecond(). rawurlencode($this->enc->encodeBase());
        openssl_sign($sig, $signature, $pri);
        openssl_free_key($pri);
        return base64_encode($signature);
    }
}