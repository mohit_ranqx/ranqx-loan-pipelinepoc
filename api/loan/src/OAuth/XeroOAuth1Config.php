<?php declare(strict_types = 1);


namespace App\OAuth;

use App\Xero\SignaturePayloadInterface;
use function strtolower;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\TypeTrait\XeroKey;

/**
 * Class XeroOAuth1Config
 * @package App\OAuth
 */
class XeroOAuth1Config implements OAuth1ConfigInterface, SignaturePayloadInterface
{
    use XeroKey;
    /**
     * @var ParameterBagInterface
     */
    private $parames;
    /**
     * @var UrlGeneratorInterface
     */
    private $router;
    /**
     * @var NonceTime
     */
    private $nonceTime;

    /**
     * @var array
     */
    private $extra;

    /**
     * XeroOAuth1Config constructor.
     * @param ParameterBagInterface $params
     * @param UrlGeneratorInterface $router
     * @param NonceTime $nonceTime
     */
    public function __construct(ParameterBagInterface $params, UrlGeneratorInterface $router, NonceTime $nonceTime)
    {
        $this->parames = $params;
        $this->router = $router;
        $this->nonceTime = $nonceTime;
        $this->extra = [];
    }

    /**
     * @return array
     */
    public function payload(): array
    {
        $temp['oauth_callback'] = $this->callback();
        $temp['oauth_consumer_key'] = $this->consumerKey();
        $temp['oauth_nonce'] = $this->getNonce();
        $temp['oauth_signature_method'] = $this->algo();
        $temp['oauth_timestamp'] = $this->getTime();
        $temp['oauth_version'] = $this->oauthVersion();
        ksort($temp);
        return $temp;
    }


    /**
     * @return array
     */
    public function parameters() :array
    {
        $temp = $this->payload();
        $result = array_merge($temp, $this->extra);
        ksort($result);
        return $result;
    }


    /**
     * @param string $key
     * @param string $value
     */
    public function addExtra(string $key, string $value): void
    {
        $this->extra[$key] = $value;
    }

    /**
     * @return string
     */
    public function callback(): string
    {
        return $this->router->generate(
            'app_xero_success',
            [
                'appId' => $this->extra['appId'],
            ],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
    }

    /**
     * @return strings
     */
    public function getNonce(): string
    {
        return $this->nonceTime->getNonce();
    }

    /**
     * @return int
     */
    public function getTime(): int
    {
        return $this->nonceTime->getTime();
    }

    /**
     * @return string
     */
    public function algo() : string
    {
        $type = $this->parames->get('xero.application_type');
        return $this->parames->get('xero.signature.method.' . strtolower($type));
    }

    /**
     * @return string
     */
    public function consumerKey(): string
    {
        return $this->parames->get('xero.consumer_key');
    }

    /**
     * @return string
     */
    public function oauthVersion(): string
    {
        return $this->parames->get('xero.oauth.version');
    }

    /**
     * @return string
     */
    public function requestTokenUri(): string
    {
        return $this->parames->get('xero.request_token_url');
    }

    /**
     * @return string
     */
    public function authorizationUri(): string
    {
        return $this->parames->get('xero.authorization_url');
    }

    /**
     * @return string
     */
    public function accessTokenUri(): string
    {
        return $this->parames->get('xero.access_token_url');
    }

    /**
     * @return string
     */
    public function getPublicKey(): string
    {
        return $this->publicKey();
    }

    /**
     * @return string
     */
    public function getPrivateKey(): string
    {
        return $this->privateKey();
    }

    /**
     * @return string
     */
    public function apiUri(): string
    {
        return $this->parames->get('xero.api_uri');
    }
}
