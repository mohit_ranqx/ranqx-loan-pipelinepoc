<?php declare(strict_types = 1);


namespace App\OAuth;

/**
 * Class XeroAccessToken
 * @package App\OAuth
 *
 * get access token.
 */
class XeroAccessToken
{
    /**
     * @var string
     */
    private $accessTokenStr;

    /**
     * XeroAccessToken constructor.
     * @param string $accessTokenStr
     *
     *  $accessTokenStr looking as below:
     *
     *  oauth_token=VMELVG0K07OWEQKSMSQRFKKWO63SMJ&oauth_token_secret=MJLX9FHES9BFMX6VZLWVLXBIMYFS0U
     *  &oauth_expires_in=1800&oauth_session_handle=M723JS90ORQBY8SDIDU8
     *  &oauth_authorization_expires_in=315360000&xero_org_muid=KWGgdLkBU4wPb3FsGvMiDW
     */
    public function __construct(string $accessTokenStr)
    {
        $this->accessTokenStr = $accessTokenStr;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        parse_str($this->accessTokenStr, $result);
        return $result;
    }

    /**
     * @return string
     */
    public function token(): string
    {
        $r = $this->toArray();
        return $r['oauth_token'] ?? '';
    }

    /**
     * @return string
     */
    public function secret() : string
    {
        $r = $this->toArray();
        return $r['oauth_token_secret'] ?? '';
    }

    public function sessionHandle() : string
    {
        $r = $this->toArray();
        return $r['oauth_session_handle'] ?? '';
    }

    public function __toString()
    {
        return $this->accessTokenStr;
    }
}
