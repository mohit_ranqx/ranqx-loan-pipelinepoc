<?php declare(strict_types=1);


namespace App\OAuth;

/**
 * Class OAuthNonce
 * @package App\OAuth
 */
class OAuthNonce
{
    /**
     * @var string
     */
    private $nonceChars;

    /**
     * OAuthNonce constructor.
     * @param string $nonceChars
     */
    public function __construct($nonceChars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')
    {
        $this->nonceChars = $nonceChars;
    }

    /**
     * @param int $length
     * @return string
     */
    public function nonce($length = 5) : string
    {
        $result = '';
        $cLength = strlen($this->nonceChars);
        for ($i = 0; $i < $length; $i++) {
            $rnum = rand(0, $cLength);
            $result .= substr($this->nonceChars, $rnum, 1);
        }

        return $result;
    }
}
