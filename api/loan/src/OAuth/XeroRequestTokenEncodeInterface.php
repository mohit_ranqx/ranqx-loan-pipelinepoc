<?php declare(strict_types = 1);


namespace App\OAuth;

/**
 * Interface XeroRequestTokenEncodeInterface
 * @package App\OAuth
 */
interface XeroRequestTokenEncodeInterface
{
    /**
     * @return string
     */
    public function encodeFirst(): string;

    /**
     * @return string
     */
    public function encodeSecond(): string;

    /**
     * @return string
     */
    public function encodeBase(): string;
}
