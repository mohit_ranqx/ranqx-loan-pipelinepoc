<?php declare(strict_types = 1);

namespace App\OAuth;

/**
 * Class OAuthFixedNonceTime
 * @package App\OAuth
 *
 */
class OAuthFixedNonceTime implements NonceTime
{
    /**
     * @var OAuthNonce
     */
    private $nonceProvider;

    /**
     * @var string
     */
    private $nonce;

    /**
     * @var int
     */
    private $time;

    /**
     * OAuthFixedNonceTime constructor.
     * @param string $nonceChars
     */
    public function __construct($nonceChars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')
    {
        $this->nonceProvider = new OAuthNonce($nonceChars);
    }

    /**
     * @return array
     * this method always returns same nonce and time, except first time.
     */
    public function run() : array
    {
        return ['nonce' => $this->getNonce(), 'time' => $this->getTime()];
    }

    /**
     * @return string
     */
    public function getNonce(): string
    {
        if ($this->nonce === null) {
            $this->nonce = $this->nonceProvider->nonce();
        }
        return $this->nonce;
    }

    /**
     * @return int
     */
    public function getTime(): int
    {
        if ($this->time === null) {
            $this->time = time();
        }
        return $this->time;
    }
}
