<?php declare(strict_types = 1);


namespace App\Message;


use App\Entity\Organisation;

class FinalEmailMessage
{
    private $orgId;
    private $result;
    public function __construct($orgId, array $decisionResult)
    {
        $this->orgId = $orgId;
        $this->result = $decisionResult;
    }

    public function orgId()
    {
        return $this->orgId;
    }

    public function decision()
    {
        return $this->result;
    }
}