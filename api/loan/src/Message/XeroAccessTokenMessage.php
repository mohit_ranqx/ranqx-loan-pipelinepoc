<?php declare(strict_types = 1);


namespace App\Message;

use App\OAuth\XeroAccessToken;
use function json_encode;

/**
 * Class XeroAccessTokenMessage
 * @package App\Message
 */
class XeroAccessTokenMessage
{
    /**
     * @var XeroAccessToken
     */
    private $token;
    /**
     * @var int
     */
    private $appId;

    /**
     * XeroAccessTokenMessage constructor.
     * @param XeroAccessToken $token
     */
    public function __construct(XeroAccessToken $token, int $appId)
    {
        $this->token = $token;
        $this->appId = $appId;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {

        $data['token'] = $this->token->__toString();
        $data['appId'] = $this->appId;
        $str = json_encode($data);

        if (!$str) {
            return '';
        }
        return $str;
    }

    /**
     * @return int
     */
    public function getAppId() : int
    {
        return $this->appId;
    }
}
