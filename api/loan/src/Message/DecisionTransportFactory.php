<?php declare(strict_types = 1);


namespace App\Message;

use Aws\Sqs\SqsClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Transport\TransportFactoryInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

class DecisionTransportFactory implements TransportFactoryInterface
{
    /**
     * @var SqsClient
     */
    private $sqs;
    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * XerosqsTransportFactory constructor.
     * @param SqsClient $sqs
     * @param ParameterBagInterface $params
     */
    public function __construct(SqsClient $sqs, ParameterBagInterface $params)
    {
        $this->sqs = $sqs;
        $this->params = $params;
    }

    /**
     * @param string $dsn
     * @param array $options
     * @return TransportInterface
     */
    public function createTransport(string $dsn, array $options): TransportInterface
    {
        return new DecisionTransport($this->sqs, $this->params, $options);
    }

    /**
     * @param string $dsn
     * @param array $options
     * @return bool
     */
    public function supports(string $dsn, array $options): bool
    {
        return 0 === strpos($dsn, 'decision:');
    }
}
