<?php declare(strict_types = 1);


namespace App\Message;

use Aws\Sqs\SqsClient;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Transport\TransportFactoryInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

class CalculusTransportFactory implements TransportFactoryInterface
{
    private $sqs;
    private $logger;
    private $params;

    public function __construct(SqsClient $sqs, LoggerInterface $logger,ParameterBagInterface $params)
    {
        $this->sqs = $sqs;
        $this->logger = $logger;
        $this->params = $params;
    }

    public function createTransport(string $dsn, array $options): TransportInterface
    {
        return new CalculusTransport($this->sqs, $this->params, $options);
    }

    public function supports(string $dsn, array $options): bool
    {
        return 0 === strpos($dsn, 'orgid:');
    }
}
