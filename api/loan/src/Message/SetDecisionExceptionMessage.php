<?php declare(strict_types=1);

namespace App\Message;

/**
 * Class SetDecisionExceptionMessage
 * @package App\Message
 */
class SetDecisionExceptionMessage
{
    /**
     * @var int
     */
    public $appId;

    /**
     * SetDecisionExceptionMessage constructor.
     * @param int $appId
     */
    public function __construct(int $appId)
    {
        $this->appId = $appId;
    }

    /**
     * @return int
     */
    public function getAppId() : int
    {
        return $this->appId;
    }
}