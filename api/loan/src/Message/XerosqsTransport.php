<?php declare(strict_types = 1);


namespace App\Message;

use App\SQS\XeroReceiveFIFOParameter;
use App\SQS\XeroSenderFIFOParameter;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\TransportInterface;
use Aws\Sqs\SqsClient;
use App\SQS\XeroFIFOQueueReceive;
use App\SQS\FIFOQueueSender;

/**
 * Class XerosqsTransport
 * @package App\Message
 */
class XerosqsTransport implements TransportInterface
{
    /**
     * @var array
     */
    private $options;
    /**
     * @var SqsClient
     */
    private $sqs;

    /**
     * @var ParameterBagInterface
     */
    private $params;


    /**
     * XerosqsTransport constructor.
     * @param SqsClient $client
     * @param ParameterBagInterface $params
     * @param array $options
     */
    public function __construct(SqsClient $client, ParameterBagInterface $params, array $options = [])
    {
        $this->params = $params;
        $this->options  = $options;
        $this->sqs = $client;
    }

    /**
     * @param callable $handler
     */
    public function receive(callable $handler): void
    {
        $params = new XeroReceiveFIFOParameter($this->params);
        $queue = new XeroFIFOQueueReceive($params, $this->sqs);
        $queue->receive($handler);
    }

    /**
     *
     */
    public function stop(): void
    {
    }

    /**
     * @param Envelope $envelope
     * @return Envelope
     */
    public function send(Envelope $envelope): Envelope
    {
        $params = new XeroSenderFIFOParameter($this->params, $envelope);
        $sender = new FIFOQueueSender($params, $this->sqs);
        return $sender->send($envelope);
    }
}
