<?php declare(strict_types = 1);


namespace App\Message;

class CalculusReceiveMessage
{
    private $org;
    public function __construct(int $orgId)
    {
        $this->org = $orgId;
    }

    /**
     * @return int
     */
    public function getContent(): int
    {
        return $this->org;
    }
}
