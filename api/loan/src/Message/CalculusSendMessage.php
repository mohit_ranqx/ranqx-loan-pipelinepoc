<?php declare(strict_types = 1);


namespace App\Message;

use App\Entity\Organisation;

//TODO: not only send org is, but also contain other information.
//      so, our queue is not depend on DB any more.
//      this way, make testing much more easy. because each testing environment is associated with a
//      sqs queue, and inside the queue, contains a DB table id, the DB table id is associated with a particular
//      physical table. and we probably have many testing environments , each testing environment linked to a different DB,
//      and all the environments linked to one queue , this may leads to environments fetching wrong DB table id.
//      to make our testing environment to fetch correct table id, we have to set up a different queue for a different
//      test environment.

//      so how to get ride off DB dependent?
//      the only way is to serilaize org data , applicant data, and financial data into queue.
class CalculusSendMessage
{
    private $orgId;

    public function __construct($orgId)
    {
        $this->orgId = $orgId;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        $str = json_encode($this->orgId);
        if (!$str) {
            return '';
        }
        return $str;
    }
}
