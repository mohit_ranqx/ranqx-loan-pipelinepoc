<?php declare(strict_types=1);


namespace App\Message;


class ApplicantExceptionMessage
{
    private $appId;
    private $message;
    public function __construct(int $appId, $message)
    {
        $this->message = $message;
        $this->appId = $appId;
    }

    /**
     * @return int
     */
    public function getAppId(): int
    {
        return $this->appId;
    }

    /**
     * @param int $appId
     */
    public function setAppId(int $appId): void
    {
        $this->appId = $appId;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = $message;
    }
}