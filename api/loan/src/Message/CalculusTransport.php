<?php declare(strict_types = 1);


namespace App\Message;

use App\OAuth\XeroAccessToken;
use App\SQS\CalculusFIFOQueueReceive;
use App\SQS\CalculusReveiveFIFOParameter;
use App\SQS\CalculusSenderFIFOParameter;
use App\SQS\XeroFIFOQueueReceive;
use App\SQS\XeroReceiveFIFOParameter;
use Aws\Exception\AwsException;
use Aws\Result;
use Aws\Sqs\SqsClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\TransportInterface;
use App\SQS\FIFOQueueSender;

class CalculusTransport implements TransportInterface
{
    private $params;
    private $client;
    private $options;
    public function __construct(SqsClient $client, ParameterBagInterface $params, array $options = [])
    {
        $this->params = $params;
        $this->options  = $options;
        $this->client = $client;
    }

    /**
     * @param callable $handler
     */
    public function receive(callable $handler): void
    {
        $param = new CalculusReveiveFIFOParameter($this->params);
        $queue = new CalculusFIFOQueueReceive($param, $this->client);
        $queue->receive($handler);
    }

    /**
     *
     */
    public function stop(): void
    {
    }

    /**
     * @param Envelope $envelope
     * @return Envelope
     * @throws AwsException
     */
    public function send(Envelope $envelope): Envelope
    {
        $param = new CalculusSenderFIFOParameter($this->params, $envelope);
        $queue = new FIFOQueueSender($param, $this->client);
        return $queue->send($envelope);
    }
}
