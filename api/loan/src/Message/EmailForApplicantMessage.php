<?php declare(strict_types = 1);


namespace App\Message;


class EmailForApplicantMessage
{
    private $appId;
    public function __construct($appId)
    {
        $this->appId = $appId;
    }

    public function getContent(): int
    {
        return $this->appId;
    }
}