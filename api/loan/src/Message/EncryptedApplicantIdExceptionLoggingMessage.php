<?php declare(strict_types=1);


namespace App\Message;

class EncryptedApplicantIdExceptionLoggingMessage
{
    private $message;
    private $appId;

    public function __construct(string $appId, string $message)
    {
        $this->appId = $appId;
        $this->message = $message;
    }

    public function getConent(): array
    {
        return [
            'appId' => $this->appId,
            'message' => $this->message
        ];
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    public function getMessage():string
    {
        return $this->message;
    }
}