<?php declare(strict_types = 1);


namespace App\Message;

/**
 * Class DecisionRequestMessage
 * @package App\Message
 */
class DecisionRequestMessage
{
    /**
     * @var string
     */
    private $loanType;

    /**
     * @var string
     */
    private $bank;
    /**
     * @var int
     */
    private $applicantId;

    /**
     * @var int
     */
    private $orgId;


    /**
     * DecisionRequestMessage constructor.
     * @param int $orgId
     * @param int $applicantId
     * @param string $loanType
     * @param string $bank
     */
    public function __construct(?int $orgId, int $applicantId, string $loanType, string $bank)
    {
        $this->applicantId = $applicantId;
        $this->bank = $bank;
        $this->loanType = $loanType;
        $this->orgId = $orgId;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return ['loanType' => $this->loanType,
            'applicantId' => $this->applicantId,
            'bank' => $this->bank,
            'orgId' => $this->orgId];
    }
}
