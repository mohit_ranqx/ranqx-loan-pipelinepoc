<?php declare(strict_types = 1);


namespace App\Message;

use App\SQS\FIFOQueueSender;
use App\SQS\DecisionSenderFIFOParameter;
use Aws\Sqs\SqsClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\TransportInterface;

class DecisionTransport implements TransportInterface
{
    /**
     * @var array
     */
    private $options;
    /**
     * @var SqsClient
     */
    private $sqs;

    /**
     * @var ParameterBagInterface
     */
    private $params;


    /**
     * XerosqsTransport constructor.
     * @param SqsClient $client
     * @param ParameterBagInterface $params
     * @param array $options
     */
    public function __construct(SqsClient $client, ParameterBagInterface $params, array $options = [])
    {
        $this->params = $params;
        $this->options  = $options;
        $this->sqs = $client;
    }

    /**
     * @param callable $handler
     */
    public function receive(callable $handler): void
    {
        //we do not receive message here.
        //we receive message at decision engine app.
    }

    /**
     *
     */
    public function stop(): void
    {
    }

    /**
     * @param Envelope $envelope
     * @return Envelope
     */
    public function send(Envelope $envelope): Envelope
    {
        $params = new DecisionSenderFIFOParameter($this->params, $envelope);
        $sender = new FIFOQueueSender($params, $this->sqs);
        return $sender->send($envelope);
    }
}
