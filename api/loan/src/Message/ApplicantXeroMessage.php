<?php declare(strict_types = 1);


namespace App\Message;

use App\OAuth\XeroAccessToken;

/**
 * Class ApplicantXeroMessage
 * @package App\Message
 */
class ApplicantXeroMessage
{
    /**
     * @var XeroAccessToken
     */
    private $token;
    /**
     * @var mixed
     */
    private $appId;

    /**
     * ApplicantXeroMessage constructor.
     * @param XeroAccessToken $token
     * @param mixed $appId
     */
    public function __construct(XeroAccessToken $token, $appId)
    {
        $this->token = $token;
        $this->appId = $appId;
    }

    /**
     * @return XeroAccessToken
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return mixed
     */
    public function getAppId()
    {
        return $this->appId;
    }
}
