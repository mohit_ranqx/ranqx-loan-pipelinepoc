<?php declare(strict_types = 1);


namespace App\Message;


class DecisionMakeMessage
{
    private $orgId;
    public function __construct($organisationId)
    {
        $this->orgId = $organisationId;
    }

    public function getContent(): int
    {
        return $this->orgId;
    }
}