<?php declare(strict_types = 1);


namespace App\SQS;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Envelope;

/**
 * Class DecisionSenderFIFOParameter
 * @package App\SQS
 */
class DecisionSenderFIFOParameter implements FIFOParameterInterface
{
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var Envelope
     */
    private $message;

    /**
     * DecisionSenderFIFOParameter constructor.
     * @param ParameterBagInterface $params
     * @param Envelope $message
     */
    public function __construct(ParameterBagInterface $params, Envelope $message)
    {
        $this->params = $params;
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function parameters(): array
    {
        $body = $this->message->getMessage();
        $ranqx = $this->params->get('ranqx');
        $params = [
            'QueueUrl' => $ranqx['aws']['queue']['decision']['dsn'],
            'MessageBody' => json_encode($body->getContent()),
            'MessageAttributes' => [],
            'MessageGroupId' => $ranqx['aws']['queue']['xero']['groupId'],
            'MessageDeduplicationId' => md5(json_encode($body->getContent()))
        ];
        return $params;
    }
}
