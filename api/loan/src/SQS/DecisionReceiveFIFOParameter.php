<?php declare(strict_types = 1);


namespace App\SQS;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class DecisionReceiveFIFOParameter
 * @package App\SQS
 */
class DecisionReceiveFIFOParameter implements FIFOParameterInterface
{
    /**
     * @var ParameterBagInterface
     */
    private $parameter;

    /**
     * DecisionReceiveFIFOParameter constructor.
     * @param ParameterBagInterface $params
     */
    public function __construct(ParameterBagInterface $params)
    {
        $this->parameter = $params;
    }

    /**
     * @return array
     */
    public function parameters(): array
    {
        $ranqx = $this->parameter->get('ranqx');
        $paras = [
            'QueueUrl' => $ranqx['aws']['queue']['decision']['dsn'],
            'AttributeNames' => ['All'],
            'MessageAttributeNames' => ['All'],
            'MaxNumberOfMessages' => 1,
            'VisibilityTimeout' =>  30,
            'WaitTimeSeconds' =>  20
        ];
        return $paras;
    }
}
