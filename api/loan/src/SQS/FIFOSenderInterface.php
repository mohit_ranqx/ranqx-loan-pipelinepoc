<?php declare(strict_types = 1);


namespace App\SQS;

use Symfony\Component\Messenger\Envelope;

/**
 * Interface FIFOSenderInterface
 * @package App\SQS
 */
interface FIFOSenderInterface
{
    /**
     * @param Envelope $message
     * @return Envelope
     */
    public function send(Envelope $message) : Envelope;
}
