<?php declare(strict_types = 1);


namespace App\SQS;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Envelope;
use App\Message\CalculusSendMessage;

/**
 * Class CalculusSenderFIFOParameter
 * @package App\SQS
 */
class CalculusSenderFIFOParameter implements FIFOParameterInterface
{
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var Envelope
     */
    private $message;

    /**
     * CalculusSenderFIFOParameter constructor.
     * @param ParameterBagInterface $params
     * @param Envelope $message
     */
    public function __construct(ParameterBagInterface $params, Envelope $message)
    {
        $this->params = $params;
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function parameters(): array
    {
        /** @var CalculusSendMessage $token **/
        $token = $this->message->getMessage();
        $ranqx = $this->params->get('ranqx');
        $params = [
            'QueueUrl' => $ranqx['aws']['queue']['calculus']['dsn'],
            'MessageBody' => $token->getContent(),
            'MessageAttribut' => [],
            'MessageGroupId' => $ranqx['aws']['queue']['calculus']['groupId']
        ];
        $params['MessageDeduplicationId'] = md5(\json_encode($params) )."_". uniqid('xero', true);
        return $params;
    }
}
