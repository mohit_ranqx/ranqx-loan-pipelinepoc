<?php declare(strict_types = 1);


namespace App\SQS;

use Aws\Result;
use Aws\Sqs\SqsClient;
use Symfony\Component\Messenger\Envelope;
use App\Message\CalculusReceiveMessage;

class CalculusFIFOQueueReceive implements FIFOReceiverInterface
{

    /**
     * @var FIFOParameterInterface
     */
    private $params;
    /**
     * @var SqsClient
     */
    private $client;

    public function __construct(FIFOParameterInterface $parameter, SqsClient $client)
    {
        $this->params = $parameter;
        $this->client = $client;
    }

    public function receive(callable $handler): void
    {
        $params = $this->params->parameters();
        /** @var Result $result **/
        $result = $this->client->receiveMessage($params);

        $messages = $result->get('Messages') ?? [];
        foreach ($messages as $message) {
            //TODO: use gearman
            //system auto matching argument,
            //look at services.yaml file, we mapped App\MessageHandler\XeroAccessTokenMessageHandler
            //to XeroAccessToken class

            $messageHandler = $message['ReceiptHandle'] ?? [];
            if ($messageHandler) {
                $this->client->deleteMessage([
                    'QueueUrl' => $params['QueueUrl'],
                    'ReceiptHandle' => $messageHandler
                ]);
            }

            // this handler should run after deleteMessage, reasons see above comments.
            $id = (int)trim($message['Body'], '"');
            $handler(new Envelope(new CalculusReceiveMessage($id)));
        }
    }
}
