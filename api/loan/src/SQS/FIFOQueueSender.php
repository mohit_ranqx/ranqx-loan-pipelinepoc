<?php declare(strict_types = 1);


namespace App\SQS;

use Aws\Sqs\SqsClient;
use Symfony\Component\Messenger\Envelope;

/**
 * Class FIFOQueueSender
 * @package App\SQS
 */
class FIFOQueueSender implements FIFOSenderInterface
{
    /**
     * @var FIFOParameterInterface
     */
    private $params;
    /**
     * @var SqsClient
     */
    private $client;

    /**
     * FIFOQueueSender constructor.
     * @param FIFOParameterInterface $parameter
     * @param SqsClient $client
     */
    public function __construct(FIFOParameterInterface $parameter, SqsClient $client)
    {
        $this->params = $parameter;
        $this->client = $client;
    }

    /**
     * @param Envelope $message
     * @return Envelope
     */
    public function send(Envelope $message): Envelope
    {
        $params = $this->params->parameters();
        $result = $this->client->sendMessage($params);
        return new Envelope($result);
    }
}
