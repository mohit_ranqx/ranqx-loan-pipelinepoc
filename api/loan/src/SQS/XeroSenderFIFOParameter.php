<?php declare(strict_types = 1);


namespace App\SQS;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Envelope;

class XeroSenderFIFOParameter implements FIFOParameterInterface
{
    private $params;
    private $message;

    public function __construct(ParameterBagInterface $params, Envelope $message)
    {
        $this->params = $params;
        $this->message = $message;
    }

    public function parameters(): array
    {
        $body = $this->message->getMessage();
        $ranqx = $this->params->get('ranqx');
        $params = [
            'QueueUrl' => $ranqx['aws']['queue']['xero']['dsn'],
            'MessageBody' => (string)$body->getContent(),
            'MessageAttributes' => [],
            'MessageGroupId' => $ranqx['aws']['queue']['xero']['groupId'],
            //'MessageDeduplicationId' => uniqid('xero', true)
        ];
        $params['MessageDeduplicationId'] = md5(\json_encode($params) )."_". uniqid('xero', true);
        return $params;
    }
}
