<?php


namespace App\SQS;


interface FIFOParameterInterface
{
    public function parameters(): array;
}