<?php declare(strict_types = 1);


namespace App\SQS;

use App\OAuth\XeroAccessToken;
use Aws\Result;
use Aws\Sqs\SqsClient;
use Symfony\Component\Messenger\Envelope;
use App\Message\ApplicantXeroMessage;

class XeroFIFOQueueReceive implements FIFOReceiverInterface
{
    /**
     * @var FIFOParameterInterface
     */
    private $params;
    /**
     * @var SqsClient
     */
    private $client;

    public function __construct(FIFOParameterInterface $parameter, SqsClient $client)
    {
        $this->params = $parameter;
        $this->client = $client;
    }

    public function receive(callable $handler): void
    {
        $params = $this->params->parameters();
        /** @var Result $result **/
        $result = $this->client->receiveMessage($params);

        $messages = $result->get('Messages') ?? [];
        foreach ($messages as $message) {
            //TODO: use gearman
            //system auto matching argument,
            //look at services.yaml file, we mapped App\MessageHandler\XeroAccessTokenMessageHandler
            //to XeroAccessToken class

            $messageHandler = $message['ReceiptHandle'] ?? [];
            if ($messageHandler) {
                //ReceiptHandle is invalid
                //This is the expected behavior of SQS given a visibility timeout of 0 seconds
                //When working with a FIFO queue, DeleteMessage operations will fail
                // if the request is received outside of the visibility timeout window/
                //I have a visibility timeout 30s and same error which can only
                // mean I tried to delete a message after 30s am I right?
                // Yes, DeleteMessage will fail with an error if the visibility window has already expired
                $this->client->deleteMessage([
                    'QueueUrl' => $params['QueueUrl'],
                    'ReceiptHandle' => $messageHandler
                ]);
            }

            //ApplicantXeroMessage
            $data = \json_decode( trim($message['Body'],'""'), true);
            $obj = new ApplicantXeroMessage(new XeroAccessToken($data['token']), $data['appId']);


            // this handler should run after deleteMessage, reasons see above comments.
            $handler(new Envelope($obj));
        }
    }
}
