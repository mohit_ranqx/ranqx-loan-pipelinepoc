<?php

namespace App\Entity;

use App\Enumerable\BaseAccountTypes;
use App\Enumerable\IncomeStatementAccountTypes;
use Exception;

class LedgerAccount
{
    /**
     * @var int|string
     */
    private $id;

    /**
     * @var mixed
     */
    private $code;

    /**
     * @var string
     */
    private $title;

    /**
     * @var double
     */
    private $price;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $old_type;

    /**
     * @var bool
     */
    private $new;

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return $this
     * @throws Exception
     */
    public function setType($type)
    {
        if (($type = strtoupper($type)) && BaseAccountTypes::isValidType($type)) {
            $this->type = $type;
        } else {
            throw new Exception('The provided account category "'.$type.'" is not valid');
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getOldType()
    {
        return $this->old_type;
    }

    /**
     * @param string $old_type
     * @return $this
     */
    public function setOldType($old_type)
    {
        $this->old_type = $old_type;

        return $this;
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * @param bool $new
     * @return $this
     */
    public function setNew($new)
    {
        $this->new = $new;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBalanceSheet()
    {
        return strpos($this->type, 'IS-') === false;
    }

    /**
     * @return bool
     */
    public function isAsset()
    {
        return strpos($this->type, 'A-') === 0;
    }

    /**
     * @return bool
     */
    public function isLiability()
    {
        return strpos($this->type, 'L-') === 0;
    }

    /**
     * @return bool
     */
    public function isEquity()
    {
        return strpos($this->type, 'E-') === 0;
    }

    /**
     * @return bool
     */
    public function isIncomeStatement()
    {
        return strpos($this->type, 'IS-') === 0;
    }

    /**
     * @return bool
     */
    public function isRevenue()
    {
        return IncomeStatementAccountTypes::REVENUE == $this->type;
    }

    /**
     * @return bool
     */
    public function isCog()
    {
        return IncomeStatementAccountTypes::COSTOFGOODS == $this->type;
    }

    /**
     * @return bool
     */
    public function isPeople()
    {
        return IncomeStatementAccountTypes::PEOPLE == $this->type;
    }

    /**
     * @return bool
     */
    public function isRent()
    {
        return IncomeStatementAccountTypes::RENT == $this->type;
    }

    /**
     * @return bool
     */
    public function isOperatingCost()
    {
        return IncomeStatementAccountTypes::OPERATINGCOSTS == $this->type;
    }

    /**
     * @return bool
     */
    public function isOtherCost()
    {
        return in_array($this->type, [
            IncomeStatementAccountTypes::OTHER,
            IncomeStatementAccountTypes::OTHER_DEPRECIATION,
            IncomeStatementAccountTypes::OTHER_INTEREST,
            IncomeStatementAccountTypes::OTHER_TAX,
            IncomeStatementAccountTypes::OTHER_EXCLUSIONS,
            IncomeStatementAccountTypes::LEGACY_OTHER,
        ]);
    }
}
