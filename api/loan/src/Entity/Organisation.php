<?php


namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrganisationRepository")
 * @ORM\Table(name="organisation")
 */
class Organisation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /** @ORM\Column(type="string", length=255) ***/
    protected $name;
    /** @ORM\Column(type="string", length=255) ***/
    protected $legalName;
    /** @ORM\Column(type="boolean") ***/
    protected $paysTax = false;
    /** @ORM\Column(type="string", length=255) ***/
    protected $providerName;
    /** @ORM\Column(type="string", length=32) ***/
    protected $organisationType;
    /** @ORM\Column(type="string", length=8) ***/
    protected $version;
    /** @ORM\Column(type="string", length=4) **/
    protected $baseCurrency;
    /** @ORM\Column(type="string", length=2) **/
    protected $countryCode;
    /** @ORM\Column(type="boolean") **/
    protected $isDemoCompany = false;
    /** @ORM\Column(type="string", length=8) **/
    protected $organisationStatus = 'active';
    /** @ORM\Column(type="integer") ***/
    protected $financialYearEndDay;
    /** @ORM\Column(type="integer") ****/
    protected $financialYearEndMonth;
    /** @ORM\Column(type="string", length=32) ****/
    protected $salesTaxBasis;
    /** @ORM\Column(type="string", length=32) ****/
    protected $defaultSalesTax;
    /** @ORM\Column(type="string", length=32) ****/
    protected $defaultPurchasesTax;

    //CreatedDateUTC
    /** @ORM\Column(type="datetime") ****/
    protected $createdInXero;
    /** @ORM\Column(type="string", length=16) ****/
    protected $organisationEntityType;
    /** @ORM\Column(type="string", length=64) */
    protected $organisationID;
    /** @ORM\Column(type="string", length=16) ****/
    protected $edition;
    /** @ORM\Column(type="string", length=16) ****/
    protected $xeroClass;

    /** @ORM\Embedded(class = "Address") */
    protected $address;

    /** @ORM\Column(type="datetime") ****/
    protected $created;

    /** @ORM\OneToMany(targetEntity="\App\Entity\Finance", mappedBy="organisation", cascade={"persist"}) **/
    protected $finances;

    /**
     * @ORM\OneToMany(targetEntity="\App\Entity\FinanceMonth", mappedBy="organisation", cascade={"persist"})
     * @ORM\OrderBy({"period" = "ASC"})
     **/
    protected $financeMonths;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Applicant", inversedBy="organisation")
     * @ORM\JoinColumn(name="applicant_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $applicant;


    /** @ORM\Column(type="string", length=256) ****/
    protected $lineOfBusiness;
    /** @ORM\Column(type="string", length=64) ****/
    protected $taxNumber = '';

    /**
     * Organisation constructor.
     */
    public function __construct()
    {
        //$this->finances = new ArrayCollection();
        //$this->financeMonths = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTaxNumber()
    {
        return $this->taxNumber;
    }

    /**
     * @param mixed $taxNumber
     */
    public function setTaxNumber($taxNumber): void
    {
        $this->taxNumber = $taxNumber;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getLegalName()
    {
        return $this->legalName;
    }

    /**
     * @param mixed $legalName
     */
    public function setLegalName($legalName): void
    {
        $this->legalName = $legalName;
    }

    /**
     * @return mixed
     */
    public function getPaysTax()
    {
        return $this->paysTax;
    }

    /**
     * @param mixed $paysTax
     */
    public function setPaysTax($paysTax): void
    {
        $this->paysTax = $paysTax;
    }

    /**
     * @return mixed
     */
    public function getProviderName()
    {
        return $this->providerName;
    }

    /**
     * @param mixed $providerName
     */
    public function setProviderName($providerName): void
    {
        $this->providerName = $providerName;
    }

    /**
     * @return mixed
     */
    public function getOrganisationType()
    {
        return $this->organisationType;
    }

    /**
     * @param mixed $organisationType
     */
    public function setOrganisationType($organisationType): void
    {
        $this->organisationType = $organisationType;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     */
    public function setVersion($version): void
    {
        $this->version = $version;
    }

    /**
     * @return mixed
     */
    public function getBaseCurrency()
    {
        return $this->baseCurrency;
    }

    /**
     * @param mixed $baseCurrency
     */
    public function setBaseCurrency($baseCurrency): void
    {
        $this->baseCurrency = $baseCurrency;
    }

    /**
     * @return mixed
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param mixed $countryCode
     */
    public function setCountryCode($countryCode): void
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return mixed
     */
    public function getIsDemoCompany()
    {
        return $this->isDemoCompany;
    }

    /**
     * @param mixed $isDemoCompany
     */
    public function setIsDemoCompany($isDemoCompany): void
    {
        $this->isDemoCompany = $isDemoCompany;
    }

    /**
     * @return mixed
     */
    public function getOrganisationStatus()
    {
        return $this->organisationStatus;
    }

    /**
     * @param mixed $organisationStatus
     */
    public function setOrganisationStatus($organisationStatus): void
    {
        $this->organisationStatus = $organisationStatus;
    }

    /**
     * @return mixed
     */
    public function getFinancialYearEndDay()
    {
        return $this->financialYearEndDay;
    }

    /**
     * @param mixed $financialYearEndDay
     */
    public function setFinancialYearEndDay($financialYearEndDay): void
    {
        $this->financialYearEndDay = $financialYearEndDay;
    }

    /**
     * @return int
     */
    public function getFinancialYearEndMonth(): int
    {
        return $this->financialYearEndMonth;
    }

    /**
     * @param mixed $financialYearEndMonth
     */
    public function setFinancialYearEndMonth($financialYearEndMonth): void
    {
        $this->financialYearEndMonth = $financialYearEndMonth;
    }

    /**
     * @return mixed
     */
    public function getSalesTaxBasis()
    {
        return $this->salesTaxBasis;
    }

    /**
     * @param mixed $salesTaxBasis
     */
    public function setSalesTaxBasis($salesTaxBasis): void
    {
        $this->salesTaxBasis = $salesTaxBasis;
    }

    /**
     * @return mixed
     */
    public function getDefaultSalesTax()
    {
        return $this->defaultSalesTax;
    }

    /**
     * @param mixed $defaultSalesTax
     */
    public function setDefaultSalesTax($defaultSalesTax): void
    {
        $this->defaultSalesTax = $defaultSalesTax;
    }

    /**
     * @return mixed
     */
    public function getDefaultPurchasesTax()
    {
        return $this->defaultPurchasesTax;
    }

    /**
     * @param mixed $defaultPurchasesTax
     */
    public function setDefaultPurchasesTax($defaultPurchasesTax): void
    {
        $this->defaultPurchasesTax = $defaultPurchasesTax;
    }

    /**
     * @return mixed
     */
    public function getCreatedInXero()
    {
        return $this->createdInXero;
    }

    /**
     * @param mixed $createdInXero
     */
    public function setCreatedInXero($createdInXero): void
    {
        $this->createdInXero = $createdInXero;
    }

    /**
     * @return mixed
     */
    public function getOrganisationEntityType()
    {
        return $this->organisationEntityType;
    }

    /**
     * @param mixed $organisationEntityType
     */
    public function setOrganisationEntityType($organisationEntityType): void
    {
        $this->organisationEntityType = $organisationEntityType;
    }

    /**
     * @return mixed
     */
    public function getOrganisationID()
    {
        return $this->organisationID;
    }

    /**
     * @param mixed $organisationID
     */
    public function setOrganisationID($organisationID): void
    {
        $this->organisationID = $organisationID;
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address): void
    {
        $this->address = $address;
    }



    /**
     * @return mixed
     */
    public function getEdition()
    {
        return $this->edition;
    }

    /**
     * @param mixed $edition
     */
    public function setEdition($edition): void
    {
        $this->edition = $edition;
    }

    /**
     * @return mixed
     */
    public function getXeroClass()
    {
        return $this->xeroClass;
    }

    /**
     * @param mixed $xeroClass
     */
    public function setXeroClass($xeroClass): void
    {
        $this->xeroClass = $xeroClass;
    }

    /**
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     */
    public function setCreated(DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return Collection
     */
    public function getFinances(): Collection
    {
        return $this->finances;
    }

    /**
     * @param Collection $finances
     */
    public function setFinances(Collection $finances): void
    {
        $this->finances = $finances;
    }

    /**
     * @param Finance $finance
     * @return bool
     */
    public function addFinance(Finance $finance): bool
    {
        return $this->finances->add($finance);
    }

    /**
     * @return Collection
     */
    public function getFinanceMonths(): Collection
    {
        return $this->financeMonths;
    }

    /**
     * @param Collection $financeMonths
     */
    public function setFinanceMonths(Collection $financeMonths): void
    {
        $this->financeMonths = $financeMonths;
    }

    /**
     * @param FinanceMonth $financeMonth
     * @return bool
     */
    public function addFinanceMonth(FinanceMonth $financeMonth): bool
    {
        return $this->financeMonths->add($financeMonth);
    }

    /**
     * @return Applicant | null
     */
    public function getApplicant() :? Applicant
    {
        return $this->applicant;
    }

    /**
     * @param mixed $applicant
     */
    public function setApplicant($applicant): void
    {
        $this->applicant = $applicant;
    }

    /**
     * @return mixed
     */
    public function getLineOfBusiness()
    {
        return $this->lineOfBusiness;
    }

    /**
     * @param mixed $lineOfBusiness
     */
    public function setLineOfBusiness($lineOfBusiness): void
    {
        $this->lineOfBusiness = $lineOfBusiness;
    }
}
