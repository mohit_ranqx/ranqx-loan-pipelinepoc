<?php declare(strict_types = 1);


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use \DateTime;
use Swagger\Annotations as SWG;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ApplicantRepository")
 *
 * @ORM\Table(name="Applicant")
 */
class Applicant
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $referenceNumber;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\NotBlank()
     * @Assert\Length(max=64)
     * @SWG\Property(type="string",description="first name")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\NotBlank()
     * @Assert\Length(max=64)
     * @SWG\Property(type="string",description="last name")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=256)
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Assert\Length(max=255)
     * @SWG\Property(type="string",description="Email")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\NotBlank()
     * @Assert\Length(max=32)
     * @SWG\Property(type="string",description="Phone")
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     * @Assert\Length(max=256)
     * @SWG\Property(type="string",description="Business Name")
     */
    private $businessName;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=4, nullable=true)
     * @Assert\GreaterThan(0)
     * @SWG\Property(type="number",description="Loan Amount")
     */
    private $loanAmount;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @SWG\Property(type="string",description="Loan Reason")
     * ***/
    private $loanReason;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $nzbnNumber;

    /**
     * @ORM\Column(type="boolean")
     * @SWG\Property(type="boolean",description="is customer")
     *
     * ******/
    private $isCustomer = true;

    /**
     * @ORM\Column(type="boolean")
     * ****/
    private $accepted = false;

    /**
     * @ORM\Column(type="blob", nullable=true)
     * ****/
    private $pdf;

    /**
     * @ORM\Column(type="integer", length=10, nullable=true)
     * @Assert\Length(max=10)
     * @SWG\Property(type="string",description="Length of loan")
     */
    private $loanTerm = 0;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     * @Assert\Length(max=32)
     * @SWG\Property(type="string",description="Applicants salutation (Mr, Ms...)")
     */
    private $salutation;

    /** @ORM\Column(type="json") ***/
    private $equiFaxRaw;

    /** @ORM\Column(type="boolean") ***/
    private $decision = false;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private $updated;

    /**
     * @ORM\OneToOne(targetEntity="\App\Entity\Organisation", mappedBy="applicant", cascade={"persist"})
     **/
    private $organisation;

    /**
     * @ORM\OneToOne(targetEntity="\App\Entity\Integration", mappedBy="applicant", cascade={"persist"})
     **/
    protected $integration;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * ****/
    private $ipAddress;

    public function __construct()
    {
        $this->created = new DateTime();
        $this->updated = new DateTime();
        $this->equiFaxRaw = [];
    }

    /**
     * @return int|null
     */
    public function getId() :? int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    /**
     * @param mixed $referenceNumber
     */
    public function setReferenceNumber($referenceNumber): void
    {
        $this->referenceNumber = $referenceNumber;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getBusinessName()
    {
        return $this->businessName;
    }

    /**
     * @param mixed $businessName
     */
    public function setBusinessName($businessName): void
    {
        $this->businessName = $businessName;
    }

    /**
     * @return mixed
     */
    public function getLoanAmount()
    {
        return $this->loanAmount;
    }

    /**
     * @param mixed $loanAmount
     */
    public function setLoanAmount($loanAmount): void
    {
        $this->loanAmount = $loanAmount;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created): void
    {
        $this->created = $created;
    }

    /**
     * @return Organisation|null
     */
    public function getOrganisation():? Organisation
    {
        return $this->organisation;
    }

    /**
     * @param Organisation $organisation
     */
    public function setOrganisation(Organisation $organisation): void
    {
        $this->organisation = $organisation;
    }

    /**
     * @return mixed
     */
    public function getLoanReason()
    {
        return $this->loanReason;
    }

    /**
     * @param mixed $loanReason
     */
    public function setLoanReason($loanReason): void
    {
        $this->loanReason = $loanReason;
    }

    /**
     * @return mixed
     */
    public function getNzbnNumber()
    {
        return $this->nzbnNumber;
    }

    /**
     * @param mixed $nzbnNumber
     */
    public function setNzbnNumber($nzbnNumber): void
    {
        $this->nzbnNumber = $nzbnNumber;
    }

    /**
     * @return mixed
     */
    public function getIsCustomer()
    {
        return $this->isCustomer;
    }

    /**
     * @param mixed $isCustomer
     */
    public function setIsCustomer($isCustomer): void
    {
        $this->isCustomer = $isCustomer;
    }

    /**
     * @return mixed
     */
    public function getAccepted()
    {
        return $this->accepted;
    }

    /**
     * @param mixed $accepted
     */
    public function setAccepted($accepted): void
    {
        $this->accepted = $accepted;
    }

    /**
     * @return mixed
     */
    public function getEquiFaxRaw()
    {
        return $this->equiFaxRaw;
    }

    /**
     * @param mixed $equiFaxRaw
     */
    public function setEquiFaxRaw($equiFaxRaw): void
    {
        $this->equiFaxRaw = $equiFaxRaw;
    }

    /**
     * @return mixed
     */
    public function getDecision()
    {
        return $this->decision;
    }

    /**
     * @param mixed $decision
     */
    public function setDecision($decision): void
    {
        $this->decision = $decision;
    }

    /**
     * @return mixed
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * @param mixed $pdf
     */
    public function setPdf($pdf): void
    {
        $this->pdf = $pdf;
    }

    /**
     * @return mixed
     */
    public function getIntegration()
    {
        return $this->integration;
    }

    /**
     * @param mixed $integration
     */
    public function setIntegration($integration): void
    {
        $this->integration = $integration;
    }

    /**
     * @return DateTime
     */
    public function getUpdated(): DateTime
    {
        return $this->updated;
    }

    /**
     * @param DateTime $updated
     */
    public function setUpdated(DateTime $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return mixed
     */
    public function getLoanTerm()
    {
        return $this->loanTerm;
    }

    /**
     * @param mixed $loanTerm
     */
    public function setLoanTerm($loanTerm)
    {
        $this->loanTerm = $loanTerm;
    }

    /**
     * @return mixed
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * @param mixed $salutation
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;
    }

    /**
     * @return mixed
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @param mixed $ipAddress
     */
    public function setIpAddress($ipAddress): void
    {
        $this->ipAddress = $ipAddress;
    }
}
