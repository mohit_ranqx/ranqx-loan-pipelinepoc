<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PayLoadRepository")
 *
 * @ORM\Table(name="pay_load")
 */


class PayLoad extends AbstractPayLoad
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisation", inversedBy="payLoads")
     * @ORM\JoinColumn(name="organisation_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $organisation;

    /**
     * @return mixed
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * @param mixed $organisation
     */
    public function setOrganisation($organisation): void
    {
        $this->organisation = $organisation;
    }
}