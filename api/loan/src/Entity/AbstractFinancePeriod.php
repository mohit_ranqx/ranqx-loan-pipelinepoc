<?php


namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;
use App\Enumerable\IncomeStatementAccountTypes;

/**
 * Class AbstractFinancePeriod
 * @package App\Entity
 */
class AbstractFinancePeriod extends AbstractFinance
{
    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    protected $period;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $rawData;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @ORM\Column(type="float", scale=2, nullable=true)
     * @Assert\NotBlank()
     */
    protected $interest;

    /**
     * @ORM\Column(type="float", scale=2, nullable=true)
     * @Assert\NotBlank()
     */
    protected $depreciation;

    /**
     * @ORM\Column(type="float", scale=2, nullable=true)
     * @Assert\NotBlank()
     */
    protected $tax;


    /**
     * Finance constructor.
     */
    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();
    }

    /**
     * @return mixed
     */
    public function getDepreciation()
    {
        return $this->depreciation;
    }

    /**
     * @param mixed $depreciation
     */
    public function setDepreciation($depreciation): void
    {
        $this->depreciation = $depreciation;
    }



    /**
     * @return DateTime
     */
    public function getPeriod(): DateTime
    {
        //why do this?
        if ($this->period instanceof DateTime) {
            $this->period->modify('last day of this month');
        }

        return $this->period;
    }

    /**
     * @return mixed
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param mixed $tax
     */
    public function setTax($tax): void
    {
        $this->tax = $tax;
    }


    /**
     * @param DateTime|string $period
     * @return AbstractFinancePeriod
     * @throws Exception
     */
    public function setPeriod($period): self
    {
        /*if ($period instanceof \DateTime) {
            $this->period = $period->modify('last day of this month')->setTime(11, 59,59);

        } elseif (is_string($period)) {
            $this->period = (new \DateTime($period))->modify('last day of this month');
            $this->period->setTime(11, 59, 59);
        } else {
            throw new \InvalidArgumentException('The period should be string or DateTime format');
        }*/

        $this->period = $period;

        return $this;
    }

    /**
     * @return string
     */
    public function getRawData(): string
    {
        return $this->rawData;
    }

    /**
     * @param string $rawData
     * @return $this
     */
    public function setRawData($rawData): self
    {
        $this->rawData = $rawData;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     * @return AbstractFinancePeriod
     */
    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /* VIRTUAL */

    /**
     * @return mixed
     */
    public function getRawDataArray()
    {
        return json_decode($this->rawData ?: '[]', true);
    }

    /**
     * @return mixed
     */
    public function getFilteredRawDataArray()
    {
        $ledgerAccounts = new ArrayCollection();

        if ($json = json_decode($this->rawData, true)) {
            foreach ($json as $row) {
                $ledgerAccount = [
                    'id' => @$row['id'],
                    'code' => @$row['code'],
                    'title' => @$row['title'],
                    'price' => @$row['price'],
                    'type' => @$row['type'],
                    'old_type' => @$row['old_type']
                ];

                $ledgerAccounts[] = $ledgerAccount;
            }
        }

        return $ledgerAccounts;
    }


    /**
     * @return ArrayCollection
     * @throws Exception
     */
    public function getLedgerAccounts(): ArrayCollection
    {
        return $this->parseRawDataToArray($this->rawData);
    }

    /**
     * @param ArrayCollection|LedgerAccount[] $ledgerAccounts
     * @return $this
     */
    public function setLedgerAccounts($ledgerAccounts): self
    {
        $this->rawData = $this->parseArrayToRawData($ledgerAccounts);

        return $this;
    }

    /**
     * @return float
     * @throws Exception
     */
    public function getTotalPeopleOther(): float
    {
        $pplo = $this->getLedgerAccounts()->filter(function (LedgerAccount $ledgerAccount) {
            return $ledgerAccount->getType() == IncomeStatementAccountTypes::PEOPLE_OTHER;
        })->first();

        return $pplo instanceof LedgerAccount ? $pplo->getPrice() : 0.0;
    }


    /**
     * @param float $total
     * @return AbstractFinancePeriod
     * @throws Exception
     */
    public function setTotalPeopleOther($total) : self
    {
        $accounts = $this->getLedgerAccounts();

        //Search for existing Other Wage account
        $pplo = $accounts->filter(function (LedgerAccount $ledgerAccount) {
            return $ledgerAccount->getType() === IncomeStatementAccountTypes::PEOPLE_OTHER;
        })->first();

        if ($pplo) {
            $accounts->removeElement($pplo);
            $pplo->setPrice($total);
        } else {
            //Create new if not exists
            $pplo = new LedgerAccount();
            $pplo
                ->setId('is-pplo')
                ->setTitle('Adjustments')
                ->setType(IncomeStatementAccountTypes::PEOPLE_OTHER)
                ->setOldType(IncomeStatementAccountTypes::PEOPLE_OTHER)
                ->setCode(IncomeStatementAccountTypes::PEOPLE_OTHER)
                ->setPrice($total);
        }

        $accounts->add($pplo);
        $this->setLedgerAccounts($accounts);

        return $this;
    }


    /**
     * @param string $rawData
     * @return ArrayCollection
     * @throws Exception
     */
    private function parseRawDataToArray($rawData): ArrayCollection
    {
        $ledgerAccounts = new ArrayCollection();

        if ($json = json_decode($rawData, true)) {
            foreach ($json as $row) {
                $ledgerAccount = new LedgerAccount();

                $ledgerAccount
                    ->setId(@$row['id'])
                    ->setCode(@$row['code'])
                    ->setTitle(@$row['title'])
                    ->setPrice(@$row['price'])
                    ->setType(@$row['type'])
                    ->setOldType(@$row['old_type']);

                $ledgerAccounts[] = $ledgerAccount;
            }
        }

        return $ledgerAccounts;
    }

    /**
     * @param ArrayCollection|LedgerAccount[] $ledgerAccounts
     * @return string|false
     */
    private function parseArrayToRawData($ledgerAccounts)
    {
        $rawData = [];

        foreach ($ledgerAccounts as $ledgerAccount) {
            $item = [];

            $item['id'] = $ledgerAccount->getId();
            $item['code'] = $ledgerAccount->getCode();
            $item['title'] = $ledgerAccount->getTitle();
            $item['price'] = $ledgerAccount->getPrice();
            $item['type'] = $ledgerAccount->getType();
            $item['old_type'] = $ledgerAccount->getOldType();

            $rawData[] = $item;
        }

        return json_encode($rawData);
    }

    /**
     * @return mixed
     */
    public function getInterest()
    {
        return $this->interest;
    }

    /**
     * @param mixed $interest
     */
    public function setInterest($interest): void
    {
        $this->interest = $interest;
    }

}
