<?php


namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="finance_invoice")
 */


class FinanceInvoice
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $organisation_id;

    /**
     * @ORM\Column(type="string")
     */
    private $currency;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $debtorConcentration;

    /**
     * @ORM\Column(type="string")
     */
    private $invoiceDataReturned;

    /**
     * @ORM\Column(name="period_start", type="datetime", nullable=true)
     */
    private $periodStart;

    /**
     * @ORM\Column(name="period_end", type="datetime", nullable=true)
     */
    private $periodEnd;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $rawData;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() : int
    {
        return $this->id;
    }

    public function getOrganisationId(): ?int
    {
        return $this->organisation_id;
    }

    public function setOrganisationId(int $organisation_id): self
    {
        $this->organisation_id = $organisation_id;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency() : string
    {
        return $this->currency;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return $this
     */
    public function setCurrency($currency) : self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getDebtorConcentration()
    {
        return $this->debtorConcentration;
    }

    public function setDebtorConcentration($debtorConcentration): self
    {
        $this->debtorConcentration = $debtorConcentration;

        return $this;
    }

    public function getInvoiceDataReturned() : string
    {
        return $this->invoiceDataReturned;
    }

    public function setInvoiceDataReturned($invoiceDataReturned): self
    {
        $this->invoiceDataReturned = $invoiceDataReturned;

        return $this;
    }

    public function getPeriodStart(): ?\DateTimeInterface
    {
        return $this->periodStart;
    }

    public function setPeriodStart(\DateTimeInterface $periodStart): self
    {
        $this->periodStart = $periodStart;

        return $this;
    }

    public function getPeriodEnd(): ?\DateTimeInterface
    {
        return $this->periodEnd;
    }

    public function setPeriodEnd(\DateTimeInterface $periodEnd): self
    {
        $this->periodEnd = $periodEnd;

        return $this;
    }

    /**
     * Finance constructor.
     */
    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();
    }

    /**
     * @return string
     */
    public function getRawData(): string
    {
        return $this->rawData;
    }

    /**
     * @param string $rawData
     * @return $this
     */
    public function setRawData($rawData): self
    {
        $this->rawData = $rawData;

        return $this;
    }

}