<?php


namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;
use Swagger\Annotations as SWG;

/**
 * Class AbstractPayLoad
 * @package App\Entity
 */
abstract class AbstractPayLoad
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;
    /**
     * @ORM\Column(name="raw_data", type="text", nullable=true)
     */
    protected $rawData;

    /**
     * @ORM\Column(name="file_path", type="text", nullable=true)
     */
    protected $filePath;

    /**
     * @ORM\Column(name="file_name", type="text", nullable=true)
     */
    protected $fileName;

    /**
     * @ORM\Column(name="process_status", type="string", length=32, nullable=true)
     * @Assert\Length(max=32)
     * @SWG\Property(type="string",description="Did the file upload and zip - success, fail")
     */
    protected $processStatus;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


    /**
     * @return string
     */
    public function getRawData(): string
    {
        return $this->rawData;
    }

    /**
     * @param string $rawData
     * @return $this
     */
    public function setRawData($rawData): self
    {
        $this->rawData = $rawData;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * @param string $filePath
     * @return $this
     */
    public function setFilePath($filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     * @return $this
     */
    public function setFileName($fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return string
     */
    public function getProcessStatus(): string
    {
        return $this->processStatus;
    }

    /**
     * @param string $processStatus
     * @return $this
     */
    public function setProcessStatus($processStatus): self
    {
        $this->processStatus = $processStatus;

        return $this;
    }

}