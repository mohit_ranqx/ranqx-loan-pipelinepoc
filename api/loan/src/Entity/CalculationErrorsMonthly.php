<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CalculationErrorsMonthlyRepository")
 */
class CalculationErrorsMonthly
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $calulate_finance_month_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $orginisation_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $calculation_name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $calculation_value;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $error_message;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCalulateFinanceMonthId(): ?int
    {
        return $this->calulate_finance_month_id;
    }

    public function setCalulateFinanceMonthId(int $calulate_finance_month_id): self
    {
        $this->calulate_finance_month_id = $calulate_finance_month_id;

        return $this;
    }

    public function getOrginisationId(): ?int
    {
        return $this->orginisation_id;
    }

    public function setOrginisationId(int $orginisation_id): self
    {
        $this->orginisation_id = $orginisation_id;

        return $this;
    }

    public function getCalculationName(): ?string
    {
        return $this->calculation_name;
    }

    public function setCalculationName(?string $calculation_name): self
    {
        $this->calculation_name = $calculation_name;

        return $this;
    }

    public function getCalculationValue(): ?int
    {
        return $this->calculation_value;
    }

    public function setCalculationValue(?int $calculation_value): self
    {
        $this->calculation_value = $calculation_value;

        return $this;
    }

    public function getErrorMessage(): ?string
    {
        return $this->error_message;
    }

    public function setErrorMessage(?string $error_message): self
    {
        $this->error_message = $error_message;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }
}
