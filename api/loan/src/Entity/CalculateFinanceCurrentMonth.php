<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\CalculateFinanceCurrentMonthRepository")
 * @ORM\Table(name="calculate_finance_month_current")
 */

class CalculateFinanceCurrentMonth
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $organisation_id;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $revenue_current;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $gross_margin_value_current;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $gross_margin_percentage_current;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $ebitda_value_current;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $ebitda_margin_current;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $interest_cover_current;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $marginal_cashflow_current;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $working_capital_ratio_current;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $quick_ratio_current;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $cash_ratio_current;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $net_cash_balance_current;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $debt_ratio_current;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $equity_ratio_current;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $people_cost_ratio_current;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $operating_cost_ratio_current;

    /**
     * @ORM\Column(type="datetime")
     */
    private $period_start;

    /**
     * @ORM\Column(type="datetime")
     */
    private $period_end;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrganisationId(): ?int
    {
        return $this->organisation_id;
    }

    public function setOrganisationId(int $organisation_id): self
    {
        $this->organisation_id = $organisation_id;

        return $this;
    }

    public function getRevenueCurrent()
    {
        return $this->revenue_current;
    }

    public function setRevenueCurrent($revenue_current): self
    {
        $this->revenue_current = $revenue_current;

        return $this;
    }

    public function getGrossMarginValueCurrent()
    {
        return $this->gross_margin_value_current;
    }

    public function setGrossMarginValueCurrent($gross_margin_value_current): self
    {
        $this->gross_margin_value_current = $gross_margin_value_current;

        return $this;
    }

    public function getGrossMarginPercentageCurrent()
    {
        return $this->gross_margin_percentage_current;
    }

    public function setGrossMarginPercentageCurrent($gross_margin_percentage_current): self
    {
        $this->gross_margin_percentage_current = $gross_margin_percentage_current;

        return $this;
    }

    public function getEbitdaValueCurrent()
    {
        return $this->ebitda_value_current;
    }

    public function setEbitdaValueCurrent($ebitda_value_current): self
    {
        $this->ebitda_value_current = $ebitda_value_current;

        return $this;
    }

    public function getEbitdaMarginCurrent()
    {
        return $this->ebitda_margin_current;
    }

    public function setEbitdaMarginCurrent($ebitda_margin_current): self
    {
        $this->ebitda_margin_current = $ebitda_margin_current;

        return $this;
    }

    public function getInterestCoverCurrent()
    {
        return $this->interest_cover_current;
    }

    public function setInterestCoverCurrent($interest_cover_current): self
    {
        $this->interest_cover_current = $interest_cover_current;

        return $this;
    }

    public function getMarginalCashflowCurrent()
    {
        return $this->marginal_cashflow_current;
    }

    public function setMarginalCashflowCurrent($marginal_cashflow_current): self
    {
        $this->marginal_cashflow_current = $marginal_cashflow_current;

        return $this;
    }

    public function getWorkingCapitalRatioCurrent()
    {
        return $this->working_capital_ratio_current;
    }

    public function setWorkingCapitalRatioCurrent($working_capital_ratio_current): self
    {
        $this->working_capital_ratio_current = $working_capital_ratio_current;

        return $this;
    }

    public function getQuickRatioCurrent()
    {
        return $this->quick_ratio_current;
    }

    public function setQuickRatioCurrent($quick_ratio_current): self
    {
        $this->quick_ratio_current = $quick_ratio_current;

        return $this;
    }

    public function getCashRatioCurrent()
    {
        return $this->cash_ratio_current;
    }

    public function setCashRatioCurrent($cash_ratio_current): self
    {
        $this->cash_ratio_current = $cash_ratio_current;

        return $this;
    }

    public function getNetCashBalanceCurrent()
    {
        return $this->net_cash_balance_current;
    }

    public function setNetCashBalanceCurrent($net_cash_balance_current): self
    {
        $this->net_cash_balance_current = $net_cash_balance_current;

        return $this;
    }

    public function getDebtRatioCurrent()
    {
        return $this->debt_ratio_current;
    }

    public function setDebtRatioCurrent($debt_ratio_current): self
    {
        $this->debt_ratio_current = $debt_ratio_current;

        return $this;
    }

    public function getEquityRatioCurrent()
    {
        return $this->equity_ratio_current;
    }

    public function setEquityRatioCurrent($equity_ratio_current): self
    {
        $this->equity_ratio_current = $equity_ratio_current;

        return $this;
    }

    public function getOperatingCostRatioCurrent()
    {
        return $this->operating_cost_ratio_current;
    }

    public function setOperatingCostRatioCurrent($operating_cost_ratio_current): self
    {
        $this->operating_cost_ratio_current = $operating_cost_ratio_current;

        return $this;
    }

    public function getPeopleCostRatioCurrent()
    {
        return $this->people_cost_ratio_current;
    }

    public function setPeopleCostRatioCurrent($people_cost_ratio_current): self
    {
        $this->people_cost_ratio_current = $people_cost_ratio_current;

        return $this;
    }

    public function getPeriodStart(): ?\DateTimeInterface
    {
        return $this->period_start;
    }

    public function setPeriodStart(\DateTimeInterface $period_start): self
    {
        $this->period_start = $period_start;

        return $this;
    }

    public function getPeriodEnd(): ?\DateTimeInterface
    {
        return $this->period_end;
    }

    public function setPeriodEnd(\DateTimeInterface $period_end): self
    {
        $this->period_end = $period_end;

        return $this;
    }
}
