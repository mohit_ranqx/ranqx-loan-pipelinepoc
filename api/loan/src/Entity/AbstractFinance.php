<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractFinance
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $currency;
    /**
     * @ORM\Column(type="float", scale=2)
     * @Assert\NotBlank()
     */
    protected $totalRevenue = 0.0;

    /**
     * @ORM\Column(type="float", scale=2)
     * @Assert\NotBlank()
     */
    protected $totalCog = 0.0;

    /**
     * @ORM\Column(type="float", scale=2)
     * @Assert\NotBlank()
     */
    protected $totalPeople = 0.0;

    /**
     * @ORM\Column(type="float", scale=2)
     * @Assert\NotBlank()
     */
    protected $totalRent = 0.0;

    /**
     * @ORM\Column(type="float", scale=2)
     * @Assert\NotBlank()
     */
    protected $totalOperating = 0.0;

    /**
     * @ORM\Column(type="float", scale=2)
     * @Assert\NotBlank()
     */
    protected $totalOther = 0.0;

    /**
     * @ORM\Column(type="float", scale=2, nullable=false)
     */
    protected $totalAssetsCurrentBank = 0.0;

    /**
     * @ORM\Column(type="float", scale=2, nullable=false)
     */
    protected $totalAssetsCurrentDebtors = 0.0;

    /**
     * @ORM\Column(type="float", scale=2, nullable=false)
     */
    protected $totalAssetsCurrentInventory = 0.0;

    /**
     * @ORM\Column(type="float", scale=2, nullable=false)
     */
    protected $totalAssetsCurrentOther = 0.0;

    /**
     * @ORM\Column(type="float", scale=2, nullable=false)
     */
    protected $totalAssetsFixed = 0.0;

    /**
     * @ORM\Column(type="float", scale=2, nullable=false)
     */
    protected $totalAssetsOther = 0.0;

    /**
     * @ORM\Column(type="float", scale=2, nullable=false)
     */
    protected $totalLiabilitiesCurrentBank = 0.0;

    /**
     * @ORM\Column(type="float", scale=2, nullable=false)
     */
    protected $totalLiabilitiesCurrentCreditors = 0.0;

    /**
     * @ORM\Column(type="float", scale=2, nullable=false)
     */
    protected $totalLiabilitiesCurrentOther = 0.0;

    /**
     * @ORM\Column(type="float", scale=2, nullable=false)
     */
    protected $totalLiabilitiesOther = 0.0;

    /**
     * @ORM\Column(type="float", scale=2, nullable=false)
     */
    protected $totalEquityCapital = 0.0;

    /**
     * @ORM\Column(type="float", scale=2, nullable=false)
     */
    protected $totalEquityEarnings = 0.0;

    /**
     * @ORM\Column(type="float", scale=2)
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = 1,
     *      minMessage = "You must enter at least {{ limit }}",
     * )
     */
    protected $staffNumber = 1.0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency() : string
    {
        return $this->currency;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return $this
     */
    public function setCurrency($currency) : self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get staffNumber
     *
     * @return float
     */
    public function getStaffNumber() : float
    {
        return $this->staffNumber;
    }

    /**
     * Set staffNumber
     *
     * @param integer $staffNumber
     * @return $this
     */
    public function setStaffNumber($staffNumber)  : self
    {
        $this->staffNumber = $staffNumber;

        return $this;
    }

    /**
     * Get totalRevenue
     *
     * @return float
     */
    public function getTotalRevenue(): float
    {
        return $this->totalRevenue;
    }

    /**
     * Set totalRevenue
     *
     * @param float $totalRevenue
     * @return $this
     */
    public function setTotalRevenue(float $totalRevenue): self
    {
        $this->totalRevenue = $totalRevenue;

        return $this;
    }

    /**
     * Get soldGoodsCost
     *
     * @return float
     */
    public function getTotalCog(): float
    {
        return $this->totalCog;
    }

    /**
     * Set soldGoodsCost
     *
     * @param float $totalCog
     * @return $this
     */
    public function setTotalCog(float $totalCog) : self
    {
        $this->totalCog = $totalCog;

        return $this;
    }

    /**
     * Get labourCosts
     *
     * @return float
     */
    public function getTotalPeople(): float
    {
        return $this->totalPeople;
    }

    /**
     * Set labourCosts
     *
     * @param float $totalPeople
     * @return $this
     */
    public function setTotalPeople($totalPeople): self
    {
        $this->totalPeople = $totalPeople;

        return $this;
    }

    /**
     * Get rent
     *
     * @return float
     */
    public function getTotalRent(): float
    {
        return $this->totalRent;
    }

    /**
     * Set rent
     *
     * @param float $totalRent
     * @return $this
     */
    public function setTotalRent($totalRent): self
    {
        $this->totalRent = $totalRent;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalOperating() : float
    {
        return $this->totalOperating;
    }

    /**
     * @param float $totalOperating
     * @return $this
     */
    public function setTotalOperating(float $totalOperating):self
    {
        $this->totalOperating = $totalOperating;

        return $this;
    }

    /**
     * Get otherCosts
     *
     * @return float
     */
    public function getTotalOther(): float
    {
        return $this->totalOther;
    }

    /**
     * Set otherCosts
     *
     * @param float $totalOther
     * @return $this
     */
    public function setTotalOther(float $totalOther): self
    {
        $this->totalOther = $totalOther;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalAssetsCurrentBank(): float
    {
        return $this->totalAssetsCurrentBank;
    }

    /**
     * @param mixed $totalAssetsCurrentBank
     * @return $this
     */
    public function setTotalAssetsCurrentBank($totalAssetsCurrentBank): self
    {
        $this->totalAssetsCurrentBank = $totalAssetsCurrentBank;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalAssetsCurrentDebtors(): float
    {
        return $this->totalAssetsCurrentDebtors;
    }

    /**
     * @param float $totalAssetsCurrentDebtors
     * @return $this
     */
    public function setTotalAssetsCurrentDebtors($totalAssetsCurrentDebtors): self
    {
        $this->totalAssetsCurrentDebtors = $totalAssetsCurrentDebtors;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalAssetsCurrentInventory(): float
    {
        return $this->totalAssetsCurrentInventory;
    }

    /**
     * @param float $totalAssetsCurrentInventory
     * @return $this
     */
    public function setTotalAssetsCurrentInventory($totalAssetsCurrentInventory): self
    {
        $this->totalAssetsCurrentInventory = $totalAssetsCurrentInventory;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalAssetsCurrentOther(): float
    {
        return $this->totalAssetsCurrentOther;
    }

    /**
     * @param float $totalAssetsCurrentOther
     * @return $this
     */
    public function setTotalAssetsCurrentOther($totalAssetsCurrentOther): self
    {
        $this->totalAssetsCurrentOther = $totalAssetsCurrentOther;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalAssetsFixed(): float
    {
        return $this->totalAssetsFixed;
    }

    /**
     * @param float $totalAssetsFixed
     * @return $this
     */
    public function setTotalAssetsFixed($totalAssetsFixed): self
    {
        $this->totalAssetsFixed = $totalAssetsFixed;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalAssetsOther(): float
    {
        return $this->totalAssetsOther;
    }

    /**
     * @param float $totalAssetsOther
     * @return $this
     */
    public function setTotalAssetsOther($totalAssetsOther): self
    {
        $this->totalAssetsOther = $totalAssetsOther;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalLiabilitiesCurrentBank(): float
    {
        return $this->totalLiabilitiesCurrentBank;
    }

    /**
     * @param float $totalLiabilitiesCurrentBank
     * @return $this
     */
    public function setTotalLiabilitiesCurrentBank($totalLiabilitiesCurrentBank):self
    {
        $this->totalLiabilitiesCurrentBank = $totalLiabilitiesCurrentBank;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalLiabilitiesCurrentCreditors():float
    {
        return $this->totalLiabilitiesCurrentCreditors;
    }

    /**
     * @param float $totalLiabilitiesCurrentCreditors
     * @return $this
     */
    public function setTotalLiabilitiesCurrentCreditors($totalLiabilitiesCurrentCreditors): self
    {
        $this->totalLiabilitiesCurrentCreditors = $totalLiabilitiesCurrentCreditors;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalLiabilitiesCurrentOther(): float
    {
        return $this->totalLiabilitiesCurrentOther;
    }

    /**
     * @param float $totalLiabilitiesCurrentOther
     * @return $this
     */
    public function setTotalLiabilitiesCurrentOther($totalLiabilitiesCurrentOther): self
    {
        $this->totalLiabilitiesCurrentOther = $totalLiabilitiesCurrentOther;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalLiabilitiesOther(): float
    {
        return $this->totalLiabilitiesOther;
    }

    /**
     * @param float $totalLiabilitiesOther
     * @return $this
     */
    public function setTotalLiabilitiesOther($totalLiabilitiesOther): self
    {
        $this->totalLiabilitiesOther = $totalLiabilitiesOther;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalEquityCapital(): float
    {
        return $this->totalEquityCapital;
    }

    /**
     * @param float $totalEquityCapital
     * @return $this
     */
    public function setTotalEquityCapital($totalEquityCapital): self
    {
        $this->totalEquityCapital = $totalEquityCapital;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalEquityEarnings(): float
    {
        return $this->totalEquityEarnings;
    }

    /**
     * @param float $totalEquityEarnings
     * @return $this
     */
    public function setTotalEquityEarnings($totalEquityEarnings):self
    {
        $this->totalEquityEarnings = $totalEquityEarnings;

        return $this;
    }

    // Virtual properties

    /**
     * @return float
     */
    public function getTotalAssets() : float
    {
        return $this->getTotalAssetsCurrentBank()
            + $this->getTotalAssetsCurrentDebtors()
            + $this->getTotalAssetsCurrentInventory()
            + $this->getTotalAssetsCurrentOther()
            + $this->getTotalAssetsFixed()
            + $this->getTotalAssetsOther();
    }

    /**
     * @return float
     */
    public function getTotalAssetsCurrent(): float
    {
        return $this->getTotalAssetsCurrentBank()
            + $this->getTotalAssetsCurrentDebtors()
            + $this->getTotalAssetsCurrentInventory()
            + $this->getTotalAssetsCurrentOther();
    }

    /**
     * @return float
     */
    public function getTotalLiabilities() : float
    {
        return $this->getTotalLiabilitiesCurrentBank()
            + $this->getTotalLiabilitiesCurrentCreditors()
            + $this->getTotalLiabilitiesCurrentOther()
            + $this->getTotalLiabilitiesOther();
    }

    /**
     * @return float
     */
    public function getTotalLiabilitiesCurrent(): float
    {
        return $this->getTotalLiabilitiesCurrentCreditors()
            + $this->getTotalLiabilitiesCurrentBank()
            + $this->getTotalLiabilitiesCurrentOther();
    }

    /**
     * @return float
     */
    public function getTotalEquity(): float
    {
        return $this->getTotalEquityCapital()
            + $this->getTotalEquityEarnings();
    }

    /**
     * @return float
     */
    public function getGrossProfit(): float
    {
        return $this->getTotalRevenue() - $this->getTotalCog();
    }

    /**
     * @return float
     */
    public function getOperatingProfit(): float
    {
        return $this->getTotalRevenue() - (
            $this->getTotalCog()
                + $this->getTotalPeople()
                + $this->getTotalRent()
                + $this->getTotalOperating()
            );
    }

    /**
     * @return float
     */
    public function getWorkingCapital(): float
    {
        return  $this->getTotalAssetsCurrent() - $this->getTotalLiabilitiesCurrent();
    }

    /**
     * @return float
     */
    public function getNetCash(): float
    {
        return $this->getTotalAssetsCurrentBank() - $this->getTotalLiabilitiesCurrentBank();
    }

    //Ratio

    /**
     * @return float
     */
    public function getWorkingCapitalRatio(): float
    {
        if ($this->getTotalLiabilitiesCurrent() === 0.0) {
            return 0.0;
        }

        return round(
            $this->getTotalAssetsCurrent()  / $this->getTotalLiabilitiesCurrent(),
            2
        );
    }

    /**
     * @return float
     */
    public function getEquityRatio(): float
    {
        if ($this->getTotalEquity() === 0.0) {
            return 0.0;
        }

        return round($this->getTotalAssets() / $this->getTotalEquity(), 2);
    }

    /**
     * @return float
     */
    public function getQuickRatio(): float
    {
        if ($this->getTotalLiabilitiesCurrentBank()
            + $this->getTotalLiabilitiesCurrentCreditors() === 0.0
        ) {
            return 0.0;
        }

        return round(
            (
                $this->getTotalAssetsCurrentBank()
                + $this->getTotalAssetsCurrentDebtors()
            ) / (
                $this->getTotalLiabilitiesCurrentBank()
                + $this->getTotalLiabilitiesCurrentCreditors()
            ),
            2
        );
    }

    /**
     * @param int $multiplier
     * @return float
     */
    public function getEstimatedValue($multiplier): float
    {
        if ($multiplier === 0) {
            return 0;
        }

        return $this->getOperatingProfit() * $multiplier;
    }

    /**
     * @return float
     */
    public function getDebtRatio(): float
    {
        if ($this->getTotalAssets() === 0.0) {
            return 0.0;
        }

        return round($this->getTotalLiabilities() / $this->getTotalAssets(), 2);
    }

    // Percentage

    /**
     * @param float $value
     * @return float
     */
    public function getRevenuePercent($value): float
    {
        if ($this->getTotalRevenue() !== 0) {
            return round(($value / $this->getTotalRevenue()) * 100);
        }
        return 0.0;
    }

    /**
     * @return float
     */
    public function getSoldGoodsCostPercentage(): float
    {
        return $this->getRevenuePercent($this->getTotalCog());
    }

    /**
     * @return float
     */
    public function getLabourCostsPercentage(): float
    {
        return $this->getRevenuePercent($this->getTotalPeople());
    }

    /**
     * @return float
     */
    public function getRentPercentage() : float
    {
        return $this->getRevenuePercent($this->getTotalRent());
    }

    /**
     * @return float
     */
    public function getOperatingPercentage(): float
    {
        return $this->getRevenuePercent($this->getTotalOperating());
    }

    /**
     * @return float
     */
    public function getOtherCostsPercentage(): float
    {
        return $this->getRevenuePercent($this->getTotalOther());
    }

    /**
     * @param float $value
     * @return float
     */
    public function getAssetsPercent($value): float
    {
        if ($this->getTotalAssets() !== 0) {
            return round(($value / $this->getTotalAssets()) * 100);
        }
        return 0.0;
    }

    /**
     * @return float
     */
    public function getTotalLiabilitiesPercentage(): float
    {
        return $this->getAssetsPercent($this->getTotalLiabilities());
    }

    /**
     * @return float
     */
    public function getTotalEquityPercentage(): float
    {
        return $this->getAssetsPercent($this->getTotalEquity());
    }

    /**
     * @return float
     */
    public function getEquityPercentage(): float
    {
        return $this->getAssetsPercent($this->getTotalEquity());
    }

    // Virtual properties percentage

    /**
     * @return float
     */
    public function getGrossProfitPercentage(): float
    {
        return $this->getPercent($this->getGrossProfit(), $this->getTotalRevenue());
    }

    /**
     * @return float|int
     */
    public function getOperatingProfitPercentage()
    {
        return $this->getPercent($this->getOperatingProfit(), $this->getTotalRevenue());
    }

    /**
     * @return float
     */
    public function getDebtPercentage(): float
    {
        return $this->getPercent($this->getTotalLiabilities(), $this->getTotalAssets());
    }

    /**
     * @return float
     */
    public function getWorkingCapitalPercentage(): float
    {
        return $this->getPercent($this->getWorkingCapital(), $this->getTotalAssets());
    }

    // Values per staff member

    /**
     * @return float
     */
    public function getTotalRevenuePerStaff(): float
    {
        return $this->getValuePerStaff($this->getTotalRevenue());
    }

    /**
     * @return float
     */
    public function getSoldGoodsCostPerStaff(): float
    {
        return $this->getValuePerStaff($this->getTotalCog());
    }

    /**
     * @return float
     */
    public function getLabourCostsPerStaff(): float
    {
        return $this->getValuePerStaff($this->getTotalPeople());
    }

    /**
     * @return float
     */
    public function getRentPerStaff(): float
    {
        return $this->getValuePerStaff($this->getTotalRent());
    }

    /**
     * @return float
     */
    public function getOperatingPerStaff(): float
    {
        return $this->getValuePerStaff($this->getTotalOperating());
    }

    /**
     * @return float
     */
    public function getOtherCostsPerStaff(): float
    {
        return $this->getValuePerStaff($this->getTotalOther());
    }

    /**
     * @return float
     */
    public function getTotalAssetsPerStaff(): float
    {
        return $this->getValuePerStaff($this->getTotalAssets());
    }

    /**
     * @return float
     */
    public function getTotalLiabilitiesPerStaff(): float
    {
        return $this->getValuePerStaff($this->getTotalLiabilities());
    }

    /**
     * @return float
     */
    public function getTotalEquityPerStaff(): float
    {
        return $this->getValuePerStaff($this->getTotalEquity());
    }

    // Virtual properties per staff member

    /**
     * @return float
     */
    public function getOperatingProfitPerStaff(): float
    {
        return round($this->getStaffNumber() ? $this->getOperatingProfit() / $this->getStaffNumber() : 0, 2);
    }

    /**
     * @return float
     */
    public function getGrossProfitPerStaff(): float
    {
        return round($this->getStaffNumber() ? $this->getGrossProfit() / $this->getStaffNumber() : 0, 2);
    }

    // Percentage per staff

    /**
     * @return float
     */
    public function getGrossProfitPerStaffPercentage(): float
    {
        return $this->getPercent($this->getGrossProfitPerStaff(), $this->getTotalRevenuePerStaff());
    }

    // Differences

    /**
     * @param AbstractFinance $other
     * @param bool            $perStaff
     * @return float|null
     */
    public function getTotalRevenueDifference(AbstractFinance $other, $perStaff = false) :?float
    {
        if ($perStaff) {
            return $this->getPercentDifference($this->getTotalRevenuePerStaff(), $other->getTotalRevenuePerStaff());
        }

        return $this->getPercentDifference($this->getTotalRevenue(), $other->getTotalRevenue());
    }

    /**
     * @param AbstractFinance $other
     * @param bool            $perStaff
     * @return float|null
     */
    public function getSoldGoodsCostDifference(AbstractFinance $other, $perStaff = false):?float
    {
        if ($perStaff) {
            return $this->getPercentDifference($this->getSoldGoodsCostPerStaff(), $other->getSoldGoodsCostPerStaff());
        }

        return $this->getFieldDiff($this->getSoldGoodsCostPercentage(), $other->getSoldGoodsCostPercentage());
    }

    /**
     * @param AbstractFinance $other
     * @param bool            $perStaff
     * @return float|null
     */
    public function getLabourCostsDifference(AbstractFinance $other, $perStaff = false) :?float
    {
        if ($perStaff) {
            return $this->getPercentDifference($this->getLabourCostsPerStaff(), $other->getLabourCostsPerStaff());
        }

        return $this->getFieldDiff($this->getLabourCostsPercentage(), $other->getLabourCostsPercentage());
    }

    /**
     * @param AbstractFinance $other
     * @param bool            $perStaff
     * @return float|null
     */
    public function getRentDifference(AbstractFinance $other, $perStaff = false) :?float
    {
        if ($perStaff) {
            return $this->getPercentDifference($this->getRentPerStaff(), $other->getRentPerStaff());
        }

        return $this->getFieldDiff($this->getRentPercentage(), $other->getRentPercentage());
    }

    /**
     * @param AbstractFinance $other
     * @param bool            $perStaff
     * @return float|null
     */
    public function getOperatingDifference(AbstractFinance $other, $perStaff = false) :?float
    {
        if ($perStaff) {
            return $this->getPercentDifference($this->getOperatingPerStaff(), $other->getOperatingPerStaff());
        }

        return $this->getFieldDiff($this->getOperatingPercentage(), $other->getOperatingPercentage());
    }

    /**
     * @param AbstractFinance $other
     * @param bool            $perStaff
     * @return float|null
     */
    public function getOtherCostsDifference(AbstractFinance $other, $perStaff = false) :?float
    {
        if ($perStaff) {
            return $this->getPercentDifference($this->getOtherCostsPerStaff(), $other->getOtherCostsPerStaff());
        }

        return $this->getFieldDiff($this->getOtherCostsPercentage(), $other->getOtherCostsPercentage());
    }

    /**
     * @param AbstractFinance $other
     * @param bool            $perStaff
     * @return float|null
     */
    public function getOperatingProfitDifference(AbstractFinance $other, $perStaff = false) :?float
    {
        if ($perStaff) {
            return $this->getPercentDifference(
                $this->getOperatingProfitPerStaff(),
                $other->getOperatingProfitPerStaff()
            );
        }

        return $this->getFieldDiff($this->getOperatingProfitPercentage(), $other->getOperatingProfitPercentage());
    }

    /**
     * @param AbstractFinance $other
     * @param bool            $perStaff
     * @return float|null
     */
    public function getGrossProfitDifference(AbstractFinance $other, $perStaff = false) :?float
    {
        if ($perStaff) {
            return $this->getPercentDifference($this->getGrossProfitPerStaff(), $other->getGrossProfitPerStaff());
        }

        return $this->getFieldDiff($this->getGrossProfitPercentage(), $other->getGrossProfitPercentage());
    }

    /**
     * @param AbstractFinance $other
     * @return float|null
     */
    public function getNetCashDifference(AbstractFinance $other) :?float
    {
        return $this->getPercentDifference($this->getNetCash(), $other->getNetCash());
    }

    /**
     * @param AbstractFinance $other
     * @return float|null
     */
    public function getQuickRatioDifference(AbstractFinance $other) :?float
    {
        return $this->getPercentDifference($this->getQuickRatio(), $other->getQuickRatio());
    }

    /**
     * @param AbstractFinance $other
     * @return float|null
     */
    public function getWorkingCapitalDifference(AbstractFinance $other) :?float
    {
        return $this->getPercentDifference($this->getWorkingCapitalRatio(), $other->getWorkingCapitalRatio());
    }

    /**
     * @param AbstractFinance $other
     * @return float|null
     */
    public function getDebtRatioDifference(AbstractFinance $other) :?float
    {
        return $this->getPercentDifference($this->getDebtRatio(), $other->getDebtRatio());
    }

    /**
     * @param AbstractFinance $other
     * @return float|null
     */
    public function getEquityRatioDifference(AbstractFinance $other) :?float
    {
        return $this->getPercentDifference($this->getEquityRatio(), $other->getEquityRatio());
    }

    /**
     * @param AbstractFinance $other
     * @param int             $multiplier
     * @return float|null
     */
    public function getEstimatedValueDifference(AbstractFinance $other, $multiplier) :?float
    {
        return $this->getPercentDifference(
            $this->getEstimatedValue($multiplier),
            $other->getEstimatedValue($multiplier)
        );
    }

    /**
     * @param int|float $value
     * @param int|float $other
     * @return float
     */
    protected function getFieldDiff($value, $other) : float
    {
        //RDP-1437
        if ($other) {
            $total = (($value - $other) / $other);

            return round($total * 100);
        }

        return $value;
    }

    /**
     * @param float $value
     * @return float
     */
    private function getValuePerStaff($value): float
    {
        if ($this->getStaffNumber() !== 0) {
            return round($value / $this->getStaffNumber(), 2);
        }
        return 0.0;
    }

    /**
     * @param float $value
     * @param float $other
     * @return float|null
     */
    private function getPercentDifference($value, $other):? float
    {
        if ($other !== 0.0) {
            $result = abs(($value - $other) / $other) * 100;

            if ($other > $value) {
                $result *= -1;
            }

            return round($result);
        }
        return null;
    }

    /**
     * @param float $numerator
     * @param float $denominator
     * @return float
     */
    private function getPercent($numerator, $denominator): float
    {
        if ($denominator !== 0) {
            return round(($numerator / $denominator) * 100);
        }

        return 0.0;
    }
}
