<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="\App\Repository\CalculateFinanceMonthRepository")
 * @ORM\Table(name="calculate_finance_month_rolling")
 */


class CalculateFinanceMonth
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $organisation_id;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $revenue_rolling;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $gross_margin_value_rolling;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $gross_margin_percentage_rolling;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $ebitda_value_rolling;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $ebitda_value_percentage_rolling;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $interest_cover_rolling;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $marginal_cashflow_rolling;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $debt_leverage_rolling;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $debitor_days_rolling;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $creditor_days_rolling;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $defence_internal_ratio_rolling;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $expense_cover_ratio_rolling;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $operating_cost_ratio_rolling;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $people_cost_ratio_rolling;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $kbDefensiveInterval;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $kbExpenseCoverRatio;

    /**
     * @ORM\Column(type="datetime")
     */
    private $period_start;

    /**
     * @ORM\Column(type="datetime")
     */
    private $period_end;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrganisationId(): ?int
    {
        return $this->organisation_id;
    }

    public function setOrganisationId(int $organisation_id): self
    {
        $this->organisation_id = $organisation_id;

        return $this;
    }

    public function getRevenueRolling()
    {
        return $this->revenue_rolling;
    }

    public function setRevenueRolling($revenue_rolling): self
    {
        $this->revenue_rolling = $revenue_rolling;

        return $this;
    }

    public function getGrossMarginValueRolling()
    {
        return $this->gross_margin_value_rolling;
    }

    public function setGrossMarginValueRolling($gross_margin_value_rolling): self
    {
        $this->gross_margin_value_rolling = $gross_margin_value_rolling;

        return $this;
    }

    public function getGrossMarginPercentageRolling()
    {
        return $this->gross_margin_percentage_rolling;
    }

    public function setGrossMarginPercentageRolling($gross_margin_percentage_rolling): self
    {
        $this->gross_margin_percentage_rolling = $gross_margin_percentage_rolling;

        return $this;
    }

    public function getEbitdaValueRolling()
    {
        return $this->ebitda_value_rolling;
    }

    public function setEbitdaValueRolling($ebitda_value_rolling): self
    {
        $this->ebitda_value_rolling = $ebitda_value_rolling;

        return $this;
    }

    public function getEbitdaValuePercentageRolling()
    {
        return $this->ebitda_value_percentage_rolling;
    }

    public function setEbitdaValuePercentageRolling($ebitda_value_percentage_rolling): self
    {
        $this->ebitda_value_percentage_rolling = $ebitda_value_percentage_rolling;

        return $this;
    }

    public function getInterestCoverRolling()
    {
        return $this->interest_cover_rolling;
    }

    public function setInterestCoverRolling($interest_cover_rolling): self
    {
        $this->interest_cover_rolling = $interest_cover_rolling;

        return $this;
    }

    public function getMarginalCashflowRolling()
    {
        return $this->marginal_cashflow_rolling;
    }

    public function setMarginalCashflowRolling($marginal_cashflow_rolling): self
    {
        $this->marginal_cashflow_rolling = $marginal_cashflow_rolling;

        return $this;
    }

    public function getDebtLeverageRolling()
    {
        return $this->debt_leverage_rolling;
    }

    public function setDebtLeverageRolling($debt_leverage_rolling): self
    {
        $this->debt_leverage_rolling = $debt_leverage_rolling;

        return $this;
    }

    public function getDebitorDaysRolling()
    {
        return $this->debitor_days_rolling;
    }

    public function setDebitorDaysRolling($debitor_days_rolling): self
    {
        $this->debitor_days_rolling = $debitor_days_rolling;

        return $this;
    }

    public function getCreditorDaysRolling()
    {
        return $this->creditor_days_rolling;
    }

    public function setCreditorDaysRolling($creditor_days_rolling): self
    {
        $this->creditor_days_rolling = $creditor_days_rolling;

        return $this;
    }

    public function getDefenceInternalRatioRolling()
    {
        return $this->defence_internal_ratio_rolling;
    }

    public function setDefenceInternalRatioRolling($defence_internal_ratio_rolling): self
    {
        $this->defence_internal_ratio_rolling = $defence_internal_ratio_rolling;

        return $this;
    }

    public function getExpenseCoverRatioRolling()
    {
        return $this->expense_cover_ratio_rolling;
    }

    public function setExpenseCoverRatioRolling($expense_cover_ratio_rolling): self
    {
        $this->expense_cover_ratio_rolling = $expense_cover_ratio_rolling;

        return $this;
    }

    public function getOperatingCostRatioRolling()
    {
        return $this->operating_cost_ratio_rolling;
    }

    public function setOperatingCostRatioRolling($operating_cost_ratio_rolling): self
    {
        $this->operating_cost_ratio_rolling = $operating_cost_ratio_rolling;

        return $this;
    }

    public function getPeopleCostRatioRolling()
    {
        return $this->people_cost_ratio_rolling;
    }

    public function setPeopleCostRatioRolling($people_cost_ratio_rolling): self
    {
        $this->people_cost_ratio_rolling = $people_cost_ratio_rolling;

        return $this;
    }

    public function getPeriodStart(): ?\DateTimeInterface
    {
        return $this->period_start;
    }

    public function setPeriodStart(\DateTimeInterface $period_start): self
    {
        $this->period_start = $period_start;

        return $this;
    }

    public function getPeriodEnd(): ?\DateTimeInterface
    {
        return $this->period_end;
    }

    public function setPeriodEnd(\DateTimeInterface $period_end): self
    {
        $this->period_end = $period_end;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getKbDefensiveInterval()
    {
        return $this->kbDefensiveInterval;
    }

    /**
     * @param mixed $kbDefensiveInterval
     */
    public function setKbDefensiveInterval($kbDefensiveInterval): void
    {
        $this->kbDefensiveInterval = $kbDefensiveInterval;
    }

    /**
     * @return mixed
     */
    public function getKbExpenseCoverRatio()
    {
        return $this->kbExpenseCoverRatio;
    }

    /**
     * @param mixed $kbExpenseCoverRatio
     */
    public function setKbExpenseCoverRatio($kbExpenseCoverRatio): void
    {
        $this->kbExpenseCoverRatio = $kbExpenseCoverRatio;
    }
}
