<?php declare(strict_types = 1);


namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * IMPORANT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *
 * do not add any associations with this entity,
 * this entity is only for holding nznb company data.
 * and those data may be removed in future.
 *
 *
 * *******/

/**
 * @ORM\Entity(repositoryClass="App\Repository\NZBNCompanyRepository")
 * @ORM\Table(name="nzbn_company")
 * @ORM\Table(indexes={@ORM\Index(columns={"name"}, flags={"fulltext"})},
 *            uniqueConstraints={@ORM\UniqueConstraint(name="nzbn_idx", columns={"nzbn"})})
 */
class NZBNCompany
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /** @ORM\Column(type="string", length=512) ***/
    protected $name;

    /** @ORM\Column(type="string", length=16) ***/
    protected $nzbn;
    /** @ORM\Column(type="string", length=64) ***/
    protected $companyNumber;
    /** @ORM\Column(type="string", length=16) ***/
    protected $sourceRegister;

    /** @ORM\Column(type="boolean") ***/
    protected $status = false;
    /** @ORM\Column(type="string", length=128) ***/
    protected $countryOfOrigin;

    /** @ORM\Column(type="datetime") ****/
    protected $created;

    /** @ORM\Column(type="datetime") ****/
    protected $registrationDate;

    /** @ORM\Column(type="string", length=64) ***/
    protected $classificationCode;
    /** @ORM\Column(type="json", nullable=true) **/
    protected $jsonDirectors;

    /** @ORM\Column(type="text") ***/
    protected $classificationDescription;

    public function __construct()
    {
        $this->setCreated(new DateTime());
    }
    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created): void
    {
        $this->created = $created;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getNzbn()
    {
        return $this->nzbn;
    }

    /**
     * @param mixed $nzbn
     */
    public function setNzbn($nzbn): void
    {
        $this->nzbn = $nzbn;
    }

    /**
     * @return mixed
     */
    public function getCompanyNumber()
    {
        return $this->companyNumber;
    }

    /**
     * @param mixed $companyNumber
     */
    public function setCompanyNumber($companyNumber): void
    {
        $this->companyNumber = $companyNumber;
    }

    /**
     * @return mixed
     */
    public function getSourceRegister()
    {
        return $this->sourceRegister;
    }

    /**
     * @param mixed $sourceRegister
     */
    public function setSourceRegister($sourceRegister): void
    {
        $this->sourceRegister = $sourceRegister;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCountryOfOrigin()
    {
        return $this->countryOfOrigin;
    }

    /**
     * @param mixed $countryOfOrigin
     */
    public function setCountryOfOrigin($countryOfOrigin): void
    {
        $this->countryOfOrigin = $countryOfOrigin;
    }

    /**
     * @return DateTime
     */
    public function getRegistrationDate(): DateTime
    {
        return $this->registrationDate;
    }

    /**
     * @param DateTime $registrationDate
     */
    public function setRegistrationDate(DateTime $registrationDate): void
    {
        $this->registrationDate = $registrationDate;
    }

    /**
     * @return mixed
     */
    public function getClassificationCode()
    {
        return $this->classificationCode;
    }

    /**
     * @param mixed $classificationCode
     */
    public function setClassificationCode($classificationCode): void
    {
        $this->classificationCode = $classificationCode;
    }

    /**
     * @return mixed
     */
    public function getClassificationDescription()
    {
        return $this->classificationDescription;
    }

    /**
     * @param mixed $classificationDescription
     */
    public function setClassificationDescription($classificationDescription): void
    {
        $this->classificationDescription = $classificationDescription;
    }

    /**
     * @return mixed
     */
    public function getJsonDirectors()
    {
        return $this->jsonDirectors;
    }

    /**
     * @param mixed $jsonDirectors
     */
    public function setJsonDirectors($jsonDirectors): void
    {
        $this->jsonDirectors = $jsonDirectors;
    }
}
