<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\CalculateFinanceYearlyRepository")
 * @ORM\Table(name="calculate_finance_yearly")
 */

class CalculateFinanceYearly
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $organisation_id;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $revenue_growth_yearly;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=7, nullable=true)
     */
    private $stock_days_yearly;

    /**
     * @ORM\Column(type="datetime")
     */
    private $period_start;

    /**
     * @ORM\Column(type="datetime")
     */
    private $period_end;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrganisationId(): ?int
    {
        return $this->organisation_id;
    }

    public function setOrganisationId(?int $organisation_id): self
    {
        $this->organisation_id = $organisation_id;

        return $this;
    }

    public function getRevenueGrowthYearly()
    {
        return $this->revenue_growth_yearly;
    }

    public function setRevenueGrowthYearly($revenue_growth_yearly): self
    {
        $this->revenue_growth_yearly = $revenue_growth_yearly;

        return $this;
    }

    public function getStockDaysYearly()
    {
        return $this->stock_days_yearly;
    }

    public function setStockDaysYearly($stock_days_yearly): self
    {
        $this->stock_days_yearly = $stock_days_yearly;

        return $this;
    }

    public function getPeriodStart(): ?\DateTimeInterface
    {
        return $this->period_start;
    }

    public function setPeriodStart(\DateTimeInterface $period_start): self
    {
        $this->period_start = $period_start;

        return $this;
    }

    public function getPeriodEnd(): ?\DateTimeInterface
    {
        return $this->period_end;
    }

    public function setPeriodEnd(\DateTimeInterface $period_end): self
    {
        $this->period_end = $period_end;

        return $this;
    }

}
