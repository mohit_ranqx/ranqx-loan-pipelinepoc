<?php declare(strict_types = 1);


namespace App\Decision;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class BadBureauScore
 * @package App\Decision
 */
class BadBureauScore extends Policy implements IRule
{
    /**
     * @var mixed $data
     */
    private $data;
    /**
     * @var ExpressionLanguage
     */
    private $expression;

    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_008';

    private $refId = 'BB02';

    /**
     * BadBureauScore constructor.
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->expression = new ExpressionLanguage();
    }

    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $ret250 = $this->expression->evaluate(
            'bad_bureau_score_250 ' . $this->getExpression('bad_bureau_score_250'),
            [
                'bad_bureau_score_250' => $this->data,
            ]
        );


        if ($ret250) {
            return [
                'result' => Policy::DECLINE,
                'reason' => $this->getReason('bad_bureau_score', '250'),
                'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
            ];
        }

        return ['result' => Policy::PASS,
            'reason' => $this->getReason('bad_bureau_score', '250_pass'),
            'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
    }

    /**
     * @param string $name
     * @return string
     */
    protected function getExpression(string $name): string
    {
        $conditions = $this->getBankConditions();
        if ('bad_bureau_score_250' === $name) {
            return $conditions[$name] ?? '< 250';
        }

        if ('bad_bureau_score_350' === $name) {
            return $conditions[$name] ?? '< 350';
        }

        return '< 250';
    }

    public function dataMissing(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Bad Bureau Score data is missing.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }

    public function dataIsBad(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Bad Bureau Score data has bad format',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }
}