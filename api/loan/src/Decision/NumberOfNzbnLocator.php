<?php declare(strict_types = 1);


namespace App\Decision;


use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class NumberOfNzbnLocator
 * @package App\Decision
 */
class NumberOfNzbnLocator implements IDecisionLocator
{
    /**
     * @param array $dataSrc
     * @return IRule
     * @throws KiwiBankDataSrcNoExistException
     * @throws KiwiBankDataSrcBadException
     */
    public function findData(array $dataSrc): IRule
    {
        if (isset($dataSrc['nzbn'])) {
            if (!is_numeric($dataSrc['nzbn'])) {
                $obj = new NumberOfNzbn($dataSrc['nzbn']);
                throw new KiwiBankDataSrcBadException(
                    $obj,
                    ($obj->dataIsBad())['reason']
                );
            }
            return new NumberOfNzbn($dataSrc['nzbn']);
        }

        $obj = new NumberOfNzbn(null);
        throw new KiwiBankDataSrcNoExistException(
            $obj,
            ($obj->dataMissing())['reason']
        );
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Multiple Applications';
    }
}