<?php declare(strict_types = 1);


namespace App\Decision;

use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class AgeOfCompanyLocator
 * @package App\Decision
 */
class AgeOfCompanyLocator implements IDecisionLocator
{

    /**
     * @param array $dataSrc
     * @return IRule
     * @throws KiwiBankDataSrcNoExistException
     */
    public function findData(array $dataSrc): IRule
    {
        if (isset($dataSrc['equiFax']['decision']['companyDetails']['ageOfCompany'])) {
            return new AgeOfCompany((string)$dataSrc['equiFax']['decision']['companyDetails']['ageOfCompany']);
        }
        $obj = new AgeOfCompany('');
        throw new KiwiBankDataSrcNoExistException(
            $obj,
            ($obj->dataMissing())['reason']
        );
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Registration Date';
    }
}