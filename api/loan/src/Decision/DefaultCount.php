<?php declare(strict_types = 1);


namespace App\Decision;


use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class DefaultCount
 * @package App\Decision
 */
class DefaultCount extends Policy implements IRule
{
    /**
     * @var mixed $data
     */
    private $data;

    /**
     * @var ExpressionLanguage
     */
    private $expression;

    /**
     * @var string
     */
    private $refId = 'DE01';

    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_014';

    /**
     * DefaultCount constructor.
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->expression = new ExpressionLanguage();
    }

    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $ret = $this->expression->evaluate(
            'data ' . $this->getExpression(),
            [
                'data' => $this->data
            ]
        );

        if ($ret) {
            return ['result' => Policy::REFER,
                'reason' => $this->getReason('defaults_count'),
                'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
        }
        return ['result' => Policy::PASS, 'reason' => $this->getReason('defaults_count', 'negative'),
            'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
    }

    /**
     * @return string
     */
    protected function getExpression(): string
    {
        $conditions = $this->getBankConditions();
        return $conditions['defaults_count'] ?? '>= 1';
    }

    public function dataMissing(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Default Count data is missing.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }

    public function dataIsBad(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Default Count data has bad format.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }


}