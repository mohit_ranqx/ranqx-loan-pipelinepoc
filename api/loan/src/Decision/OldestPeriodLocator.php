<?php declare(strict_types = 1);


namespace App\Decision;

use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class OldestPeriodLocator
 * @package App\Decision
 */
class OldestPeriodLocator implements IDecisionLocator
{
    /**
     * @param array $dataSrc
     * @return IRule
     * @throws KiwiBankDataSrcNoExistException
     */
    public function findData(array $dataSrc): IRule
    {
        if (isset($dataSrc['xeroData']['allZero'])) {
            return new OldestPeriod($dataSrc['xeroData']);
        }

        $obj = new OldestPeriod([]);
        throw new KiwiBankDataSrcNoExistException(
            $obj,
            ($obj->dataMissing())['reason']
        );
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Age of Xero Data';
    }
}