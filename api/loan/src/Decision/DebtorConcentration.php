<?php declare(strict_types = 1);


namespace App\Decision;

/**
 * Class DebtorConcentration
 * @package App\Decision
 */
class DebtorConcentration extends Policy implements IRule
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var string
     */
    private $refId = 'DC03';

    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_018';


    /**
     * DebtorConcentration constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        if (empty($this->data)) {
            return $this->dataMissing();
        }

        if (!isset($this->data['data'], $this->data['debtorConcentration'])) {
            return $this->dataIsBad();
        }

        if (strtolower($this->data['data']) === 'no') {
            return ['result' => Policy::REFER, 'reason' => 'There are no debtor invoices recorded',
                'RefId' => $this->refId, 'RuleValue' => $this->data['data'], 'InternalId' => $this->internalId];
        }

        $number = (float)$this->data['debtorConcentration'];
        if (($number - 0.0) < 0.00000001) {
            return ['result' => Policy::REFER, 'reason' => 'The total value of all invoices is zero',
                'RefId' => $this->refId, 'RuleValue' => $number, 'InternalId' => $this->internalId];
        }

        return ['result' => Policy::PASS, 'reason' => 'Invoice total > $0',
            'RefId' => $this->refId, 'RuleValue' => $number, 'InternalId' => $this->internalId];
    }

    public function dataMissing(): array
    {
        return ['result' => Policy::REFER,
            'reason' => 'Debtor Concentration has no value',
            'RefId' => $this->refId, 'RuleValue' => '', 'InternalId' => $this->internalId];
    }

    public function dataIsBad(): array
    {
        return ['result' => Policy::REFER, 'reason' => 'Debtor Concentration has no valid data.',
            'RefId' => $this->refId, 'RuleValue' => '', 'InternalId' => $this->internalId];
    }


}