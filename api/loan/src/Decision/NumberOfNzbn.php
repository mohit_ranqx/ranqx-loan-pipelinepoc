<?php declare(strict_types = 1);


namespace App\Decision;


use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class NumberOfNzbn
 * @package App\Decision
 *
 * KB_US_PR_002
 * https://ranqxltd.atlassian.net/wiki/spaces/LO/pages/651886596/Ranqx+Decision+Engine
 */
class NumberOfNzbn extends Policy implements IRule
{
    /**
     * @var mixed
     */
    private $counter;
    /**
     * @var ExpressionLanguage
     */
    protected $expression;
    /**
     * @var string
     */
    private $refId = 'MA01';
    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_002';

    /**
     * NumberOfNzbn constructor.
     * @param mixed $counter
     */
    public function __construct($counter)
    {
        $this->counter = $counter;
        $this->expression = new ExpressionLanguage();
    }
    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $ret = $this->expression->evaluate(
            'counter ' . $this->getExpression(),
            [
                'counter' => $this->counter
            ]
        );

        if ($ret) {
            return ['result' => Policy::REFER, 'reason' => $this->getReason('multiple_applications'),
                'RefId' => $this->refId, 'RuleValue' => $this->counter, 'InternalId' => $this->internalId];
        }
        return ['result' => Policy::PASS, 'reason' => $this->getReason('multiple_applications', 'negative'),
            'RefId' => $this->refId, 'RuleValue' => $this->counter, 'InternalId' => $this->internalId];
    }

    /**
     * @return string
     */
    protected function getExpression(): string
    {
        $conditions = $this->getBankConditions();
        return $conditions['multiple_applications'] ?? '>= 2';
    }

    public function dataMissing(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Multiple Applications data is missing.',
            'RefId' => $this->refId,'RuleValue' => $this->counter, 'InternalId' => $this->internalId
        ];
    }

    public function dataIsBad(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Multiple Applications data has bad format.',
            'RefId' => $this->refId,'RuleValue' => $this->counter, 'InternalId' => $this->internalId
        ];
    }

}