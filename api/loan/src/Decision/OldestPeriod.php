<?php declare(strict_types = 1);


namespace App\Decision;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class OldestPeriod
 * @package App\Decision
 *
 * KB_US_PR_003
 * Check the oldest period in the finance_month table for the current application.
   * If there are zeros in ALL the columns for the period, this means we don't have 24 months worth of data.
 */
class OldestPeriod extends Policy implements IRule
{

    /**
     * @var array
     */
    private $data;
    /**
     * @var ExpressionLanguage
     */
    protected $expression;
    /**
     * @var string
     */
    private $refId = 'AX02';
    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_003';

    /**
     * OldestPeriod constructor.
     * @param array $oldestFinanceMonth
     */
    public function __construct(array $oldestFinanceMonth)
    {
        $this->data = $oldestFinanceMonth;
        $this->expression = new ExpressionLanguage();
    }

    /**
     * @return int
     */
    public function policyRule(): int
    {
        if (isset($this->data['oldest']['period'])) {
            unset($this->data['oldest']['period']);
        }
        if (isset($this->data['oldest']['staffNumber'])) {
            unset($this->data['oldest']['staffNumber']);
        }

        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $allZero = $this->data['allZero'] === 'true';
        $ret = $this->expression->evaluate(
            'number ' . $this->getExpression(),
            [
                'number' => $this->data['total']
            ]
        );

        if ($ret) {
            return ['result' => Policy::REFER, 'reason' => $this->getReason('age_of_xero_data'),
                'RefId' => $this->refId, 'RuleValue' => $this->data['total'], 'InternalId' => $this->internalId];
        }
        if ($allZero) {
            return ['result' => Policy::REFER, 'reason' => $this->getReason('age_of_xero_data', 'allZero'),
                'RefId' => $this->refId, 'RuleValue' => $this->data['total'], 'InternalId' => $this->internalId];
        }

        return ['result' => Policy::PASS, 'reason' => $this->getReason('age_of_xero_data', 'negative'),
            'RefId' => $this->refId, 'RuleValue' => $this->data['total'], 'InternalId' => $this->internalId];
    }

    /**
     * @return string
     */
    protected function getExpression(): string
    {
        $conditions = $this->getBankConditions();
        return $conditions['age_of_xero_data'] ?? '< 24';
    }

    public function dataMissing(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Age of Xero Data data is missing.',
            'RefId' => $this->refId,'RuleValue' => '', 'InternalId' => $this->internalId
        ];
    }

    public function dataIsBad(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Age of Xero Data data has bad format.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }
}