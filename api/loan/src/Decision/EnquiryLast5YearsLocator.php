<?php declare(strict_types = 1);


namespace App\Decision;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class EnquiryLast5YearsLocator
 * @package App\Decision
 */
class EnquiryLast5YearsLocator implements IDecisionLocator
{

    /**
     * @param array $dataSrc
     * @return IRule
     * @throws KiwiBankDataSrcNoExistException
     * @throws KiwiBankDataSrcBadException
     */
    public function findData(array $dataSrc): IRule
    {
        if (isset($dataSrc['equiFax']['decision']['enquiryHistory']['enquiryTotalsLast5Years']['count'])) {
            $data = $dataSrc['equiFax']['decision']['enquiryHistory']['enquiryTotalsLast5Years']['count'];
            if (!is_numeric($data)) {
                $obj = new EnquiryLast5Years($data);
                throw new KiwiBankDataSrcBadException(
                    $obj,
                    ($obj->dataIsBad())['reason']
                );
            }
            return new EnquiryLast5Years($data);
        }

        $obj = new EnquiryLast5Years(null);
        throw new KiwiBankDataSrcNoExistException(
            $obj,
            ($obj->dataMissing())['reason']
        );
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Enquiry Totals Last 5 Years';
    }
}