<?php declare(strict_types = 1);


namespace App\Decision;

/**
 * Class LoanAmount
 * @package App\Decision
 */
class LoanAmount extends Policy implements IRule
{
    /**
     * @var mixed $data
     */
    private $data;

    /**
     * @var string
     */
    private $refId = 'LA01';

    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_019';

    /**
     * LoanAmount constructor.
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        if ($this->data > 50000) {
            return ['result' => Policy::REFER,
                'reason' => 'Loan Amount > $50,000',
                'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
        }
        return ['result' => Policy::PASS,
            'reason' => 'Loan Amount <= $50,000',
            'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
    }

    /**
     * @return array
     */
    public function dataMissing(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Loan Amount data is missing.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }

    /**
     * @return array
     */
    public function dataIsBad(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Loan Amount data has bad format.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }
}