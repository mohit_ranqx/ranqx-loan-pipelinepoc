<?php declare(strict_types = 1);


namespace App\Decision;

use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class ExcludedIndustryLocator
 * @package App\Decision
 */
class ExcludedIndustryLocator implements IDecisionLocator
{
    /**
     * @param array $dataSrc
     * @return IRule
     * @throws KiwiBankDataSrcNoExistException
     */
    public function findData(array $dataSrc): IRule
    {
        if (isset($dataSrc['equiFax']['decision']['companyDetails']['industryCode'])) {
            if ($dataSrc['equiFax']['decision']['companyDetails']['industryCode'] === null ||
                trim($dataSrc['equiFax']['decision']['companyDetails']['industryCode']) === '') {
                $obj = new ExcludedIndustry('');
                throw new KiwiBankDataSrcNoExistException(
                    $obj,
                    ($obj->dataMissing())['reason']
                );
            }

            return new ExcludedIndustry((string)$dataSrc['equiFax']['decision']['companyDetails']['industryCode']);
        }

        $obj = new ExcludedIndustry('');
        throw new KiwiBankDataSrcNoExistException(
            $obj,
            ($obj->dataMissing())['reason']
        );
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Excluded Industry';
    }
}