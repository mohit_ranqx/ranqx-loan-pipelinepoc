<?php declare(strict_types = 1);


namespace App\Decision;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class ExcessiveRecentEnquiriesLocator
 * @package App\Decision
 */
class ExcessiveRecentEnquiriesLocator implements IDecisionLocator
{

    /**
     * @param array $dataSrc
     * @return IRule
     * @throws KiwiBankDataSrcNoExistException
     * @throws KiwiBankDataSrcBadException
     */
    public function findData(array $dataSrc): IRule
    {
        if (isset($dataSrc['equiFax']['decision']['enquiryHistory']['totalEnquires'])) {
            $data = $dataSrc['equiFax']['decision']['enquiryHistory']['totalEnquires'];
            if (!is_numeric($data)) {
                $obj = new ExcessiveRecentEnquiries($data);
                throw new KiwiBankDataSrcBadException(
                    $obj,
                    ($obj->dataIsBad())['reason']
                );
            }
            return new ExcessiveRecentEnquiries($data);
        }
        $obj = new ExcessiveRecentEnquiries(null);
        throw new KiwiBankDataSrcNoExistException(
            $obj,
            ($obj->dataMissing())['reason']
        );
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Excessive Recent Enquiries';
    }
}