<?php declare(strict_types = 1);


namespace App\Decision;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class CourtJudgements
 * @package App\Decision
 */
class CourtJudgements extends Policy implements IRule
{
    /**
     * @var mixed $data
     */
    private $data;
    /**
     * @var ExpressionLanguage
     */
    private $expression;

    /**
     * @var string
     */
    private $refId = 'CJ01';

    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_015';

    /**
     * AgeOfCompany constructor.
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->expression = new ExpressionLanguage();
    }

    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $ret = $this->expression->evaluate(
            'data ' . $this->getExpression(),
            [
                'data' => $this->data,
            ]
        );

        if ($ret) {
            return ['result' => Policy::REFER,
                'reason' => $this->getReason('court_judgements'),
                'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
        }
        return ['result' => Policy::PASS,
            'reason' => $this->getReason('court_judgements', 'negative'),
            'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
    }

    /**
     * @return string
     */
    protected function getExpression(): string
    {
        $conditions = $this->getBankConditions();
        return $conditions['court_judgements'] ?? '>= 1';
    }

    public function dataMissing(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Court Judgements data is missing.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }

    public function dataIsBad(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Court Judgements data is not a number.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }


}