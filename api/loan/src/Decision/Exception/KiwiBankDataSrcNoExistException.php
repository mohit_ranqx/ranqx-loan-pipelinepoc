<?php declare(strict_types = 1);


namespace App\Decision\Exception;

use App\Decision\Policy;
use Symfony\Component\Messenger\Exception\InvalidArgumentException;
use Throwable;

class KiwiBankDataSrcNoExistException extends InvalidArgumentException
{
    private $policy;
    public function __construct(Policy $policy, $message = '', $code = 0, Throwable $previous = null)
    {
        $this->policy = $policy;
        parent::__construct($message, $code, $previous);
    }

    public function getPolicyInformation(): array
    {
        return $this->policy->dataMissing();
    }
}