<?php declare(strict_types = 1);


namespace App\Decision;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class InterestCoverCurrentLocator
 * @package App\Decision
 */
class InterestCoverCurrentLocator implements IDecisionLocator
{
    /**
     * @param array $dataSrc
     * @return IRule
     * @throws KiwiBankDataSrcNoExistException
     */
    public function findData(array $dataSrc): IRule
    {
        //calculate_finance_month_current.interest_cover_current column
        if (isset($dataSrc['calFinanceCurrentRolling'][0]['interest_cover_rolling'])) {
            $data = $dataSrc['calFinanceCurrentRolling'][0]['interest_cover_rolling'];
            if (!is_numeric($data)) {
                $obj = new InterestCoverCurrent($data);
                throw new KiwiBankDataSrcBadException(
                    $obj,
                    ($obj->dataIsBad())['reason']
                );
            }
            return new InterestCoverCurrent((float)$data);
        }

        $obj = new InterestCoverCurrent(null);
        throw new KiwiBankDataSrcNoExistException(
            $obj,
            ($obj->dataMissing())['reason']
        );
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Interest Cover Current';
    }

}