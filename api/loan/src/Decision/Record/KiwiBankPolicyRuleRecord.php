<?php declare(strict_types = 1);


namespace App\Decision\Record;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\IDecisionLocator;
use App\Decision\IRule;
use App\Decision\Policy;
use App\Decision\DecisionDataLocatorChain;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use App\Decision\PolicyResult;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class KiwiBankPolicyRuleRecord
 * @package App\Decision\Record
 */
class KiwiBankPolicyRuleRecord
{
    use PolicyResult;

    /**
     * @var array
     */
    private $dataSrc;
    /**
     * @var mixed
     */
    private $data;
    /**
     * @var DecisionDataLocatorChain
     */
    private $locators;
    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * KiwiBankPolicyRuleRecord constructor.
     * @param array $dataSrc
     * @param DecisionDataLocatorChain $locatorChain
     * @param ParameterBagInterface $bag
     */
    public function __construct(
        array $dataSrc,
        DecisionDataLocatorChain $locatorChain,
        ParameterBagInterface $bag
    ) {
        $this->dataSrc = $dataSrc;
        $this->locators = $locatorChain;
        $this->params = $bag;
    }

    /**
     * @return array
     */
    public function policyResult(): array
    {
        $records = [];

        /** @var IDecisionLocator $locator ***/
        foreach ($this->locators->getLocators() as $locator) {
            try {
                $src = $locator->getDecisionClass();
                $decision = $locator->findData($this->dataSrc);
                /** @var Policy $decision */
                $decision->setConfig($this->params);

                /** @var IRule $decision */
                $result = $decision->policyRule();
                $r  = $decision->toArray();
                $id = $r['InternalId'];

                $records[$id]['decision'] = $this->resultToString($result);
                $records[$id]['reason'] = $r['reason'] ?? '';
                $records[$id]['RefId'] = $r['RefId'] ?? '';
                $records[$id]['RuleValue'] = $r['RuleValue'] ?? '';
                $records[$id]['InternalId'] = $r['InternalId'] ?? '';
                $records[$id]['name'] = $src;

            } catch (KiwiBankDataSrcNoExistException $e) {
                $src = $locator->getDecisionClass();
                $info = $e->getPolicyInformation();
                $info['name'] = $src;
                $info['decision'] = $this->resultToString($info['result']);
                $id = $info['InternalId'];
                unset($info['result']);
                $records[$id] = $info;
            } catch (KiwiBankDataSrcBadException $e) {
                $src = $locator->getDecisionClass();
                $info = $e->getPolicyInformation();
                $info['name'] = $src;
                $info['decision'] = $this->resultToString($info['result']);
                $id = $info['InternalId'];
                unset($info['result']);
                $records[$id] = $info;
            }
        }

        return $records;
    }
}