<?php declare(strict_types = 1);


namespace App\Decision;


/**
 * Interface IDecisionLocator
 * @package App\Decision
 */
interface IDecisionLocator
{
    /**
     * @param array $dataSrc
     * @return IRule
     */
    public function findData(array $dataSrc) : IRule;
    public function getDecisionClass(): string;
}