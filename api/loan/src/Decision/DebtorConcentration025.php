<?php declare(strict_types = 1);


namespace App\Decision;

/**
 * Class DebtorConcentration025
 * @package App\Decision
 */
class DebtorConcentration025 extends Policy implements IRule
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var string
     */
    private $refId = 'DC02';

    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_007';

    /**
     * DebtorConcentration025 constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        if (empty($this->data)) {
            return $this->dataMissing();
        }

        if (!isset($this->data['data']) || !isset($this->data['debtorConcentration'])) {
            return $this->dataIsBad();
        }

        if (strtolower($this->data['data']) === 'no') {
            return ['result' => Policy::REFER, 'reason' => 'There are no debtor invoices recorded',
                'RefId' => $this->refId, 'RuleValue' => $this->data['data'], 'InternalId' => $this->internalId];
        }

        $number = (float)$this->data['debtorConcentration'];

        if ($number >= 0.25) {
            return ['result' => Policy::REFER, 'reason' => 'Company\'s Revenue by Debtor >=0.25',
                'RefId' => $this->refId, 'RuleValue' => $number, 'InternalId' => $this->internalId];
        }

        return ['result' => Policy::PASS, 'reason' => 'Invoice total < 0.25',
            'RefId' => $this->refId, 'RuleValue' => $number, 'InternalId' => $this->internalId];
    }

    /**
     * @return array
     */
    public function dataMissing(): array
    {
        return ['result' => Policy::REFER,
            'reason' => 'Debtor Concentration has no data',
            'RefId' => $this->refId, 'RuleValue' => '', 'InternalId' => $this->internalId];
    }

    /**
     * @return array
     */
    public function dataIsBad(): array
    {
        $value = 'bad data';

        return ['result' => Policy::REFER, 'reason' => 'Debtor Concentration has no valid data.',
            'RefId' => $this->refId, 'RuleValue' => $value, 'InternalId' => $this->internalId];
    }


}