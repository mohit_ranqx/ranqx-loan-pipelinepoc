<?php declare(strict_types = 1);

namespace App\Decision;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class ExcludedIndustry
 * @package App\Decision
 */
class ExcludedIndustry extends Policy implements IRule
{
    /**
     * @var string $data
     */
    private $data;

    /**
     * @var ExpressionLanguage
     */
    private $expression;

    /**
     * @var string
     */
    private $refId = 'EI02';

    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_017';

    /**
     * DefaultCount constructor.
     * @param string $data
     */
    public function __construct(string $data)
    {
        $this->data = $data;
        $this->expression = new ExpressionLanguage();
    }

    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        //KB_US_PR_017
        if (!$this->data) {
            return $this->dataMissing();
        }

        //excluded_industry

        /*$pattern = "%^C111|";
        $pattern .= '^L671150|';
        $pattern .= '^A04|';
        $pattern .= '^A03|';
        $pattern .= '^E3211|';
        $pattern .= '^L6632|';
        $pattern .= '^H440055|';
        $pattern .= '^J5514|';
        $pattern .= '^C161|';
        $pattern .= '^J541|';
        $pattern .= '^K622|';
        $pattern .= '^R920|';
        $pattern .= '^D261110|';
        $pattern .= '^A015985|';
        $pattern .= '^B0700|';
        $pattern .= '^S953410';
        $pattern .= '%ism';*/


        $pattern = $this->pattern();
        if( preg_match($pattern, $this->data, $m) ) {
            $key = $m[0];
            //farming and tobacco farming share same prefix
            return ['result' => Policy::DECLINE,
                'reason' => $this->getReason('excluded_industry',$key) . ' Industry code is one of the industries that our bank will not lend to',
                'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
        }
        return ['result' => Policy::PASS,
            'reason' => 'Industry code is not in exclude list.',
            'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
    }

    /**
     * @return array
     */
    protected function getExpression(): array
    {
        $conditions = $this->getBankConditions();
        return $conditions['excluded_industry'] ?? [];
    }

    /**
     * @return string
     */
    private function pattern()
    {
        $pa = $this->getExpression();

        $str = '%';
        foreach ($pa as $p) {
            $str .= $p .'|';
        }
        $str = rtrim($str, '|');
        $str .= '%ism';
        return $str;
    }

    public function dataMissing(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Excluded Industry data is missing.',
            'RefId' => $this->refId,'RuleValue' => '', 'InternalId' => $this->internalId
        ];
    }

    public function dataIsBad(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Excluded Industry data has bad format.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }
}

/*
 *
 * C111*** ,Meat processing

L671150 , Property Investment

A04**** ,Commercial Fishing

A01**** , Farming

A03**** ,Forestry and Logging

E3211** , Land Development and Subdivision

L6632** ,Video and electronic media rental

H440055 ,Private Hotel – short term accommodation

J5514** , Movie Theatres

C161*** , Printing

J541*** , Publishing

K622*** , Money Markets

R920***, Casino Operation

D261110 , Fossil fuels electricity generation

A015985 , Tobacco Farming

B0700** , Oil and Gas extraction

S953410, Brothel Keeping
 * ******/