<?php declare(strict_types = 1);


namespace App\Decision;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class DebtRatioLocator
 * @package App\Decision
 */
class DebtRatioLocator implements IDecisionLocator
{
    /**
     * @param array $dataSrc
     * @return IRule
     * @throws KiwiBankDataSrcNoExistException
     * @throws KiwiBankDataSrcBadException
     */
    public function findData(array $dataSrc): IRule
    {
        //calculate_finance_month_current.debt_ratio_current column
        if (isset($dataSrc['calFinanceCurrentMonth'][0]['debt_ratio_current'])) {
            $data = $dataSrc['calFinanceCurrentMonth'][0]['debt_ratio_current'];
            if (!is_numeric($data)) {
                throw new KiwiBankDataSrcBadException(
                    new DebtRatioCurrent($data),
                    'Debt Ratio data has bad format.'
                );
            }
            return new DebtRatioCurrent((float)$dataSrc['calFinanceCurrentMonth'][0]['debt_ratio_current']);
        }
        throw new KiwiBankDataSrcNoExistException(
            new DebtRatioCurrent(null),
            'debt_ratio_current field has not been found.'
        );
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Debt Ratio Current';
    }
}