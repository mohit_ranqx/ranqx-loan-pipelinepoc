<?php declare(strict_types = 1);


namespace App\Decision;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class BadBureauScoreLocator
 * @package App\Decision
 */
class BadBureauScoreLocator implements IDecisionLocator
{

    public function findData(array $dataSrc): IRule
    {
        if (isset($dataSrc['equiFax']['decision']['score'][0]['score'])) {
            //Note that the score type we should use is TBC
            $data = $dataSrc['equiFax']['decision']['score'][0]['score'];
            if (!is_numeric($data)) {
                throw new KiwiBankDataSrcBadException(
                    new BadBureauScore($data),
                    'Bad Bureau Score field has bad format.'
                );
            }
            return new BadBureauScore($dataSrc['equiFax']['decision']['score'][0]['score']);
        }

        throw new KiwiBankDataSrcNoExistException(
            new BadBureauScore(null),
            'Bad Bureau Score field is missing.'
        );
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Bad Bureau Score';
    }
}