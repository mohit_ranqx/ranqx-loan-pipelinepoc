<?php declare(strict_types = 1);


namespace App\Decision;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class LoanAmountLocator
 * @package App\Decision
 */
class LoanAmountLocator implements IDecisionLocator
{

    /**
     * @param array $dataSrc
     * @return IRule
     * @throws KiwiBankDataSrcNoExistException
     */
    public function findData(array $dataSrc): IRule
    {
        if (isset($dataSrc['loanAmount'])) {
            $data = $dataSrc['loanAmount'];
            if (!is_numeric($data)) {
                $obj = new LoanAmount($data);
                throw new KiwiBankDataSrcBadException(
                    $obj,
                    ($obj->dataIsBad())['reason']
                );
            }
            return new LoanAmount((int)$dataSrc['loanAmount']);
        }
        $obj = new LoanAmount(null);
        throw new KiwiBankDataSrcNoExistException(
            $obj,
            ($obj->dataMissing())['reason']
        );
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Loan Amount';
    }
}