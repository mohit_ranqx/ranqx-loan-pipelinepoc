<?php declare(strict_types = 1);


namespace App\Decision;

trait PolicyResult
{
    public function resultToString(int $result): string
    {
        switch ($result) {
            case Policy::PASS:
                return 'PASS';
            case Policy::REFER:
                return 'REFER';
            case Policy::DECLINE:
                return 'DECLINE';
            case Policy::NODATA:
                return 'NODATA';
            case Policy::APPROVE:
                return 'APPROVE';
        }
        return '';
    }

    public function fromStringToPolicy(string $result): int
    {
        switch ($result) {
            case 'PASS':
                return Policy::PASS;
            case 'REFER':
                return Policy::REFER;
            case 'DECLINE':
                return Policy::DECLINE;
            case 'No Data':
            case 'NODATA':
                return Policy::NODATA;
            case 'APPROVE':
                return Policy::APPROVE;
        }
        return Policy::NODATA;
    }
}