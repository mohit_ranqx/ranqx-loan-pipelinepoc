<?php declare(strict_types = 1);


namespace App\Decision;

/**
 * Class IndustryCode
 * @package App\Decision
 */
class IndustryCode extends Policy implements IRule
{

    /**
     * @var string
     */
    private $data;

    /**
     * @var string
     */
    private $refId = 'NI01';

    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_001';


    /**
     * IndustryCode constructor.
     * @param string $data
     */
    public function __construct(string $data)
    {
        $this->data = $data;
    }
    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        if ($this->data === '') {
            return $this->dataMissing();
        }
        return ['result' => Policy::PASS, 'reason' => 'Industry Code found against the company record',
            'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
    }

    public function dataMissing(): array
    {
        return ['result' => Policy::REFER, 'reason' => 'Industry Code missing',
            'RefId' => $this->refId, 'RuleValue' => '', 'InternalId' => $this->internalId];
    }

    public function dataIsBad(): array
    {
        return ['result' => Policy::REFER, 'reason' => 'Industry Code has bad format',
            'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
    }
}