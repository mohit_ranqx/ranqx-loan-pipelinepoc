<?php declare(strict_types = 1);


namespace App\Decision;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class BadBureauScore350Locator
 * @package App\Decision
 */
class BadBureauScore350Locator implements IDecisionLocator
{

    /**
     * @param array $dataSrc
     * @return IRule
     * @throws KiwiBankDataSrcBadException
     * @throws KiwiBankDataSrcNoExistException
     */
    public function findData(array $dataSrc): IRule
    {

        if (isset($dataSrc['equiFax']['decision']['score'][0]['score'])) {
            //Note that the score type we should use is TBC
            $data = $dataSrc['equiFax']['decision']['score'][0]['score'];
            if (!is_numeric($data)) {
                $obj = new BadBureauScore350($data);
                throw new KiwiBankDataSrcBadException(
                    $obj,
                    ($obj->dataIsBad())['reason']
                );
            }
            return new BadBureauScore350($data);
        }

        $obj = new BadBureauScore350(null);
        throw new KiwiBankDataSrcNoExistException(
            $obj,
            ($obj->dataMissing())['reason']
        );
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Bad Bureau Score 350';
    }

}