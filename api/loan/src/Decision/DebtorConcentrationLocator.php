<?php declare(strict_types = 1);

namespace App\Decision;

/**
 * Class DebtorConcentrationLocator
 * @package App\Decision
 */
class DebtorConcentrationLocator implements IDecisionLocator
{
    /**
     * @param array $dataSrc
     * @return IRule
     */
    public function findData(array $dataSrc): IRule
    {
        $result = $dataSrc['debtorConcentration'] ?? [];
        return new DebtorConcentration($result);
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Debtor Concentration';
    }
}