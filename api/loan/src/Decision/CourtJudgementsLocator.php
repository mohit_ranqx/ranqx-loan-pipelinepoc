<?php declare(strict_types = 1);


namespace App\Decision;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class CourtJudgementsLocator
 * @package App\Decision
 */
class CourtJudgementsLocator implements IDecisionLocator
{

    /**
     * @param array $dataSrc
     * @return IRule
     * @throws KiwiBankDataSrcBadException
     * @throws KiwiBankDataSrcNoExistException
     */
    public function findData(array $dataSrc): IRule
    {
        if (isset($dataSrc['equiFax']['decision']['summary']['judgementsCount'])) {
            $data = $dataSrc['equiFax']['decision']['summary']['judgementsCount'];
            if (!is_numeric($data)) {
                throw new KiwiBankDataSrcBadException(
                    new CourtJudgements($data),
                    'judgements Count data has bad format'
                );
            }
            return new CourtJudgements($dataSrc['equiFax']['decision']['summary']['judgementsCount']);
        }

        throw new KiwiBankDataSrcNoExistException(
            new CourtJudgements(null),
            'judgements Count data is missing.'
        );
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Judgements Count';
    }

}