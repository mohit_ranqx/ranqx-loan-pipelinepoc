<?php declare(strict_types = 1);


namespace App\Decision;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


/**
 * Class Policy
 * @package App\Decision
 */
abstract class Policy
{
    /**
     *
     */
    public const PASS = 1;
    /**
     *
     */
    public const REFER = 2;
    /**
     *
     */
    public const DECLINE = 3;
    /**
     *
     */
    public const NODATA = 4;
    /**
     *
     */
    public const APPROVE = 5;

    /** @var ParameterBagInterface | null $config ***/
    protected $config;

    /**
     * @return array
     */
    abstract public function toArray(): array;

    /**
     * @param ParameterBagInterface $bag
     */
    public function setConfig(ParameterBagInterface $bag) : void
    {
        $this->config = $bag;
    }

    /**
     * @return array
     */
    public function getBankConditions(): array
    {
        if ($this->config) {
            $decision = $this->config->get('decision');
            $bank = $decision['bank'];
            return $decision['conditions'][$bank];
        }
        return [];
    }

    /**
     * @param string $rule
     * @param string $positiveOrNegative
     * @return string
     */
    public function getReason(string $rule, string $positiveOrNegative = 'positive'): string
    {
        if ($this->config) {
            $decision = $this->config->get('decision');
            $bank = $decision['bank'];
            return $decision['reason'][$bank][$rule][$positiveOrNegative] ?? '';
        }
        return '';
    }

    /**
     * @return array
     */
    abstract public function dataMissing(): array;

    /**
     * @return array
     */
    abstract public function dataIsBad(): array;
}