<?php declare(strict_types = 1);


namespace App\Decision;


/**
 * Class DecisionDataLocatorChain
 * @package App\Decision
 */
class DecisionDataLocatorChain
{
    /**
     * @var IDecisionLocator[] $locators
     */
    private $locators;

    /**
     * DecisionDataLocatorChain constructor.
     */
    public function __construct()
    {
        $this->locators = [];
    }

    /**
     * @param IDecisionLocator $locator
     */
    public function addLocator(IDecisionLocator $locator): void
    {
        $this->locators[] = $locator;
    }


    /**
     * @return IDecisionLocator[]
     */
    public function getLocators(): array
    {
        return $this->locators;
    }
}