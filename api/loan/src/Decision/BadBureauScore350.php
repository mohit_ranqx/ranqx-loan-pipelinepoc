<?php declare(strict_types = 1);

namespace App\Decision;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class BadBureauScore350
 * @package App\Decision
 */
class BadBureauScore350 extends Policy implements IRule
{
    /**
     * @var mixed
     */
    private $data;
    /**
     * @var string
     */
    private $refId = 'BB01';

    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_009';

    /**
     * @var ExpressionLanguage
     */
    private $expression;

    /**
     * BadBureauScore constructor.
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->expression = new ExpressionLanguage();
    }

    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $ret350 = $this->expression->evaluate(
            'bad_bureau_score_350 ' . $this->getExpression('bad_bureau_score_350'),
            [
                'bad_bureau_score_350' => $this->data,
            ]
        );

        if ($ret350) {
            return ['result' => Policy::REFER,
                'reason' => $this->getReason('bad_bureau_score', '350'),
                'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
        } else {
            return ['result' => Policy::PASS,
                'reason' => $this->getReason('bad_bureau_score', 'other'),
                'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
        }
    }

    /**
     * @param string $name
     * @return string
     */
    protected function getExpression(string $name): string
    {
        $conditions = $this->getBankConditions();
        if ('bad_bureau_score_250' === $name) {
            return $conditions[$name] ?? '< 250';
        }

        if ('bad_bureau_score_350' === $name) {
            return $conditions[$name] ?? '< 350';
        }

        return '< 250';
    }

    public function dataMissing(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Bad Bureau Score data is missing.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }

    public function dataIsBad(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Bad Bureau Score data has bad format.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }
}