<?php declare(strict_types = 1);


namespace App\Decision\Rule;


class Revenue
{
    private $data;
    public function __construct(float $data)
    {
        $this->data = $data;
    }

    public function accept($threshHold) : bool
    {
        return $this->data >= $threshHold;
    }
}