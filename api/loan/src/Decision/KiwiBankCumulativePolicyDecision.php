<?php declare(strict_types = 1);


namespace App\Decision;

/**
 * Class KiwiBankCumulativePolicyDecision
 * @package App\Decision
 */
class KiwiBankCumulativePolicyDecision
{
    use PolicyResult;

    /**
     * @var array
     */
    private $data;

    /**
     * KiwiBankCumulativePolicyDecision constructor.
     * @param array $policyData
     */
    public function __construct(array $policyData)
    {
        $this->data = [];
        $this->extractData($policyData);
    }

    private function extractData($policyData)
    {
        foreach ($policyData as $decision) {
            $this->data[] = $this->fromStringToPolicy($decision['decision']);
        }
    }

    /**
     * @return int
     */
    public function make(): int
    {
        if (empty($this->data)) {
            return Policy::REFER;
        }

        if ($this->any(Policy::DECLINE)) {
            return Policy::DECLINE;
        }

        //If there are no DECLINE Policy Rule Results for the Loan Application but there are any REFER results,
        // then set the Cumulative Policy Decision for the Loan Application to REFER.
        if ($this->noDecline() && $this->any(Policy::REFER)) {
            return Policy::REFER;
        }

        //If there are no DECLINE or REFER Policy Rule Results,
        // then set the Cumulative Policy Decision for the Loan Application to PASS

        if($this->noDecline() && !$this->any(Policy::REFER)) {
            return Policy::PASS;
        }

        return Policy::NODATA;
    }

    /**
     * @return bool
     */
    private function noDecline()
    {
        return !$this->any(Policy::DECLINE);
    }

    /**
     * @param int $policyStatus
     * @return bool
     */
    private function any(int $policyStatus)
    {
        return in_array($policyStatus, $this->data);
    }
}