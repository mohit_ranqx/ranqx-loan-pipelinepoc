<?php declare(strict_types = 1);


namespace App\Decision;

use App\Decision\Exception\KiwiBankDataSrcBadException;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class DefaultCountLocator
 * @package App\Decision
 */
class DefaultCountLocator implements IDecisionLocator
{
    /**
     * @param array $dataSrc
     * @return IRule
     * @throws KiwiBankDataSrcNoExistException
     * @throws KiwiBankDataSrcBadException
     */
    public function findData(array $dataSrc): IRule
    {
        if (isset($dataSrc['equiFax']['decision']['summary']['defaultsCount'])) {
            $data = $dataSrc['equiFax']['decision']['summary']['defaultsCount'];
            if (!is_numeric($data)) {
                $obj = new DefaultCount($data);
                throw new KiwiBankDataSrcBadException(
                    $obj,
                    ($obj->dataIsBad())['reason']
                );
            }
            return new DefaultCount($data);
        }
        $obj = new DefaultCount(null);
        throw new KiwiBankDataSrcNoExistException(
            $obj,
            ($obj->dataMissing())['reason']
        );
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Defaults Count';
    }

}