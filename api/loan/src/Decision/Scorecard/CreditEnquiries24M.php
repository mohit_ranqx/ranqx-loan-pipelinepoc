<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;

/**
 * Class CreditEnquiries24M
 * @package App\Decision\Scorecard
 */
class CreditEnquiries24M implements IScorecardRule
{
    /**
     * @var float|null
     */
    private $data;
    private $internalId = 'KB_US_SR_008';

    /**
     * CreditEnquiries24M constructor.
     * @param float|null $data
     */
    public function __construct(?float $data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function evaluate(): int
    {
        $result = $this->toArray();
        return $result['result'];
    }

    public function toArray(): array
    {
        if ($this->data === null) {
            return ['result' => -38, 'reason' => 'Value is missing','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data < 1.00001) {
            return ['result' => 42, 'reason' => 'Credit Enquiries 24M < 1.00001','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data >= 1.00001 && $this->data < 4.00001) {
            return ['result' => 29, 'reason' => '1.00001 <= Credit Enquiries 24M < 4.00001','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data >= 4.00001) {
            return ['result' => 0, 'reason' => '4.00001 <= Credit Enquiries 24M','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }
    }
}