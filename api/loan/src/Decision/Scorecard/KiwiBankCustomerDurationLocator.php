<?php declare(strict_types=1);


namespace App\Decision\Scorecard;


/**
 * Class KiwiBankCustomerDurationLocator
 * @package App\Decision\Scorecard
 */
class KiwiBankCustomerDurationLocator implements IScorecardLocator
{
    /**
     * @param array $dataSrc
     * @return IScorecardRule
     */
    public function findData(array $dataSrc): IScorecardRule
    {
        return new KiwiBankCustomerDuration(null);
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Kiwi bank customer duration';
    }

}