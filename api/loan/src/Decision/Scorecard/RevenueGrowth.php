<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;


/**
 * Class RevenueGrowth
 * @package App\Decision\Scorecard
 */
class RevenueGrowth implements IScorecardRule
{
    /**
     * @var float|null
     */
    private $revenueGrowthYearly;
    private $internalId = 'KB_US_SR_001';

    /**
     * RevenueGrowth constructor.
     * @param float|null $revenueGrowthYearly
     */
    public function __construct(?float $revenueGrowthYearly)
    {
        $this->revenueGrowthYearly = $revenueGrowthYearly;
    }

    /**
     * @return int
     */
    public function evaluate(): int
    {
        $result = $this->toArray();
        $result['internalId'] = $this->internalId;
        $result['RuleValue'] = $this->revenueGrowthYearly;
        return $result['result'];
    }

    public function toArray(): array
    {
        if ($this->revenueGrowthYearly === null) {
            return ['result' => 12, 'reason' => 'Value is missing','internalId' => $this->internalId,
                'RuleValue' => $this->revenueGrowthYearly];
        }
        if ($this->revenueGrowthYearly < 0) {
            return ['result' => 21, 'reason' => 'Revenue Growth Yearly < 0','internalId' => $this->internalId,
                'RuleValue' => $this->revenueGrowthYearly];
        }
        //0<= revenue_growth < 0.25
        if ($this->revenueGrowthYearly >= 0 && $this->revenueGrowthYearly < 0.25) {
            return ['result' => 32, 'reason' => '0 <= Revenue Growth Yearly < 0.25','internalId' => $this->internalId,
                'RuleValue' => $this->revenueGrowthYearly];
        }
        //0.25 <= revenue_growth < 0.55
        if ($this->revenueGrowthYearly >= 0.25 && $this->revenueGrowthYearly < 0.55) {
            return ['result' => 51, 'reason' => '0.25 <= Revenue Growth Yearly < 0.55','internalId' => $this->internalId,
                'RuleValue' => $this->revenueGrowthYearly];
        }

        //0.55 <= revenue_growth
        if ($this->revenueGrowthYearly >= 0.55) {
            return ['result' => 31, 'reason' => '0.55 <= Revenue Growth Yearly','internalId' => $this->internalId,
                'RuleValue' => $this->revenueGrowthYearly];
        }
    }
}