<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;

use App\Decision\Policy;
use App\Decision\PolicyResult;

/**
 * Class KiwibankCumulativeScorecardDecision
 * @package App\Decision\Scorecard
 */
class KiwibankCumulativeScorecardDecision
{
    use PolicyResult;

    /**
     * @var array
     */
    private $data;

    /**
     * KiwibankCumulativeScorecardDecision constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function make(): int
    {
        $total = (int)$this->data['total'];

        if ($total >= 100 && $total <= 449) {
            return Policy::REFER;
        }

        if ($total >= 450) {
            return Policy::APPROVE;
        }

        if ($total <= 99) {
            return Policy::DECLINE;
        }
    }
}