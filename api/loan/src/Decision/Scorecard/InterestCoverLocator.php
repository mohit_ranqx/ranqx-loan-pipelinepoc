<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;

use App\TypeTrait\InputToFloat;

/**
 * Class InterestCoverLocator
 * @package App\Decision\Scorecard
 */
class InterestCoverLocator implements IScorecardLocator
{
    use InputToFloat;
    /**
     * @param array $dataSrc
     * @return IScorecardRule
     */
    public function findData($dataSrc): IScorecardRule
    {
        $data = $dataSrc['calFinanceCurrentRolling'][0]['interest_cover_rolling'] ?? null;
        return new InterestCover($this->toFloat($data));
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Interest Cover';
    }

}