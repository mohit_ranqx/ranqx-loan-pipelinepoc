<?php declare(strict_types=1);


namespace App\Decision\Scorecard;

use App\TypeTrait\InputToFloat;

class KBCustomDefensiveIntervalRatioLocator implements IScorecardLocator
{
    use InputToFloat;
    public function findData(array $dataSrc): IScorecardRule
    {
        $data = $dataSrc['calFinanceCurrentRolling'][0]['kbDefensiveInterval'] ?? null;
        return new KBCustomDefensiveIntervalRatio($this->toFloat($data));
    }

    public function getDecisionClass(): string
    {
        return 'KB Custom Defensive Interval Ratio';
    }

}