<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;


/**
 * Class ScorecardDataLocatorChain
 * @package App\Scorecard
 */
class ScorecardDataLocatorChain
{

    /**
     * @var IScorecardLocator[] $locators
     */
    private $locators;

    /**
     * ScorecardDataLocatorChain constructor.
     */
    public function __construct()
    {
        $this->locators = [];
    }

    /**
     * @param IScorecardLocator $locator
     */
    public function addLocator(IScorecardLocator $locator): void
    {
        $this->locators[] = $locator;
    }


    /**
     * @return IScorecardLocator[]
     */
    public function getLocators(): array
    {
        return $this->locators;
    }
}