<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;

/**
 * Class CurrentRatio
 * @package App\Decision\Scorecard
 */
class CurrentRatio implements IScorecardRule
{
    /**
     * @var float|null
     */
    private $data;
    private $internalId = 'KB_US_SR_003';

    /**
     * CurrentRatio constructor.
     * @param float|null $currentRatio
     */
    public function __construct(?float $currentRatio)
    {
        $this->data = $currentRatio;
    }

    /**
     * @return int
     */
    public function evaluate(): int
    {
        $result = $this->toArray();
        return $result['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        if ($this->data === null) {
            return ['result' => 11, 'reason' => 'Value is missing','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }
        if ($this->data < 1) {
            return ['result' => 18, 'reason' => 'Current Ratio < 1','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }
        if ($this->data >= 1 && $this->data < 4.5) {
            return ['result' => 26, 'reason' => ' 1 <= Current Ratio < 4.5','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data >= 4.5) {
            return ['result' => 74, 'reason' => 'Current Ratio > 4.5','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }
    }
}