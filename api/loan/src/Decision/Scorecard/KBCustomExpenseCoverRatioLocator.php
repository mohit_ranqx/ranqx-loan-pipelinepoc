<?php declare(strict_types=1);


namespace App\Decision\Scorecard;

use App\TypeTrait\InputToFloat;

/**
 * Class KBCustomExpenseCoverRatioLocator
 * @package App\Decision\Scorecard
 */
class KBCustomExpenseCoverRatioLocator implements IScorecardLocator
{
    use InputToFloat;

    /**
     * @param array $dataSrc
     * @return IScorecardRule
     */
    public function findData(array $dataSrc): IScorecardRule
    {
        $data = $dataSrc['calFinanceCurrentRolling'][0]['kbExpenseCoverRatio'] ?? null;
        return new KBCustomExpenseCoverRatio($this->toFloat($data));
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'KB Custom Expense Cover Ratio';
    }

}