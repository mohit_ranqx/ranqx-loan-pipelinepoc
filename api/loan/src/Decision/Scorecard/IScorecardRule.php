<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;


interface IScorecardRule
{
    public function evaluate(): int;
    public function toArray(): array;
}