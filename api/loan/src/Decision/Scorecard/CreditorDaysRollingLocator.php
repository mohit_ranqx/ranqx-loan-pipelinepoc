<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;

use App\TypeTrait\InputToFloat;

/**
 * Class CreditorDaysRollingLocator
 * @package App\Decision\Scorecard
 */
class CreditorDaysRollingLocator implements IScorecardLocator
{
    use InputToFloat;
    /**
     * @param array $dataSrc
     * @return IScorecardRule
     */
    public function findData(array $dataSrc): IScorecardRule
    {
        $data = $dataSrc['calFinanceCurrentRolling'][0]['creditor_days_rolling'] ?? null;
        return new CreditorDaysRolling($this->toFloat($data));
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Creditor Days Rolling';
    }

}