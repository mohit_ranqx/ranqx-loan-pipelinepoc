<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;


use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use App\Decision\IDecisionLocator;

class KiwiBankScorecardRecord
{
    private $data;
    private $locators;
    private $seed;

    public function __construct($seed, array $dataSrc, ScorecardDataLocatorChain $locatorChain)
    {
        $this->data = $dataSrc;
        $this->locators = $locatorChain;
        $this->seed = $seed;
    }

    public function result(): array
    {
        $records = [];

        /** @var IScorecardLocator $locator ***/
        foreach ($this->locators->getLocators() as $locator) {
            $card = $locator->findData($this->data);
            $result = $card->evaluate();
            $src = $locator->getDecisionClass();
            $r = $card->toArray();
            $id = $r['internalId'];
            $records[$id]['decision'] = $result;
            $records[$id]['reason'] = $r['reason'] ?? '';
            $records[$id]['internalId'] = $r['internalId'] ?? '';
            $records[$id]['RuleValue'] = $r['RuleValue'] ?? '';
            $records[$id]['name'] = $src;
        }

        $records['total'] = $this->seed;
        foreach ($records as $record) {
            $records['total'] += $record['decision'];
        }

        $records['seed'] = $this->seed;
        return $records;
    }
}