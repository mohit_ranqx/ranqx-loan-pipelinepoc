<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;


/**
 * Class InterestCover
 * @package App\Decision\Scorecard
 */
class InterestCover implements IScorecardRule
{
    /**
     * @var float|null
     */
    private $interestCover;
    private $internalId = 'KB_US_SR_002';

    /**
     * InterestCover constructor.
     * @param float|null $interestCover
     */
    public function __construct(?float $interestCover)
    {
        $this->interestCover = $interestCover;
    }

    /**
     * @return int
     */
    public function evaluate(): int
    {
        $result = $this->toArray();
        $result['internalId'] = $this->internalId;
        $result['RuleValue'] = $this->interestCover;
        return $result['result'];
    }

    public function toArray():array
    {
        if ($this->interestCover === null) {
            return ['result' => 27, 'reason' => 'Value is missing','internalId' => $this->internalId,
                'RuleValue' => $this->interestCover];
        }

        if (($this->interestCover - 0.000001) < 0) {
            return ['result' => 8, 'reason' => 'Interest cover < 0.000001','internalId' => $this->internalId,
                'RuleValue' => $this->interestCover];
        }

        //0.000001 <= interest_cover < 50
        if (($this->interestCover - 0.000001) >= 0 && $this->interestCover < 50) {
            return ['result' => 20, 'reason' => ' 0.000001 <= Interest cover < 50','internalId' => $this->internalId,
                'RuleValue' => $this->interestCover];
        }

        if ($this->interestCover >= 50) {
            return ['result' => 57, 'reason' => '50 <= Interest cover','internalId' => $this->internalId,
                'RuleValue' => $this->interestCover];
        }
    }
}