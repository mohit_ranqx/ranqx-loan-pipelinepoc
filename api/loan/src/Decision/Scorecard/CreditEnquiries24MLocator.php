<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;

use App\TypeTrait\InputToFloat;

/**
 * Class CreditEnquiries24MLocator
 * @package App\Decision\Scorecard
 */
class CreditEnquiries24MLocator implements IScorecardLocator
{
    use InputToFloat;
    /**
     * @param array $dataSrc
     * @return IScorecardRule
     */
    public function findData(array $dataSrc): IScorecardRule
    {
        $data = $dataSrc['equiFax']['decision']['enquiryHistory']['credit_enquiries_24M'] ?? null;
        return new CreditEnquiries24M($this->toFloat($data));
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Number of credit enquiries in the past 24 months';
    }
}