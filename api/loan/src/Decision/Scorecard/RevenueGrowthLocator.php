<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;

use App\TypeTrait\InputToFloat;

/**
 * Class RevenueGrothLocator
 * @package App\Scorecard
 */
class RevenueGrowthLocator implements IScorecardLocator
{
    use InputToFloat;
    /**
     * @param array $dataSrc
     * @return IScorecardRule
     */
    public function findData(array $dataSrc): IScorecardRule
    {
        $data = $dataSrc['calFinanceYearly'][0]['revenue_growth_yearly'] ?? null;
        return new RevenueGrowth($this->toFloat($data));
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Revenue Growth Yearly';
    }

}