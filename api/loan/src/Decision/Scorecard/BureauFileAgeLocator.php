<?php declare(strict_types=1);


namespace App\Decision\Scorecard;

/**
 * Class BureauFileAgeLocator
 * @package App\Decision\Scorecard
 */
class BureauFileAgeLocator implements IScorecardLocator
{

    /**
     * @param array $dataSrc
     * @return IScorecardRule
     */
    public function findData(array $dataSrc): IScorecardRule
    {
        $data = $dataSrc['equiFax']['db']['creditActivitySummary']['data']['body']['ageOfCreditFile'] ?? null;
        return new BureauFileAge($data);
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Bureau File Age';
    }
}