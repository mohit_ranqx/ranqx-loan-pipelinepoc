<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;

/**
 * Class CreditorDaysRolling
 * @package App\Decision\Scorecard
 */
class CreditorDaysRolling implements IScorecardRule
{
    /**
     * @var float|null
     */
    private $data;
    private $internalId = 'KB_US_SR_004';

    /**
     * CreditorDaysRolling constructor.
     * @param float|null $data
     */
    public function __construct(?float $data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function evaluate(): int
    {
        $result = $this->toArray();
        return $result['result'];
    }

    public function toArray():array
    {
        if ($this->data === null) {
            return ['result' => 27, 'reason' => 'Value is missing','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data < 30) {
            return ['result' => 35, 'reason' => 'Creditor days rolling < 30','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data >= 30 && $this->data < 300) {
            return ['result' => 23, 'reason' => '30 <= Creditor days rolling < 300','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data >= 300) {
            return ['result' => -44, 'reason' => 'Creditor days rolling >= 300','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }
    }

}