<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;

use App\TypeTrait\InputToFloat;

/**
 * Class DefenceInternalRatioRollingLocator
 * @package App\Decision\Scorecard
 * @see https://ranqxltd.atlassian.net/wiki/spaces/LO/pages/651886596/Ranqx+Decision+Engine
 * removed
 */
class DefenceInternalRatioRollingLocator
{
    use InputToFloat;
    /**
     * @param array $dataSrc
     * @return IScorecardRule
     */
    public function findData(array $dataSrc): IScorecardRule
    {
        $data = $dataSrc['calFinanceCurrentRolling'][0]['defence_internal_ratio_rolling'] ?? null;
        return new DefenceInternalRatioRolling($this->toFloat($data));
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Defence Internal Ratio Rolling';
    }
}