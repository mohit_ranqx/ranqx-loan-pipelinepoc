<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;

/**
 * Class KiwiBankCustomerDuration
 * @package App\Decision\Scorecard
 */
class KiwiBankCustomerDuration implements IScorecardRule
{
    /**
     * @var ?float $data
     */
    private $data;
    private $internalId = 'KB_US_SR_010';

    /**
     * KiwiBankCustomerDuration constructor.
     * @param ?float $data
     */
    public function __construct(?float $data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function evaluate(): int
    {
        $result = $this->toArray();
        return $result['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return ['result' => 27, 'reason' => 'This data will NOT be available yet, 27 is default value for kiwibank'
            ,'internalId' => $this->internalId,
            'RuleValue' => $this->data];
    }
}