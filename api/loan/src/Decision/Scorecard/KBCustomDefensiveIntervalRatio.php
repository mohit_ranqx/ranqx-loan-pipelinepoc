<?php declare(strict_types=1);


namespace App\Decision\Scorecard;

/**
 * Class KBCustomDefensiveIntervalRatio
 * @package App\Decision\Scorecard
 */
class KBCustomDefensiveIntervalRatio implements IScorecardRule
{
    /**
     * @var float|null
     */
    private $data;
    /**
     * @var string
     */
    private $internalId = 'KB_US_SR_012';

    /**
     * KBCustomDefensiveIntervalRatio constructor.
     * @param float|null $data
     */
    public function __construct(?float $data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function evaluate(): int
    {
        $result = $this->toArray();
        return $result['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        if ($this->data === null) {
            return ['result' => 14, 'reason' => 'Value is missing','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data < 21) {
            return ['result' => 9, 'reason' => 'KB Defensive interval < 21','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        //0.000001 <= interest_cover < 50
        if (($this->data) >= 21 && $this->data < 90) {
            return ['result' => 55, 'reason' => '21 <= KB Defensive interval < 90','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data >= 90) {
            return ['result' => 18, 'reason' => '90 <= KB Defensive interval','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }
    }

}