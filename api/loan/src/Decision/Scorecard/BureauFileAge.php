<?php


namespace App\Decision\Scorecard;


/**
 * Class BureauFileAge
 * @package App\Decision\Scorecard
 */
class BureauFileAge implements IScorecardRule
{
    /**
     * @var string|null
     */
    private $data;

    private $internalId = 'KB_US_SR_009';

    /**
     * BureauFileAge constructor.
     * @param string|null $data
     */
    public function __construct(?string $data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function evaluate(): int
    {
        $result = $this->toArray();
        return $result['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $number = $this->toNumber();

        if ($number === null) {
            return [
                'result' => -39,
                'reason' => 'Value is missing',
                'internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($number < 7.0056) {
            return ['result' => 12, 'reason' => 'Bureau File Age < 7.0056','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($number >= 7.0056) {
            return ['result' => 59, 'reason' => '7.0056 <= Bureau File Age','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }
    }

    /**
     * @return float|null
     */
    private function toNumber(): ?float
    {
        //12 years 8 months
        $pattern = "%(^[\d]+)\s*years\s*([\d]+)%ism";
        if (!$this->data) {
            return null;
        }

        preg_match($pattern, $this->data, $m);
        if (!empty($m)) {
            $year = $m[1] ?? 0;
            $month = $m[2] ?? 0;
            $r = round($month / 12, 4);
            return $year + $r;
        }

        return null;
    }
}