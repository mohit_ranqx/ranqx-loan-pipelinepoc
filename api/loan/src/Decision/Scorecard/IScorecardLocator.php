<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;

/**
 * Interface IScorecardLocator
 * @package App\Decision\Scorecard
 */
interface IScorecardLocator
{
    /**
     * @param array $dataSrc
     * @return IScorecardRule
     */
    public function findData(array $dataSrc) : IScorecardRule;

    /**
     * @return string
     */
    public function getDecisionClass(): string;
}