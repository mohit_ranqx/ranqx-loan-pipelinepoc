<?php declare(strict_types=1);


namespace App\Decision\Scorecard;

/**
 * Class KBCustomInterestCoverRatio
 * @package App\Decision\Scorecard
 */
class KBCustomInterestCoverRatio implements IScorecardRule
{
    /**
     * @var float|null
     */
    private $interestCover;
    /**
     * @var string
     */
    private $internalId = 'KB_US_SR_011';

    /**
     * KBCustomInterestCoverRatio constructor.
     * @param float|null $data
     */
    public function __construct(?float $data)
    {
        $this->interestCover = $data;
    }

    /**
     * @return int
     */
    public function evaluate(): int
    {
        $result = $this->toArray();
        return $result['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        if ($this->interestCover === null) {
            return ['result' => 27, 'reason' => 'Value is missing','internalId' => $this->internalId,
                'RuleValue' => $this->interestCover];
        }

        if (($this->interestCover - 0.000001) < 0) {
            return ['result' => 8, 'reason' => 'KB Custom interest cover ratio < 0.000001','internalId' => $this->internalId,
                'RuleValue' => $this->interestCover];
        }

        //0.000001 <= interest_cover < 50
        if (($this->interestCover - 0.000001) >= 0 && $this->interestCover < 50) {
            return ['result' => 20, 'reason' => '0.000001 <= KB Custom interest cover ratio < 50','internalId' => $this->internalId,
                'RuleValue' => $this->interestCover];
        }

        if ($this->interestCover >= 50) {
            return ['result' => 57, 'reason' => '50 <= KB Custom interest cover','internalId' => $this->internalId,
                'RuleValue' => $this->interestCover];
        }
    }

}