<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;


/**
 * Class DefenceInternalRatioRolling
 * @package App\Decision\Scorecard
 */
class DefenceInternalRatioRolling implements IScorecardRule
{
    /**
     * @var float|null
     */
    private $data;
    private $internalId = 'KB_US_SR_005';

    /**
     * DefenceInternalRatioRolling constructor.
     * @param float|null $data
     */
    public function __construct(?float $data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function evaluate(): int
    {
        $result = $this->toArray();
        return $result['result'];
    }

    public function toArray(): array
    {
        if ($this->data === null) {
            return ['result' => 14, 'reason' => 'Value is missing','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data < 21) {
            return ['result' => 9, 'reason' => 'Defence internal ratio < 21','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data >= 21 && $this->data < 90) {
            return ['result' => 55, 'reason' => ' 21 <= Defence internal ratio < 90','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data >= 90) {
            return ['result' => 18, 'reason' => 'Defence internal ratio >= 90','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }
    }
}