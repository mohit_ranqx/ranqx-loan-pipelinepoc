<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;

use App\TypeTrait\InputToFloat;

/**
 * Class CurrentRatioLocator
 * @package App\Decision\Scorecard
 */
class CurrentRatioLocator implements IScorecardLocator
{
    use InputToFloat;
    /**
     * @param array $dataSrc
     * @return IScorecardRule
     */
    public function findData(array $dataSrc): IScorecardRule
    {
        $data = $dataSrc['calFinanceCurrentMonth'][0]['working_capital_ratio_current'] ?? null;
        return new CurrentRatio($this->toFloat($data));
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Current Ratio';
    }

}