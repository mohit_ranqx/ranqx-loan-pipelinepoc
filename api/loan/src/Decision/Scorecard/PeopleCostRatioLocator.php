<?php declare(strict_types=1);


namespace App\Decision\Scorecard;

use App\TypeTrait\InputToFloat;

/**
 * Class PeopleCostRatioLocator
 * @package App\Decision\Scorecard
 */
class PeopleCostRatioLocator implements IScorecardLocator
{
    use InputToFloat;
    /**
     * @param array $dataSrc
     * @return IScorecardRule
     */
    public function findData(array $dataSrc): IScorecardRule
    {
        $data = $dataSrc['calFinanceCurrentRolling'][0]['people_cost_ratio_rolling'] ?? null;
        return new PeopleCostRatio($this->toFloat($data));
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'People cost ratio rolling';
    }

}