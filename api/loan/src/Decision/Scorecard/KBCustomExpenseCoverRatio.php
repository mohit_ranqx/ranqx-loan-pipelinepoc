<?php declare(strict_types=1);


namespace App\Decision\Scorecard;

/**
 * Class KBCustomExpenseCoverRatio
 * @package App\Decision\Scorecard
 */
class KBCustomExpenseCoverRatio implements IScorecardRule
{
    /**
     * @var float|null
     */
    private $data;
    /**
     * @var string
     */
    private $internalId = 'KB_US_SR_013';

    /**
     * KBCustomExpenseCoverRatio constructor.
     * @param float|null $data
     */
    public function __construct(?float $data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function evaluate(): int
    {
        $result = $this->toArray();
        return $result['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        if ($this->data === null) {
            return ['result' => 13, 'reason' => 'Value is missing','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data < 0.2) {
            return ['result' => 35, 'reason' => 'KB Expense cover ratio < 0.2','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }


        if ($this->data >= 0.2) {
            return ['result' => 4, 'reason' => 'KB Expense cover ratio >= 0.2 ','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }
    }
}