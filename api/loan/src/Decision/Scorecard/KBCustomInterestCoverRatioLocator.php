<?php declare(strict_types=1);


namespace App\Decision\Scorecard;

use App\TypeTrait\InputToFloat;

/**
 * Class KBCustomInterestCoverRatioLocator
 * @package App\Decision\Scorecard
 * @see https://ranqxltd.atlassian.net/wiki/spaces/LO/pages/651886596/Ranqx+Decision+Engine
 * removed
 */
class KBCustomInterestCoverRatioLocator
{
    use InputToFloat;
    /**
     * @param array $dataSrc
     * @return IScorecardRule
     */
    public function findData(array $dataSrc): IScorecardRule
    {
        $data = $dataSrc['calFinanceCurrentMonth'][0]['kb_custom_interest_cover_ratio_current'] ?? null;
        return new KBCustomInterestCoverRatio($this->toFloat($data));
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'KB Custom Interest Cover Ratio';
    }
}