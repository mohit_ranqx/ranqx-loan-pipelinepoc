<?php declare(strict_types = 1);


namespace App\Decision\Scorecard;

/**
 * Class PeopleCostRatio
 * @package App\Decision\Scorecard
 */
class PeopleCostRatio implements IScorecardRule
{
    /**
     * @var ?float $data
     */
    private $data;
    private $internalId = 'KB_US_SR_007';

    /**
     * PeopleCostRatio constructor.
     * @param ?float $data
     */
    public function __construct(?float $data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function evaluate(): int
    {
        $result = $this->toArray();
        return $result['result'];
    }

    public function toArray(): array
    {
        if ($this->data === null) {
            return ['result' => 17, 'reason' => 'Value is missing','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data < 0.000001) {
            //people_cost_ratio < 0.000001
            return ['result' => 23, 'reason' => 'People cost ratio < 0.000001','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data >= 0.000001 && $this->data < 0.125) {
            return ['result' => 0, 'reason' => ' 0.000001 <= People cost ratio < 0.125','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data >= 0.125 && $this->data < 0.25) {
            return ['result' => 30, 'reason' => ' 0.125 <= People cost ratio < 0.25','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }

        if ($this->data >= 0.25) {
            return ['result' => 46, 'reason' => ' 0.25 <= People cost ratio','internalId' => $this->internalId,
                'RuleValue' => $this->data];
        }
    }

}