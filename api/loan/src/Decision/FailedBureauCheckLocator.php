<?php declare(strict_types = 1);


namespace App\Decision;

use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class FailedBureauCheckLocator
 * @package App\Decision
 */
class FailedBureauCheckLocator implements IDecisionLocator
{
    /**
     * @param array $dataSrc
     * @return IRule
     */
    public function findData(array $dataSrc): IRule
    {
        if (isset($dataSrc['equiFax']['exception'])) {
            return new FailedBureauCheck($dataSrc['equiFax']['exception']);
        }

        if (!isset($dataSrc['equiFax'])) {
            $obj = new FailedBureauCheck('');
            throw new KiwiBankDataSrcNoExistException(
                $obj,
                ($obj->dataMissing())['reason']
            );
        }
        return new FailedBureauCheck('');
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Failed Bureau Check';
    }
}