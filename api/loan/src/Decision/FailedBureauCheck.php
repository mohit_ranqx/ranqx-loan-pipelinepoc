<?php declare(strict_types = 1);


namespace App\Decision;

/**
 * Class FailedBureauCheck
 * @package App\Decision
 */
class FailedBureauCheck extends Policy implements IRule
{
    /**
     * @var string $data
     */
    private $data;


    /**
     * @var string
     */
    private $refId = 'BC01';

    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_016';

    /**
     * DefaultCount constructor.
     * @param string $data
     */
    public function __construct(string $data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        if (!$this->data) {
            return ['result' => Policy::PASS,
                'reason' => 'Got successful response from the Equifax API',
                'RefId' => $this->refId, 'RuleValue' => '', 'InternalId' => $this->internalId];
        }
        return ['result' => Policy::REFER,
            'reason' => 'No successful response from the Equifax API',
            'RefId' => $this->refId, 'RuleValue' => '', 'InternalId' => $this->internalId];
    }

    /**
     * @return array
     */
    public function dataMissing(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'EuqiFax data is missing.',
            'RefId' => $this->refId,'RuleValue' => '', 'InternalId' => $this->internalId
        ];
    }

    /**
     * @return array
     */
    public function dataIsBad(): array
    {
        return [];
    }


}