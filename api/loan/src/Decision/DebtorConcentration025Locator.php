<?php declare(strict_types = 1);


namespace App\Decision;

class DebtorConcentration025Locator implements IDecisionLocator
{
    /**
     * @param array $dataSrc
     * @return IRule
     */
    public function findData(array $dataSrc): IRule
    {
        $result = $dataSrc['debtorConcentration'] ?? [];
        return new DebtorConcentration025($result);
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Debtor Concentration 025';
    }
}