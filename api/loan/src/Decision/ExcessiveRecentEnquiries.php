<?php declare(strict_types = 1);


namespace App\Decision;


use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class ExcessiveRecentEnquiries
 * @package App\Decision
 */
class ExcessiveRecentEnquiries extends Policy implements IRule
{
    /**
     * @var mixed $data
     */
    private $data;
    /**
     * @var ExpressionLanguage
     */
    private $expression;
    /**
     * @var string
     */
    private $refId = 'RE01';
    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_010';

    /**
     * ExcessiveRecentEnquiries constructor.
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->expression = new ExpressionLanguage();
    }

    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $ret = $this->expression->evaluate(
            'data ' . $this->getExpression(),
            [
                'data' => $this->data,
            ]
        );

        if ($ret) {
            return ['result' => Policy::REFER,
                'reason' => $this->getReason('excessive_recent_enquiries'),
                'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
        }
        return ['result' => Policy::PASS,
            'reason' => $this->getReason('excessive_recent_enquiries', 'negative'),
            'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
    }

    /**
     * @return string
     */
    protected function getExpression(): string
    {
        $conditions = $this->getBankConditions();
        return $conditions['excessive_recent_enquiries'] ?? '>= 5';
    }

    public function dataMissing(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Excessive Recent Enquiries data is missing.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }

    public function dataIsBad(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Excessive Recent Enquiries data has bad format.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }
}