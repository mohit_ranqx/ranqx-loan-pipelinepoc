<?php declare(strict_types = 1);


namespace App\Decision;

/**
 * Class DecisionPolicyResult
 * @package App\Decision
 */
class DecisionPolicyResult
{
    /**
     * @var int
     */
    public $result;
    /**
     * @var string
     */
    public $reason;
    /**
     * @var string
     */
    public $RefId;
    /**
     * @var mixed
     */
    public $RuleValue;
    /**
     * @var string
     */
    public $InternalId;
}