<?php declare(strict_types = 1);


namespace App\Decision;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class EnquiryLast5Years
 * @package App\Decision
 */
class EnquiryLast5Years extends Policy implements IRule
{
    /**
     * @var mixed $data
     */
    private $data;

    /**
     * @var ExpressionLanguage
     */
    private $expression;

    /**
     * @var string
     */
    private $refId = 'EE01';

    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_013';

    /**
     * EnquiryLast5Years constructor.
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->expression = new ExpressionLanguage();
    }

    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        //enquiry_last_5years
        $ret = $this->expression->evaluate(
            'data ' . $this->getExpression(),
            [
                'data' => $this->data,
            ]
        );
        if ($ret) {
            return ['result' => Policy::REFER,
                'reason' => $this->getReason('enquiry_last_5years'),
                'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
        }

        return ['result' => Policy::PASS,
            'reason' => $this->getReason('enquiry_last_5years', 'negative'),
            'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
    }

    /**
     * @return string
     */
    protected function getExpression(): string
    {
        $conditions = $this->getBankConditions();
        return $conditions['enquiry_last_5years'] ?? '>= 20';
    }

    public function dataMissing(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Enquiry Last 5 Years data is missing.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }

    public function dataIsBad(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Enquiry Last 5 Years data has bad format.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }
}