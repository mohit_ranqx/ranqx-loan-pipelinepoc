<?php declare(strict_types = 1);


namespace App\Decision;

use DateTime;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class AgeOfCompany
 * @package App\Decision
 */
class AgeOfCompany extends Policy implements IRule
{
    /**
     * @var string
     */
    private $data;
    /**
     * @var ExpressionLanguage
     */
    private $expression;

    /**
     * @var string
     */
    private $refId = 'AC02';

    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_004';

    /**
     * AgeOfCompany constructor.
     * @param string $data
     */
    public function __construct(string $data)
    {
        $this->data = $data;
        $this->expression = new ExpressionLanguage();
    }


    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        if ($this->data === '') {
            return $this->dataMissing();
        }

        try {
            $today = new DateTime();
            $companyDate = DateTime::createFromFormat('Y-m-d', $this->data);
        } catch (\Exception $e) {
            return $this->dataIsBad();
        }

        if ($companyDate === false) {
            return $this->dataIsBad();
        }

        $diff = $today->diff($companyDate);

        $ret = $this->expression->evaluate(
            'date ' . $this->getExpression(),
            [
                'date' => $diff->y,
            ]
        );

        if ($ret) {
            return ['result' => Policy::DECLINE,
                'reason' => $this->getReason('incorporation_date'),
                'RefId' => $this->refId,
                'RuleValue' => $this->data, 'InternalId' => $this->internalId];
        }

        return ['result' => Policy::PASS,
            'reason' => $this->getReason('incorporation_date', 'negative'),
            'RefId' => $this->refId,
            'RuleValue' => $this->data, 'InternalId' => $this->internalId];
    }

    /**
     * @return array
     */
    public function dataMissing(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Company incorporation date is missing.',
            'RefId' => $this->refId,
            'RuleValue' => $this->data, 'InternalId' => $this->internalId];
    }

    /**
     * @return array
     */
    public function dataIsBad(): array
    {
        return ['result' => Policy::REFER,
            'reason' => 'Company incorporation date is in wrong format.',
            'RefId' => $this->refId,
            'RuleValue' => $this->data, 'InternalId' => $this->internalId];
    }

    /**
     * @return string
     */
    protected function getExpression(): string
    {
        $conditions = $this->getBankConditions();
        return $conditions['incorporation_date'] ?? '< 2';
    }
}