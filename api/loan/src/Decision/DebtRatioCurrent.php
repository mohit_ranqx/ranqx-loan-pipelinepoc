<?php declare(strict_types = 1);


namespace App\Decision;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class DebtRatioCurrent
 * @package App\Decision
 * KB_US_PR_005
 *
 */
class DebtRatioCurrent extends Policy implements IRule
{
    /**
     * @var mixed
     */
    private $data;

    /**
     * @var string
     */
    private $refId = 'DC01';

    /**
     * @var string
     */
    private $internalId = 'KB_US_PR_005';

    /**
     * @var ExpressionLanguage
     */
    private $expression;

    /**
     * DebtRatioCurrent constructor.
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->expression = new ExpressionLanguage();
    }

    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $ret = $this->expression->evaluate(
            'Debt_Ratio ' . $this->getExpression(),
            [
                'Debt_Ratio' => $this->data
            ]
        );

        if ($ret) {
            return ['result' => Policy::REFER, 'reason' => $this->getReason('insufficient_debt_coverage'),
                'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
        }
        return ['result' => Policy::PASS,
            'reason' => $this->getReason('insufficient_debt_coverage', 'negative'),
            'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
    }

    /**
     * @return string
     */
    protected function getExpression(): string
    {
        $conditions = $this->getBankConditions();
        return $conditions['insufficient_debt_coverage'];
    }

    /**
     * @return array
     */
    public function dataMissing(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Debt Ratio data is missing.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }

    /**
     * @return array
     */
    public function dataIsBad(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Debt Ratio data has bad format.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }
}