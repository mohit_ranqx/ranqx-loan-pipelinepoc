<?php declare(strict_types = 1);


namespace App\Decision;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class InterestCoverCurrent
 * @package App\Decision
 * KB_US_PR_006
 */
class InterestCoverCurrent extends Policy implements IRule
{
    /**
     * @var mixed
     */
    private $data;

    private $refId = 'IC01';

    /**
     * @var ExpressionLanguage
     */
    protected $expression;

    private $internalId = 'KB_US_PR_006';

    /**
     * InterestCoverCurrent constructor.
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->expression = new ExpressionLanguage();
    }

    /**
     * @return int
     */
    public function policyRule(): int
    {
        return ($this->toArray())['result'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $ret = $this->expression->evaluate(
            'insufficient_interest_coverage ' . $this->getExpression(),
            [
                'insufficient_interest_coverage' => $this->data,
            ]
        );

        if ($ret) {
            return ['result' => Policy::REFER, 'reason' => $this->getReason('insufficient_interest_coverage'),
                'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
        }
        return ['result' => Policy::PASS,
            'reason' => $this->getReason('insufficient_interest_coverage', 'negative'),
            'RefId' => $this->refId, 'RuleValue' => $this->data, 'InternalId' => $this->internalId];
    }

    /**
     * @return string
     */
    protected function getExpression(): string
    {
        $conditions = $this->getBankConditions();
        return $conditions['insufficient_interest_coverage'] ?? '< 2';
    }

    public function dataMissing(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Interest Cover Current data is missing.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }

    public function dataIsBad(): array
    {
        return [
            'result' => Policy::REFER,
            'reason' => 'Interest Cover Current data has bad format.',
            'RefId' => $this->refId,'RuleValue' => $this->data, 'InternalId' => $this->internalId
        ];
    }


}