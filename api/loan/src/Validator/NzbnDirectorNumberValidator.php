<?php declare(strict_types=1);

namespace App\Validator;

use App\Normalizer\ConstraintViolationListNormalizer;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class NzbnDirectorNumberValidator
 * @package App\Validator
 */
class NzbnDirectorNumberValidator
{
    /**
     * @var string
     */
    private $number;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * NzbnDirectorNumberValidator constructor.
     * @param string $number
     * @param ValidatorInterface $validator
     */
    public function __construct(string $number, ValidatorInterface $validator)
    {
        $this->number = $number;
        $this->validator = $validator;
    }

    /**
     * @return array
     */
    public function validate(): array
    {
        $violations = $this->validator->validate(
            $this->number,
            [new Assert\Length(['max' => 13, 'min' => 13]), new Assert\NotBlank()]
        );

        $normalizer = new ConstraintViolationListNormalizer();
        return $normalizer->normalize($violations, null, ['title' => 'nzbn number']);
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->number;
    }
}