<?php declare(strict_types = 1);


namespace App\MessageHandler;

use App\DataInterface\Equifax;
use App\DataInterface\RanqxDataHandler;
use App\Entity\Organisation;
use App\Message\ApplicantExceptionMessage;
use App\Message\DecisionRequestMessage;
use App\Message\RanqxExceptionMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Decision\DecisionDataLocatorChain;
use App\Decision\Scorecard\ScorecardDataLocatorChain;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;


/**
 * Class DecisionMakeMessagehandler
 * @package App\MessageHandler
 */
class DecisionMakeMessagehandler implements MessageHandlerInterface
{
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @var \App\Decision\DecisionDataLocatorChain
     */
    private $locators;

    /**
     * @var \App\Decision\Scorecard\ScorecardDataLocatorChain
     */
    private $scoreLocatorChain;


    /**
     * DecisionMakeMessagehandler constructor.
     * @param EntityManagerInterface $objectManager
     * @param MessageBusInterface $bus
     * @param DecisionDataLocatorChain $locatorChain
     * @param ScorecardDataLocatorChain $scoreLocatorChain
     * @param ParameterBagInterface $params
     */
    public function __construct(
        EntityManagerInterface $objectManager,
        MessageBusInterface $bus,
        DecisionDataLocatorChain $locatorChain,
        ScorecardDataLocatorChain $scoreLocatorChain,
        ParameterBagInterface $params
    ) {
        $this->objectManager = $objectManager;
        $this->bus = $bus;
        $this->locators = $locatorChain;
        $this->scoreLocatorChain = $scoreLocatorChain;
        $this->params = $params;
    }


    /**
     * @param DecisionRequestMessage $message
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __invoke(DecisionRequestMessage $message)
    {
        /** @var array $data**/
        $data = $message->getContent();
        if (!empty($data) ) {
            $dataSrc = $this->getDecisionData($data['orgId'], $data['applicantId']);
            $dataSrc['bankData'] = $data;
            if (isset($data['orgId']) && $data['orgId']) {
                /** @var Organisation|null $org */
                $org = $this->objectManager->getRepository(Organisation::class)->find($data['orgId']);
                if ($org) {
                    $app = $org->getApplicant();
                    if ($app) {
                        $pdf = $dataSrc['equiFax']['db']['pdf'] ?? '';

                        if (isset($dataSrc['equiFax']['db']['pdf'])) {
                            unset($dataSrc['equiFax']['db']['pdf']);
                        }

                        $app->setEquiFaxRaw($dataSrc);
                        $app->setPdf($pdf);
                        $app->setDecision(true);
                    }
                } else {
                    $this->bus->dispatch(
                        new RanqxExceptionMessage('Organisation id has not been found. Id is ' . $data['orgId'])
                    );
                }
            }

        } else {
            $this->bus->dispatch(
                new RanqxExceptionMessage('DecisionRequestMessage is empty.')
            );
        }
        $this->objectManager->flush();
    }


    /**
     * @param int $orgId
     * @param int $appId
     * @return array|false|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getDecisionData(?int $orgId, int $appId)
    {
        $handler = new RanqxDataHandler($this->objectManager, new Equifax($this->params), $this->params);
        $data = $handler->allData($orgId, $appId);
        if (empty($data)) {
            $data = json_encode(new \stdClass);
        }

        return $data;
    }
}
