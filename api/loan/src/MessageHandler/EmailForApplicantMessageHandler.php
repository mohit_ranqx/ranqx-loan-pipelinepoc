<?php declare(strict_types = 1);


namespace App\MessageHandler;

use App\Entity\Applicant;
use Aws\Ses\SesClient;
use Doctrine\ORM\EntityManagerInterface;
use Html2Text\Html2Text;
use PHPMailer\PHPMailer\PHPMailer;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Environment;
use App\Message\EmailForApplicantMessage;
use App\TypeTrait\EnvEmail;

class EmailForApplicantMessageHandler
{
    use EnvEmail;
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    /**
     * @var SesClient
     */
    private $mailer;

    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * @var Environment
     */
    private $twig;


    /**
     * FinalEmailMessageHandler constructor.
     * @param EntityManagerInterface $objectManager
     * @param SesClient $mailer
     * @param ParameterBagInterface $params
     * @param Environment $twig
     */
    public function __construct(
        EntityManagerInterface $objectManager,
        SesClient $mailer,
        ParameterBagInterface $params,
        Environment $twig
    ) {
        $this->objectManager = $objectManager;
        $this->mailer = $mailer;
        $this->params = $params;
        $this->twig = $twig;
    }

    /**
     * @param EmailForApplicantMessage $message
     */
    public function __invoke(EmailForApplicantMessage $message)
    {
        $appId = $message->getContent();
        if ($appId) {
            /** @var Applicant | null $app **/
            $app = $this->objectManager->getRepository(Applicant::class)->find($appId);
            if ($app) {
                $this->sendEmail($app);
            }
        }
    }

    private function sendEmail(Applicant $app)
    {
        $projectDir = $this->params->get('project_dir');
        $envEmails = $this->getEnvEmails();

        $mail = new PHPMailer();

        $mail->CharSet = 'UTF-8';

        $mail->setFrom($envEmails['from']);
        $mail->Subject = 'Thank you for applying for a Kiwibank Business Overdraft';

        // Todo: use traits
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbEmailHeaderOpt1.jpg', 'kbHeader', 'kbEmailHeaderOpt1.jpg');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbLogo.png', 'kbLogo', 'kbLogo.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbShield.png', 'kbShield', 'kbShield.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbComputer.png', 'kbComputer', 'kbComputer.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbTwitter.png', 'kbTwitter', 'kbTwitter.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbPoint.png', 'kbPoint', 'kbPoint.png');
        $mail->addEmbeddedImage($projectDir . '/assets/images/kbFB.png', 'kbFB', 'kbFB.png');

        // Render HTMl content
        $htmlBody = $this->twig->render('\kb\forApplicant.html.twig',
            [
                'referenceNumber' => $app->getReferenceNumber(),
                'emailAddress' => $app->getEmail(),
                'firstName' => $app->getFirstName(),
                'companyName' => $app->getBusinessName(),
            ]);

        $html2TextConverter = new Html2Text($htmlBody);

        $mail->Body = $htmlBody;
        $mail->AltBody = $html2TextConverter->getText();

        $mail->IsHTML(true);

        foreach ($this->bccToArray() as $bcc) {
            $mail->addBCC($bcc);
        }

        $recipientEmails = [$app->getEmail()];
        if ($this->isOverrideExist()) {
            $recipientEmails = [$envEmails['override']];
        }

        foreach ($recipientEmails as $recipientEmail) {
            $mail->addAddress($recipientEmail);
        }
        $mail->addReplyTo($envEmails['from']);

        $mail->preSend();
        $message = $mail->getSentMIMEMessage();
        $result = $this->mailer->sendRawEmail([
            'RawMessage' => [
                'Data' => $message
            ]
        ]);
    }
}
