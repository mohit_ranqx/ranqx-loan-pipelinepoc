<?php declare(strict_types=1);

namespace App\MessageHandler;

use App\Entity\Applicant;
use App\Entity\ApplicantException;
use App\Entity\RanqxException;
use App\Service\PrivateKeyCrypt;
use Doctrine\ORM\EntityManagerInterface;
use App\Message\EncryptedApplicantIdExceptionLoggingMessage;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\TypeTrait\XeroKey;

/**
 * Class EncryptedApplicantIdExceptionLoggingMessageHandler
 * @package App\MessageHandler
 */
class EncryptedApplicantIdExceptionLoggingMessageHandler implements MessageHandlerInterface
{
    use XeroKey;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * EncryptedApplicantIdExceptionLoggingMessageHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param ParameterBagInterface $bag
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ParameterBagInterface $bag
    ) {
        $this->entityManager = $entityManager;
        $this->params = $bag;
    }

    /**
     * @param EncryptedApplicantIdExceptionLoggingMessage $message
     */
    public function __invoke(EncryptedApplicantIdExceptionLoggingMessage $message)
    {
        try {
            $appIdString = $message->getAppId();
            $crypt = new PrivateKeyCrypt(
                $this->privateKey(),
                $this->publicKey()
            );

            $idJSON = json_decode($crypt->decode($appIdString));
            $appId = (int) $idJSON->appId;

            $app = $this->entityManager->getRepository(Applicant::class)
                ->find($appId);

            if ($app) {
                $this->entityManager->transactional(static function (EntityManagerInterface $em) use ($app, $message) {
                    $appEx = new ApplicantException();
                    $appEx->setApplicant($app);
                    $appEx->setException($message->getMessage());
                    $em->persist($appEx);
                });
            } else {
                $this->entityManager->transactional(static function (EntityManagerInterface $em) {
                    $ranqEx = new RanqxException();
                    $ranqEx->setException('Could find appicant id. in file EncryptedApplicantIdExceptionLoggingMessage');
                    $em->persist($ranqEx);
                });
            }

        } catch (\Exception $e) {
            $this->entityManager->transactional(static function (EntityManagerInterface $em) use ($e) {
                $ranqEx = new RanqxException();
                $ranqEx->setException($e->getTraceAsString());
                $em->persist($ranqEx);
            });
        }
    }
}