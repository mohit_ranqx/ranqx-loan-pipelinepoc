<?php declare(strict_types = 1);


namespace App\MessageHandler;

use App\Entity\Applicant;
use App\Entity\ApplicantException;
use App\Entity\Integration;
use App\Entity\Organisation;
use App\Entity\RanqxException;
use App\Message\ApplicantExceptionMessage;
use App\Message\CalculusSendMessage;
use App\Message\DecisionRequestMessage;
use App\Message\RanqxExceptionMessage;
use App\OAuth\Consumer\Xero;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use \Exception;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Message\ApplicantXeroMessage;
use App\TypeTrait\DataDogMetric;
use App\Message\EmailForApplicantMessage;

/**
 * Class XeroAccessTokenMessageHandler
 * @package App\MessageHandler
 */
final class XeroAccessTokenMessageHandler implements MessageHandlerInterface
{

    use DataDogMetric;
    /**
     * @var Xero
     */
    private $xero;

    /**
     * @var EntityManagerInterface
     */
    private $objectManager;

    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ParameterBagInterface
     */
    private $param;


    /**
     * XeroAccessTokenMessageHandler constructor.
     * @param Xero $xero
     * @param EntityManagerInterface $objectManager
     * @param MessageBusInterface $messageBus
     * @param LoggerInterface $logger
     * @param ParameterBagInterface $param
     */
    public function __construct(
        Xero $xero,
        EntityManagerInterface $objectManager,
        MessageBusInterface $messageBus,
        LoggerInterface $logger,
        ParameterBagInterface $param
    ) {
        $this->xero = $xero;
        $this->objectManager = $objectManager;
        $this->bus = $messageBus;
        $this->logger = $logger;
        $this->param = $param;
    }


    /**
     * invoked by a receiver
     * @param ApplicantXeroMessage $token
     * @throws Exception
     */
    public function __invoke(ApplicantXeroMessage $token)
    {
        /** @var Applicant | null $app ***/
        $app = $this->objectManager->getRepository(Applicant::class)->find($token->getAppId());

        if ($app) {
            $this->objectManager->transactional(static function (EntityManagerInterface $em) use ($app, $token) {
                /** @var Integration|null $inte**/
                $inte = $em->getRepository(Integration::class)->findOneBy(['applicant' => $app]);
                if ($inte === null) {
                    $inte = new Integration();
                }
                $inte->setName('Xero');
                $inte->setToken($token->getToken());
                $inte->setApplicant($app);
                $em->persist($inte);
            });

            sleep(5); //xero rate limit

            $org = null;
            try {
                //step 1
                $org = $this->xero->xeroOrg($token->getToken());
                $this->objectManager->persist($org);
                $org->setApplicant($app);
                //flash org here, since invoice need org
                $this->objectManager->flush();
            } catch (\Exception $e) {
                $this->bus->dispatch(new ApplicantExceptionMessage($app->getId(), $e->getMessage()));
            }

            if ($org && $org->getId()) {
                $this->bus->dispatch(new EmailForApplicantMessage($app->getId()));
                //step 2
                try {
                    $fin = $this->xero->xeroInvoice($token->getToken(), $org);
                    $this->objectManager->persist($fin);
                } catch (\Exception $e) {
                    $this->bus->dispatch(new ApplicantExceptionMessage($app->getId(), $e->getMessage()));
                }

                //step 3, load balance and profit data
                try {
                    $this->xero->import($token->getToken(), $this->objectManager, $org);
                } catch (\Exception $e) {
                    $this->bus->dispatch(new ApplicantExceptionMessage($app->getId(), $e->getMessage()));
                }

                $this->bus->dispatch(new CalculusSendMessage($org->getId()));
                $this->objectManager->flush();
            } else {
                //no organisaton? we still need to download equiFax data
                try {
                    $decision = $this->param->get('decision');
                    $this->bus->dispatch(new DecisionRequestMessage(
                        null,
                        $app->getId(),
                        'What is loan type',
                        $decision['bank']
                    ));
                } catch (\Exception $e) {
                    $this->bus->dispatch(new ApplicantExceptionMessage($app->getId(), $e->getMessage()));
                } finally {
                    $app->setDecision(true);
                    $this->objectManager->flush();
                }
            }

        } else {
            $this->bus->dispatch(new RanqxExceptionMessage('Applicant is not found. Id ' . $token->getAppId()));
            $this->objectManager->flush();
        }
    }
}
