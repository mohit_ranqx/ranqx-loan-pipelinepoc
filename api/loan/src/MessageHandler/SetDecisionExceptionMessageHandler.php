<?php declare(strict_types=1);


namespace App\MessageHandler;

use App\Entity\Applicant;
use App\Message\SetDecisionExceptionMessage;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SetDecisionExceptionMessageHandler implements MessageHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var LoggerInterface
     */
    private $logger;


    /**
     * ApplicantExceptionMessageHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    public function __invoke(SetDecisionExceptionMessage $message)
    {
        try {
            $app = $this->entityManager->getRepository(Applicant::class)->find($message->getAppId());
            if ($app) {
                $app->setDecision(true);
                $this->entityManager->flush();
            }
            $this->logger->emergency("SetDecisionExceptionMessageHandler: couldn't  find Applicant");
        } catch (\Exception $e) {
            $this->logger->emergency("SetDecisionExceptionMessageHandler: " . $e->getMessage());
        }
    }
}