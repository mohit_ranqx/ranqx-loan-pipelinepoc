<?php declare(strict_types=1);


namespace App\MessageHandler;

use App\Entity\RanqxException;
use App\Message\RanqxExceptionMessage;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\TypeTrait\DataDogMetric;

/**
 * Class RanqxExceptionMessageHandle
 * @package App\MessageHandler
 */
class RanqxExceptionMessageHandle implements MessageHandlerInterface
{
    use DataDogMetric;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * RanqxExceptionMessageHandle constructor.
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @param RanqxExceptionMessage $message
     */
    public function __invoke(RanqxExceptionMessage $message)
    {
        $this->entityManager->transactional(static function (EntityManagerInterface $em) use ($message) {
            $ranqEx = new RanqxException();
            $ranqEx->setException($message->getMessage());
            $em->persist($ranqEx);
        });
        $this->logger->critical($message->getMessage());
        $this->metric('ranqx_error');
    }
}