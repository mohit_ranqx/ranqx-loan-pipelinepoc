<?php declare(strict_types = 1);


namespace App\MessageHandler;

use App\Decision\Rule\Revenue;
use App\Entity\DecisionData;
use App\Entity\Organisation;
use App\Entity\RanqxException;
use App\Message\DecisionMakeMessage;
use App\Message\DecisionRequestMessage;
use App\Message\RanqxExceptionMessage;
use App\Service\CalculateRevenue;
use Aws\Ses\SesClient;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\Message\CalculusReceiveMessage;
use App\Service\MakeRevenueRollDecision;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class OrgCalculusMessageHandler
 * @package App\MessageHandler
 */
class OrgCalculusMessageHandler implements MessageHandlerInterface
{


    /**
     * @var EntityManagerInterface
     */
    private $objectManager;

    /**
     * @var MessageBusInterface
     */
    private $bus;
    /**
     * @var ParameterBagInterface
     */
    private $para;

    private $logger;


    /**
     * OrgCalculusMessageHandler constructor.
     * @param EntityManagerInterface $objectManager
     * @param MessageBusInterface $messageBus
     * @param ParameterBagInterface $bag
     */
    public function __construct(
        EntityManagerInterface $objectManager,
        MessageBusInterface $messageBus,
        ParameterBagInterface $bag,
        LoggerInterface $logger
    ) {
        $this->objectManager = $objectManager;
        $this->bus = $messageBus;
        $this->para = $bag;
        $this->logger = $logger;
    }

    /**
     * @param CalculusReceiveMessage $message
     */
    public function __invoke(CalculusReceiveMessage $message)
    {
        /** @var Organisation|null $org **/
        $org = $this->objectManager->getRepository(Organisation::class)->find($message->getContent());
        if ($org) {
            $cal = new CalculateRevenue($org);
            $cal->calculate($this->objectManager);

            if (($app = $org->getApplicant()) !== null) {
                $decision = $this->para->get('decision');
                if ($app->getId() !== null) {
                    $this->bus->dispatch(new DecisionRequestMessage(
                        $org->getId(),
                        $app->getId(),
                        'What is loan type',
                        $decision['bank']
                    ));
                } else {
                    $this->bus->dispatch(new RanqxExceptionMessage('[CalculusReceiveMessage] Applicant id not exits.'));
                    $this->logger->critical('[CalculusReceiveMessage] Applicant id not exits.');
                }
            } else {
                $message = '[CalculusReceiveMessage] Applicant not exists for org, id is ['.$org->getId().']';
                $message .= ' name is ' . $org->getName();
                $this->bus->dispatch(new RanqxExceptionMessage($message));
                $this->logger->critical($message);
            }
        }
        $this->objectManager->flush();
    }
}
