<?php declare(strict_types=1);


namespace App\MessageHandler;

use App\Entity\Applicant;
use App\Entity\ApplicantException;
use App\Entity\RanqxException;
use App\Message\ApplicantExceptionMessage;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\TypeTrait\DataDogMetric;

/**
 * Class ApplicantExceptionMessageHandler
 * @package App\MessageHandler
 */
class ApplicantExceptionMessageHandler implements MessageHandlerInterface
{
    use DataDogMetric;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var LoggerInterface
     */
    private $logger;


    /**
     * ApplicantExceptionMessageHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @param ApplicantExceptionMessage $message
     */
    public function __invoke(ApplicantExceptionMessage $message)
    {
        try {
            $appId = $message->getAppId();
            $app = $this->entityManager->getRepository(Applicant::class)
                ->find($appId);

            if ($app) {
                $this->entityManager->transactional(static function (EntityManagerInterface $em) use ($app, $message) {
                    /** @var ApplicantException ** */
                    $appEx = new ApplicantException();
                    $appEx->setApplicant($app);
                    $appEx->setException($message->getMessage());
                    $em->persist($appEx);
                });
            } else {
                $this->entityManager->transactional(static function (EntityManagerInterface $em) use ($appId) {
                    $ranqEx = new RanqxException();
                    $ranqEx->setException("couldn't find applicant from " . $appId . ' in ApplicantExceptionMessageHandler');
                    $em->persist($ranqEx);
                });
            }
            $this->metric('applicant_error');
            $this->logger->critical($message->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            $this->metric('applicant_exception_error');
        }
    }
}