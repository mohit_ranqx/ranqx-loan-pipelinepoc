<?php declare(strict_types=1);


namespace App\MessageHandler;

use App\Entity\Organisation;
use App\Message\FinalEmailMessage;
use App\TypeTrait\EnvEmail;
use Aws\Ses\SesClient;
use Doctrine\ORM\EntityManagerInterface;
use PHPMailer\PHPMailer\Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Environment;
use PHPMailer\PHPMailer\PHPMailer;
use App\Entity\Finance;
use App\Entity\FinanceMonth;
use App\Entity\FinanceInvoice;
use App\Entity\CalculateFinanceMonth;
use App\Entity\CalculateFinanceCurrentMonth;
use App\Entity\CalculateFinanceYearly;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class FinalEmailMessageHandler
 * @package App\MessageHandler
 */
class FinalEmailMessageHandler
{
    use EnvEmail;
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    /**
     * @var SesClient
     */
    private $mailer;

    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * @var Environment
     */
    private $twig;


    /**
     * FinalEmailMessageHandler constructor.
     * @param EntityManagerInterface $objectManager
     * @param SesClient $mailer
     * @param ParameterBagInterface $params
     * @param Environment $twig
     */
    public function __construct(
        EntityManagerInterface $objectManager,
        SesClient $mailer,
        ParameterBagInterface $params,
        Environment $twig
    )
    {
        $this->objectManager = $objectManager;
        $this->mailer = $mailer;
        $this->params = $params;
        $this->twig = $twig;
    }

    /**
     * @param FinalEmailMessage $message
     * @throws Exception
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function __invoke(FinalEmailMessage $message)
    {
        $orgId = $message->orgId();
        if ($orgId) {
            $org = $this->objectManager->getRepository(Organisation::class)->find($orgId);
            if ($org) {
                $decision = $message->decision();
                $this->sendEmail($org, $decision);
            }
        }
    }

    /**
     * @param Organisation $org
     * @param array $answer
     * @throws Exception
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function sendEmail(Organisation $org, array $answer)
    {
        //get form database payload once it is in there
        $_payLoadFileNameAddition = '1906091359_applicationID_LoanDecision_payload_';

        if ($org) {
            $envEmails = $this->getEnvEmails();
            $mailText = $this->twig
                ->render('\kb\forKB.html.twig', ['org' => $org, 'answer' => $answer]);

            $mail = new PHPMailer();

            $mail->setFrom($envEmails['from']);

            foreach ($this->bccToArray() as $bcc) {
                $mail->addBCC($bcc);
            }

            if ($this->isOverrideExist()) {
                $mail->addAddress($envEmails['override'], 'override');
            } else {
                $mail->addAddress($org->getApplicant()->getEmail());
            }


            $filesystem = new Filesystem();

            /**
             * Rmove Finance CSV year from payload
             *

            $finCsv = $this->financialCsv($org);
            $path = $filesystem->tempnam(sys_get_temp_dir(), 'ranqx_fin_');
            $filesystem->dumpFile($path, $finCsv);
            $mail->addAttachment($path, $_payLoadFileNameAddition.'finance.csv');
            */

            $finMonthCsv = $this->financialMonthCsv($org);
            $path = $filesystem->tempnam(sys_get_temp_dir(), 'ranqx_fin_month_');
            $filesystem->dumpFile($path, $finMonthCsv);
            $mail->addAttachment($path, $_payLoadFileNameAddition.'finance_month.csv');

            $cal = $this->calculateDataCsv($org);
            $path = $filesystem->tempnam(sys_get_temp_dir(), 'ranqx_cal_');
            $filesystem->dumpFile($path, $cal);
            $mail->addAttachment($path, $_payLoadFileNameAddition.'finance_calc_month_rolling.csv');

            $cal = $this->calculateCurrentMonthDataCsv($org);
            $path = $filesystem->tempnam(sys_get_temp_dir(), 'ranqx_cal_');
            $filesystem->dumpFile($path, $cal);
            $mail->addAttachment($path, $_payLoadFileNameAddition.'finance_calc_month.csv');

            $cal = $this->calculateYearlyDataCsv($org);
            $path = $filesystem->tempnam(sys_get_temp_dir(), 'ranqx_cal_');
            $filesystem->dumpFile($path, $cal);
            $mail->addAttachment($path, $_payLoadFileNameAddition.'finance_calc_year.csv');

            $cal = $this->debtorConcentrationCsv($org);
            $path = $filesystem->tempnam(sys_get_temp_dir(), 'ranqx_fin_debt_');
            $filesystem->dumpFile($path, $cal);
            $mail->addAttachment($path, $_payLoadFileNameAddition.'debtor_concentration.csv');


            $mail->Subject = 'Kiwibank Loan Application';

            $plaintext_body = 'Applicant Name:' . $org->getApplicant()->getFirstName() . ' ' . $org->getApplicant()->getLastName() . "\n";
            $plaintext_body .= 'Applicant Business Name: ' . $org->getApplicant()->getBusinessName() . "\n";
            $plaintext_body .= 'Applicant Loan Amount ' . $org->getApplicant()->getLoanAmount() . "\n\n";
            $plaintext_body .= "Decision Made: " . $answer['answer'];


            $mail->Body = $mailText;
            $mail->AltBody = $plaintext_body;

            $mail->preSend();
            $message = $mail->getSentMIMEMessage();
            $result = $this->mailer->sendRawEmail([
                'RawMessage' => [
                    'Data' => $message
                ]
            ]);
        }
    }

    private function financialCsv(Organisation $organisation)
    {
        $header = 'Period, Created At,Currency,Total Revenue,Total COG,Total People,Total Rent,Total Operating,Total Other,Total Assets Current Bank, Total Assets Current Debtors,Total Assets Current Inventory,Total Assets Current Other,Total Assets Fixed,Total Assets Other,Total Liabilities Current Bank,Total Liabilities Current Creditors,Total Liabilities Current Other,Total Liabilities Other,Total Equity Capital,Total Equity Earnings,Interest';
        $body = '';
        /** @var Finance $finance ** */
        foreach ($organisation->getFinances() as $finance) {
            $body .= $finance->getPeriod()->format('Y-m-d H:i:s') . ',';
            $body .= $finance->getCreatedAt()->format('Y-m-d H:i:s') . ',';
            $body .= $finance->getCurrency() . ',';
            $body .= $finance->getTotalRevenue() . ',';
            $body .= $finance->getTotalCog() . ',';
            $body .= $finance->getTotalPeople() . ',';
            $body .= $finance->getTotalRent() . ',';
            $body .= $finance->getTotalOperating() . ',';
            $body .= $finance->getTotalOther() . ',';
            $body .= $finance->getTotalAssetsCurrentBank() . ',';
            $body .= $finance->getTotalAssetsCurrentDebtors() . ',';
            $body .= $finance->getTotalAssetsCurrentInventory() . ',';
            $body .= $finance->getTotalAssetsCurrentOther() . ',';
            $body .= $finance->getTotalAssetsFixed() . ',';
            $body .= $finance->getTotalAssetsOther() . ',';
            $body .= $finance->getTotalLiabilitiesCurrentBank() . ',';
            $body .= $finance->getTotalLiabilitiesCurrentCreditors() . ',';
            $body .= $finance->getTotalLiabilitiesCurrentOther() . ',';
            $body .= $finance->getTotalLiabilitiesOther() . ',';
            $body .= $finance->getTotalEquityCapital() . ',';
            $body .= $finance->getTotalEquityEarnings() . ',';
            $body .= $finance->getInterest() . "\n";
        }
        return str_replace("\n", '', $header) . "\n" . $body;
    }

    private function financialMonthCsv(Organisation $organisation)
    {
        $header = 'Period, Created At,Currency,Total Revenue,Total COG,Total People,Total Rent,Total Operating,Total Other,Total Assets Current Bank, Total Assets Current Debtors,Total Assets Current Inventory,Total Assets Current Other,Total Assets Fixed,Total Assets Other,Total Liabilities Current Bank,Total Liabilities Current Creditors,Total Liabilities Current Other,Total Liabilities Other,Total Equity Capital,Total Equity Earnings,Interest';

        $body = '';
        /** @var FinanceMonth $finance ** */
        foreach ($organisation->getFinanceMonths() as $finance) {
            $body .= $finance->getPeriod()->format('Y-m-d H:i:s') . ',';
            $body .= $finance->getCreatedAt()->format('Y-m-d H:i:s') . ',';
            $body .= $finance->getCurrency() . ',';
            $body .= $finance->getTotalRevenue() . ',';
            $body .= $finance->getTotalCog() . ',';
            $body .= $finance->getTotalPeople() . ',';
            $body .= $finance->getTotalRent() . ',';
            $body .= $finance->getTotalOperating() . ',';
            $body .= $finance->getTotalOther() . ',';
            $body .= $finance->getTotalAssetsCurrentBank() . ',';
            $body .= $finance->getTotalAssetsCurrentDebtors() . ',';
            $body .= $finance->getTotalAssetsCurrentInventory() . ',';
            $body .= $finance->getTotalAssetsCurrentOther() . ',';
            $body .= $finance->getTotalAssetsFixed() . ',';
            $body .= $finance->getTotalAssetsOther() . ',';
            $body .= $finance->getTotalLiabilitiesCurrentBank() . ',';
            $body .= $finance->getTotalLiabilitiesCurrentCreditors() . ',';
            $body .= $finance->getTotalLiabilitiesCurrentOther() . ',';
            $body .= $finance->getTotalLiabilitiesOther() . ',';
            $body .= $finance->getTotalEquityCapital() . ',';
            $body .= $finance->getTotalEquityEarnings() . ',';
            $body .= $finance->getInterest() . "\n";
        }
        return str_replace("\n", '', $header) . "\n" . $body;
    }

    private function calculateDataCsv(Organisation $organisation)
    {
        $results = $this->objectManager->getRepository(CalculateFinanceMonth::class)
            ->findBy(['organisation_id' => $organisation->getId()]);

        $header = 'Revenue Rolling,Gross Margin Value Rolling,Gross Margin Percentage Rolling,Ebitda Value Rolling,Ebitda Value Percentage Rolling,Interest Cover Rolling,Mrginal Cashflow Rolling,Debt Leverage Rolling,Debitor Days Rolling,Creditor Days Rolling,Defence Internal Ratio Rolling,Expense Cover Ratio Rolling,Operating Cost Ratio Rolling,People Cost Ratio Rolling,KB Defensive Ratio,KB Expense Cover Ratio,Period start,Period End';

        $body = '';
        /** @var CalculateFinanceMonth $fm ** */
        foreach ($results as $fm) {
            $body .= $fm->getRevenueRolling(). ',';
            $body .= $fm->getGrossMarginValueRolling(). ',';
            $body .= $fm->getGrossMarginPercentageRolling(). ',';
            $body .= $fm->getEbitdaValueRolling(). ',';
            $body .= $fm->getEbitdaValuePercentageRolling(). ',';
            $body .= $fm->getInterestCoverRolling(). ',';
            $body .= $fm->getMarginalCashflowRolling(). ',';
            $body .= $fm->getDebtLeverageRolling(). ',';
            $body .= $fm->getDebitorDaysRolling(). ',';
            $body .= $fm->getCreditorDaysRolling(). ',';
            $body .= $fm->getDefenceInternalRatioRolling(). ',';
            $body .= $fm->getExpenseCoverRatioRolling(). ',';
            $body .= $fm->getOperatingCostRatioRolling(). ',';
            $body .= $fm->getPeopleCostRatioRolling(). ',';
            $body .= $fm->getKbDefensiveInterval(). ',';
            $body .= $fm->getKbExpenseCoverRatio() . ',';

            if ($fm->getPeriodStart()) {
                $body .= $fm->getPeriodStart()->format('Y-m-d H:i:s') . ',';
            }

            if ($fm->getPeriodEnd()) {
                $body .= $fm->getPeriodEnd()->format('Y-m-d H:i:s');
            }

            $body .= "\n";
        }
        return str_replace("\n", '', $header) . "\n" . $body;
    }

    private function calculateCurrentMonthDataCsv(Organisation $organisation)
    {
        $results = $this->objectManager->getRepository(CalculateFinanceCurrentMonth::class)
            ->findBy(['organisation_id' => $organisation->getId()]);

        $header = 'Revenue,Gross Margin Value,Gross Margin Percentage,Working Capital Ratio,Ebitda Value,Ebitda Value Percentage,Interest Cover,Marginal Cashflow,
        Quick Ratio,Cash Ratio,Net Cash Balance,Debt Ratio,Equity Ratio,Operating Cost Ratio,People Cost Ratio,Period start,Period End';
        $body = '';
        /** @var CalculateFinanceCurrentMonth $fcm ** */
        foreach ($results as $fcm) {
            $body .= $fcm->getRevenueCurrent() . ',';
            $body .= $fcm->getGrossMarginValueCurrent() . ',';
            $body .= $fcm->getGrossMarginPercentageCurrent() . ',';
            $body .= $fcm->getWorkingCapitalRatioCurrent() . ',';
            $body .= $fcm->getEbitdaValueCurrent() . ',';
            $body .= $fcm->getEbitdaMarginCurrent() . ',';
            $body .= $fcm->getInterestCoverCurrent() . ',';
            $body .= $fcm->getMarginalCashflowCurrent() . ',';
            $body .= $fcm->getQuickRatioCurrent() . ',';
            $body .= $fcm->getCashRatioCurrent() . ',';
            $body .= $fcm->getNetCashBalanceCurrent() . ',';
            $body .= $fcm->getDebtRatioCurrent() . ',';
            $body .= $fcm->getEquityRatioCurrent() . ',';
            $body .= $fcm->getOperatingCostRatioCurrent() . ',';
            $body .= $fcm->getPeopleCostRatioCurrent() . ',';

            if ($fcm->getPeriodStart()) {
                $body .= $fcm->getPeriodStart()->format('Y-m-d H:i:s') . ',';
            }
            if ($fcm->getPeriodEnd()) {
                $body .= $fcm->getPeriodEnd()->format('Y-m-d H:i:s');
            }
            $body .= "\n";
        }
        return str_replace("\n", '', $header) . "\n" . $body;
    }

    private function calculateYearlyDataCsv(Organisation $organisation)
    {
        $results = $this->objectManager->getRepository(CalculateFinanceYearly::class)
            ->findBy(['organisation_id' => $organisation->getId()]);

        $header = 'Revenue Growth Yearly,Stock Days Yearly,Period Start,Period End';
        $body = '';
        /** @var CalculateFinanceYearly $fy ** */
        foreach ($results as $fy) {
            $body .= $fy->getRevenueGrowthYearly() . ',';
            $body .= $fy->getStockDaysYearly() . ',';

            if ($fy->getPeriodStart()) {
                $body .= $fy->getPeriodStart()->format('Y-m-d H:i:s') . ',';
            }

            if ($fy->getPeriodEnd()) {
                $body .= $fy->getPeriodEnd()->format('Y-m-d H:i:s');
            }
            $body .= "\n";
        }
        return str_replace("\n", '', $header) . "\n" . $body;
    }

    private function debtorConcentrationCsv(Organisation $organisation)
    {
        $results = $this->objectManager->getRepository(FinanceInvoice::class)
            ->findBy(['organisation_id' => $organisation->getId()]);

        $header = 'Debtor Concentration,Period Start,Period End';
        $body = '';
        /** @var FinanceInvoice $fy ** */
        foreach ($results as $fy) {
            $body .= $fy->getDebtorConcentration() . ',';

            if ($fy->getPeriodStart()) {
                $body .= $fy->getPeriodStart()->format('Y-m-d H:i:s') . ',';
            }

            if ($fy->getPeriodEnd()) {
                $body .= $fy->getPeriodEnd()->format('Y-m-d H:i:s');
            }
            $body .= "\n";
        }
        return str_replace("\n", '', $header) . "\n" . $body;
    }
}
