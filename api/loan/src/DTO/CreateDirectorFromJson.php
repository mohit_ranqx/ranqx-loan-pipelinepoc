<?php declare(strict_types=1);

namespace App\DTO;

/**
 * Class CreateDirectorFromJson
 * @package App\DTO
 */
class CreateDirectorFromJson
{
    /**
     * @var string
     */
    private $json;

    /**
     * CreateDirectorFromJson constructor.
     * @param string $json
     */
    public function __construct(string $json)
    {
        $this->json = $json;
    }

    public function toArray(): array
    {
        $data = \json_decode($this->json, true);
        $directors = [];
        if (isset($data['roles'])) {
            foreach ($data['roles'] as $role) {
                if ($role['endDate'] === null) {
                    $directors[] = $role['rolePerson'][0];
                }
            }
        }

        return $directors;
    }
}