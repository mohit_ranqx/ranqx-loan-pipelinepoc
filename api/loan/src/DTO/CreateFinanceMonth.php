<?php declare(strict_types = 1);


namespace App\DTO;

use App\Entity\FinanceMonth;
use DateTime;

class CreateFinanceMonth
{
    private $data;

    public function __construct(array $dataFromRequest)
    {
        $this->data = $dataFromRequest;
    }

    public function build() : FinanceMonth
    {
        $fin = new FinanceMonth();
        $fin->setCreatedAt(new DateTime($this->data['createdAt']));
        $fin->setRawData($this->data['rawData']);
        $fin->setPeriod(new DateTime($this->data['period']));
        $fin->setUpdatedAt(new DateTime());
        $fin->setCurrency($this->data['currency']);
        $fin->setTotalRevenue($this->data['totalRevenue']);
        $fin->setTotalCog($this->data['totalCog']);
        $fin->setTotalPeople($this->data['totalPeople']);
        $fin->setTotalRent($this->data['totalRent']);
        $fin->setTotalOperating($this->data['totalOperating']);
        $fin->setTotalOther($this->data['totalOther']);
        $fin->setTotalAssetsCurrentBank($this->data['totalAssetsCurrentBank']);
        $fin->setTotalAssetsCurrentDebtors($this->data['totalAssetsCurrentDebtors']);
        $fin->setTotalAssetsCurrentInventory($this->data['totalAssetsCurrentInventory']);
        $fin->setTotalAssetsCurrentOther($this->data['totalAssetsCurrentOther']);
        $fin->setTotalAssetsFixed($this->data['totalAssetsFixed']);
        $fin->setTotalAssetsOther($this->data['totalAssetsOther']);
        $fin->setTotalLiabilitiesCurrentBank($this->data['totalLiabilitiesCurrentBank']);
        $fin->setTotalLiabilitiesCurrentCreditors($this->data['totalLiabilitiesCurrentCreditors']);
        $fin->setTotalLiabilitiesCurrentOther($this->data['totalLiabilitiesCurrentOther']);
        $fin->setTotalLiabilitiesOther($this->data['totalLiabilitiesOther']);
        $fin->setTotalEquityCapital($this->data['totalEquityCapital']);
        $fin->setTotalEquityEarnings($this->data['totalEquityEarnings']);
        $fin->setStaffNumber(1);
        $fin->setOrganisation($this->data['organisation']);
        return $fin;
    }
}
