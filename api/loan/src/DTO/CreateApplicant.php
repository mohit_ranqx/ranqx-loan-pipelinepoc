<?php declare(strict_types = 1);


namespace App\DTO;

use App\Entity\Applicant;

/**
 * Class CreateApplicant
 * @package App\DTO
 */
class CreateApplicant
{
    /**
     * @var array
     */
    private $data;

    /**
     * CreateApplicant constructor.
     * @param array $dataFromRequest
     */
    public function __construct(array $dataFromRequest)
    {
        $this->data = $dataFromRequest;
    }

    /**
     * @return Applicant
     */
    public function build() : Applicant
    {
        /**
            businessName	ranqx
            email	anru.chen@ranqx.com
            firstName	anru
            id	null
            isCustomer	no
            lastName	chen
            loanAmount	55000
            loanReason	fg
            phone	02041355177
            salutation null
            loanTerm
         ***/
        $app = new Applicant();
        foreach($this->data as $key => $data) {
            if (!property_exists($app, $key)) {
                continue;
            }

            $functionName = "set" . ucfirst($key);

            if (!method_exists($app, $functionName)) {
                continue;
            }

            $app->$functionName($data);
        }

        return $app;
    }
}
