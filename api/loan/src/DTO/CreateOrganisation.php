<?php declare(strict_types = 1);


namespace App\DTO;

use App\Entity\Address;
use App\Entity\Organisation;
use DateTime;
use Exception;

/**
 * Class CreateOrganisation
 * @package App\DTO
 */
class CreateOrganisation
{

    /**
     * @var array
     */
    private $data;

    /**
     * CreateOrganisation constructor.
     * @param array $dataFromRequest
     */
    public function __construct(array $dataFromRequest)
    {
        $this->data = $dataFromRequest;
    }


    /**
     * @return Organisation
     * @throws Exception
     */
    public function build() : Organisation
    {
        $org = new Organisation();
        $org->setProviderName($this->data['providerName'] ?? '');
        $org->setName($this->data['name'] ?? '');
        $org->setLegalName($this->data['legalName'] ?? '');
        $org->setPaysTax($this->data['paysTax'] ?? false);
        $org->setVersion($this->data['version'] ?? '');
        $org->setOrganisationType($this->data['organisationType'] ?? '');
        $org->setBaseCurrency($this->data['currency'] ?? '');
        $org->setCountryCode($this->data['countryCode'] ?? '');
        $org->setIsDemoCompany($this->data['isDemoCompany'] ?? false);
        $org->setOrganisationStatus($this->data['organisationStatus'] ?? 'active');
        $org->setFinancialYearEndDay((int)($this->data['financialYearEndDay'] ?? 28));
        $org->setFinancialYearEndMonth((int)($this->data['financialYearEndMonth'] ?? 5));
        $org->setSalesTaxBasis($this->data['salesTaxBasis'] ?? '');
        $org->setDefaultSalesTax($this->data['defaultSalesTax'] ?? 'Tax Exclusive');
        $org->setDefaultPurchasesTax($this->data['defaultPurchasesTax'] ?? 'Tax Exclusive');
        $org->setCreatedInXero(new DateTime($this->data['createdInXero']));
        $org->setOrganisationEntityType($this->data['organisationEntityType'] ?? 'COMPANY');
        //OrganisationID
        $org->setOrganisationID($this->data['organisationID'] ?? '8f02130b-f4f6-4d62-9b33-6314b826c034');
        $org->setEdition($this->data['edition'] ?? 'BUSINESS');
        $org->setXeroClass($this->data['xeroClass'] ?? 'STANDARD');
        $org->setLineOfBusiness($this->data['lineOfBusiness'] ?? 'Test data');

        $address = new Address();
        $address->setAddressLine1($this->data['addressLine1'] ?? '');
        $address->setAddressLine2($this->data['addressLine2'] ?? '');
        $address->setAddressType($this->data['addressType'] ?? 'POBOX');
        $address->setCity($this->data['city'] ?? '');
        $address->setCountry($this->data['country'] ?? '');
        $address->setPostalCode($this->data['postalCode'] ?? '');
        $org->setAddress($address);
        $org->setCreated(new DateTime());
        return $org;
    }
}
