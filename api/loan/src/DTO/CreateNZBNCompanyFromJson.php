<?php declare(strict_types=1);


namespace App\DTO;

use App\Entity\NZBNCompany;

/**
 * Class CreateNZBNCompanyFromJson
 * @package App\DTO
 */
class CreateNZBNCompanyFromJson
{
    /**
     * @var string
     */
    private $json;

    /**
     * CreateNZBNCompanyFromJson constructor.
     * @param string $json
     */
    public function __construct(string $json)
    {
        $this->json = $json;
    }

    /**
     * @return NZBNCompany
     * @throws \Exception
     */
    public function toNZBNCompany(): NZBNCompany
    {
        $data = \json_decode($this->json, true);
        $nzbn = new NZBNCompany();
        $nzbn->setName($data['entityName'] ?? '');
        $nzbn->setCompanyNumber($data['sourceRegisterUniqueIdentifier'] ?? '');
        $nzbn->setNzbn($data['nzbn'] ?? '');
        $nzbn->setStatus($this->getStatus($data));

        $nzbn->setCountryOfOrigin($this->getCountryOfOrigin($data));
        $nzbn->setRegistrationDate(new \DateTime($data['registrationDate'] ?? ''));
        $nzbn->setSourceRegister($data['sourceRegister'] ?? '');

        $nzbn->setClassificationCode($data['industryClassification'][0]['classificationCode'] ?? '');
        $nzbn->setClassificationDescription($data['industryClassification'][0]['classificationDescription'] ?? '');

        return $nzbn;
    }

    /**
     * @param array $data
     * @return bool
     */
    private function getStatus(array $data): bool
    {
        return isset($data['entityStatusCode']) && $data['entityStatusCode'] === '50';
    }

    /**
     * @param array $data
     * @return string
     */
    private function getCountryOfOrigin(array $data): string
    {
        if (isset($data['company'][0]['countryOfOrigin']) && $data['company'][0]['countryOfOrigin'] !== null) {
            return $data['company'][0]['countryOfOrigin'];
        }
        return '';
    }
}