<?php declare(strict_types = 1);


namespace App\DataFixtures;


use App\DTO\CreateApplicant;
use App\DTO\CreateFinanceMonth;
use App\DTO\CreateOrganisation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class OldestDataFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $app = $this->loadOneApplicant();
        $manager->persist($app);
        $manager->flush();

        $org = $this->loadOneOrganisation();
        $org->setApplicant($app);
        $manager->persist($org);
        $manager->flush();

        for ($i = 0; $i < 23; $i++) {
            $fin = $this->loadFinanceMonth($org);
            $manager->persist($fin);
        }

        $fin = $this->loadFinanceMonthWithDate($org, '2018-08-02 03:41:20');
        $manager->persist($fin);
        $manager->flush();
    }

    private function loadOneOrganisation()
    {
        $data = [];
        $data['providerName'] = 'Ranqx Limited';
        $data['name'] = 'Bob\'s Building London';
        $data['legalName'] = 'Bob\'s Building London';
        $data['paysTax'] = 0;
        $data['version'] = 'NZ';
        $data['organisationType'] = 'COMPANY';
        $data['currency'] = 'NZD';
        $data['countryCode'] = 'NZ';
        $data['isDemoCompany'] = 0;
        $data['organisationStatus'] = 'ACTIVE';
        $data['financialYearEndDay'] = 31;
        $data['financialYearEndMonth'] = 3;
        $data['salesTaxBasis'] = 'NONE';
        $data['defaultSalesTax'] = 'Tax Exclusive';
        $data['defaultPurchasesTax'] = 'Tax Exclusive';
        $data['createdInXero'] = '2015-11-14 19:22:32';
        $data['organisationEntityType'] = 'COMPANY';
        $data['organisationID'] = '8f02130b-f4f6-4d62-9b33-6314b826c034';
        $data['edition'] = 'BUSINESS';
        $data['xeroClass'] = 'STANDARD';
        $data['addressLine1'] = 'Box 109570';
        $data['addressLine2'] = 'Newmarket';
        $data['addressType'] = 'POBOX';
        $data['city'] = 'Auckland';
        $data['country'] = 'New Zealand';
        $data['postalCode'] = '1149';
        $data['lineOfBusiness'] = 'Residential Building';

        $org = new CreateOrganisation($data);
        return $org->build();
    }

    private function loadFinanceMonthWithDate($org, $date)
    {
        $data = [];
        $data['organisation'] = $org;
        $data['totalEquityEarnings'] = 160257.64;
        $data['totalEquityCapital'] = 243115;
        $data['totalLiabilitiesOther'] = 249436;
        $data['totalLiabilitiesCurrentOther'] = 263120.85;
        $data['totalLiabilitiesCurrentCreditors'] = 20411.57;
        $data['totalLiabilitiesCurrentBank'] = 14000;
        $data['totalAssetsOther'] = 0;
        $data['totalAssetsFixed'] = 548752.17;
        $data['totalAssetsCurrentOther'] = 133056;
        $data['totalAssetsCurrentInventory'] = 137307;
        $data['totalAssetsCurrentDebtors'] = 76475.89;
        $data['totalAssetsCurrentBank'] = 54750;
        $data['totalOther'] = 1829.58;
        $data['totalOperating'] = 12158.08;
        $data['totalRent'] = 5519.92;
        $data['totalPeople'] = 42315;
        $data['totalCog'] = 68463.17;
        $data['totalRevenue'] = 163153.04;
        $data['currency'] = 'NZD';
        $data['period'] = $date;
        $data['rawData'] = '[{"id":"1b55de0b-919a-4748-8ec2-83e6d358ef17","code":"ASS:BANK","title":"Main Account","price":52750,"type":"A-CB","old_type":"A-CB"},{"id":"c56dc000-d107-4085-b104-f22625408c65","code":"ASS:BANK","title":"Online Sales Receipts","price":13000,"type":"L-CB","old_type":"L-CB"},{"id":"f15d09af-1506-4c7e-b9da-e436ef61aaca","code":"ASS.CUR.REC.TRA:CURRENT","title":"Accounts Receivable","price":88475.89,"type":"A-CD","old_type":"A-CD"},{"id":"d59bbf29-868b-4e9b-bff7-ece0b2a0093a","code":"ASS.CUR.REC.TRA:CURRENT","title":"less Provision for Doubtful Debts","price":-12000,"type":"A-CD","old_type":"A-CD"},{"id":"b19370ca-b989-4f7f-a219-f114dc026d56","code":"ASS:CURRENT","title":"Debtors","price":83056,"type":"A-CO","old_type":"A-CO"},{"id":"77c94cc4-2477-47c2-aa58-d6ebaab49409","code":"ASS.CUR.REC.PRE:CURRENT","title":"Prepayments","price":50000,"type":"A-CO","old_type":"A-CO"},{"id":"6713ee31-97c2-4bcd-8553-77febef1fc0e","code":"ASS:CURRENT","title":"Stock on Hand","price":137307,"type":"A-CS","old_type":"A-CS"},{"id":"12550f43-7434-4b87-8ca2-554d271e4cae","code":"ASS:FIXED","title":"Accumulated Depreciation on Vehicles","price":-42000,"type":"A-F","old_type":"A-F"},{"id":"9c59505a-99c3-4f05-8d01-a7d61834de76","code":"ASS.NCA.FIX.OWN:FIXED","title":"Computer Equipment","price":65152.17,"type":"A-F","old_type":"A-F"},{"id":"48623a3b-e8b5-4002-8259-1b9aa1964d63","code":"ASS.NCA.FIX.OWN.ACC:FIXED","title":"Less Accumulated Depreciation on Computer Equipment","price":-34000,"type":"A-F","old_type":"A-F"},{"id":"6576fa1a-f9d7-4625-ac2f-fba034712fc0","code":"ASS:FIXED","title":"Intangible Assets","price":125000,"type":"A-F","old_type":"A-F"},{"id":"50291630-b1ee-4d40-b5e6-0c4633c49c91","code":"ASS:FIXED","title":"Motor Vehicles","price":147000,"type":"A-F","old_type":"A-F"},{"id":"fbdbde1d-a9de-45df-a846-a9b3ca8cf912","code":"ASS.NCA.FIX.OWN.FUR:FIXED","title":"Office Equipment","price":184600,"type":"A-F","old_type":"A-F"},{"id":"8247dd54-d998-4863-9d01-96a9d2970137","code":"ASS.NCA.FIX.OWN.FUR.ACC:FIXED","title":"Less Accumulated Depreciation on Office Equipment","price":-74000,"type":"A-F","old_type":"A-F"},{"id":"81c5fd54-c5df-4435-a6c0-de776499063b","code":"ASS:FIXED","title":"Plant","price":247000,"type":"A-F","old_type":"A-F"},{"id":"d45dcea9-e630-4f4f-8f30-d2a0d56832eb","code":"ASS:FIXED","title":"Plant Depreciation","price":-70000,"type":"A-F","old_type":"A-F"},{"id":"aa026d6e-181e-4408-a680-4e489d613a5b","code":"LIA.CUR.PAY.TRA:CURRLIAB","title":"Accounts Payable","price":20411.57,"type":"L-CC","old_type":"L-CC"},{"id":"cceea8cd-55c1-425f-a3b0-b4288b8d35aa","code":"LIA:CURRLIAB","title":"Creditors","price":95011,"type":"L-CO","old_type":"L-CO"},{"id":"f3f44281-97e2-4c2c-a40c-c4ff5369c558","code":"ASS:BANK","title":"FMC - CC1","price":1000,"type":"L-CB","old_type":"L-CB"},{"id":"99e005f4-55ff-4bc3-a505-54baf9400bd9","code":"ASS:BANK","title":"FMC - CC2","price":2000,"type":"A-CB","old_type":"A-CB"},{"id":"dfd0d5d1-bf07-4385-ba7a-a9cba822bb1c","code":"LIA.CUR.TAX.GST:CURRLIAB","title":"GST","price":1108.85,"type":"L-CO","old_type":"L-CO"},{"id":"15533467-5b1d-40b5-acad-f00d84fc60fd","code":"LIA.CUR.TAX.INC:CURRLIAB","title":"Income Tax","price":87000,"type":"L-CO","old_type":"L-CO"},{"id":"db1e8b72-f1b9-4e39-af61-fff10f2f0fb1","code":"LIA.CUR.PAY.PAY:CURRLIAB","title":"PAYE Payable","price":80000,"type":"L-CO","old_type":"L-CO"},{"id":"b36328e3-c15b-489b-a245-fb1106177680","code":"LIA:CURRLIAB","title":"Salary Received","price":1,"type":"L-CO","old_type":"L-CO"},{"id":"1230759c-5adf-462e-9df9-d4f9e6f5ff45","code":"LIA.NCL.LOA:TERMLIAB","title":"Loan","price":249436,"type":"L-O","old_type":"L-O"},{"id":"5e835c08-d925-4015-a0c6-7406a08ff9dd","code":"LIA.NCL.ADV:EQUITY","title":"Owner A Funds Introduced","price":243115,"type":"E-C","old_type":"E-C"},{"id":"5fcf2a18-e841-4d25-ba7e-cd73b7ae6395","code":"EQU.RET:EQUITY","title":"Retained Earnings","price":127390.35,"type":"E-E","old_type":"E-E"},{"id":"a25285a7-0c2a-4a45-8212-185aa35827fc","code":"REV.TRA:REVENUE","title":"Sales","price":163153.04,"type":"IS-REV","old_type":"IS-REV"},{"id":"3f3e8c46-f497-4ed1-b499-913bb51393e1","code":"EXP.COS:DIRECTCOSTS","title":"Cost of Goods Sold","price":68463.17,"type":"IS-COG","old_type":"IS-COG"},{"id":"41860524-5254-40fe-b1c5-59ed058dc598","code":"EXP:EXPENSE","title":"Administration Salaries","price":12604,"type":"IS-PPL","old_type":"IS-PPL"},{"id":"1d58ebb6-28c0-4003-bcbf-67d36cc6a8d0","code":"EXP:OVERHEADS","title":"Advertising","price":152.58,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"b7eec02d-38ff-478d-93b7-e7ad27c3dda9","code":"EXP:EXPENSE","title":"Computer Costs","price":710.92,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"21a6cb7c-d72a-4350-835c-dcbf7a81bfdc","code":"EXP:OVERHEADS","title":"Consulting & Accounting","price":177.67,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"b6e9e6de-c4ed-4f90-be9a-a993a0f3b113","code":"EXP.DEP:OVERHEADS","title":"Depreciation","price":1187.58,"type":"IS-ODN","old_type":"IS-ODN"},{"id":"63eab9b6-2cd0-4dcd-a4e6-74a9908dcdc6","code":"EXP.ENT:OVERHEADS","title":"Entertainment","price":456,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"40ed0ed5-8812-41f8-bb4b-3f0cb83d7c53","code":"EXP.INT:OVERHEADS","title":"Interest Expense","price":642,"type":"IS-OIN","old_type":"IS-OIN"},{"id":"7c8c5b90-92f7-4de8-a1bc-f6c988ea5e83","code":"EXP:OVERHEADS","title":"Light, Power, Heating","price":1362.08,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"397760b5-3a0c-43ea-afbd-08141fcc5308","code":"EXP:EXPENSE","title":"Management Fees","price":402,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"a7b8571f-ab00-4281-98e2-1923a30919a6","code":"EXP:OVERHEADS","title":"Office Expenses","price":52,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"ee5c0e8e-c86a-4a7b-b599-1afa5f28980d","code":"EXP:OVERHEADS","title":"Printing & Stationery","price":71.42,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"dbacb810-17c7-480f-80ff-4def498e6226","code":"EXP:EXPENSE","title":"Rates","price":1033.25,"type":"IS-RNT","old_type":"IS-RNT"},{"id":"ae440bdc-882e-417c-a829-3c837194fbca","code":"EXP.REN:OVERHEADS","title":"Rent","price":4486.67,"type":"IS-RNT","old_type":"IS-RNT"},{"id":"a448e6a2-780c-415d-9227-ccc2de37c4d7","code":"EXP.REP:OVERHEADS","title":"Repairs and Maintenance","price":1461.67,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"e906ec95-e6c0-4297-a3eb-227798575db3","code":"EXP.WAG:OVERHEADS","title":"Salaries","price":29711,"type":"IS-PPL","old_type":"IS-PPL"},{"id":"a93929f8-f0cf-4d06-a2e1-2c157baca138","code":"EXP:OVERHEADS","title":"Telephone & Internet","price":1871.33,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"5bfb74ad-fbfb-4d40-a2c5-a805ec0742ea","code":"EXP.TRA.NAT:OVERHEADS","title":"Travel - National","price":273.33,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"e5afdda3-16c9-4428-b3cf-a73433d0003c","code":"EXP:EXPENSE","title":"Vehicle","price":5167.08,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"is-pplo","code":"IS-PPLO","title":"Adjustments","price":0,"type":"IS-PPLO","old_type":"IS-PPLO"},{"id":"CYE","code":null,"title":"Current Year Earnings","price":32867.29000000001,"type":"E-E","old_type":"E-E"},{"id":"is-pplo","code":"IS-PPLO","title":"Adjustments","price":0,"type":"IS-PPLO","old_type":"IS-PPLO"}]';
        $data['createdAt'] = '2019-07-31 03:41:20';

        $fin = new CreateFinanceMonth($data);
        return $fin->build();
    }

    private function loadFinanceMonth($org)
    {
        $data = [];
        $data['organisation'] = $org;
        $data['totalEquityEarnings'] = 160257.64;
        $data['totalEquityCapital'] = 243115;
        $data['totalLiabilitiesOther'] = 249436;
        $data['totalLiabilitiesCurrentOther'] = 263120.85;
        $data['totalLiabilitiesCurrentCreditors'] = 20411.57;
        $data['totalLiabilitiesCurrentBank'] = 14000;
        $data['totalAssetsOther'] = 0;
        $data['totalAssetsFixed'] = 548752.17;
        $data['totalAssetsCurrentOther'] = 133056;
        $data['totalAssetsCurrentInventory'] = 137307;
        $data['totalAssetsCurrentDebtors'] = 76475.89;
        $data['totalAssetsCurrentBank'] = 54750;
        $data['totalOther'] = 1829.58;
        $data['totalOperating'] = 12158.08;
        $data['totalRent'] = 5519.92;
        $data['totalPeople'] = 42315;
        $data['totalCog'] = 68463.17;
        $data['totalRevenue'] = 163153.04;
        $data['currency'] = 'NZD';
        $data['period'] = '2019-07-31 03:41:20';
        $data['rawData'] = '[{"id":"1b55de0b-919a-4748-8ec2-83e6d358ef17","code":"ASS:BANK","title":"Main Account","price":52750,"type":"A-CB","old_type":"A-CB"},{"id":"c56dc000-d107-4085-b104-f22625408c65","code":"ASS:BANK","title":"Online Sales Receipts","price":13000,"type":"L-CB","old_type":"L-CB"},{"id":"f15d09af-1506-4c7e-b9da-e436ef61aaca","code":"ASS.CUR.REC.TRA:CURRENT","title":"Accounts Receivable","price":88475.89,"type":"A-CD","old_type":"A-CD"},{"id":"d59bbf29-868b-4e9b-bff7-ece0b2a0093a","code":"ASS.CUR.REC.TRA:CURRENT","title":"less Provision for Doubtful Debts","price":-12000,"type":"A-CD","old_type":"A-CD"},{"id":"b19370ca-b989-4f7f-a219-f114dc026d56","code":"ASS:CURRENT","title":"Debtors","price":83056,"type":"A-CO","old_type":"A-CO"},{"id":"77c94cc4-2477-47c2-aa58-d6ebaab49409","code":"ASS.CUR.REC.PRE:CURRENT","title":"Prepayments","price":50000,"type":"A-CO","old_type":"A-CO"},{"id":"6713ee31-97c2-4bcd-8553-77febef1fc0e","code":"ASS:CURRENT","title":"Stock on Hand","price":137307,"type":"A-CS","old_type":"A-CS"},{"id":"12550f43-7434-4b87-8ca2-554d271e4cae","code":"ASS:FIXED","title":"Accumulated Depreciation on Vehicles","price":-42000,"type":"A-F","old_type":"A-F"},{"id":"9c59505a-99c3-4f05-8d01-a7d61834de76","code":"ASS.NCA.FIX.OWN:FIXED","title":"Computer Equipment","price":65152.17,"type":"A-F","old_type":"A-F"},{"id":"48623a3b-e8b5-4002-8259-1b9aa1964d63","code":"ASS.NCA.FIX.OWN.ACC:FIXED","title":"Less Accumulated Depreciation on Computer Equipment","price":-34000,"type":"A-F","old_type":"A-F"},{"id":"6576fa1a-f9d7-4625-ac2f-fba034712fc0","code":"ASS:FIXED","title":"Intangible Assets","price":125000,"type":"A-F","old_type":"A-F"},{"id":"50291630-b1ee-4d40-b5e6-0c4633c49c91","code":"ASS:FIXED","title":"Motor Vehicles","price":147000,"type":"A-F","old_type":"A-F"},{"id":"fbdbde1d-a9de-45df-a846-a9b3ca8cf912","code":"ASS.NCA.FIX.OWN.FUR:FIXED","title":"Office Equipment","price":184600,"type":"A-F","old_type":"A-F"},{"id":"8247dd54-d998-4863-9d01-96a9d2970137","code":"ASS.NCA.FIX.OWN.FUR.ACC:FIXED","title":"Less Accumulated Depreciation on Office Equipment","price":-74000,"type":"A-F","old_type":"A-F"},{"id":"81c5fd54-c5df-4435-a6c0-de776499063b","code":"ASS:FIXED","title":"Plant","price":247000,"type":"A-F","old_type":"A-F"},{"id":"d45dcea9-e630-4f4f-8f30-d2a0d56832eb","code":"ASS:FIXED","title":"Plant Depreciation","price":-70000,"type":"A-F","old_type":"A-F"},{"id":"aa026d6e-181e-4408-a680-4e489d613a5b","code":"LIA.CUR.PAY.TRA:CURRLIAB","title":"Accounts Payable","price":20411.57,"type":"L-CC","old_type":"L-CC"},{"id":"cceea8cd-55c1-425f-a3b0-b4288b8d35aa","code":"LIA:CURRLIAB","title":"Creditors","price":95011,"type":"L-CO","old_type":"L-CO"},{"id":"f3f44281-97e2-4c2c-a40c-c4ff5369c558","code":"ASS:BANK","title":"FMC - CC1","price":1000,"type":"L-CB","old_type":"L-CB"},{"id":"99e005f4-55ff-4bc3-a505-54baf9400bd9","code":"ASS:BANK","title":"FMC - CC2","price":2000,"type":"A-CB","old_type":"A-CB"},{"id":"dfd0d5d1-bf07-4385-ba7a-a9cba822bb1c","code":"LIA.CUR.TAX.GST:CURRLIAB","title":"GST","price":1108.85,"type":"L-CO","old_type":"L-CO"},{"id":"15533467-5b1d-40b5-acad-f00d84fc60fd","code":"LIA.CUR.TAX.INC:CURRLIAB","title":"Income Tax","price":87000,"type":"L-CO","old_type":"L-CO"},{"id":"db1e8b72-f1b9-4e39-af61-fff10f2f0fb1","code":"LIA.CUR.PAY.PAY:CURRLIAB","title":"PAYE Payable","price":80000,"type":"L-CO","old_type":"L-CO"},{"id":"b36328e3-c15b-489b-a245-fb1106177680","code":"LIA:CURRLIAB","title":"Salary Received","price":1,"type":"L-CO","old_type":"L-CO"},{"id":"1230759c-5adf-462e-9df9-d4f9e6f5ff45","code":"LIA.NCL.LOA:TERMLIAB","title":"Loan","price":249436,"type":"L-O","old_type":"L-O"},{"id":"5e835c08-d925-4015-a0c6-7406a08ff9dd","code":"LIA.NCL.ADV:EQUITY","title":"Owner A Funds Introduced","price":243115,"type":"E-C","old_type":"E-C"},{"id":"5fcf2a18-e841-4d25-ba7e-cd73b7ae6395","code":"EQU.RET:EQUITY","title":"Retained Earnings","price":127390.35,"type":"E-E","old_type":"E-E"},{"id":"a25285a7-0c2a-4a45-8212-185aa35827fc","code":"REV.TRA:REVENUE","title":"Sales","price":163153.04,"type":"IS-REV","old_type":"IS-REV"},{"id":"3f3e8c46-f497-4ed1-b499-913bb51393e1","code":"EXP.COS:DIRECTCOSTS","title":"Cost of Goods Sold","price":68463.17,"type":"IS-COG","old_type":"IS-COG"},{"id":"41860524-5254-40fe-b1c5-59ed058dc598","code":"EXP:EXPENSE","title":"Administration Salaries","price":12604,"type":"IS-PPL","old_type":"IS-PPL"},{"id":"1d58ebb6-28c0-4003-bcbf-67d36cc6a8d0","code":"EXP:OVERHEADS","title":"Advertising","price":152.58,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"b7eec02d-38ff-478d-93b7-e7ad27c3dda9","code":"EXP:EXPENSE","title":"Computer Costs","price":710.92,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"21a6cb7c-d72a-4350-835c-dcbf7a81bfdc","code":"EXP:OVERHEADS","title":"Consulting & Accounting","price":177.67,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"b6e9e6de-c4ed-4f90-be9a-a993a0f3b113","code":"EXP.DEP:OVERHEADS","title":"Depreciation","price":1187.58,"type":"IS-ODN","old_type":"IS-ODN"},{"id":"63eab9b6-2cd0-4dcd-a4e6-74a9908dcdc6","code":"EXP.ENT:OVERHEADS","title":"Entertainment","price":456,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"40ed0ed5-8812-41f8-bb4b-3f0cb83d7c53","code":"EXP.INT:OVERHEADS","title":"Interest Expense","price":642,"type":"IS-OIN","old_type":"IS-OIN"},{"id":"7c8c5b90-92f7-4de8-a1bc-f6c988ea5e83","code":"EXP:OVERHEADS","title":"Light, Power, Heating","price":1362.08,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"397760b5-3a0c-43ea-afbd-08141fcc5308","code":"EXP:EXPENSE","title":"Management Fees","price":402,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"a7b8571f-ab00-4281-98e2-1923a30919a6","code":"EXP:OVERHEADS","title":"Office Expenses","price":52,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"ee5c0e8e-c86a-4a7b-b599-1afa5f28980d","code":"EXP:OVERHEADS","title":"Printing & Stationery","price":71.42,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"dbacb810-17c7-480f-80ff-4def498e6226","code":"EXP:EXPENSE","title":"Rates","price":1033.25,"type":"IS-RNT","old_type":"IS-RNT"},{"id":"ae440bdc-882e-417c-a829-3c837194fbca","code":"EXP.REN:OVERHEADS","title":"Rent","price":4486.67,"type":"IS-RNT","old_type":"IS-RNT"},{"id":"a448e6a2-780c-415d-9227-ccc2de37c4d7","code":"EXP.REP:OVERHEADS","title":"Repairs and Maintenance","price":1461.67,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"e906ec95-e6c0-4297-a3eb-227798575db3","code":"EXP.WAG:OVERHEADS","title":"Salaries","price":29711,"type":"IS-PPL","old_type":"IS-PPL"},{"id":"a93929f8-f0cf-4d06-a2e1-2c157baca138","code":"EXP:OVERHEADS","title":"Telephone & Internet","price":1871.33,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"5bfb74ad-fbfb-4d40-a2c5-a805ec0742ea","code":"EXP.TRA.NAT:OVERHEADS","title":"Travel - National","price":273.33,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"e5afdda3-16c9-4428-b3cf-a73433d0003c","code":"EXP:EXPENSE","title":"Vehicle","price":5167.08,"type":"IS-OPC","old_type":"IS-OPC"},{"id":"is-pplo","code":"IS-PPLO","title":"Adjustments","price":0,"type":"IS-PPLO","old_type":"IS-PPLO"},{"id":"CYE","code":null,"title":"Current Year Earnings","price":32867.29000000001,"type":"E-E","old_type":"E-E"},{"id":"is-pplo","code":"IS-PPLO","title":"Adjustments","price":0,"type":"IS-PPLO","old_type":"IS-PPLO"}]';
        $data['createdAt'] = '2019-07-31 03:41:20';

        $fin = new CreateFinanceMonth($data);
        return $fin->build();
    }

    private function loadOneApplicant()
    {
        $app['businessName'] = 'ranqx';
        $app['email']	= 'anru.chen@ranqx.com';
        $app['firstName'] = 'anru';
        $app['isCustomer'] = 'no';
        $app['lastName'] = 'chen';
        $app['loanAmount']	= 55000;
        $app['loanReason']	= 'fg';
        $app['phone'] =	'02041355177';
        $app['nzbnNumber'] =	'1111111111111'; //13 1s
        $obj = new CreateApplicant($app);
        $user = $obj->build();
        $user->setCreated(new \DateTime('2019-05-30 12:00:00'));
        return $user;
    }

    public static function getGroups(): array
    {
        return ['OldestDataFixtures'];
    }
}