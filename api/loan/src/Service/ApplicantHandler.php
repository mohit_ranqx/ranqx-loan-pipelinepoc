<?php declare(strict_types = 1);


namespace App\Service;

use App\DTO\CreateApplicant;
use App\Normalizer\ConstraintViolationListNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApplicantHandler implements HttpEntityHandlerInterface
{
    private $data;
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function handler(ValidatorInterface $validator): array
    {
        $build = new CreateApplicant($this->data);
        $app = $build->build();

        $violations = $validator->validate($app);
        $normalizer = new ConstraintViolationListNormalizer();
        return $normalizer->normalize($violations);
    }
}
