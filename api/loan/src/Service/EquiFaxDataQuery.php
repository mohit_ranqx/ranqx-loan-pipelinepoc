<?php declare(strict_types=1);

namespace App\Service;

/**
 * Class EquiFaxDataQuery
 * @package App\Service
 */
class EquiFaxDataQuery
{
    /**
     * @var array
     */
    private $data;

    /**
     * EquiFaxDataQuery constructor.
     * @param array $jsonData
     */
    public function __construct(array $jsonData = [])
    {
        $this->data = $jsonData;
    }

    /**
     * @return string
     */
    public function companyName(): string
    {
        return $this->data['equiFax']['db']['companyDetails']['data']['body']['businessDetails']['registeredName'] ?? '';
    }

    /**
     * @return string
     */
    public function standardScore(): string
    {
        //TODO: not implemented yet.
        return '';
    }

    /**
     * @return string
     */
    public function advanceScore(): string
    {
        //TODO: not implemented yet.
        return '';
    }

    /**
     * @return array
     */
    public function directoryNames(): array
    {
        $temp = [];
        if (isset($this->data['equiFax']['db']['directors']['data']['directors'])) {
            foreach($this->data['equiFax']['db']['directors']['data']['directors'] as $value) {
                $str = '';
                if (isset($value['rolePerson']['firstName'])) {
                    $str .= trim($value['rolePerson']['firstName']);
                }

                if (isset($value['rolePerson']['middleNames'])) {
                    $str .= ' '. trim($value['rolePerson']['middleNames']);
                }

                if (isset($value['rolePerson']['lastName'])) {
                    $str .= ' '. trim($value['rolePerson']['lastName']);
                }
                $temp[] = $str;
            }
        }
        return $temp;
    }

    /**
     * @return string
     */
    public function NZCN(): string
    {
        return $this->data['equiFax']['db']['companyDetails']['data']['body']['businessDetails']['organisationNumber'] ?? '';
    }

    /**
     * @return string
     */
    public function ANZSIC(): string
    {
        return $this->data['equiFax']['db']['companyDetails']['data']['body']['businessDetails']['industry']['code'] ?? '';
    }

    /**
     * @return string
     */
    public function ANZSICDesc(): string
    {
        return $this->data['equiFax']['db']['companyDetails']['data']['body']['businessDetails']['industry']['description'] ?? '';
    }

    /**
     * @param string $type
     * @return string
     */
    private function address(string $type): string
    {
        $addresses = $this->data['equiFax']['db']['companyDetails']['data']['body']['addressDetails'] ?? [];

        foreach ($addresses as $add) {
            if (trim($add['typeName']) === $type) {
                return $add['address'] ?? '';
            }
        }
        return '';
    }

    /**
     * @return string
     */
    public function registedAddress(): string
    {
        return $this->address('Registered Address');
    }

    /**
     * @return string
     */
    public function businessAddress(): string
    {
        return $this->address('Address for Service');
    }

    /**
     * @return string
     */
    public function postalAddress(): string
    {
        $add = $this->address('Postal Address');
        if ($add === '') {
            return $this->registedAddress();
        }
        return $add;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function ageOfBusiness(): string
    {
        $age = $this->data['equiFax']['db']['companyDetails']['data']['body']['businessDetails']['registrationDate'] ?? '';
        if ($age !== '') {
            $date = new \DateTime($age);
            return $date->format('d-m-Y');
        }
        return '';
    }

    /**
     * @return string
     */
    public function ageOfBureauFile(): string
    {
        return $this->data['equiFax']['db']['creditActivitySummary']['data']['body']['ageOfCreditFile'] ?? '';
    }

    /**
     * @return string
     */
    public function enqueryInLast2years(): string
    {
        $d = $this->data['equiFax']['decision']['enquiryHistory']['credit_enquiries_24M'] ?? '';
        return (string)$d;
    }

    /**
     * @return string
     */
    public function bureauDefaultsInLast3Years(): string
    {
        //totalEnquires
        $d = $this->data['equiFax']['decision']['enquiryHistory']['totalEnquires'] ?? '';
        return (string)$d;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function timeInBusiness() :string
    {
        return $this->ageOfBusiness();
    }

    /**
     * @return array
     */
    public function individuals(): array
    {
        $ids = $this->data['equiFax']['db']['management']['data']['body']['currentOfficeHolders'] ?? [];
        $temp = [];
        foreach ($ids as $id) {
            if ($id['officeHolderDetails']['role'] === 'Director') {
                $holder = $id['officeHolderDetails'];
                /*$names = explode(' ', $holder['name']);
                if (count($names) === 2) {
                    $holder['firstName'] = $names[0];
                    $holder['lastName'] = $names[1];
                } else if(count($names) === 3) {
                    $holder['firstName'] = $names[0];
                    $holder['middleName'] = $names[1];
                    $holder['lastName'] = $names[2];
                }*/
                $temp[] = $holder;
            }
        }
        return $temp;
    }
}