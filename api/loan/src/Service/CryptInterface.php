<?php declare(strict_types = 1);


namespace App\Service;


interface CryptInterface
{
    public function encode($data) : string ;
    public function decode($data) : string ;
}