<?php declare(strict_types = 1);


namespace App\Service;


use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/**
 * Class S3RanqxClient
 * @package App\Service
 */
class S3RanqxClient
{
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var S3Client
     */
    private $client;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * S3RanqxClient constructor.
     * @param ParameterBagInterface $bag
     * @param S3Client $client
     * @param LoggerInterface $logger
     */
    public function __construct(ParameterBagInterface $bag, S3Client $client, LoggerInterface $logger)
    {
        $this->params = $bag;
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @param string $fileName
     * @return bool
     */
    public function upload(string $fileName) : bool
    {
        $ranqx = $this->params->get('ranqx');
        $bucketName = $ranqx['aws']['s3']['bucket'];

        try {
            // Upload data.
            $info = pathinfo($fileName);
            $result = $this->client->putObject([
                'Bucket' => $bucketName,
                'Key'    => $info['basename'],
                'SourceFile' => $fileName
            ]);

            return true;
        } catch (S3Exception $e) {
            $this->logger->critical($e->getMessage());
            return false;
        }
    }

    /**
     *
     */
    public function createBucket()
    {

    }
}