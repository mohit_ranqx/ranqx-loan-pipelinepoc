<?php declare(strict_types=1);


namespace App\Service;

use App\Entity\NZBNCompany;

/**
 * Class NZBNCompanyService
 * @package App\Service
 */
class NZBNCompanyService
{
    /**
     * @var NZBNCompany
     */
    private $company;

    /**
     * NZBNCompanyService constructor.
     * @param NZBNCompany $company
     */
    public function __construct(NZBNCompany $company)
    {
        $this->company = $company;
    }

    /**
     * @return bool
     */
    public function excluded(): bool
    {
        return !$this->isActive() || $this->excludedIndustry() || $this->excludeForeignCompanyAndNonLtd();
    }

    /**
     * @return bool
     */
    public function isActive():bool
    {
        return $this->company->getStatus();
    }

    /**
     * @return bool
     */
    public function excludedIndustry(): bool
    {
        $pattern = "%^C111|";
        $pattern .= '^L671150|';
        $pattern .= '^A04|';
        $pattern .= '^A03|';
        $pattern .= '^ES211|';
        $pattern .= '^L6632|';
        $pattern .= '^H440055|';
        $pattern .= '^J5514|';
        $pattern .= '^C161|';
        $pattern .= '^J541|';
        $pattern .= '^K622|';
        $pattern .= '^R920|';
        $pattern .= '^D261110|';
        $pattern .= '^A015985|';
        $pattern .= '^B0700|';
        $pattern .= '^S953410';
        $pattern .= '%ism';

        return preg_match($pattern, $this->company->getClassificationCode()) === 1;
    }

    /**
     * @return bool
     */
    public function excludeForeignCompanyAndNonLtd(): bool
    {
        return $this->company->getSourceRegister() !== 'LTD';
    }
}