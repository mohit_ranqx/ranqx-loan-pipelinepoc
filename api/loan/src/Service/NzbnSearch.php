<?php declare(strict_types = 1);


namespace App\Service;

use App\Entity\NZBNCompany;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Psr\Log\LoggerInterface;

/**
 * Class NzbnSearch
 * @package App\Service
 */
class NzbnSearch
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * NzbnSearch constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param LoggerInterface $logger;
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->logger = $logger;
    }

    /**
     * @param string $term
     * @return mixed
     */
    public function search(string $term)
    {
        $handler = new NzbnHandler($term);
        $errors = $handler->handler($this->validator);
        //TODO:standardize our response .
        $rest['http_code'] = 200;
        $rest['data'] = [];
        $rest['error'] = $errors;

        if (empty($errors)) {
            try {
                $value = $this->processedSearchTerm($term);

                if ($value === null) {
                    $error = 'search term is null, or contains invalid characters after char processing';
                    $rest['http_code'] = 422;
                    $rest['error'] = $error;
                    $this->logger->error($error . '[' . $term. ']');
                    return $rest;
                }

                $breakOutTerm = $this->breakSearchTerm($value);
                $query = $this->getQuery($breakOutTerm);
                $qb = null;
                if ($this->searchByShort($breakOutTerm) || $this->detectingShortSentence($term)) {
                    $query = $this->shortQuery($term);
                    if ($query !== null) {
                        $qb = $this->entityManager->getRepository(NZBNCompany::class)
                            ->searchByShort(trim($query));
                    }
                } else {
                    /** @var QueryBuilder $qb * */
                    $qb = $this->entityManager->getRepository(NZBNCompany::class)
                        ->matchTerm(trim($query));
                }
                if ($qb) {
                    $result = $qb->getQuery()->setMaxResults(10)->getArrayResult();
                    $rest['data'] = $result;
                } else {
                    $rest['data'] = [];
                }
            } catch (Exception $e) {
                $this->logger->critical($e->getMessage());
                $rest['http_code'] = 500;
            }
        } else {
            $rest['http_code'] = 422; //Unprocessable Entity
        }

        return $rest;
    }

    /**
     * @param array $breakOutTerm
     * @param int $lenLimit
     * @return bool
     */
    private function searchByShort(array $breakOutTerm, $lenLimit = 4): bool
    {
        $len = 0;
        foreach ($breakOutTerm as $item) {
            if (trim($item) !== '') {
                $len += strlen($item);
            }
        }

        return $len < $lenLimit;
    }

    /**
     * @param string $symbol
     * @param mixed $searchTerm
     * @return bool
     */
    private function contains(string $symbol, $searchTerm): bool
    {
        return strpos($searchTerm, $symbol) !== -1;
    }

    /**
     * @param string $searchTerm
     * @return string|null
     */
    private function shortQuery(string $searchTerm): ?string
    {
        if ($this->contains('&', $searchTerm)) {
            //L & Y
            if ($this->detectingShortSentence($searchTerm)) {
                return $searchTerm;
            }

            if (strlen(trim($searchTerm)) < 6) {
                return  $searchTerm;
            }
        }

        return $this->processedSearchTerm($searchTerm);
    }

    /**
     * @param array $searchTerms
     * @return string
     */
    private function getQuery(array $searchTerms): string
    {
        $query = array_reduce($searchTerms, function ($carry, $item) {
            if (trim($item) !== '') {
                $carry .= ' '.$item;
            }
            return $carry;
        }, '');

        return $query;
    }

    /**
     * @param string $search
     * @return string|null
     */
    private function processedSearchTerm(string $search): ?string
    {
        //TODO: 'limited' is a stop word, better load it into Mysql DB.
        return preg_replace("/[^A-Za-z0-9]|(limited)/ism", ' ', trim($search));
    }

    /**
     * @param string $search
     * @return array
     */
    private function breakSearchTerm(string $search): array
    {
        return explode(' ', trim($search));
    }

    /**
     * @param string $search
     * @return bool
     */
    private function detectingShortSentence(string $search): bool
    {
        //L & Y limited, K & Z limited,
        $pattern = '%^[a-zA-Z]\s*&\s*[a-zA-Z]%ism';
        return (bool)preg_match($pattern, $search);
    }
}
