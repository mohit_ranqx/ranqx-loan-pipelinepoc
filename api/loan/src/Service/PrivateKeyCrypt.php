<?php declare(strict_types = 1);


namespace App\Service;

use InvalidArgumentException;
use RuntimeException;

/**
 * Class PrivateKeyCrypt
 * @package App\Service
 */
class PrivateKeyCrypt implements CryptInterface
{
    /**
     * @var resource|bool
     */
    private $key;

    /**
     * @var resource|bool
     */
    private $pub;

    //Block size for encryption block cipher
    /**
     * @var int
     */
    private $ENCRYPT_BLOCK_SIZE = 200;// this for 2048 bit key for example, leaving some room

    //Block size for decryption block cipher
    /**
     * @var int
     */
    private $DECRYPT_BLOCK_SIZE = 256;// this again for 2048 bit key

    /**
     * PrivateKeyCrypt constructor.
     * @param string $whereIsPrivateKey
     */
    public function __construct(string $whereIsPrivateKey, string $whereIsPublicKey)
    {
        $this->key = openssl_pkey_get_private($whereIsPrivateKey);
        if (!$this->key) {
            throw new InvalidArgumentException('unable to access private key file.');
        }

        $this->pub = openssl_pkey_get_public($whereIsPublicKey);
        if (!$this->pub) {
            throw new InvalidArgumentException('unable to access public key file.');
        }
    }


    /**
     * @param string $data
     * @return string
     * @throws RuntimeException
     */
    public function decode($data): string
    {
        $decrypted = '';
        //decode must be done before spliting for getting the binary String
        $result = str_split($this->base64url_decode($data), $this->DECRYPT_BLOCK_SIZE);

        if (is_array($result)) {
            foreach ($result as $chunk) {
                $partial = '';

                //be sure to match padding
                if (!is_bool($this->pub)) {
                    $decryptionOK = openssl_public_decrypt($chunk, $partial, $this->pub, OPENSSL_PKCS1_PADDING);

                    if ($decryptionOK === false) {
                        new RuntimeException('decryption failed.');
                    }
                    $decrypted .= $partial;
                } else {
                    new RuntimeException('decryption failed. public key is a bool value, not string.');
                }
            }

            return $decrypted;
        }

        throw new RuntimeException('decryption decode split failed.');
    }

    /**
     * @param string|false|null $plainData
     * @return string
     */
    public function encode($plainData): string
    {
        $encrypted = '';
        $result = str_split($plainData, $this->ENCRYPT_BLOCK_SIZE);
        if (is_array($result)) {
            foreach ($result as $chunk) {
                $partialEncrypted = '';

                //using for example OPENSSL_PKCS1_PADDING as padding
                if (!is_bool($this->key)) {
                    $encryptionOk = openssl_private_encrypt($chunk, $partialEncrypted, $this->key,
                        OPENSSL_PKCS1_PADDING);

                    if ($encryptionOk === false) {
                        new RuntimeException('encryption failed.');
                    }
                    $encrypted .= $partialEncrypted;
                } else {
                    throw new RuntimeException('decryption failed. private key is a bool value.');
                }
            }
            return $this->base64url_encode($encrypted);//encoding the whole binary String as MIME base 64
        }

        throw new RuntimeException('decryption encode split failed.');
    }

    /**
     * @param string $data
     * @return string
     */
    private function base64url_encode(string $data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    /**
     * @param string $data
     * @return string
     * @throws RuntimeException;
     */
    private function base64url_decode(string $data): string
    {
        $result =  base64_decode(strtr($data, '-_', '+/') . str_repeat('=', 3 - (3 + strlen($data)) % 4));
        if ($result === false) {
            new RuntimeException('encryption base64 decode failed.');
        } else {
            return $result;
        }
    }

    /**
     *
     */
    public function __destruct()
    {
        if (!is_bool($this->key)) {
            openssl_free_key($this->key);
        }
        if (!is_bool($this->pub)) {
            openssl_free_key($this->pub);
        }
    }
}
