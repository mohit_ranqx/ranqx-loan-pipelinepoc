<?php declare(strict_types = 1);


namespace App\Service;
use App\Normalizer\ConstraintViolationListNormalizer;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class NzbnHandler
 * @package App\Service
 */
class NzbnHandler implements HttpEntityHandlerInterface
{
    /**
     * @var string $data
     */
    private $data;

    /**
     * NzbnHandler constructor.
     * @param string $searchTerm
     */
    public function __construct(string $searchTerm)
    {
        $this->data = $searchTerm;
    }

    /**
     * @param ValidatorInterface $validator
     * @return array
     */
    public function handler(ValidatorInterface $validator): array
    {
        $value = trim($this->data);

        $violations = $validator->validate(
            $value,
            [new Assert\Length(['min' => 3, 'max' => 64]), new Assert\NotBlank()]
        );

        $normalizer = new ConstraintViolationListNormalizer();
        return $normalizer->normalize($violations, null, ['title' => 'search term']);
    }

}