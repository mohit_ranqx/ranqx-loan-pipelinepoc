<?php declare(strict_types = 1);


namespace App\Service;

use App\DTO\CreateApplicant;
use App\Entity\Applicant;
use App\Entity\User;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\TypeTrait\ReferenceNumber;
use App\Message\EmailForApplicantMessage;
use App\TypeTrait\XeroKey;

/**
 * Class LoadApplicant
 * @package App\Service
 */
class LoadApplicant implements LoadEntityInterface
{
    use ReferenceNumber;
    use XeroKey;
    /**
     * @var array
     */
    private $data;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var ParameterBagInterface
     */
    private $params;

    private $bus;

    /**
     * LoadApplicant constructor.
     * @param array $data
     * @param ValidatorInterface $validator
     */
    public function __construct(
        array $data,
        ValidatorInterface $validator
    ) {
        $this->data = $data;
        $this->validator = $validator;
    }

    /**
     * @param ObjectManager $manager
     * @return array
     */
    public function load(ObjectManager $manager): array
    {
        $handler = new ApplicantHandler($this->data);
        $violations = $handler->handler($this->validator);

        $temp = ['http_code' => 400];

        if (empty($violations)) {
            $build = new CreateApplicant($this->data);
            $app = $build->build();

            $crypt = new PrivateKeyCrypt(
                $this->privateKey(),
                $this->publicKey()
            );

            if (isset($this->data['id']) && $this->data['id'] !== null) {
                $idJSON = json_decode($crypt->decode($this->data['id']));
                $id = $idJSON->appId;
                $app->setId($id);
                $manager->merge($app);
            } else {
                $manager->persist($app);
            }

            $manager->flush();

            //applicant is save into db, we need to generate a reference number.
            //assumption here, we only use one DB.
            //potential problem.
            //in test environment, this reference number is used to generate file name
            //and when test DB re-load, we will get duplicated reference number.
            $rf = $this->KBRefNumber($app->getId());
            $app->setReferenceNumber($rf);
            $this->data['referenceNumber'] = $rf;
            $manager->flush();

            $temp['http_code'] = 200;

            // Strip query if in referer
            $referer = strtok($this->data['tenant'], "?");

            $tenantEntity = $manager->getRepository(User::class);
            $tenantRecord = $tenantEntity->findBy(['referer' => $referer, 'active' => 1]);
            $tenant = null;

            if (isset($tenantRecord[0])) {
                /** @var User $objT**/
                $objT = $tenantRecord[0];
                $tenant = $objT->getName();
            }

            $idJSON = [
                'appId' => (string) $app->getId(),
                'tenant' => $tenant,
            ];

            $this->data['id'] = $crypt->encode(json_encode($idJSON));
        }
        $temp['data'] = $this->data;
        $temp['error'] = $violations;
        return $temp;
    }
}
