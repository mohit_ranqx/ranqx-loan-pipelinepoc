<?php declare(strict_types=1);


namespace App\Service;


use App\Entity\NZBNCompany;
use App\Normalizer\ConstraintViolationListNormalizer;
use App\Validator\NzbnDirectorNumberValidator;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class NzbnDirectors
 * @package App\Service
 */
class NzbnDirectors
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * NzbnDirectors constructor.
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     * @param ValidatorInterface $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->validator = $validator;
    }

    /**
     * @param int $nzbnNumber
     * @return array
     */
    public function search(int $nzbnNumber): array
    {
        $errors = $this->validate($nzbnNumber);

        $rest['http_code'] = 200;
        $rest['data'] = [];
        $rest['error'] = $errors;

        if (empty($errors)) {
            try {
                /** @var NZBNCompany|null $obj **/
                $obj = $this->entityManager->getRepository(NZBNCompany::class)
                    ->findOneBy(['nzbn' => (string)$nzbnNumber]);
                if ($obj) {
                    $rest['data'] = $obj->getJsonDirectors();
                }
            } catch (\Exception $e) {
                $rest['http_code'] = 500;
                $this->logger->emergency($e->getMessage());
            } finally {
                return $rest;
            }
        } else {
            $rest['http_code'] = 422; //Unprocessable Entity
        }
        return $rest;
    }

    /**
     * @param int $nzbnNumber
     * @return array
     */
    public function validate(int $nzbnNumber): array
    {
        $v = new NzbnDirectorNumberValidator((string)$nzbnNumber, $this->validator);
        return $v->validate();
    }
}