<?php declare(strict_types = 1);


namespace App\Service;

use Doctrine\Common\Persistence\ObjectManager;

/**
 * Interface LoadEntityInterface
 * @package App\Service
 */
interface LoadEntityInterface
{

    /**
     * @param ObjectManager $manager
     * @return array
     */
    public function load(ObjectManager $manager) : array;
}
