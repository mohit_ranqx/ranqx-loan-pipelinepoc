<?php declare(strict_types = 1);


namespace App\Service;


use Symfony\Component\Validator\Validator\ValidatorInterface;

interface HttpEntityHandlerInterface
{
    public function handler(ValidatorInterface $validator): array;
}