<?php //declare(strict_types = 1);

namespace App\Service;

use App\Entity\Organisation;
use DataDog\DogStatsd;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use DateTime;
use App\Entity\CalculateFinanceMonth;
use App\Entity\CalculateFinanceCurrentMonth;
use App\Entity\CalculateFinanceYearly;

/**
 * Class CalculateRevenue
 *
 * @package App\Service
 *
 */
class CalculateRevenue
{
    /**
     * @var int
     */
    private $totalRevenue = 0;

    /**
     * @var int
     */
    private $totalCog = 0;

    /**
     * @var Organisation
     */
    private $organisation;

    /**
     * @var int
     */
    private $organisationId;

    /**
     * @var int Number of months in rolling period
     */
    private $periodLength = 12;

    /**
     * @var int
     */
    private $endDate;

    /**
     * @var int
     */
    private $stepDate = 1;

    /**
     * @var DateTime
     */
    private $startPeriod;

    /**
     * @var DateTime
     */
    private $endPeriod;

    /**
     * @var int
     */
    private $totalPeople = 0;

    /**
     * @var int
     */
    private $totalRent = 0;

    /**
     * @var int
     */
    private $totalOperating = 0;

    /**
     * @var int
     */
    private $totalOther = 0;

    /**
     * @var int
     */
    private $totalAssetsCurrentBank = 0;

    /**
     * @var int
     */
    private $totalAssetsCurrentDebtors = 0;

    /**
     * @var int
     */
    private $totalAssetsCurrentInventory = 0;

    /**
     * @var int
     */
    private $totalAssetsCurrentOther = 0;

    /**
     * @var int
     */
    private $totalAssetsFixed = 0;

    /**
     * @var int
     */
    private $totalLiabilitiesCurrentOther = 0;

    /**
     * @var float|int
     */
    private $totalGrossMarginValue = 0;

    /**
     * @var float|int
     */
    private $totalGrossMarginPercentage = 0;

    /**
     * @var float|int
     */
    private $totalEbitdaValueRolling = 0;

    /**
     * @var int
     */
    private $totalAssetsOther = 0;

    /**
     * @var int
     */
    private $totalLiabilitiesCurrentBank = 0;

    /**
     * @var int
     */
    private $totalLiabilitiesCurrentCreditors = 0;

    /**
     * @var int
     */
    private $totalLiabilitiesOther = 0;

    /**
     * @var int
     */
    private $totalEquityCapital = 0;

    /**
     * @var int
     */
    private $totalEquityEarnings = 0;

    /**
     * @var int
     */
    private $totalStaffNumber = 0;

    /**
     * @var float|int
     */
    private $totalEbitdaMargin = 0;

    /**
     * @var float|int
     */
    private $totalMarginalCashflow = 0;

    /**
     * @var float|int
     */
    private $totalDebtLeverage = 0;

    /**
     * @var float|int
     */
    private $totalDebitorDays = 0;

    /**
     * @var float|int
     */
    private $totalCreditorDays = 0;

    /**
     * @var int
     */
    private $totalDefenceInternalRatio = 0;

    /**
     * @var float|int
     */
    private $totalOperatingCostRatio = 0;

    /**
     * @var float|int
     */
    private $totalPeopleCostRatio = 0;

    /**
     * @var float|int
     */
    private $totalInterestCover = 0;

    /**
     * @var int
     */
    private $totalInterest = 0;

    /**
     * @var float|int
     */
    private $totalExpenseCoverRatio = 0;

    /**
     * @var float|int
     */
    private $totalDefenceInternalRatioDaysExpenses = 0;

    /**
     * @var int
     */
    private $totalRevenueFirstPeriodSet = 0;

    /**
     * @var int
     */
    private $totalRevenueSecondPeriodSet = 0;

    /**
     * @var int
     */
    private $totalCostOfGoodsSoldFirstPeriodSet = 0;

    /**
     * @var int
     */
    private $revenueGrowthYearOnYear = 0;

    /**
     * @var float|int
     */
    private $stockDaysYearOnYear = 0;

    /**
     * @var int
     */
    private $monthRevenue = 0;

    /**
     * @var int
     */
    private $monthGrossMargin = 0;

    /**
     * @var int
     */
    private $monthGrossMarginPercentage = 0;

    /**
     * @var int
     */
    private $monthEbitdaValue = 0;

    /**
     * @var int
     */
    private $monthEbitdaMargin = 0;

    /**
     * @var int
     */
    private $monthInterestCover = 0;

    /**
     * @var float|int
     */
    private $monthMarginalCashflow = 0;

    /**
     * @var float|int
     */
    private $monthCurrentWorkingCapitalRatio = 0;

    /**
     * @var float|int
     */
    private $monthCashRatio = 0;

    /**
     * @var float|int
     */
    private $monthQuickRatio = 0;

    /**
     * @var int
     */
    private $monthNetCashBalance = 0;

    /**
     * @var float|int
     */

    private $monthDebtRatio = 0;

    /**
     * @var float|int
     */
    private $monthEquityRatio = 0;

    /**
     * @var float|int
     */
    private $monthOperatingCostRatio = 0;

    /**
     * @var float|int
     */
    private $monthPeopleCostRatio = 0;

    /**
     * @var float|int
     */
    private $totalKbDefensiveInterval = 0;

    /**
     * @var float|int
     */
    private $totalKbExpenseCoverRatio = 0;

    /**
     * @var DateTime
     */
    private $periodYearlyStart;

    /**
     * @var DateTime
     */
    private $periodYearlyEnd;

    /**
     * @var int Decimal places when handling floats
     */
    private $decimalAccuracy = 7;

    /**
     * @var DogStatsd Datadog logger
     */
    private $statsd;

    /**
     * CalculateRevenue constructor.
     *
     * @param Organisation $organisation entity related to organisation table
     */
    public function __construct(Organisation $organisation)
    {
        $this->statsd = new DogStatsd();

        $this->organisation = $organisation;
        $this->organisationId = $this->organisation->getId();
    }

    /**
     * Bundles all the calculations together to be called by worker
     * @param EntityManagerInterface $entityManager
     * @throws DBALException
     */
    public function calculate(EntityManagerInterface $entityManager)
    {
        if (!$this->calculateRollingFinances($entityManager) ||
            !$this->calculateCurrentMonthFinances($entityManager) ||
            !$this->calculateYearlyFinances($entityManager)
        ) {
            $this->statsd->event(
                'Failed to run all calculations',
                array(
                    'text' => json_encode([
                        'Name' => $this->organisation->getName(),
                        'Organisation' => $this->organisationId,
                    ]),
                    'alert_type' => 'error'
                )
            );
        }
    }

    /**
     * Calculates rolling finances
     *
     * @param EntityManagerInterface $entityManager
     *
     * @return bool
     * @throws DBALException
     * @throws Exception
     */
    public function calculateRollingFinances(EntityManagerInterface $entityManager)
    {
        $financeMonthQuery = "SELECT * 
                                 FROM finance_month
                                 WHERE organisation_id = " . $this->organisationId . "
                                 ORDER BY period DESC";

        $statement = $entityManager->getConnection()->query($financeMonthQuery);
        $financeMonthPeriods = $statement->fetchAll();
        if (!$financeMonthPeriods) {
            return false;
        }
        for ($month = 0; $month != $this->periodLength; $month++) {
            /**
             * Reset the vars on subsequent passes as there will be 12
             * passes of the data on rolling 12 month
             */
            $this->endDate = $month + $this->periodLength;
            $this->totalRevenue = 0;
            $this->totalCog = 0;
            $this->totalPeople = 0;
            $this->totalRent = 0;
            $this->totalOperating = 0;
            $this->totalOther = 0;
            $this->totalAssetsCurrentBank = 0;
            $this->totalAssetsCurrentDebtors = 0;
            $this->totalAssetsCurrentInventory = 0;
            $this->totalAssetsCurrentOther = 0;
            $this->totalAssetsFixed = 0;
            $this->totalLiabilitiesCurrentOther = 0;
            $this->totalGrossMarginValue = 0;
            $this->totalGrossMarginPercentage = 0;
            $this->totalEbitdaValueRolling = 0;
            $this->totalAssetsOther = 0;
            $this->totalLiabilitiesCurrentBank = 0;
            $this->totalLiabilitiesCurrentCreditors = 0;
            $this->totalLiabilitiesOther = 0;
            $this->totalEquityCapital = 0;
            $this->totalEquityEarnings = 0;
            $this->totalStaffNumber = 0;
            $this->totalEbitdaMargin = 0;
            $this->totalMarginalCashflow = 0;
            $this->totalDebtLeverage = 0;
            $this->totalDebitorDays = 0;
            $this->totalCreditorDays = 0;
            $this->totalDefenceInternalRatio = 0;
            $this->totalOperatingCostRatio = 0;
            $this->totalPeopleCostRatio = 0;
            $this->totalInterest = 0;
            $this->totalInterestCover = 0;
            $this->totalDefenceInternalRatioDaysExpenses = 0;
            $this->totalExpenseCoverRatio = 0;
            $this->totalKbDefensiveInterval = 0;
            $this->totalKbExpenseCoverRatio = 0;

            $this->startPeriod = new \DateTime($financeMonthPeriods[$this->endDate]['period']);
            $this->startPeriod
                ->modify('first day of next month')
                ->setTime(0, 0, 0);

            $this->endPeriod = new \DateTime($financeMonthPeriods[$month]['period']);

            /**
             * Add up a rolling 12 months on each
             * of the fields by period run.
             *
             * Balance sheet data - only source the latest data.
             */
            $this->totalAssetsCurrentBank = $financeMonthPeriods[$month]['total_assets_current_bank']; //1
            $this->totalAssetsCurrentDebtors = $financeMonthPeriods[$month]['total_assets_current_debtors']; //2
            $this->totalAssetsCurrentInventory = $financeMonthPeriods[$month]['total_assets_current_inventory']; //3
            $this->totalAssetsCurrentOther = $financeMonthPeriods[$month]['total_assets_current_other']; //4
            $this->totalAssetsFixed = $financeMonthPeriods[$month]['total_assets_fixed']; //5
            $this->totalAssetsOther = $financeMonthPeriods[$month]['total_assets_other']; //6
            $this->totalLiabilitiesCurrentBank = $financeMonthPeriods[$month]['total_liabilities_current_bank']; //7
            $this->totalLiabilitiesCurrentCreditors = $financeMonthPeriods[$month]['total_liabilities_current_creditors']; //8
            $this->totalLiabilitiesCurrentOther = $financeMonthPeriods[$month]['total_liabilities_current_other']; //9
            $this->totalLiabilitiesOther = $financeMonthPeriods[$month]['total_liabilities_other']; //10 ??
            $this->totalEquityCapital = $financeMonthPeriods[$month]['total_equity_capital']; //11
            $this->totalEquityEarnings = $financeMonthPeriods[$month]['total_equity_earnings']; //12

            // Income statement data
            for ($period = $month;
                 $period != $this->endDate;
                 $period += $this->stepDate
            ) {
                $this->totalRevenue += $financeMonthPeriods[$period]['total_revenue']; //13
                $this->totalCog += $financeMonthPeriods[$period]['total_cog']; //14
                $this->totalPeople += $financeMonthPeriods[$period]['total_people']; //15
                $this->totalRent += $financeMonthPeriods[$period]['total_rent']; //16
                $this->totalOperating += $financeMonthPeriods[$period]['total_operating']; //17
                $this->totalOther += $financeMonthPeriods[$period]['total_other']; //not sure
                $this->totalStaffNumber += $financeMonthPeriods[$period]['staff_number']; //15
                $this->totalInterest += $financeMonthPeriods[$period]['interest']; //19
            }

            // Gross margin value
            $this->totalGrossMarginValue = ($this->totalRevenue - $this->totalCog);

            // Gross Margin Percentage
            if ($this->totalRevenue != 0) {
                $this->totalGrossMarginPercentage = (($this->totalRevenue - $this->totalCog) / $this->totalRevenue);
            }

            // Earnings before interest, tax, depreciation and amortization (EBITDA) is a measure of a company's operating performance.
            // EBITDA value
            $this->totalEbitdaValueRolling = ($this->totalRevenue - $this->totalCog - $this->totalPeople - $this->totalRent - $this->totalOperating);

            // EBITDA margin
            if ($this->totalRevenue != 0) {
                $this->totalEbitdaMargin = (($this->totalRevenue - $this->totalCog - $this->totalPeople - $this->totalRent - $this->totalOperating) / $this->totalRevenue);
            }

            // Marginal CashFlow
            if ($this->totalRevenue != 0) {
                $this->totalMarginalCashflow = (
                        ($this->totalRevenue - $this->totalCog) / $this->totalRevenue
                    ) -
                    ($this->totalAssetsCurrentDebtors + $this->totalAssetsCurrentInventory - $this->totalLiabilitiesCurrentCreditors) /
                    $this->totalRevenue;
            }

            // Debt Leverage
            if (($debtLeverageDenominator = $this->totalRevenue - $this->totalCog - $this->totalPeople - $this->totalRent - $this->totalOperating) != 0) {
                $this->totalDebtLeverage = (
                    ($this->totalLiabilitiesCurrentBank + $this->totalLiabilitiesCurrentCreditors + $this->totalLiabilitiesCurrentOther + $this->totalLiabilitiesOther) /
                    $debtLeverageDenominator
                );
            }

            // Debitor Days
            if ($this->totalRevenue != 0) {
                $this->totalDebitorDays = (365 * ($this->totalAssetsCurrentDebtors / $this->totalRevenue));
            }

            // Creditor Days
            if ($this->totalCog != 0) {
                $this->totalCreditorDays = (365 * ($this->totalLiabilitiesCurrentCreditors / $this->totalCog));
            }

            // Defence Internal Ratio (Days Expenses) (1+2+3+4)/((14+15+16+17+19)/365days)
            if (($defenceInternalRatioDenominator = $this->totalCog + $this->totalPeople + $this->totalRent + $this->totalOperating + $this->totalInterest) !== 0) {
                $this->totalDefenceInternalRatioDaysExpenses = (
                    ($this->totalAssetsCurrentBank + $this->totalAssetsCurrentDebtors + $this->totalAssetsCurrentInventory + $this->totalAssetsCurrentOther) /
                    ($defenceInternalRatioDenominator / 365)
                );
            }

            // Total Operating Cost ratio 17/13
            if ($this->totalRevenue != 0) {
                $this->totalOperatingCostRatio = ($this->totalOperating / $this->totalRevenue);
            }

            // Total People Cost ratio 5/13
            if ($this->totalRevenue != 0) {
                $this->totalPeopleCostRatio = ($this->totalPeople / $this->totalRevenue);
            }

            // Total Interest Cover (13-14-15-16-17)/19
            if ($this->totalInterest != 0) {
                $this->totalInterestCover = (($this->totalRevenue - $this->totalCog - $this->totalPeople - $this->totalRent - $this->totalOperating) / $this->totalInterest);
            }

            // Total expense cover ratio (1+7)/((14+15+16+17+19)/365*91days)
            if (($expenseCoverRatioDenominator = $this->totalCog + $this->totalPeople + $this->totalRent + $this->totalOperating + $this->totalInterest) !== 0) {
                $this->totalExpenseCoverRatio = (
                    ($this->totalAssetsCurrentBank + $this->totalLiabilitiesCurrentBank) /
                    (($expenseCoverRatioDenominator / 365) * 91)
                );
            }

            // Custom KB Defensive Interval Ratio 365 * (total_assets_current_bank + total_assets_current_debtors + total_assets_current_inventory + total_assets_current_other) / (total_cog + total_people + total_rent + total_operating + Interest)
            if (($kbDefensiveIntervalDenominator = $this->totalCog + $this->totalPeople + $this->totalRent + $this->totalOperating + $this->totalInterest) !== 0) {
                $this->totalKbDefensiveInterval = 365 * ($this->totalAssetsCurrentBank + $this->totalAssetsCurrentDebtors + $this->totalAssetsCurrentInventory + $this->totalAssetsCurrentOther) / $kbDefensiveIntervalDenominator;
            }

            // KB Expense Cover Ratio  ((total_assets_current_bank)/ (( total_cog + total_people + total_rent + total_operating+ Interest)/365 *91))
            if (($kbExpenseRatioDenominator = $this->totalCog + $this->totalPeople + $this->totalRent + $this->totalOperating + $this->totalInterest) !== 0) {
                $this->totalKbExpenseCoverRatio = $this->totalAssetsCurrentBank /
                    ($kbExpenseRatioDenominator / 365 * 91);
            }

            $calculateFinanceMonth = new CalculateFinanceMonth();
            $calculateFinanceMonth->setRevenueRolling($this->passAsFloat($this->totalRevenue));
            $calculateFinanceMonth->setGrossMarginValueRolling($this->passAsFloat($this->totalGrossMarginValue));
            $calculateFinanceMonth->setGrossMarginPercentageRolling($this->passAsFloat($this->totalGrossMarginPercentage));
            $calculateFinanceMonth->setEbitdaValueRolling($this->passAsFloat($this->totalEbitdaValueRolling));
            $calculateFinanceMonth->setEbitdaValuePercentageRolling($this->passAsFloat($this->totalEbitdaMargin));
            $calculateFinanceMonth->setMarginalCashflowRolling($this->passAsFloat($this->totalMarginalCashflow));
            $calculateFinanceMonth->setDebtLeverageRolling($this->passAsFloat($this->totalDebtLeverage));
            $calculateFinanceMonth->setDebitorDaysRolling($this->passAsFloat($this->totalDebitorDays));
            $calculateFinanceMonth->setCreditorDaysRolling($this->passAsFloat($this->totalCreditorDays));
            $calculateFinanceMonth->setOperatingCostRatioRolling($this->passAsFloat($this->totalOperatingCostRatio));
            $calculateFinanceMonth->setPeopleCostRatioRolling($this->passAsFloat($this->totalPeopleCostRatio));
            $calculateFinanceMonth->setInterestCoverRolling($this->passAsFloat($this->totalInterestCover));
            $calculateFinanceMonth->setDefenceInternalRatioRolling($this->passAsFloat($this->totalDefenceInternalRatioDaysExpenses));
            $calculateFinanceMonth->setExpenseCoverRatioRolling($this->passAsFloat($this->totalExpenseCoverRatio));
            $calculateFinanceMonth->setKbDefensiveInterval($this->passAsFloat($this->totalKbDefensiveInterval));
            $calculateFinanceMonth->setKbExpenseCoverRatio($this->passAsFloat($this->totalKbExpenseCoverRatio));
            $calculateFinanceMonth->setPeriodEnd($this->endPeriod);
            $calculateFinanceMonth->setPeriodStart($this->startPeriod);
            $calculateFinanceMonth->setOrganisationId($this->organisationId);
            $entityManager->persist($calculateFinanceMonth);
        }

        $entityManager->flush();

        return true;
    }

    /**
     * Calculates current month finances and stores in
     * calculate_finance_month_current
     *
     * @param EntityManagerInterface $entityManager
     * @return bool
     * @throws DBALException
     * @throws Exception
     */
    public function calculateCurrentMonthFinances(EntityManagerInterface $entityManager)
    {
        $financeMonthQuery = "SELECT * 
                                 FROM finance_month
                                 WHERE organisation_id = " . $this->organisationId . "
                                 ORDER BY period DESC
                                 LIMIT 1";

        $statement = $entityManager->getConnection()->query($financeMonthQuery);
        $financeMonth = $statement->fetchAll();

        if (!$financeMonth) {
            return false;
        }

        $financeMonth = $financeMonth[0];

        // Revenue 13
        $this->monthRevenue = $financeMonth['total_revenue'];

        // Gross margin - value 13-14
        $this->monthGrossMargin = ($financeMonth['total_revenue'] - $financeMonth['total_cog']);

        // Gross margin percentage (13-14)/13
        if ($financeMonth['total_revenue'] != 0) {
            $this->monthGrossMarginPercentage = (($financeMonth['total_revenue'] - $financeMonth['total_cog']) / $financeMonth['total_revenue']);
        }

        // EBITDA value 13-14-15-16-17
        $this->monthEbitdaValue = ($financeMonth['total_revenue'] - $financeMonth['total_cog'] - $financeMonth['total_people'] - $financeMonth['total_rent'] - $financeMonth['total_operating']);

        // EBITDA margin (13-14-15-16-17)/13
        if ($financeMonth['total_revenue'] != 0) {
            $this->monthEbitdaMargin = (($financeMonth['total_revenue'] - $financeMonth['total_cog'] - $financeMonth['total_people'] - $financeMonth['total_rent'] - $financeMonth['total_operating']) / $financeMonth['total_revenue']);
        }

        // Interest cover (13-14-15-16-17)/19
        if ($financeMonth['interest'] != 0) {
            $this->monthInterestCover = (($financeMonth['total_revenue'] - $financeMonth['total_cog'] - $financeMonth['total_people'] - $financeMonth['total_rent'] - $financeMonth['total_operating']) / $financeMonth['interest']);
        }

        // Marginal Cashflow ((13-14)/13) - ((2+3-8)/13))
        if ($financeMonth['total_revenue'] != 0) {
            $this->monthMarginalCashflow = (
                (($financeMonth['total_revenue'] - $financeMonth['total_cog']) / $financeMonth['total_revenue']) -
                ((($financeMonth['total_assets_current_debtors'] + $financeMonth['total_assets_current_inventory'] - $financeMonth['total_liabilities_current_creditors']) /
                    $financeMonth['total_revenue']))
            );
        }

        // Current (or Working Capital) ratio (1+2+3+4)/(7+8+9)
        if (($financeMonth['total_liabilities_current_bank'] + $financeMonth['total_liabilities_current_creditors'] + $financeMonth['total_liabilities_current_other']) != 0) {
            $this->monthCurrentWorkingCapitalRatio = (
                ($financeMonth['total_assets_current_bank'] + $financeMonth['total_assets_current_debtors'] + $financeMonth['total_assets_current_inventory'] + $financeMonth['total_assets_current_other']) /
                ($financeMonth['total_liabilities_current_bank'] + $financeMonth['total_liabilities_current_creditors'] + $financeMonth['total_liabilities_current_other'])
            );
        }

        // Quick Ratio (1+2-7)/(8+9)
        if (($financeMonth['total_liabilities_current_creditors'] + $financeMonth['total_liabilities_current_other']) != 0) {
            $this->monthQuickRatio = (
                ($financeMonth['total_assets_current_bank'] + $financeMonth['total_assets_current_debtors'] - $financeMonth['total_liabilities_current_bank']) /
                ($financeMonth['total_liabilities_current_creditors'] + $financeMonth['total_liabilities_current_other'])
            );
        }

        // Cash Ratio (1-7)/(8+9)
        if (($financeMonth['total_liabilities_current_creditors'] + $financeMonth['total_liabilities_current_other']) != 0) {
            $this->monthCashRatio = (
                ($financeMonth['total_assets_current_bank'] - $financeMonth['total_liabilities_current_bank']) /
                ($financeMonth['total_liabilities_current_creditors'] + $financeMonth['total_liabilities_current_other'])
            );
        }

        // Net cash balance 1-7
        $this->monthNetCashBalance = ($financeMonth['total_assets_current_bank'] - $financeMonth['total_liabilities_current_bank']);

        // Debt ratio (7+8+9+10)/(1+2+3+4+5+6)
        if (($financeMonth['total_assets_current_bank'] + $financeMonth['total_assets_current_debtors'] + $financeMonth['total_assets_current_inventory'] + $financeMonth['total_assets_current_other'] + $financeMonth['total_assets_fixed'] + $financeMonth['total_assets_other']) != 0) {
            $this->monthDebtRatio = (($financeMonth['total_liabilities_current_bank'] + $financeMonth['total_liabilities_current_creditors'] + $financeMonth['total_liabilities_current_other'] + $financeMonth['total_liabilities_other']) / ($financeMonth['total_assets_current_bank'] + $financeMonth['total_assets_current_debtors'] + $financeMonth['total_assets_current_inventory'] + $financeMonth['total_assets_current_other'] + $financeMonth['total_assets_fixed'] + $financeMonth['total_assets_other']));
        }

        // Equity ratio (11+12)/(1+2+3+4+5+6)
        if (($financeMonth['total_assets_current_bank'] + $financeMonth['total_assets_current_debtors'] + $financeMonth['total_assets_current_inventory'] + $financeMonth['total_assets_current_other'] + $financeMonth['total_assets_fixed'] + $financeMonth['total_assets_other']) != 0) {
            $this->monthEquityRatio = (($financeMonth['total_equity_capital'] + $financeMonth['total_equity_earnings']) / ($financeMonth['total_assets_current_bank'] + $financeMonth['total_assets_current_debtors'] + $financeMonth['total_assets_current_inventory'] + $financeMonth['total_assets_current_other'] + $financeMonth['total_assets_fixed'] + $financeMonth['total_assets_other']));
        }

        // Operating cost ratio 17/13
        if ($financeMonth['total_revenue'] != 0) {
            $this->monthOperatingCostRatio = ($financeMonth['total_operating'] / $financeMonth['total_revenue']);
        }

        // People cost ratio 15/13
        if ($financeMonth['total_revenue'] != 0) {
            $this->monthPeopleCostRatio = ($financeMonth['total_people'] / $financeMonth['total_revenue']);
        }

        $financeStartDateTime = new \DateTime($financeMonth['period']);
        $financeStartDateTime
            ->modify('first day of this month')
            ->setTime(0, 0, 0);

        $financeEndDateTime = new \DateTime($financeMonth['period']);
        $financeEndDateTime->setTime(23, 59, 59);

        $calculateCurrentMonth = new CalculateFinanceCurrentMonth();
        $calculateCurrentMonth->setRevenueCurrent($this->passAsFloat($this->monthRevenue));
        $calculateCurrentMonth->setGrossMarginValueCurrent($this->passAsFloat($this->monthGrossMargin));
        $calculateCurrentMonth->setGrossMarginPercentageCurrent($this->passAsFloat($this->monthGrossMarginPercentage));
        $calculateCurrentMonth->setEbitdaValueCurrent($this->passAsFloat($this->monthEbitdaValue));
        $calculateCurrentMonth->setEbitdaMarginCurrent($this->passAsFloat($this->monthEbitdaMargin));
        $calculateCurrentMonth->setInterestCoverCurrent($this->passAsFloat($this->monthInterestCover));
        $calculateCurrentMonth->setMarginalCashflowCurrent($this->passAsFloat($this->monthMarginalCashflow));
        $calculateCurrentMonth->setWorkingCapitalRatioCurrent($this->passAsFloat($this->monthCurrentWorkingCapitalRatio));
        $calculateCurrentMonth->setCashRatioCurrent($this->passAsFloat($this->monthCashRatio));
        $calculateCurrentMonth->setQuickRatioCurrent($this->passAsFloat($this->monthQuickRatio));
        $calculateCurrentMonth->setNetCashBalanceCurrent($this->passAsFloat($this->monthNetCashBalance));
        $calculateCurrentMonth->setDebtRatioCurrent($this->passAsFloat($this->monthDebtRatio));
        $calculateCurrentMonth->setEquityRatioCurrent($this->passAsFloat($this->monthEquityRatio));
        $calculateCurrentMonth->setOperatingCostRatioCurrent($this->passAsFloat($this->monthOperatingCostRatio));
        $calculateCurrentMonth->setPeopleCostRatioCurrent($this->passAsFloat($this->monthPeopleCostRatio));
        $calculateCurrentMonth->setPeriodStart($financeStartDateTime);
        $calculateCurrentMonth->setPeriodEnd($financeEndDateTime);
        $calculateCurrentMonth->setOrganisationId($this->organisationId);
        $entityManager->persist($calculateCurrentMonth);
        $entityManager->flush();

        return true;
    }

    /**
     * Runs calculations for yearly finances inserting results in
     * calculate_finance_yearly table
     *
     * Performs following calculations:
     * - total_revenue,
     * - total_assets_current_inventory,
     * - total_cog,period
     *
     * @param EntityManagerInterface $entityManager
     *
     * @return bool
     * @throws DBALException
     * @throws Exception
     */
    public function calculateYearlyFinances(EntityManagerInterface $entityManager)
    {
        // Retrieve 24 month finance records
        $financeMonthQuery = "SELECT *
                       FROM finance_month 
                       WHERE organisation_id = " . $this->organisationId . "
                       ORDER BY period DESC";

        $statement = $entityManager->getConnection()->query($financeMonthQuery);
        $financeMonthData = $statement->fetchAll();

        foreach ($financeMonthData as $key => $financeMonth) {
            /*
            * First 12 months
            */
            if ($key < $this->periodLength) {
                $this->totalRevenueFirstPeriodSet += $financeMonth['total_revenue']; //13
                $this->totalCostOfGoodsSoldFirstPeriodSet += $financeMonth['total_cog']; //14

                continue;
            }

            /*
             * Second 12 months
             */
            $this->totalRevenueSecondPeriodSet += $financeMonth['total_revenue'];
        }

        if ($this->totalRevenueSecondPeriodSet != 0) {
            $this->revenueGrowthYearOnYear = (($this->totalRevenueFirstPeriodSet - $this->totalRevenueSecondPeriodSet) / $this->totalRevenueSecondPeriodSet);
        }

        //365*((3+3(prev yr)/2)/14)
        if ($this->totalCostOfGoodsSoldFirstPeriodSet != 0) {
            $this->stockDaysYearOnYear = (
                365 *
                ((($financeMonthData[0]['total_assets_current_inventory'] + $financeMonthData[12]['total_assets_current_inventory']) / 2) /
                    $this->totalCostOfGoodsSoldFirstPeriodSet)
            );
        }

        $this->periodYearlyEnd = new \DateTime($financeMonthData[0]['period']);

        $this->periodYearlyStart = new \DateTime($financeMonthData[11]['period']);
        $this->periodYearlyStart
            ->modify('first day of this month')
            ->setTime(0, 0, 0);

        $calYearly = new CalculateFinanceYearly();
        $calYearly->setRevenueGrowthYearly($this->passAsFloat($this->revenueGrowthYearOnYear));
        $calYearly->setStockDaysYearly($this->passAsFloat($this->stockDaysYearOnYear));
        $calYearly->setPeriodStart($this->periodYearlyStart);
        $calYearly->setPeriodEnd($this->periodYearlyEnd);
        $calYearly->setOrganisationId($this->organisationId);
        $entityManager->persist($calYearly);
        $entityManager->flush();

        return true;
    }

    /**
     * Formats number values into strict float format string
     *
     * @param float|int|string $numeral
     * @return string
     */
    private function passAsFloat($numeral)
    {
        return number_format((float) $numeral, $this->decimalAccuracy, '.', '');
    }
}