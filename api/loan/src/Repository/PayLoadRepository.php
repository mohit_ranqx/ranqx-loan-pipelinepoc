<?php declare(strict_types = 1);


namespace App\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ApplicantRepository
 * @package App\Repository
 */
class PayLoadRepository extends EntityRepository
{
    /**
     * @param string $nzbn
     * @param \DateTime $date
     * @return QueryBuilder
     */
    public function findByDecision(string $organisation): QueryBuilder
    {

    }


    public function scoreCard(string $organisation): QueryBuilder
    {
        // get the scorecard JSON for the applicnat
    }

    public function equifaxPDF(string $organisation): QueryBuilder
    {
        // get the EQUIFAXPDF blob fro the applicant
        // get from the applicant table
    }

    public function finalDecision(string $organisation): QueryBuilder
    {
        // get eh finalDecision values and date created forthe applicant wothut hte JSONS
    }

    public function equiFaxRaw(string $organisation): QueryBuilder
    {
        //get the equi_fax_raw json formt eh application table.
    }

}