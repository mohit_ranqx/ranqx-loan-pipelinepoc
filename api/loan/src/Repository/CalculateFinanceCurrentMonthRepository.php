<?php

namespace App\Repository;

use App\Entity\Organisation;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class CalculateFinanceCurrentMonthRepository extends EntityRepository
{
    /**
     * @param Organisation $organisation
     * @return QueryBuilder
     */
    public function findFirstRevenueRoll(Organisation $organisation): QueryBuilder
    {
        $qb = $this->createQueryBuilder('c');
        $qb->where('c.organisation_id = :org')
            ->orderBy('c.period_start', 'DESC')
            ->setMaxResults(1)
            ->setParameter('org', $organisation->getId());
        return $qb;
    }

    public function findByOrg(Organisation $organisation): QueryBuilder
    {
        $qb = $this->createQueryBuilder('c');
        $qb->where('c.organisation_id = :org')
            ->setParameter('org', $organisation->getId());
        return $qb;
    }
}
