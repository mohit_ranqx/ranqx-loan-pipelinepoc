<?php declare(strict_types = 1);


namespace App\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ApplicantRepository
 * @package App\Repository
 */
class ApplicantRepository extends EntityRepository
{
    /**
     * @param string $nzbn
     * @param \DateTime $date
     * @return QueryBuilder
     */
    public function numberOfNzbn(string $nzbn, \DateTime $date): QueryBuilder
    {
        $qb = $this->createQueryBuilder('app');
        $qb->select('count(app.id)')->where('app.nzbnNumber = :nzbn');
        $qb->andWhere('app.created >= :date');
        $qb->setParameter('nzbn', $nzbn);
        $qb->setParameter('date', $date,Type::DATETIME);
        return $qb;
    }
}