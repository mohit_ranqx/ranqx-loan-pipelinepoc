<?php

namespace App\Repository;

use App\Entity\BlockIps;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BlockIps|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlockIps|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlockIps[]    findAll()
 * @method BlockIps[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlockIpsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BlockIps::class);
    }

    // /**
    //  * @return BlockIps[] Returns an array of BlockIps objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BlockIps
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
