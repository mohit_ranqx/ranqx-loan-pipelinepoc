<?php declare(strict_types = 1);


namespace App\Repository;

use App\Entity\Organisation;
use App\PayLoad\PayLoadPolicyRuleResult;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class FinanceMonthRepository extends EntityRepository
{
    public function findOrg(Organisation $organisation): QueryBuilder
    {
        $fields = ['finance_month.period',
            'finance_month.totalRevenue',
            'finance_month.totalCog',
            'finance_month.totalPeople',
            'finance_month.totalRent',
            'finance_month.totalOperating',
            'finance_month.totalOther',
            'finance_month.totalAssetsCurrentBank',
            'finance_month.totalAssetsCurrentDebtors',
            'finance_month.totalAssetsCurrentInventory',
            'finance_month.totalAssetsCurrentOther',
            'finance_month.totalAssetsFixed',
            'finance_month.totalAssetsOther',
            'finance_month.totalLiabilitiesCurrentBank',
            'finance_month.totalLiabilitiesCurrentCreditors',
            'finance_month.totalLiabilitiesCurrentOther',
            'finance_month.totalLiabilitiesOther',
            'finance_month.totalEquityCapital',
            'finance_month.totalEquityEarnings',
            'finance_month.tax',
            'finance_month.interest',
            'finance_month.depreciation',
            'finance_month.staffNumber'];
        $qb = $this->createQueryBuilder('finance_month');
        $qb->select($fields)->where('finance_month.organisation = :org')
            ->setParameter('org', $organisation)
            ->orderBy('finance_month.period','asc');
        return $qb;
    }

    public function lastData(Organisation $organisation): QueryBuilder
    {
        return $this->monthPeriodRange($organisation, 'asc', 1);
    }

    public function mostRecent(Organisation $organisation): QueryBuilder
    {
        return $this->monthPeriodRange($organisation, 'desc', 1);
    }

    public function balance(Organisation $organisation, $field ,$which = 'current'): QueryBuilder
    {
        if ($which === 'current') {
            $qb = $this->monthPeriodRange($organisation, 'desc', 1);
        } else {
            $qb = $this->monthPeriodRange($organisation, 'asc', 1);
            $qb->setFirstResult(11);
        }

        return $qb->select('finance_month.' . $field . ' as balance');
    }

    public function firstHalfOf24Month(Organisation $organisation): QueryBuilder
    {
        return $this->monthPeriodRange($organisation, 'asc', 12);
    }

    public function lastHalfOf24Month(Organisation $organisation): QueryBuilder
    {
        return $this->monthPeriodRange($organisation, 'desc', 12);
    }

    public function total(Organisation $organisation, $field ,$which = 'current'): QueryBuilder
    {
        if ($which === 'current') {
            $qb = $this->lastHalfOf24Month($organisation);
        } else {
            $qb = $this->firstHalfOf24Month($organisation);
        }

        return $qb->select('finance_month.' . $field . ' as total');
    }

    private function monthPeriodRange(Organisation $organisation, $sortOrder, $maxResult): QueryBuilder
    {
        $qb = $this->createQueryBuilder('finance_month');
        $qb->where('finance_month.organisation = :org')
            ->setParameter('org', $organisation)
            ->setMaxResults($maxResult)
            ->orderBy('finance_month.period',$sortOrder)
        ;
        return $qb;
    }
}