<?php declare(strict_types = 1);


namespace App\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class NZBNCompanyRepository extends EntityRepository
{
    public function matchTerm($term) : QueryBuilder
    {
        $qb = $this->createQueryBuilder('nzbn');
        $qb->select('nzbn.name, nzbn.nzbn');
        $qb->where("match (nzbn.name) against(:term BOOLEAN) > 0.0")
            ->setParameter('term', '('.$term.')');
        return $qb;
    }

    public function searchByShort($term)
    {
        $qb = $this->createQueryBuilder('nzbn');
        $qb->select('nzbn.name, nzbn.nzbn');
        $qb->where('nzbn.name LIKE :term')->setParameter('term', $term . '%');
        return $qb;
    }
}