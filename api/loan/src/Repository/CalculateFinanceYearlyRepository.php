<?php declare(strict_types = 1);


namespace App\Repository;

use App\Entity\Organisation;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * Class CalculateFinanceMonthRepository
 * @package App\Repository
 */
class CalculateFinanceYearlyRepository extends EntityRepository
{
    /**
     * @param Organisation $organisation
     * @return QueryBuilder
     */
    public function findFirstRevenueRoll(Organisation $organisation): QueryBuilder
    {
        $qb = $this->createQueryBuilder('c');
        $qb->where('c.organisation_id = :org')
            ->orderBy('c.period_start', 'DESC')
            ->setMaxResults(1)
            ->setParameter('org', $organisation->getId());
        return $qb;
    }
}
