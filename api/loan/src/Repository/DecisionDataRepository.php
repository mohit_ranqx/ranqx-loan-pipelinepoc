<?php declare(strict_types = 1);


namespace App\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class DecisionDataRepository extends EntityRepository
{
    public function findTheOnlyOne() : QueryBuilder
    {
        $qb = $this->createQueryBuilder('d');
        $qb->setMaxResults(1);
        return $qb;
    }
}