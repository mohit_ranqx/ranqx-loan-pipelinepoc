<?php declare(strict_types = 1);

namespace App\PayLoad;

use Doctrine\ORM\EntityManagerInterface;

/**
     * Class payLoadEntityBalanceSheet
     * @package App\payLoad
     */

class PayLoadEntityBalanceSheet
{
    /**
     * @var EntityManagerInterface 
     */
    private $objectManager;

    /**
     * @var array 
     */
    private $Entity;

    /**
     * @var int
     */
    private $count;

    /**
     * @var int
     */
    private $stockCurrent;

    /**
     * @var int
     */
    private $stockPrevious;

    /**
     * @var int
     */
    private $debtorsCurrent;

    /**
     * @var int
     */
    private $debtorsPrevious;

    /**
     * @var int
     */
    private $currentAccountsCurrent;

    /**
     * @var int
     */
    private $currentAccountsPervious;

    /**
     * @var int
     */
    private $otherCurrentAssetsCurrent;

    /**
     * @var int
     */
    private $otherCurrentAssetsPrevious;

    /**
     * @var int
     */
    private $totalAssetsFixedCurrent;

    /**
     * @var int
     */
    private $totalAssetsFixedPrevious;

    /**
     * @var int
     */
    private $totalAssetsOtherCurrent;

    /**
     * @var int
     */
    private $totalAssetsOtherPrevious;

    /**
     * @var int
     */
    private $totalLiabilitiesCurrentCreditorsCurrent;


    /**
     * @var int
     */
    private $totalLiabilitiesCurrentCreditorsPrevious;


    /**
     * @var int
     */
    private $liabilitiesCurrentOtherCurrent;

    /**
     * @var int
     */
    private $liabilitiesCurrentOtherPrevious;

    /**
     * @var int
     */
    private $liabilitiesOtherCurrent;

    /**
     * @var int
     */
    private $liabilitiesOtherPrevious;

    /**
     * PayLoadEntityBalanceSheet constructor.
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
        $this->Entity=array();
        $this->count=1;
        $this->stockCurrent=0;
        $this->stockPrevious=0;
        $this->debtorsCurrent=0;
        $this->debtorsPrevious=0;
        $this->currentAccountsCurrent=0;
        $this->currentAccountsPervious=0;
        $this->otherCurrentAssetsCurrent=0;
        $this->otherCurrentAssetsPrevious=0;
        $this->totalAssetsFixedCurrent=0;
        $this->totalAssetsFixedPrevious=0;
        $this->totalAssetsOtherCurrent=0;
        $this->totalAssetsOtherPrevious=0;
        $this->totalLiabilitiesCurrentCreditorsCurrent=0;
        $this->totalLiabilitiesCurrentCreditorsPrevious=0;
        $this->liabilitiesCurrentOtherCurrent=0;
        $this->liabilitiesCurrentOtherPrevious=0;
        $this->liabilitiesOtherCurrent=0;
        $this->liabilitiesOtherPrevious=0;
    }

    /**
     * @param $orgID
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */

    public function entityBalanceSheet($orgID)
    {

        $_BalanceSheet = "SELECT * FROM finance_month WHERE organisation_id='".$orgID."' ORDER BY period ASC";
        $orgData = $this->objectManager->getConnection()->query($_BalanceSheet);
        $_Data = $orgData->fetchAll();

        foreach ($_Data as $bs) {

            // Current period`
            if (($this->count == 24)) {
                $this->stockCurrent = number_format((float)$bs['total_assets_current_inventory'],2,'.',',');
                $this->debtorsCurrent = number_format((float)$bs['total_assets_current_debtors'],2,'.',',');
                $this->currentAccountsCurrent = number_format((float)$bs['total_assets_current_bank'],2,'.',',');
                $this->otherCurrentAssetsCurrent =number_format((float)$bs['total_assets_current_other'],2,'.',',');
                $this->totalAssetsFixedCurrent = number_format((float)$bs['total_assets_fixed'],2,'.',',');
                $this->totalAssetsOtherCurrent = number_format((float)$bs['total_assets_other'],2,'.',',');
                $this->totalLiabilitiesCurrentCreditorsCurrent = number_format((float)$bs['total_liabilities_current_creditors'],2,'.',',');
                $this->liabilitiesCurrentOtherCurrent = number_format((float)$bs['total_liabilities_current_other'],2,'.',',');
                $this->liabilitiesOtherCurrent = number_format((float)$bs['total_liabilities_other'],2,'.',',');

            }
            if (($this->count == 12)) {
                $this->stockPrevious = number_format((float)$bs['total_assets_current_inventory'],2,'.',',');
                $this->debtorsPrevious = number_format((float)$bs['total_assets_current_debtors'],2,'.',',');
                $this->currentAccountsPervious = number_format((float)$bs['total_assets_current_bank'],2,'.',',');
                $this->otherCurrentAssetsPrevious = number_format((float)$bs['total_assets_current_other'],2,'.',',');
                $this->totalAssetsFixedPrevious =number_format((float)$bs['total_assets_fixed'],2,'.',',');
                $this->totalAssetsOtherPrevious = number_format((float)$bs['total_assets_other'],2,'.',',');
                $this->totalLiabilitiesCurrentCreditorsPrevious = number_format((float)$bs['total_liabilities_current_creditors'],2,'.',',');
                $this->liabilitiesCurrentOtherPrevious = number_format((float)$bs['total_liabilities_current_other'],2,'.',',');
                $this->liabilitiesOtherPrevious = number_format((float)$bs['total_liabilities_other'],2,'.',',');
            }
            $this->count++;
        }

        $this->Entity[] = array(
            "BalanceSheet" =>
                array(
                    // Stock
                    "StockCurrent" => $this->stockCurrent  ?? NULL,
                    "StockPrevious" =>  $this->stockPrevious ?? NULL,

                    //Debtors
                    "DebtorsCurrent" =>  $this->debtorsCurrent ?? NULL,
                    "DebtorsPrevious" =>  $this->debtorsPrevious ?? NULL,

                    //Current Accounts
                    "CurrentAccountsCurrent" =>  $this->currentAccountsCurrent ?? NULL,
                    "CurrentAccountsPrevious" =>  $this->currentAccountsPervious ?? NULL,

                    //Other Current Assets
                    "OtherCurrentAssetsCurrent" =>  $this->otherCurrentAssetsCurrent ?? NULL,
                    "OtherCurrentAssetsPrevious" =>  $this->otherCurrentAssetsPrevious ?? NULL,

                    //Fixed Assets
                    "FixedAssetsCurrent" =>  $this->totalAssetsFixedCurrent ?? NULL,
                    "FixedAssetsPrevious" =>  $this->totalAssetsFixedPrevious ?? NULL,

                    //Total Assets -- no value kb
                   // "TotalAssetsCurrent" => '',
                   // "TotalAssetsPrevious" => '',

                    //Intangible Assets
                    "IntangibleAssetsCurrent" =>  $this->totalAssetsOtherCurrent ?? NULL,
                    "IntangibleAssetsPrevious" =>  $this->totalAssetsOtherPrevious ?? NULL,

                    //Working Capital -- no value kb
                    // "WorkingCapitalCurrent" => '',
                    // "WorkingCapitalPrevious" => '',

                    //Tangible Equity -- no value kb
                    // "TangibleEquityCurrent" => '',
                    // "TangibleEquityPrevious" => '',

                    //Quasi Equity -- no value kb
                    // "QuasiEquityCurrent" => '',
                    // "QuasiEquityPrevious" => '',

                    //Creditors
                    "CreditorsCurrent" =>  $this->totalLiabilitiesCurrentCreditorsCurrent ?? NULL,
                    "CreditorsPrevious" =>  $this->totalLiabilitiesCurrentCreditorsPrevious ?? NULL,

                    //Overdraft -- no value kb
                    // "OverdraftCurrent" => '',
                    // "OverdraftPrevious" => '',

                    //Other Current Liabilities
                    "OtherCurrentLiabilitiesCurrent" => $this->liabilitiesCurrentOtherCurrent ?? NULL,
                    "OtherCurrentLiabilitiesPrevious" => $this->liabilitiesCurrentOtherPrevious ?? NULL,

                    //Total Current Liabilities -- no value kb
                   // "TotalCurrentLiabilitiesCurrent" => '',
                   // "TotalCurrentLiabilitiesPrevious" => '',

                    // Term loans -- no value kb
                    //"TermloansCurrent" => '',
                   // "TermLoansPrevious" => '',

                    // Shareholder Loans -- no value kb
                    //"ShareholderLoansCurrent" => '',
                   // "ShareholderloansPrevious" => '',

                    //Other Non-Current Liabilities
                    "OtherNonCurrentLiabilitiesCurrent" =>  $this->liabilitiesOtherCurrent ?? NULL,
                    "OtherNonCurrentLiabilitiesPrevious" =>  $this->liabilitiesOtherPrevious ?? NULL,

                   // "TotalLiabilitiesCurrent" => '',
                   // "TotalLiabilitiesPrevious" => ''
                )
        );

        return $this->Entity;
    }
}