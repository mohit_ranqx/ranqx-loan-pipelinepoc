<?php


namespace App\PayLoad;

use App\Command\AwsS3ObjectCommand;
use App\Entity\Organisation;
use App\Entity\PayLoad;
use App\Service\S3RanqxClient;
use DataDog\DogStatsd;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\NullOutput;

/**
 * Class PayLoadZip
 * @package App\PayLoad
 */
class PayLoadZip
{

    /**
     * @var EntityManagerInterface
     */
    private $objectManager;

    /**
     * @var int
     */
    private $org;

    /**
     * @var object|null
     */
    private $orgID;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DogStatsd
     */
    private $statsd;

    /**
     * @var string
     */
    private $zipFilename;

    /**
     * @var S3RanqxClient
     */
    private $client;


    /**
     * PayLoadZip constructor.
     * @param EntityManagerInterface $objectManager
     * @param LoggerInterface $logger
     * @param S3RanqxClient $client
     */
    public function __construct(
        EntityManagerInterface $objectManager,
        S3RanqxClient $client,
        LoggerInterface $logger

    )
    {
        $this->objectManager = $objectManager;
        //$this->orgID=$this->objectManager->getRepository(Organisation::class)->find($this->org);
        $this->logger = $logger;
        $this->client = $client;
        $this->statsd = new DogStatsd();
    }

    /**
     * @param $source
     * @param $destination
     * @return bool
     */
    public function payLoadZip($source, $destination)
    {

            if (!extension_loaded('zip') || !file_exists($source)) {
                return false;
            }

            $zip = new ZipArchive();
            if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
                return false;
            }

            $source = str_replace('\\', '/', realpath($source));

            if (is_dir($source) === true)
            {
                $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

                foreach ($files as $file)
                {
                    $file = str_replace('\\', '/', $file);
                    if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                        continue;

                    $file = realpath($file);

                    if (is_dir($file) === true)
                    {
                        $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                    }
                    else if (is_file($file) === true)
                    {
                        $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                    }
                }
            }
            else if (is_file($source) === true)
            {
                $zip->addFromString(basename($source), file_get_contents($source));
            }
          //  $this->statsd->increment('zip_file_created', 1, ['org_id' =>  $this->org,'message' => 'A zip file named '.$destination.' has been created']);
            $zip->close();

        /**
         * insert file name ad path to payload table
         */
        $s3App = new Application();
        $s3App->add(new AwsS3ObjectCommand($this->client,$this->logger));
        $command = $s3App->find('app:ranqx:s3:upload');
        $arguments = [
            'command' => 'app:ranqx:s3:upload',
            'file' => $destination,
        ];

        $s3FileUplaod = new ArrayInput($arguments);
        $returnCode = $command->run($s3FileUplaod, new NullOutput());

        $this->recursiveRemove($source);
        unlink($destination);
        
       // $this->statsd->increment('payload_zip_moved_to_s3', 1, ['org_id' =>  $this->org,'message' => 'SUCCESSFULY moved'.$destination.' to s3 and clean up of files completed.']);
        return true;
        }

        private function recursiveRemove($dir) {
            $structure = glob(rtrim($dir, "/").'/*');
            if (is_array($structure)) {
                foreach($structure as $file) {
                    if (is_dir($file)) $this->recursiveRemove($file);
                    elseif (is_file($file)) unlink($file);
                }
            }
            rmdir($dir);
        return true;
        }

}
