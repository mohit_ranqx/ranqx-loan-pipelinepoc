<?php declare(strict_types = 1);

namespace App\PayLoad;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class payLodEntityFinancialRatios
 * @package App\payLod
 */

class PayLoadIndividual
{
    /**
     * @var EntityManagerInterface
     */
    private $entity;

    /**
     * @var array
     */
    private $Individual;

    /**
     * @var array
     */
    private $equifaxDirectorData;

    /**
     * PayLoadIndividual constructor.
     * @param EntityManagerInterface $entity
     */
    public function __construct(EntityManagerInterface $entity)
    {
        $this->entity=$entity;
        $this->Individual=array();
        $this->equifaxDirectorData=array();
    }

    /**
     * @param $orgID
     * @return array
     */
    public function individualsDetails($orgID) {

        $equiFax = new PayLoadEquifaxData($this->entity);
        $equifaxMap = $equiFax->equifaxRawJson($orgID);

        $eqDirectors=$equifaxMap['equiFax']['db']['directors']['data']['directors'] ?? NULL;

        if($eqDirectors!=NULL) {
            foreach ($eqDirectors as $key => $directors) {

                $this->equifaxDirectorData[] = array(
                    "directors" => array(
                        "roleAddress1" => $directors['roleAddress'][0]['address1'] ?? NULL,
                        "roleAddress2" => $directors['roleAddress'][0]['address2'] ?? NULL,
                        "roleAddress3" => $directors['roleAddress'][0]['address3'] ?? NULL,
                        "roleAddress4" => $directors['roleAddress'][0]['address4'] ?? NULL,
                        "countryCode" => $directors['roleAddress'][0]['countryCode'] ?? NULL,
                        "description" => $directors['roleAddress'][0]['description'] ?? NULL,
                        "startDate" => $directors['roleAddress'][0]['startDate'] ?? NULL,

                        "rolePersonFirstName" => $directors['rolePerson']['firstName'] ?? NULL,
                        "rolePersonLastName" => $directors['rolePerson']['lastName'] ?? NULL,
                        "rolePersonMiddleNames" => $directors['rolePerson']['middleNames'] ?? NULL,

                        "roleStatus" => $directors['roleStatus'] ?? NULL,
                        "roleType" => $directors['roleType'] ?? NULL
                    )
                );

                $this->Individual[] = array(
                    "Details" =>
                        array(
                            // "AccessNumber"=>'',
                            // "Title"=>'',
                            "FirstName" => $directors['rolePerson']['firstName'] ?? NULL,
                            "middleNames" => $directors['rolePerson']['middleNames'] ?? NULL,
                            "Surname" => $directors['rolePerson']['lastName'] ?? NULL,
                            // "CCR"=>'',
                            // "RequiresSecuritySwapAgreement"=>'',
                            // "CCRSCRIMI"=>'',
                            // "WorkPh"=>'',
                            // "HomePh"=>'',
                            "MobilePh" => '',
                            // "DateofBirth"=>'',
                            // "Gender"=>'',
                            // "IRDNumber"=>'',
                            // "TaxRate"=>'',
                            // "CDDStatus"=>"Choose a Value"
                        ),
                    /* "HomeAddress" =>
                         array(
                             "Address1"=>$directors['roleAddress'][0]['address1'],
                             "Address2"=>$directors['roleAddress'][0]['address2'],
                             "Address3"=>$directors['roleAddress'][0]['address3'],
                             "Address4"=>$directors['roleAddress'][0]['address4'],
                             // "TimeatAddressyears"=>'',
                             // "TimeatAddressmonths"=>''
                         ),
                     "PostalAddress" =>
                         array(
                             "Address1"=>$directors['roleAddress'][0]['address1'],
                             "Address2"=>$directors['roleAddress'][0]['address2'],
                             "Address3"=>$directors['roleAddress'][0]['address3'],
                             "Address4"=>$directors['roleAddress'][0]['address4'],
                         ),
                     "Residency" =>
                         array(
                             // "NZResidency"=>"Y",
                             // "HouseholdStatus"=>"Couple/Single",
                             // "NumberofDependants"=>''
                         ),
                     "Employment" =>
                         array(
                             // "Occupation"=>'',
                             // "ANZSCOCode"=>'',
                             // "EmploymentType"=>"Choose a Value",
                             // "Employer"=>'',
                             // "TimeatCurrentYears"=>'',
                             // "TimeatCurrentMonths"=>'',
                             // "PrevEmployer"=>'',
                             // "TimeatPreviousYears"=>'',
                             // "TimeatPreviousMonths"=>'',
                         )
                    */
                );

            }
        }

        return  $this->Individual;
    }
}