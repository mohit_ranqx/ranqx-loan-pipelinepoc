<?php

namespace App\PayLoad;

use App\Entity\Applicant;
//use App\Entity\Organisation;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Utils\DateTime;
use DateTimeZone;

class PayLoadBusinessLoanApplication
{
    /**
     * @var EntityManagerInterface
     */
    private $entity;

    /**
     * @var int
     */
    private $orgID;

    /**
     * @var array
     */
    private $payLoadBussinessData;

    /**
     * @var DateTime
     */
    private $appCreatedDate;

    /**
     * @var array
     */
    private $groupMasterClient;

    /**
     * @var array
     */
    private $payLoadDecision;

    /**
     * PayLoadBusinessLoanApplication constructor.
     * @param EntityManagerInterface $entity
     */
    public function __construct(EntityManagerInterface $entity)
    {
        $this->entity=$entity;
        $this->payLoadBussinessData = array();
        $this->appCreatedDate = date('Y-m-d H:i:s');
        $this->groupMasterClient = array();
        $this->payLoadDecision = array();
    }

    /**
     * Builds an array set from the Decision data
     * @param $orgID
     * @return array
     * @throws \Exception
     */

    public function payLoadBusinessData($orgID) {

        $results = $this->entity->getRepository(Applicant::class)
            ->findBy(['id' => $orgID]);

        foreach ($results as $app) {
            $appCreatedDate = new DateTime($app->getCreated()->format('d-m-Y H:i:s'), new DateTimeZone('Pacific/Auckland') );
            $this->appCreatedDate =  date("d-m-Y H:i:s", strtotime($appCreatedDate->setTimeZone(new DateTimeZone('Pacific/Auckland'))));
        }

        $this->payLoadBussinessData = array(
            "business" => array(
                "loanApplication" =>
                    array(
                        "Business_Loan_Application_Type"            => 'Working Capital',
                        "Customer_Type"                             => 'Limited Company',
                        "Loan_Facility_Type"                        => 'Overdraft',
                        "Loan_Application_Purpose"                  => 'BLB 50',
                        "Loan_Date_of_Application"                  => $this->appCreatedDate,
                        "Loan_Previous_Lending_Institution"         => '',
                        "Loan_Letter_of_Offer_to_be_accepted_by"    => 'Enter a Date',
                        "Loan_Loan_to_be_Settled_by"                => 'Enter a Date',
                        "Loan_Documents_required_by"                => 'Enter a Date',
                        "Loan_Loan_Referred_by_Employee_Number"     => '',
                        "Loan_Loan_Referred_by_Employee_Name"       => '',
                        "Loan_Relationship_Manager_Employee_Number" => '',
                        "Loan_Relationship_Manager_Employee_Name"   => '',
                        "Loan_Lead_Source"                          => 'C',
                        "Loan_Broker_Name"                          => '',
                        "Loan_Broker_Firm"                          => '',
                        "Loan_Business_Unit"                        => 'Business Banking Direct'

                    )
            )
        );

        return $this->payLoadBussinessData;

    }

    /**
     * Builds an array set from the Decision data
     * @return array
     */
    public function payLoadGroupMasterClient () {
        /**
         * twig layout
         * 'groupMasterClient' => $groupMasterClientDetails,
         * groupMasterClient.groupMasterClient.details.____field____
         */
        $this->groupMasterClient = array(
            "groupMasterClient" => array(
                "details" =>
                    array(
                        "Access_Number" => '',
                        "Group_Master_Client_Name" => '',
                        "Group_Type" => 'Group Master Client'
                    )
            )
        );

        return $this->groupMasterClient;
    }

    /**
     * Builds an array set from the Decision data
     * @return array
     */

    public function payLoadDecision() {

        $this->payLoadDecision = array(
            "groupMasterClient" => array(
                "payLoadDecisionDetails" =>
                    array(
                        "Purpose_Summary"    => '',
                        "Recommendations_Summary"    => ''
                    )
            )
        );

        return $this->payLoadDecision;

    }

}
