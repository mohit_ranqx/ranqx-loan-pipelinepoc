<?php declare(strict_types=1);

namespace App\PayLoad;

use App\Entity\Applicant;
use App\Entity\ApplicantException;
use App\Entity\PayLoad;
use Psr\Log\LoggerInterface;
use DataDog\DogStatsd;
use App\Entity\CalculateFinanceCurrentMonth;
use App\Entity\CalculateFinanceMonth;
use App\Entity\CalculateFinanceYearly;
use App\Entity\FinanceInvoice;
use App\Entity\FinanceMonth;
use App\Entity\Organisation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use App\Service\S3RanqxClient;
use App\TypeTrait\DataDogMetric;
use App\TypeTrait\Utc2NZ;

/**
 * Class PayLoadFileCreation
 * @package App\PayLoad
 */
class PayLoadFileCreation
{

    use DataDogMetric;
    use Utc2NZ;
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;

    /**
     * @var int
     */
    private $org;

    /**
     * @var int
     */
    private $kbReferenceNumber;

    /**
     * @var object|null
     */
    private $orgID;

    /**
     * @var S3RanqxClient
     */
    private $client;

    private $logger;


    /**
     * PayLoadFileCreation constructor.
     * @param EntityManagerInterface $objectManager

     * @param LoggerInterface $logger
     * @throws \Doctrine\DBAL\DBALException
     */
    public function __construct(
        EntityManagerInterface $objectManager,
        S3RanqxClient $client,
        LoggerInterface $logger

    )
    {
        $this->objectManager = $objectManager;
        $this->client = $client;
        $this->logger = $logger;
        $this->statsd = new DogStatsd();

    }
    /**
     * @param $dataDecision
     * @param $payLoadHtml
     * @param $lendingDecision
     * @return bool
     */
    public function createPayLoadFiles($dataDecision,$payLoadHtml,$lendingDecision,Applicant $app, $loadName = 'modelling')
    {
        /**
         * create the CSV Files
         */
        $this->kbReferenceNumber = $app->getReferenceNumber();
        $buildFinancialMonth=$this->financialMonthCsv($app->getOrganisation());
        $buildCalculateDataCsv=$this->calculateDataCsv($app->getOrganisation());
        $buildCalculateCurrentMonthDataCsv=$this->calculateCurrentMonthDataCsv($app->getOrganisation());
        $buildCalculateYearlyDataCsv=$this->calculateYearlyDataCsv($app->getOrganisation());
        $buildDebtorConcentrationCsv=$this->debtorConcentrationCsv($app->getOrganisation());
        $ScoreCardCsv=$this->ScoreCardCsv($dataDecision['scoreCard']);
        $descisionCsv=$this->descisionCsv($dataDecision['payLoadDecisionData']);
        $equifaxRawJson=\json_encode($app->getEquiFaxRaw());
        $error = $this->errorReport($app);

        /**
         * Write the files to the sys_get_temp_dir
         */
        $appDatetime = $this->convert2NZ($app->getCreated())->format('YmdHis');
        $filesystem = new Filesystem();
        try {
            $filePath = sys_get_temp_dir().'/'.date("dmYHis").'_'.$this->kbReferenceNumber.'_'.strtolower($lendingDecision);
            $fileName = $appDatetime.'_'.$this->kbReferenceNumber.'_'.strtolower($lendingDecision);
            $zipFileName = $appDatetime.'_'.$this->kbReferenceNumber.'_'.$loadName.'_'.strtolower($lendingDecision);
            $filesystem->mkdir($filePath, 0700);
            $filesystem->dumpFile($filePath.'/'.$fileName.'_financialMonth.csv',$buildFinancialMonth);
            $filesystem->dumpFile($filePath.'/'.$fileName.'_calc_month_rolling.csv',$buildCalculateDataCsv);
            $filesystem->dumpFile($filePath.'/'.$fileName.'_calc_current_month_.csv',$buildCalculateCurrentMonthDataCsv);
            $filesystem->dumpFile($filePath.'/'.$fileName.'_calc_yearly.csv',$buildCalculateYearlyDataCsv);
            $filesystem->dumpFile($filePath.'/'.$fileName.'_debtor_concentration.csv',$buildDebtorConcentrationCsv);
            if ($loadName === 'modelling') {
                $filesystem->dumpFile($filePath . '/' . $fileName . '_decision_scorecard.csv', $ScoreCardCsv);
                $filesystem->dumpFile($filePath . '/' . $fileName . '_decision_card.csv', $descisionCsv);
            }
            $filesystem->dumpFile($filePath.'/'.$fileName.'_error.csv',$error);
            $filesystem->dumpFile($filePath.'/'.$fileName.'_decision.html',$payLoadHtml);

            // mkdir for Applicant Equifax json and pdfreport
            $filesystem->mkdir($filePath.'/'.$fileName.'_equifaxData', 0700);
            $filesystem->dumpFile($filePath.'/'.$fileName.'_equifaxData/'.$fileName.'equifaxRawJson.json',$equifaxRawJson);
            $filesystem->dumpFile($filePath.'/'.$fileName.'_equifaxData/'.$fileName.'equifaxReport.pdf', $app->getPdf());

        } catch (IOExceptionInterface $exception) {
            $paylaodFailedMessage = "PAYLOAD: File creation IOException";
            $this->logger->critical($paylaodFailedMessage, [
                'cause' => json_encode([
                    'title' => "PAYLOAD",
                    'text' => 'Error creating file at',
                    'exception_path' => $exception->getPath(),
                ]),
            ]);

            $this->event('PAYLOAD: File creation IOException',
                array(
                    'title'  => 'PAYLOAD',
                    'text' => "Error creating file at: ". $exception->getPath(),
                    'alert_type' => 'error'
                ));

            /**
             * TODO: get the applicant ID in the LOG
             */
            $this->metric('files_created', 1, ['applicant_id' => $this->kbReferenceNumber, 'message' => 'Error creating file at'. $exception->getPath()]);
        }
        $createZipFile = new PayLoadZip($this->objectManager, $this->client, $this->logger);
        $paloadZipfileCreated = $createZipFile->payLoadZip($filePath, sys_get_temp_dir().'/'.$zipFileName.'.zip');
        return true;
    }



    private function errorReport(Applicant $applicant): string
    {
        $results = $this->objectManager->getRepository(ApplicantException::class)
            ->findBy(['applicant' => $applicant]);

        $header = "Error,Created\n";
        $body = '';
        /** @var ApplicantException $result ***/
        foreach ($results as $applicantException) {
            $ex = str_replace(',',' ', $applicantException->getException());
            $body .= $ex . ',';
            $body .= $applicantException->getCreated()->format('Y-m-d H:i:s') . "\n";
        }

        return $header. $body;
    }

    /**
     * financial Month Csv creation
     * @param Organisation $organisation
     * @return string
     */

    private function financialMonthCsv(?Organisation $organisation)
    {
        $header = 'Month Ending,Currency,Total Revenue,Total COG,Total People,Total Rent,Total Operating,Interest,Depreciation,Tax,Total Assets Current Bank, Total Assets Current Debtors,Total Assets Current Inventory,Total Assets Current Other,Total Assets Fixed,Total Assets Other,Total Liabilities Current Bank,Total Liabilities Current Creditors,Total Liabilities Current Other,Total Liabilities Other,Total Equity Capital,Total Equity Earnings';

        $body = '';
        /** @var FinanceMonth $finance ** */
        /**
         * TODO: sort period ASC
         * TODO: 3 new columns - interest, tax, depreciation
         */
        if ($organisation !== null && $organisation->getId()) {
            /** @var FinanceMonth $financeMonth ***/
            foreach ($organisation->getFinanceMonths() as $financeMonth) {
                $body .= $financeMonth->getPeriod()->format('Y-m-d H:i:s') . ',';
                $body .= $financeMonth->getCurrency() . ',';
                $body .= $financeMonth->getTotalRevenue() . ',';
                $body .= $financeMonth->getTotalCog() . ',';
                $body .= $financeMonth->getTotalPeople()  . ',';
                $body .= $financeMonth->getTotalRent() . ',';
                $body .= $financeMonth->getTotalOperating(). ',';
                $body .= $financeMonth->getInterest() . ',';
                $body .= $financeMonth->getDepreciation(). ',';
                $body .= $financeMonth->getTax() . ',';
                $body .= $financeMonth->getTotalAssetsCurrentBank(). ',';
                $body .= $financeMonth->getTotalAssetsCurrentDebtors() . ',';
                $body .= $financeMonth->getTotalAssetsCurrentInventory() . ',';
                $body .= $financeMonth->getTotalAssetsCurrentOther() . ',';
                $body .= $financeMonth->getTotalAssetsFixed() . ',';
                $body .= $financeMonth->getTotalAssetsOther(). ',';
                $body .= $financeMonth->getTotalLiabilitiesCurrentBank() . ',';
                $body .= $financeMonth->getTotalLiabilitiesCurrentCreditors() . ',';
                $body .= $financeMonth->getTotalLiabilitiesCurrentOther() . ',';
                $body .= $financeMonth->getTotalLiabilitiesOther() . ',';
                $body .= $financeMonth->getTotalEquityCapital(). ',';
                $body .= $financeMonth->getTotalEquityEarnings();
                $body .= "\n";
            }
        }

        return str_replace("\n", '', $header) . "\n" . $body;
    }

    /**
     * calculate Data Csv creation
     * @param Organisation $organisation
     * @return string
     * * TODO: sort period ASC
     */


    private function calculateDataCsv(?Organisation $organisation)
    {
        $results = [];
        if ($organisation !== null) {
            $results = $this->objectManager->getRepository(CalculateFinanceMonth::class)
                ->findBy(['organisation_id' => $organisation->getId()],
                    ['period_start' => 'asc']);
        }

        $header = 'Period start,Period End,Revenue Rolling,Gross Margin Value Rolling,Gross Margin Percentage Rolling,Ebitda Value Rolling,Ebitda Value Percentage Rolling,Interest Cover Rolling,Mrginal Cashflow Rolling,Debt Leverage Rolling,Debitor Days Rolling,Creditor Days Rolling,Defence Internal Ratio Rolling,Expense Cover Ratio Rolling,Operating Cost Ratio Rolling,People Cost Ratio Rolling,KB Defensive Ratio,KB Expense Cover Ratio';

        $body = '';
        /** @var CalculateFinanceMonth $fm ** */
        foreach ($results as $fm) {

            if ($fm->getPeriodStart()) {
                $body .= $fm->getPeriodStart()->format('Y-m-d H:i:s') . ',';
            }

            if ($fm->getPeriodEnd()) {
                $body .= $fm->getPeriodEnd()->format('Y-m-d H:i:s') . ',';
            }
            $body .= $fm->getRevenueRolling() . ",";
            $body .= $fm->getGrossMarginValueRolling() . ",";
            $body .= $fm->getGrossMarginPercentageRolling() . ",";
            $body .= $fm->getEbitdaValueRolling() . ",";
            $body .= $fm->getEbitdaValuePercentageRolling() . ",";
            $body .= $fm->getInterestCoverRolling() . ",";
            $body .= $fm->getMarginalCashflowRolling() . ",";
            $body .= $fm->getDebtLeverageRolling() . ",";
            $body .= $fm->getDebitorDaysRolling() . ",";
            $body .= $fm->getCreditorDaysRolling() . ",";
            $body .= $fm->getDefenceInternalRatioRolling() . ",";
            $body .= $fm->getExpenseCoverRatioRolling() . ",";
            $body .= $fm->getOperatingCostRatioRolling() . ",";
            $body .= $fm->getPeopleCostRatioRolling() . ",";
            $body .= $fm->getKbDefensiveInterval() . ",";
            $body .= $fm->getKbExpenseCoverRatio() . ",";


            $body .= "\n";
        }
        // $this->statsd->increment('Calculate_Finance_Month_CSV_file', 1, ['applicant_id' => '12348678'],['message' => 'SUCCESS']);

        return str_replace("\n", '', $header) . "\n" . $body;
    }


    /**
     * calculate Current Month Data Csv Creation
     * @param Organisation $organisation
     * @return string
     */

    private function calculateCurrentMonthDataCsv(?Organisation $organisation)
    {
        $results = [];
        if ($organisation) {
            $results = $this->objectManager->getRepository(CalculateFinanceCurrentMonth::class)
                ->findBy(['organisation_id' => $organisation->getId()],
                    ['period_start' => 'asc']);
        }

        $header = 'Period start,Period End,Revenue,Gross Margin Value,Gross Margin Percentage,Working Capital Ratio,Ebitda Value,Ebitda Value Percentage,Interest Cover,Marginal Cashflow,Quick Ratio,Cash Ratio,Net Cash Balance,Debt Ratio,Equity Ratio,Operating Cost Ratio,People Cost Ratio';
        $body = '';
        /** @var CalculateFinanceCurrentMonth $fcm ** */
        foreach ($results as $fcm) {
            if ($fcm->getPeriodStart()) {
                $body .= $fcm->getPeriodStart()->format('Y-m-d H:i:s') . ",";
            }
            if ($fcm->getPeriodEnd()) {
                $body .= $fcm->getPeriodEnd()->format('Y-m-d H:i:s') . ",";
            }
            $body .= $fcm->getRevenueCurrent() . ",";
            $body .= $fcm->getGrossMarginValueCurrent() . ",";
            $body .= $fcm->getGrossMarginPercentageCurrent() . ",";
            $body .= $fcm->getWorkingCapitalRatioCurrent() . ",";
            $body .= $fcm->getEbitdaValueCurrent() . ",";
            $body .= $fcm->getEbitdaMarginCurrent() . ",";
            $body .= $fcm->getInterestCoverCurrent() . ",";
            $body .= $fcm->getMarginalCashflowCurrent() . ",";
            $body .= $fcm->getQuickRatioCurrent() . ",";
            $body .= $fcm->getCashRatioCurrent() . ",";
            $body .= $fcm->getNetCashBalanceCurrent() . ",";
            $body .= $fcm->getDebtRatioCurrent() . ",";
            $body .= $fcm->getEquityRatioCurrent() . ",";
            $body .= $fcm->getOperatingCostRatioCurrent() . ",";
            $body .= $fcm->getPeopleCostRatioCurrent() . ",";

            $body .= "\n";
        }
        // $this->statsd->increment('calculate_Current_Month_Data_CSV_file', 1, ['applicant_id' => '12348678'],['message' => 'SUCCESS']);

        return str_replace("\n", '', $header) . "\n" . $body;
    }

    /**
     * calculate Yearly Data Csv
     * @param Organisation $organisation
     * @return string
     * * TODO: sort period ASC
     */

    private function calculateYearlyDataCsv(?Organisation $organisation)
    {
        $results = [];
        if ($organisation) {
            $results = $this->objectManager->getRepository(CalculateFinanceYearly::class)
                ->findBy(['organisation_id' => $organisation->getId()]);
        }

        $header = 'Period Start,Period End,Revenue Growth Yearly,Stock Days Yearly';
        $body = '';
        /** @var CalculateFinanceYearly $fy ** */
        foreach ($results as $fy) {
            if ($fy->getPeriodStart()) { $body .= $fy->getPeriodStart()->format('Y-m-d H:i:s') . ","; }
            if ($fy->getPeriodEnd()) { $body .= $fy->getPeriodEnd()->format('Y-m-d H:i:s') . ","; }
            $body .= $fy->getRevenueGrowthYearly() . ",";
            $body .= $fy->getStockDaysYearly() . ",";
            $body .= "\n";
        }
        // $this->statsd->increment('calculate_Yearly_Data_Csv_file', 1, ['applicant_id' => '12348678'],['message' => 'SUCCESS']);

        return str_replace("\n", '', $header) . "\n" . $body;
    }

    /**
     * @param Organisation $organisation
     * @return string
     * TODO: sort period ASC
     */

    private function debtorConcentrationCsv(?Organisation $organisation)
    {
        $results = [];
        if ($organisation) {
            $results = $this->objectManager->getRepository(FinanceInvoice::class)
                ->findBy(['organisation_id' => $organisation->getId()]);
        }

        $header = 'Period Start,Period End,Debtor Concentration';
        $body = '';
        /** @var FinanceInvoice $fy ** */
        foreach ($results as $fy) {
            if ($fy->getPeriodStart()) { $body .= $fy->getPeriodStart()->format('Y-m-d H:i:s') . ","; }
            if ($fy->getPeriodEnd()) { $body .= $fy->getPeriodEnd()->format('Y-m-d H:i:s') . ","; }
            $body .= $fy->getDebtorConcentration() . ",";
            $body .= "\n";
        }
        // $this->statsd->increment('Debtor_Concentration_Csv_file', 1, ['applicant_id' => '12348678'],['message' => 'SUCCESS']);
        return str_replace("\n", '', $header) . "\n" . $body;
    }

    /**
     * descision card Csv creation
     * @param $descisionCard
     * @return string
     */
    private function descisionCsv($descisionCard) {
        $header = 'Title,InternalId,RefId,RuleValue,Decision,Reason';
        $body = '';

        foreach ($descisionCard as $name => $descisioncard ) {
            $body .= ($descisioncard['name'] ?? NULL) . ',';
            $body .= ($descisioncard['InternalId'] ?? NULL) . ',';
            $body .= ($descisioncard['RefId'] ?? NULL) . ',';
            $body .= str_replace(',','',($descisioncard['RuleValue'] ?? NULL)). ',';
            $body .= strtoupper(($descisioncard['decision'] ?? NULL)). ',';
            $body .= str_replace(',','',($descisioncard['reason'] ?? NULL)). ',';
            $body .= "\n";
        }
        return str_replace("\n", '', $header) . "\n" . $body;
    }


    /**
     * scorecard Csv Creation
     * @param $scoreCard
     * @return string
     */

    private function ScoreCardCsv($scoreCard)
    {
        $header = 'Title,Internal ID,RuleValue,Decision,Reason';
        $body = '';

        foreach ($scoreCard as $n => $cardScore) {

            if ($n!='seed' && $n!='total') {
                $title = ucwords(($cardScore['name'] ?? NULL));
                $body .= $title . ',';
                $body .= ($cardScore['internalId'] ?? NULL) . ',';
                $body .= number_format((float)($cardScore['RuleValue'] ?? NUll), 3, '.', ',') . ',';
                $body .= ($cardScore['decision'] ?? NULL) . ',';
                $body .= str_replace(',', '', ($cardScore['reason'] ?? NULL) . ',');
                $body .= "\n";
            }
        }

        $body .= '' . ',';
        $body .= '' . ',';
        $body .= '' . ',';
        $body .= '' . ',';
        $body .= '' . ',';
        $body .= "\n";

        $body .= 'Seed' . ',';
        $body .= '' . ',';
        $body .= '' . ',';
        $body .= $scoreCard['seed'] . ',';
        $body .= '' . ',';
        $body .= "\n";

        $body .= 'Total' . ',';
        $body .= '' . ',';
        $body .= '' . ',';
        $body .= $scoreCard['total'] . ',';
        $body .= '' . ',';
        $body .= "\n";
        return str_replace("\n", '', $header) . "\n" . $body;
    }

    /**
     * @param int $organisationId
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    private function orgApplicantID(int $organisationId) {
        $org = "SELECT applicant_id FROM organisation WHERE id = " . $organisationId." LIMIT 1";
        $statement = $this->objectManager->getConnection()->query($org);
        $orgQuerry = $statement->fetchAll();
        return($orgQuerry[0]['applicant_id']);
    }

    /**
     * @param int $organisationId
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    private function decisionJson(int $organisationId) {
        $appID=$this->orgApplicantID($organisationId);
        $dj= "SELECT pdf FROM Applicant WHERE id = '". $appID ."' LIMIT 1";
        $statement = $this->objectManager->getConnection()->query($dj);
        $djQuerry = $statement->fetchAll();
        return($djQuerry[0]['pdf']);
    }

    /**
     * @param int $organisationId
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    private function kbReferenceNumber(int $organisationId) {
        $appID=$this->orgApplicantID($organisationId);
        $dj= "SELECT reference_number FROM Applicant WHERE id = '". $appID ."' LIMIT 1";
        $statement = $this->objectManager->getConnection()->query($dj);
        $djQuerry = $statement->fetchAll();
        return($djQuerry[0]['reference_number']);
    }

    /**
     * @param int $organisationId
     * @return mixed[]
     * @throws \Doctrine\DBAL\DBALException
     */
    private function financeMonth(int $organisationId) {
        $financeMonthData= "SELECT * FROM finance_month WHERE organisation_id='". $organisationId ."' ORDER BY period ASC";
        $statement = $this->objectManager->getConnection()->query($financeMonthData);
        $fmData = $statement->fetchAll();
        return($fmData);
    }
}