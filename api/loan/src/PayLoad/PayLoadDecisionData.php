<?php

namespace App\PayLoad;

use Doctrine\ORM\EntityManagerInterface;

class PayLoadDecisionData
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var array
     */
    private $scoreCardData;

    /**
     * @var array
     */
    private  $dataDecision;

    /**
     * @var array
     */
    private $decisionRawData;

    /**
     * @var array
     */
    private $AX02;

    /**
     * @var array
     */
    private $BB01;

    /**
     * @var array
     */
    private $BB02;

    /**
     * @var array
     */
    private $DC01;

    /**
     * @var array
     */
    private $DC02;

    /**
     * @var array
     */
    private $DC03;

    /**
     * @var array
     */
    private $DE01;

    /**
     * @var array
     */
    private $EE01;

    /**
     * @var array
     */
    private $RE01;

    /**
     * @var array
     */
    private $EI02;

    /**
     * @var array
     */
    private $IC01;

    /**
     * @var array
     */
    private $CJ01;

    /**
     * @var array
     */
    private $LA01;

    /**
     * @var array
     */
    private $NI01;

    /**
     * @var array
     */
    private $MA01;

    /**
     * @var array
     */
    private $AC02;

    /**
     * @var array
     */
    private $BS02;

    /**
     * @var array
     */
    private $BS01;

    /**
     * @var array
     */
    private $UB02;

    /**
     * @var array
     */
    private $FC02;

    /**
     * @var array
     */
    private $IO02;

    /**
     * PayLoadDecisionData constructor.
     * @param EntityManagerInterface $entity
     */

    public function __construct(EntityManagerInterface $entity) {
        $this->entityManager = $entity;
        $this->scoreCardData = '';
        $this->dataDecision = '';
        $this->decisionRawData = '';
        $this->AX02=array();
        $this->BB01=array();
        $this->BB02=array();
        $this->DC01=array();
        $this->DC02=array();
        $this->DC03=array();
        $this->DE01=array();
        $this->EE01=array();
        $this->RE01=array();
        $this->EI02=array();
        $this->IC01=array();
        $this->CJ01=array();
        $this->LA01=array();
        $this->NI01=array();
        $this->MA01=array();
        $this->AC02=array();
        $this->BS02=array();
        $this->BS01=array();
        $this->UB02=array();
        $this->FC02=array();
        $this->IO02=array();
    }

    /**
     * @param $orgID
     * @return array
     */

    public function decisionData(?int $orgID){
        $this->scoreCardData=json_decode($this->scoreCardData($orgID), true);
        $this->dataDecision=json_decode($this->decisionJson($orgID),true);

        if ( $this->dataDecision['KB_US_PR_001']) {
            if ( $this->dataDecision['KB_US_PR_001']['decision']=='PASS') {
                $NI01Trigged='No';
                $this->dataDecision['KB_US_PR_001']['decision']='N/A';
                $decision_CSV_KB_US_PR_001='PASS';
            } else {
                $NI01Trigged='Yes';
                $decision_CSV_KB_US_PR_001=$this->dataDecision['KB_US_PR_001']['decision'];
            }
            $this->NI01 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_001']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_001']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_001']['decision'],
                "Decision_CSV" => $decision_CSV_KB_US_PR_001,
                "RuleValue" =>  $this->dataDecision['KB_US_PR_001']['RuleValue'],
                "Name" =>  $this->dataDecision['KB_US_PR_001']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_001']['reason'],
                "Trigged" => $NI01Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_002']) {
            if ( $this->dataDecision['KB_US_PR_002']['decision']=='PASS') {
                $MA01Trigged='No';
                $this->dataDecision['KB_US_PR_002']['decision']='N/A';
                $decision_CSV_KB_US_PR_002='PASS';
            } else {
                $MA01Trigged='Yes';
                $decision_CSV_KB_US_PR_002=$this->dataDecision['KB_US_PR_002']['decision'];

            }
            $this->MA01 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_002']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_002']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_002']['decision'],
                "Decision_CSV" => $decision_CSV_KB_US_PR_002,
            "RuleValue" =>  $this->dataDecision['KB_US_PR_002']['RuleValue'],
                "Name" =>  $this->dataDecision['KB_US_PR_002']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_002']['reason'],
                "Trigged" => $MA01Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_003']) {
            if ( $this->dataDecision['KB_US_PR_003']['decision']=='PASS') {
                $AX02Trigged='No';
                $this->dataDecision['KB_US_PR_003']['decision']='N/A';
                $decision_CSV_KB_US_PR_003='PASS';
            } else {
                $AX02Trigged='Yes';
                $decision_CSV_KB_US_PR_003=$this->dataDecision['KB_US_PR_003']['decision'];

            }
            $this->AX02 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_003']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_003']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_003']['decision'],
                "Decision_CSV" => $decision_CSV_KB_US_PR_003,
            "RuleValue" => ( $this->dataDecision['KB_US_PR_003']['RuleValue'] !='' ? number_format((float) $this->dataDecision['KB_US_PR_003']['RuleValue'], 0, '.', ',') : NULL),
                "Name" =>  $this->dataDecision['KB_US_PR_003']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_003']['reason'],
                "Trigged" => $AX02Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_004']) {
            if ( $this->dataDecision['KB_US_PR_004']['decision']=='PASS') {
                $AC02Trigged='No';
                $this->dataDecision['KB_US_PR_004']['decision']='N/A';
                $decision_CSV_KB_US_PR_004='PASS';
            } else {
                $AC02Trigged='Yes';
                $decision_CSV_KB_US_PR_004=$this->dataDecision['KB_US_PR_004']['decision'];

            }
            if ( $this->dataDecision['KB_US_PR_004']['RuleValue']!='') { $AC02RuleValueDate=date("d-m-Y",strtotime( $this->dataDecision['KB_US_PR_004']['RuleValue'])); } else { $AC02RuleValueDate=''; }
            $this->AC02 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_004']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_004']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_004']['decision'],
                "Decision_CSV" => $decision_CSV_KB_US_PR_004,
                "Name" =>  $this->dataDecision['KB_US_PR_004']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_004']['reason'],
                "RuleValue" => $AC02RuleValueDate,
                "Trigged" => $AC02Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_005']) {
            if ( $this->dataDecision['KB_US_PR_005']['decision']=='PASS') {
                $DC01Trigged='No';
                $this->dataDecision['KB_US_PR_005']['decision']='N/A';
                $decision_CSV_KB_US_PR_005='PASS';
            } else {
                $DC01Trigged='Yes';
                $decision_CSV_KB_US_PR_005=$this->dataDecision['KB_US_PR_005']['decision'];

            }
            $this->DC01 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_005']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_005']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_005']['decision'],
                "Decision_CSV" =>  $decision_CSV_KB_US_PR_005,
                "RuleValue" => ( $this->dataDecision['KB_US_PR_005']['RuleValue'] !='' ? number_format((float) $this->dataDecision['KB_US_PR_005']['RuleValue'],3,'.',',') : NULL),
                "Name" =>  $this->dataDecision['KB_US_PR_005']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_005']['reason'],
                "Trigged" => $DC01Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_006']) {
            if ( $this->dataDecision['KB_US_PR_006']['decision']=='PASS') {
                $IC01Trigged='No';
                $this->dataDecision['KB_US_PR_006']['decision']='N/A';
                $decision_CSV_KB_US_PR_006='PASS';
            } else {
                $IC01Trigged='Yes';
                $decision_CSV_KB_US_PR_006=$this->dataDecision['KB_US_PR_006']['decision'];

            }
            $this->IC01 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_006']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_006']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_006']['decision'],
                "Decision_CSV" =>  $decision_CSV_KB_US_PR_006,
                "RuleValue" => ( $this->dataDecision['KB_US_PR_006']['RuleValue']!='' ? number_format((float) $this->dataDecision['KB_US_PR_006']['RuleValue'], 3, '.', ',') : NULL),
                "Name" =>  $this->dataDecision['KB_US_PR_006']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_006']['reason'],
                "Trigged" => $IC01Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_007']) {
            if ( $this->dataDecision['KB_US_PR_007']['decision']=='PASS') {
                $DC02Trigged='No';
                $this->dataDecision['KB_US_PR_007']['decision']='N/A';
                $decision_CSV_KB_US_PR_007='PASS';
            } else {
                $DC02Trigged='Yes';
                $decision_CSV_KB_US_PR_007=$this->dataDecision['KB_US_PR_007']['decision'];

            }
            $this->DC02 = array(
                "InternalId" => $this->dataDecision['KB_US_PR_007']['InternalId'],
                "RefId"=> $this->dataDecision['KB_US_PR_007']['RefId'],
                "Decision"=> $this->dataDecision['KB_US_PR_007']['decision'],
                "Decision_CSV"=> $decision_CSV_KB_US_PR_007,
                "RuleValue"=>( $this->dataDecision['KB_US_PR_007']['RuleValue'] !='' ?number_format((float) $this->dataDecision['KB_US_PR_007']['RuleValue'],3,'.',',') : NULL),
                "Name" =>  $this->dataDecision['KB_US_PR_007']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_007']['reason'],
                "Trigged"=>$DC02Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_008']) {
            if ( $this->dataDecision['KB_US_PR_008']['decision']=='PASS') {
                $BB02Trigged='No';
                $this->dataDecision['KB_US_PR_008']['decision']='N/A';
                $decision_CSV_KB_US_PR_008='PASS';
            } else {
                $BB02Trigged='Yes';
                $decision_CSV_KB_US_PR_008=$this->dataDecision['KB_US_PR_008']['decision'];

            }
            $this->BB02 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_008']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_008']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_008']['decision'],
                "Decision_CSV" => $decision_CSV_KB_US_PR_008,
                "RuleValue" => ( $this->dataDecision['KB_US_PR_008']['RuleValue'] !='' ? number_format((float) $this->dataDecision['KB_US_PR_008']['RuleValue'], 0, '.', ',') : NULL),
                "Name" =>  $this->dataDecision['KB_US_PR_008']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_008']['reason'],
                "Trigged" => $BB02Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_009']) {
            if ( $this->dataDecision['KB_US_PR_009']['decision']=='PASS') {
                $BB01Trigged='No';
                $this->dataDecision['KB_US_PR_009']['decision']='N/A';
                $decision_CSV_KB_US_PR_009='PASS';
            } else {
                $BB01Trigged='Yes';
                $decision_CSV_KB_US_PR_009=$this->dataDecision['KB_US_PR_009']['decision'];

            }
            $this->BB01 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_009']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_009']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_009']['decision'],
                "Decision_CSV" => $decision_CSV_KB_US_PR_009,
                "RuleValue" => ( $this->dataDecision['KB_US_PR_009']['RuleValue'] !='' ? number_format((float) $this->dataDecision['KB_US_PR_009']['RuleValue'], 0, '.', ',') : NULL),
                "Name" =>  $this->dataDecision['KB_US_PR_009']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_009']['reason'],
                "Trigged" => $BB01Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_010']) {
            if ( $this->dataDecision['KB_US_PR_010']['decision']=='PASS') {
                $RE01Trigged='No';
                $this->dataDecision['KB_US_PR_010']['decision']='N/A';
                $decision_CSV_KB_US_PR_010='PASS';
            } else {
                $RE01Trigged='Yes';
                $decision_CSV_KB_US_PR_010=$this->dataDecision['KB_US_PR_010']['decision'];

            }
            $this->RE01 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_010']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_010']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_010']['decision'],
                "Decision_CSV" =>  $decision_CSV_KB_US_PR_010,
                "RuleValue" => ( $this->dataDecision['KB_US_PR_010']['RuleValue'] != '' ? number_format((float) $this->dataDecision['KB_US_PR_010']['RuleValue'], 0, '.', ',') : NULL),
                "Name" =>  $this->dataDecision['KB_US_PR_010']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_010']['reason'],
                "Trigged" => $RE01Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_013']) {
            if ( $this->dataDecision['KB_US_PR_013']['decision']=='PASS') {
                $EE01Trigged='No';
                $this->dataDecision['KB_US_PR_013']['decision']='N/A';
                $decision_CSV_KB_US_PR_013='PASS';
            } else {
                $EE01Trigged='Yes';
                $decision_CSV_KB_US_PR_013=$this->dataDecision['KB_US_PR_013']['decision'];

            }
            $this->EE01 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_013']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_013']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_013']['decision'],
                "Decision_CSV" => $decision_CSV_KB_US_PR_013,
                "RuleValue" => ( $this->dataDecision['KB_US_PR_013']['RuleValue']!='' ? number_format((float) $this->dataDecision['KB_US_PR_013']['RuleValue'], 0, '.', ',') : NULL),
                "Name" =>  $this->dataDecision['KB_US_PR_013']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_013']['reason'],
                "Trigged" => $EE01Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_014']) {
            if ( $this->dataDecision['KB_US_PR_014']['decision']=='PASS') {
                $DE01Trigged='No';
                $this->dataDecision['KB_US_PR_014']['decision']='N/A';
                $decision_CSV_KB_US_PR_014='PASS';
            } else {
                $DE01Trigged='Yes';
                $decision_CSV_KB_US_PR_014=$this->dataDecision['KB_US_PR_014']['decision'];

            }
            $this->DE01 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_014']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_014']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_014']['decision'],
                "Decision_CSV" =>  $decision_CSV_KB_US_PR_014,
                "RuleValue" => ( $this->dataDecision['KB_US_PR_014']['RuleValue'] !='' ? number_format((float) $this->dataDecision['KB_US_PR_014']['RuleValue'], 0, '.', ',') : NULL),
                "Name" =>  $this->dataDecision['KB_US_PR_014']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_014']['reason'],
                "Trigged" => $DE01Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_015']) {
            if ( $this->dataDecision['KB_US_PR_015']['decision']=='PASS') {
                $CJ01Trigged='No';
                $this->dataDecision['KB_US_PR_015']['decision']='N/A';
                $decision_CSV_KB_US_PR_015='PASS';
            } else {
                $CJ01Trigged='Yes';
                $decision_CSV_KB_US_PR_015=$this->dataDecision['KB_US_PR_015']['decision'];

            }
            $this->CJ01 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_015']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_015']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_015']['decision'],
                "Decision_CSV" =>  $decision_CSV_KB_US_PR_015,
                "RuleValue" => ( $this->dataDecision['KB_US_PR_015']['RuleValue']!= '' ? number_format((float) $this->dataDecision['KB_US_PR_015']['RuleValue'], 0, '.', ',') : NULL),
                "Name" =>  $this->dataDecision['KB_US_PR_015']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_015']['reason'],
                "Trigged" => $CJ01Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_016']) {
            if ( $this->dataDecision['KB_US_PR_016']['decision']=='PASS') {
                $BC01Trigged='No';
                $this->dataDecision['KB_US_PR_016']['decision']='N/A';
                $decision_CSV_KB_US_PR_016='PASS';
            } else {
                $BC01Trigged='Yes';
                $decision_CSV_KB_US_PR_016=$this->dataDecision['KB_US_PR_016']['decision'];

            }
            $this->BC01 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_016']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_016']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_016']['decision'],
                "Decision_CSV" => $decision_CSV_KB_US_PR_016,
                "RuleValue" => ( $this->dataDecision['KB_US_PR_016']['RuleValue']!= '' ? number_format((float) $this->dataDecision['KB_US_PR_016']['RuleValue'], 0, '.', ',') : NULL),
                "Name" =>  $this->dataDecision['KB_US_PR_016']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_016']['reason'],
                "Trigged" => $BC01Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_017']) {
            if ( $this->dataDecision['KB_US_PR_017']['decision']=='PASS') {
                $EI02Trigged='No';
                $this->dataDecision['KB_US_PR_017']['decision']='N/A';
                $decision_CSV_KB_US_PR_017='PASS';
            } else {
                $EI02Trigged='Yes';
                $decision_CSV_KB_US_PR_017=$this->dataDecision['KB_US_PR_017']['decision'];

            }
            $this->EI02 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_017']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_017']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_017']['decision'],
                "Decision_CSV" => $decision_CSV_KB_US_PR_017,
                "RuleValue" => ( $this->dataDecision['KB_US_PR_017']['RuleValue']!= '' ? number_format((float) $this->dataDecision['KB_US_PR_017']['RuleValue'], 0, '.', ',') : NULL),
                "Name" =>  $this->dataDecision['KB_US_PR_017']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_017']['reason'],
                "Trigged" => $EI02Trigged
            );
        }

        if ( $this->dataDecision['KB_US_PR_018']) {
                if ( $this->dataDecision['KB_US_PR_018']['decision']=='PASS') {
                    $DC03Trigged='No';
                    $this->dataDecision['KB_US_PR_018']['decision']='N/A';
                    $decision_CSV_KB_US_PR_018='PASS';
                } else {
                    $DC03Trigged='Yes';
                    $decision_CSV_KB_US_PR_018=$this->dataDecision['KB_US_PR_018']['decision'];

                }
                $this->DC03 = array(
                "InternalId" => $this->dataDecision['KB_US_PR_018']['InternalId'],
                "RefId"=> $this->dataDecision['KB_US_PR_018']['RefId'],
                "Decision"=> $this->dataDecision['KB_US_PR_018']['decision'],
                "Decision_CSV"=> $decision_CSV_KB_US_PR_018,
                "RuleValue"=>( $this->dataDecision['KB_US_PR_018']['RuleValue'] !='' ? number_format((float) $this->dataDecision['KB_US_PR_018']['RuleValue'],3,'.',',') : NULL),
                "Name" =>  $this->dataDecision['KB_US_PR_018']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_018']['reason'],
                "Trigged"=>$DC03Trigged
            );
        }


        if ( $this->dataDecision['KB_US_PR_019']) {
            if ( $this->dataDecision['KB_US_PR_019']['decision']=='PASS') {
                $LA01Trigged='No';
                $this->dataDecision['KB_US_PR_019']['decision']='N/A';
                $decision_CSV_KB_US_PR_019='PASS';
            } else {
                $LA01Trigged='Yes';
                $decision_CSV_KB_US_PR_019=$this->dataDecision['KB_US_PR_019']['decision'];

            }
            $this->LA01 = array(
                "InternalId" =>  $this->dataDecision['KB_US_PR_019']['InternalId'],
                "RefId" =>  $this->dataDecision['KB_US_PR_019']['RefId'],
                "Decision" =>  $this->dataDecision['KB_US_PR_019']['decision'],
                "Decision_CSV" => $decision_CSV_KB_US_PR_019,
                "RuleValue" => ( $this->dataDecision['KB_US_PR_019']['RuleValue']!='' ? number_format((float) $this->dataDecision['KB_US_PR_019']['RuleValue'], 2, '.', ',') : NULL),
                "Name" =>  $this->dataDecision['KB_US_PR_019']['name'],
                "Reason" =>  $this->dataDecision['KB_US_PR_019']['reason'],
                "Trigged" => $LA01Trigged
            );
        }

        //Scorecard data $this->scoreCardData['total']
        // SCORECARD BS02 Scorecard score < 100
        if ($this->scoreCardData['total']<100) {
            $this->BS02 = array(
                "InternalId"=>'',
                "RefId" =>'BS02',
                "Decision" => 'DECLINE',
                "Decision_CSV" => 'DECLINE',
                "RuleValue" => ($this->scoreCardData['total']!='' ? $this->scoreCardData['total'] : NULL),
                "Name" => 'Scorecard score < 100',
                "Reason" => '',
                "Trigged" => 'Yes',
            );
        } else {
            $this->BS02 = array(
                "InternalId" => '',
                "RefId" =>'BS02',
                "Decision" => 'N/A',
                "Decision_CSV" => 'PASS',
                "RuleValue" => ($this->scoreCardData['total']!='' ? $this->scoreCardData['total'] : NULL),
                "Name" => 'Scorecard score > 100',
                "Reason" => '',
                "Trigged" => 'No',
            );
        }

        if ($this->scoreCardData['total']<=450) {
            $this->BS01 = array(
                "InternalId" => '',
                "RefId" =>'BS01',
                "Decision" => 'REFER',
                "Decision_CSV" => 'REFER',
                "Name" => 'Scorecard score>=100 and score<=450',
                "Reason" => '',
                "RuleValue" => ($this->scoreCardData['total']!='' ? number_format((float)$this->scoreCardData['total'],0,'.',',') : NULL),
                "Trigged" => 'Yes',
            );
        } else {
            $this->BS01 = array(
                "InternalId" => '',
                "RefId" =>'BS01',
                "Decision" => 'N/A',
                "Decision_CSV" => 'PASS',
                "RuleValue" => ($this->scoreCardData['total']!='' ? number_format((float)$this->scoreCardData['total'],0,'.',',') : NULL),
                "Name" => 'Scorecard score > 450',
                "Reason" => '',
                "Trigged" => 'No',
            );
        }

        /**
         * KIWI BANK CUSTOM decison card values
         */

        $this->UB02 = array(
            "InternalId" => '',
            "RefId"=>'UB02',
            "Decision"=>'PASS',
            "RuleValue"=>'N/A',
            "Name" => 'Underage Borrower',
            "Reason" => '',
            "Trigged"=>'No'
        );

        $this->FC02 = array(
            "InternalId" => '',
            "RefId"=>'FC02',
            "Decision"=>'PASS',
            "RuleValue"=>'N/A',
            "Name" => 'Foreign Company',
            "Reason" => '',
            "Trigged"=>'No'
        );

        $this->IO02 = array(
            "InternalId" => '',
            "RefId"=>'IO02',
            "RefId"=>'IO02',
            "Decision"=>'PASS',
            "RuleValue"=>'N/A',
            "Name" => 'Invalid Organisation',
            "Reason" => '',
            "Trigged"=>'No'
        );
       $_policyRuleKey  = array(
            'AX02','BC01','BB01','BB02','DC01','DC02','DC03','DE01','EE01','RE01','EI02','IC01','CJ01','LA01','NI01','MA01','AC02','BS02','BS01','UB02','FC02','IO02'
        );
        $_policyRuleValue = array(
            $this->AX02,$this->BC01,$this->BB01,$this->BB02,$this->DC01,$this->DC02, $this->DC03, $this->DE01, $this->EE01, $this->RE01, $this->EI02, $this->IC01, $this->CJ01, $this->LA01, $this->NI01, $this->MA01, $this->AC02, $this->BS02, $this->BS01, $this->UB02,$this->FC02,$this->IO02
        );

        return ["payLoadDecisionData"=>array_combine($_policyRuleKey, $_policyRuleValue),"scoreCard"=>json_decode($this->scoreCardData($orgID),true)];
    }

    /**
     * @param int $organisationId
     * @return bool|mixed
     * @throws \Doctrine\DBAL\DBALException
     */

    private function orgApplicantID(int $organisationId) {
        $org = "SELECT applicant_id FROM organisation WHERE id = " . $organisationId." LIMIT 1";
        $statement = $this->entityManager->getConnection()->query($org);
        $orgQuerry = $statement->fetchAll();
       if (isset($orgQuerry)) { return($orgQuerry[0]['applicant_id']);} else {
            return false;
        }

    }

    /**
     * @param int $organisationId
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    private function decisionJson(int $organisationId) {
        $appID=$this->orgApplicantID($organisationId);
        $dj= "SELECT result FROM decision_policy_result WHERE applicant_id = '". $appID ."' LIMIT 1";
        $statement = $this->entityManager->getConnection()->query($dj);
        $djQuerry = $statement->fetchAll();
        return($djQuerry[0]['result']);
    }

    /**
     * @param int $organisationId
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */

    private function scoreCardData(int $organisationId) {
        $appID=$this->orgApplicantID($organisationId);
        $dj= "SELECT scorecard FROM decision_policy_result WHERE applicant_id = '". $appID ."' LIMIT 1";
        $statement = $this->entityManager->getConnection()->query($dj);
        $djQuerry = $statement->fetchAll();
        return($djQuerry[0]['scorecard']);
    }
}