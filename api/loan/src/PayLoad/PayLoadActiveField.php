<?php declare(strict_types=1);


namespace App\PayLoad;

use App\Entity\Applicant;
use App\Entity\FinanceMonth;
use App\TypeTrait\Utc2NZ;
use App\Service\EquiFaxDataQuery;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Psr\Log\LoggerInterface;

class PayLoadActiveField
{
    use Utc2NZ;
    private $applicant;
    private $query;
    private $orm;
    private $logger;
    public function __construct(
        EntityManagerInterface $entity,
        Applicant $applicant ,
        EquiFaxDataQuery $query,
        LoggerInterface $logger)
    {
        $this->applicant = $applicant;
        $this->query = $query;
        $this->orm = $entity;
        $this->logger = $logger;
    }

    public function nzcn(): string
    {
        return $this->query->NZCN();
    }

    public function anzSIC(): string
    {
        return $this->query->ANZSIC();
    }

    public function anzSICDesc(): string
    {
        return $this->query->ANZSICDesc();
    }

    public function registedAddress(): string
    {
        return $this->query->registedAddress();
    }

    public function postalAddress(): string
    {
        return $this->query->postalAddress();
    }

    public function businessAddress(): string
    {
        return $this->query->businessAddress();
    }

    public function ageOfBusiness(): string
    {
        return $this->query->ageOfBusiness();
    }

    public function ageOfBureauFile(): string
    {
        return $this->query->ageOfBureauFile();
    }

    public function enqueryInLast2years(): string
    {
        return $this->query->enqueryInLast2years();
    }

    public function bureauDefaultsInLast3Years(): string
    {
        return $this->query->bureauDefaultsInLast3Years();
    }

    public function individuals(): array
    {
        return $this->query->individuals();
    }


    private function getEquiFaxData(): array
    {
        return $this->applicant->getEquiFaxRaw();
    }

    public function created(): string
    {
        $d = $this->convert2NZ($this->applicant->getCreated());
        return $d->format('Y-m-d H:i:s');
    }

    public function businessName(): string
    {
        return $this->applicant->getBusinessName();
    }

    public function phone(): string
    {
        return $this->applicant->getPhone();
    }
    public function email(): string
    {
        return $this->applicant->getEmail();
    }

    public function taxNumber(): string
    {
        $org = $this->applicant->getOrganisation();
        if ($org) {
            return $org->getTaxNumber();
        }
        return '';
    }

    public function financeMonthPeriod(): string
    {
        $result = '';
        if ($this->applicant->getOrganisation()) {
            /** @var \Doctrine\ORM\QueryBuilder $qb ***/
            $qb = $this->orm->getRepository(FinanceMonth::class)
                ->mostRecent($this->applicant->getOrganisation());
            /** @var FinanceMonth $financeMonth***/
            $financeMonth = $qb->getQuery()->getOneOrNullResult();
            if ($financeMonth) {
                $result = $financeMonth->getPeriod()->format('Y-m-d h:i:s');
            }
        }
        return $result;
    }

    public function financeMonthTotal($field): array
    {
        $result = ['current' => '0.00', 'previous' => '0.00'];
        if ($this->applicant->getOrganisation()) {
            try {
                /** @var \Doctrine\ORM\QueryBuilder $qb ** */
                $qb = $this->orm->getRepository(FinanceMonth::class)
                    ->total($this->applicant->getOrganisation(),$field);
                $n = (float)array_sum( array_column($qb->getQuery()->getArrayResult(), 'total'));
                $result['current'] = \round($n, 2);

                $qb = $this->orm->getRepository(FinanceMonth::class)
                    ->total($this->applicant->getOrganisation(),$field,'previous');
                $n = (float)array_sum( array_column($qb->getQuery()->getArrayResult(), 'total'));
                $result['previous'] = \round($n, 2);

            } catch (\Exception $e) {
                $result = '';
                $this->logger->emergency($e->getMessage());
            }
        }
        return $result;
    }

    public function financeMonthRent(): array
    {
        return $this->financeMonthTotal('totalRent');
    }

    public function financeMonthTotalRevenue(): array
    {
        return $this->financeMonthTotal('totalRevenue');
    }

    public function financeMonthCOG(): array
    {
        return $this->financeMonthTotal('totalCog');
    }

    public function financeMonthInterest(): array
    {
        return $this->financeMonthTotal('interest');
    }

    public function financeMonthWage(): array
    {
        return $this->financeMonthTotal('totalPeople');
    }

    public function financeMonthDepreciation(): array
    {
        return $this->financeMonthTotal('depreciation');
    }

    public function financeMonthTax(): array
    {
        return $this->financeMonthTotal('tax');
    }

    public function financeMonthOperatingCosts(): array
    {
        $result = ['current' => 0.00, 'previous' => 0.00];

        $people = $this->financeMonthWage();
        $result['current'] += (float)$people['current'];
        $result['previous'] += (float)$people['previous'];

        $operating = $this->financeMonthTotal('totalOperating');
        $result['current'] += (float)$operating['current'];
        $result['previous'] += (float)$operating['previous'];

        $interest = $this->financeMonthInterest();
        $result['current'] += (float)$interest['current'];
        $result['previous'] += (float)$interest['previous'];

        $depreciation = $this->financeMonthDepreciation();
        $result['current'] += (float)$depreciation['current'];
        $result['previous'] += (float)$depreciation['previous'];

        $rent = $this->financeMonthRent();
        $result['current'] += (float)$rent['current'];
        $result['previous'] += (float)$rent['previous'];

        $cost['current'] = \round($result['current'], 2);
        $cost['previous'] = \round($result['previous'], 2);
        return $cost;
    }

    public function balance($field): array
    {
        //''
        $result = ['current' => '0.00', 'previous' => '0.00'];
        if ($this->applicant->getOrganisation()) {
            try {
                /** @var \Doctrine\ORM\QueryBuilder $qb ** */
                $qb = $this->orm->getRepository(FinanceMonth::class)
                    ->balance($this->applicant->getOrganisation(), $field);
                $n = (float)$qb->getQuery()->getSingleScalarResult();
                $result['current'] = \number_format($n, 2);

                $qb = $this->orm->getRepository(FinanceMonth::class)
                    ->balance($this->applicant->getOrganisation(), $field, 'previous');
                $n = (float)$qb->getQuery()->getSingleScalarResult();
                $result['previous'] = \number_format($n, 2);

            } catch (\Exception $e) {
                $this->logger->emergency($e->getMessage());
            }
        }
        return $result;
    }

    public function financeMonthStock(): array
    {
        return $this->balance('totalAssetsCurrentInventory');
    }

    public function financeMonthDebtor(): array
    {
        return $this->balance('totalAssetsCurrentDebtors');
    }

    public function financeMonthBank(): array
    {
        return $this->balance('totalAssetsCurrentBank');
    }

    public function financeMonthOther(): array
    {
        return $this->balance('totalAssetsCurrentOther');
    }

    public function financeMonthAssetsFixed(): array
    {
        return $this->balance('totalAssetsFixed');
    }

    public function financeMonthIntangibleAssets(): array
    {
        return $this->balance('totalAssetsOther');
    }

    public function financeMonthCreditors(): array
    {
        return $this->balance('totalLiabilitiesCurrentCreditors');
    }

    public function financeMonthOtherCurrentLiabilities(): array
    {
        return $this->balance('totalLiabilitiesCurrentOther');
    }

    public function financeMonthNonCurrentLiabilities(): array
    {
        return $this->balance('totalLiabilitiesOther');
    }
}