<?php declare(strict_types=1);


namespace App\PayLoad;


/**
 * Class PayLoadPolicyRuleResult
 * @package App\PayLoad
 */
class PayLoadPolicyRuleResult
{
    /**
     * @var array
     */
    private $result;

    /**
     * PayLoadPolicyRuleResult constructor.
     * @param array $result
     */
    public function __construct(array $result = [])
    {
        $this->result = $result;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $presentation = [];
        foreach ($this->result as $value) {
            $temp = [];
            $temp['RuleCode'] = $value['RefId'];
            $temp['Triggered'] = $value['decision'] === 'PASS' ? 'No' : 'Yes';
            if ($temp['Triggered'] === 'No') {
                continue;
            }
            $temp['PolicyRule'] = $value['name'];
            $temp['AssementArea'] = $this->assementArea($value['RefId']);
            $temp['RuleValue'] = $value['RuleValue'];
            $temp['RuleType'] = $value['decision'];
            $presentation[] = $temp;
        }

        return $presentation;
    }

    /**
     * @param string $ruleCode
     * @return string
     */
    private function assementArea(string $ruleCode): string
    {
        $temp['EI02'] = 'Conditions';
        $temp['LA01'] = 'Common Sense';
        $temp['NI01'] = 'Common Sense';
        $temp['MA01'] = 'Common Sense';
        $temp['AX02'] = 'Character';
        $temp['AC02'] = 'Character';
        $temp['DC01'] = 'Capital';
        $temp['IC01'] = 'Capital';
        $temp['DC02'] = 'Capacity';
        $temp['BB02'] = 'Character';
        $temp['BS02'] = 'Character';
        $temp['BB01'] = 'Character';
        $temp['BS01'] = 'Character';
        $temp['RE01'] = 'Character';
        $temp['EE01'] = 'Character';
        $temp['DE01'] = 'Character';
        $temp['CJ01'] = 'Character';
        $temp['BC01'] = 'Common Sense';
        $temp['DC03'] = 'Capacity';


        return $temp[$ruleCode] ?? '';
    }
}