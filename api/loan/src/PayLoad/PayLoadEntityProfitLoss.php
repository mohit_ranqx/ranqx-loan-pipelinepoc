<?php declare(strict_types = 1);

namespace App\PayLoad;

use App\Entity\CalculateFinanceMonth;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PayLoadEntityProfitLoss
 * @package App\payLod
 */


class PayLoadEntityProfitLoss
{
    /**
     * @var EntityManagerInterface
     */
    private $entity;

    /**
     * @var int
     */
    private $count;

    /**
     * @var int
     */
    private $revenueCurrent;

    /**
     * @var int
     */
    private $revenuePrevious;

    /**
     * @var int
     */
    private $COGSCurrent;

    /**
     * @var int
     */
    private $COGSPrevious;

    /**
     * @var int
     */
    private $InterestCurrent;

    /**
     * @var int
     */
    private $InterestPrevious;

    /**
     * @var int
     */
    private $WagesCurrent;

    /**
     * @var int
     */
    private $WagesPrevious;

    /**
     * @var int
     */
    private $Tax;

    /**
     * @var int
     */
    private $Depreciation;

    /**
     * @var array
     */
    private $profitLoss;

    /**
     * PayLoadEntityProfitLoss constructor.
     * @param EntityManagerInterface $entity
     */
    public function __construct(EntityManagerInterface $entity)
    {
        $this->entity=$entity;
        $this->count=1;
        $this->revenueCurrent=0;
        $this->revenuePrevious=0;
        $this->COGSCurrent=0;
        $this->COGSPrevious=0;
        $this->InterestCurrent=0;
        $this->InterestPrevious=0;
        $this->WagesCurrent=0;
        $this->WagesPrevious=0;
        $this->Tax=0;
        $this->Depreciation=0;
        $this->profitLoss=array();
    }

    /**
     * @param $orgID
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function entityProfitLoss($orgID) {

        $results = $this->entity->getRepository(CalculateFinanceMonth::class)
            ->findBy(['organisation_id' => $orgID]);

        $_ProfitLoss= "SELECT * FROM finance_month WHERE organisation_id='".$orgID."' ORDER BY period ASC";
        $orgData = $this->entity->getConnection()->query($_ProfitLoss);
        $_Data = $orgData->fetchAll();

        foreach ($_Data as $pl) {
            // Current period  $app->getCreated()
            if (($this->count >= 13) && ($this->count <= 24)) {
                    $this->revenueCurrent += $pl['total_revenue'];
                    $this->COGSCurrent += $pl['total_cog'];
                    $this->InterestCurrent += $pl['interest'];
                    $this->WagesCurrent += $pl['total_people'];
                    $this->Tax += $pl['tax'];
                    $this->Depreciation += $pl['depreciation'];
            }

            // Previous period
            if (($this->count >= 1) && ($this->count <= 12)) {
                    $this->revenuePrevious += $pl['total_revenue'];
                    $this->COGSPrevious += $pl['total_cog'];
                    $this->InterestPrevious += $pl['interest'];
                    $this->WagesPrevious += $pl['total_people'];
                    $this->Tax += $pl['tax'];
                    $this->Depreciation += $pl['depreciation'];
            }
            $this->count++;
        }

        $this->profitLoss = array(
                        "SalesRevenueCurrent" =>  number_format((float)$this->revenueCurrent,2,'.',','),
                        "SalesRevenuePrevious" => number_format((float)$this->revenuePrevious,2,'.',','),
                        "COGSCurrent" => number_format((float)$this->COGSCurrent,2,'.',','),
                        "COGSPrevious" => number_format((float)$this->COGSPrevious,2,'.',','),
                    // "GrossProfitCurrent" => '',
                    //  "GrossProfitPrevious" => '',
                    //  "GrossMarginCurrent" => '',
                    //  "GrossMarginPrevious" => '',
                    //  "OtherIncomeCurrent" => '',
                    //  "OtherIncomePrevious" => '',
                    //  "EBITDACurrent" => '',
                    //  "EBITDAPrevious" => '',
                    //  "NPBTCurrent" => '',
                    //  "NPBTPrevious" => '',
                        "CompanyTaxCurrent" =>  number_format((float)$this->Tax,2,'.',','),
                        "CompanyTaxPrevious" =>  number_format((float)$this->Tax,2,'.',','),
                    //  "DrawingsCurrent" => '',
                    //  "DrawingsPrevious" => '',
                    //  "CashAvailableforDistribution" => '',
                    //  "TotalExpensesCurrent" => '',
                    //  "TotalExpensesPrevious" => '',
                        "InterestCurrent" => number_format((float)$this->InterestCurrent,2,'.',','),
                        "InterestPrevious" => number_format((float)$this->InterestPrevious,2,'.',','),
                        "WagesCurrent" => number_format((float)$this->WagesCurrent,2,'.',','),
                        "WagesPrevious" => number_format((float)$this->WagesPrevious,2,'.',','),
                    //  "HomeOfficeCurrent" => '',
                    //  "HomeOfficePrevious" => '',
                    //  "ShareholderSalaryCurrent" => '',
                    //  "ShareholderSalaryPrevious" => '',
                        "DepreciationCurrent" => number_format((float)$this->Depreciation,2,'.',','),
                        "DepreciationPrevious" => number_format((float)$this->Depreciation,2,'.',','),
                    //  "AmortisationCurrent" => '',
                    //  "AmortisationPrevious" => '',
                    //  "OtherAddbackCurrent" => '',
                    //  "OtherAddbackPrevious" => '',
                    //  "OtherExpensesCurrent" => '',
                    //  "OtherExpensePrevious" => '',
                    //  "BusinessServicingCommitmentsPI" => '',
                    //  "BusinessServicingCommitmentIO" => ''
        );
        return $this->profitLoss;

    }

}