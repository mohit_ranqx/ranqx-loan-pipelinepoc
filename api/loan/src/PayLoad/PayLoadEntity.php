<?php declare(strict_types = 1);

namespace App\PayLoad;

use App\Entity\Applicant;
use App\Entity\Organisation;
use Doctrine\ORM\EntityManagerInterface;


class PayLoadEntity
{
    /**
     * @var EntityManagerInterface
     */
    private $entity;

    /**
     * @var int
     */
    private $count;

    /**
     * @var int
     */
    private $EntityFinancialDetailsBalanceDate;

    /**
     * @var string
     */
    private $entityName;

    /**
     * @var string
     */
    private $entityContactDetailsDaytimePhone;

    /**
     * @var string
     */
    private $entityContactDetailsEmail;

    /**
     * @var string
     */
    private $gstID;

    /**
     * PayLoadEntity constructor.
     * @param EntityManagerInterface $entity
     */

    public function __construct(EntityManagerInterface $entity)
    {
        $this->entity=$entity;
        $this->count=0;
        $this->EntityFinancialDetailsBalanceDate=0;
        $this->entityName='';
        $this->entityContactDetailsDaytimePhone='';
        $this->entityContactDetailsEmail='';
        $this->gstID='';
    }

    /**
     * @param $orgID
     * @return mixed
     */

    public function entityDetails($orgID)
    {

        $results = $this->entity->getRepository(Applicant::class)
            ->findBy(['id' => $orgID]);

        foreach ($results as $ed) {
              $this->entityName = $ed->getBusinessName() ;
              $appId = $ed->getId();
        }

        $this->Entity['Details'] = array(
                    //Access number --- no value KB
                   // "AccessNumber" => '',

                    //applicant.business_name
                    "EntityName" => $this->entityName,

                    //DECISION JSON
                    //companyDetails->data->body->businessDetails->organisationNumber
                    "NZCN" => "companyDetails->data->body->businessDetails->organisationNumber",

                    //Customer type
                    "CustomerType" => 'Limited Company',

                    //Scorecard Segment
                    "ScorecardSegment" => 'SME Digital',

                    //Requires Security Swap Agreement -- no value kb
                    "RequiresSecuritySwapAgreement" => '',

                    //CCR_SCR_IMI -- kb value
                    "CCR_SCR_IMI" => '7',

                    //ANZSIC Code
                    "ANZSICCode" => "companyDetails->data->body->indusrty->code",
                    "ANZSICDescription" => "companyDetails->data->body->indusrty->description",

                    //DECISION JSON
                    ////equiFax->db->companyDetails->data->body->businessDetails->industry
                    "NZSICCode" => "companyDetails->data->body->businessDetails->industry->code",
                    "NZSICDescription" => "companyDetails->data->body->businessDetails->industry->description",
        );

        return $this->Entity['Details'] ;

    }

    /**
     * @param $orgID
     * @return mixed
     */
    public function entityAddress($orgID)
    {
        $this->Entity['Address'] =
                   array(
                    "RegisteredAddress" =>
                        array(
                            "Address" => "companyDetails->data->body->RegisteredAddress->address",
                            "Suburb" => "companyDetails->data->body->RegisteredAddress->Suburb",
                            "City" =>  "companyDetails->data->body->RegisteredAddress->City",
                            "Postcode" => "companyDetails->data->body->RegisteredAddress->Postcode"
                        ),
                    "BusinessAddress" =>
                        array(
                            "Address" => "companyDetails->data->body->BusinessAddress->address",
                            "Suburb" => "companyDetails->data->body->BusinessAddress->Suburb",
                            "City" =>  "companyDetails->data->body->BusinessAddress->City",
                            "Postcode" => "companyDetails->data->body->BusinessAddress->Postcode"
                        ),
                    "PostalAddress" =>
                        array(
                            "Address" => "companyDetails->data->body->PostalAddress->address",
                            "Suburb" => "companyDetails->data->body->PostalAddress->Suburb",
                            "City" =>  "companyDetails->data->body->PostalAddress->City",
                            "Postcode" => "companyDetails->data->body->PostalAddress->Postcode"
                        ),
                    "ThirdPartyAddress" =>
                        array(
                            "Address" => '',
                            "Suburb" => '',
                            "City" => '',
                            "Postcode" => '',
                        )
          );

        return $this->Entity['Address'];

    }

    /**
     * @param $orgID
     * @return mixed
     */
    public function entityContact($orgID)
    {
        $results = $this->entity->getRepository(Applicant::class)
            ->findBy(['id' => $orgID]);

        foreach ($results as $ec) {
            $this->entityContactDetailsDaytimePhone = $ec->getPhone();
            $this->entityContactDetailsEmail = $ec->getEmail();
        }

        $this->Entity['ContactDetails'] = array(
                    //get from applicant.phone
                    "DaytimePhone" => $this->entityContactDetailsDaytimePhone,
                    // no value -- kb
                    "Fax" => '',
                    //get from applicant.email
                    "Email" => $this->entityContactDetailsEmail,
        );
        return $this->Entity['ContactDetails'];
    }

    /**
     * @param $orgID
     * @return mixed
     */
    public function entityTax($orgID)
    {

        $results = $this->entity->getRepository(Organisation::class)
            ->findBy(['id' => $orgID]);

        foreach ($results as $orgTax) {
            $this->gstID = $orgTax->getTaxNumber() ;
        }

        $this->Entity['Tax'] = array(
                    //Xero tax mapping onto orgtable
                    "IRDNumber" => $this->gstID,
                    "TaxRate" => '28%',
                    "TaxExemptionCertificateHeld" => 'Choose Yes or No',
                    //no value -- kb
                    "Taxexemptioncertificateexpirydate" => '',
                    "Taxexemptcertno" => '',

                    //Xero tax mapping onto orgtable
                    "GSTNumber" => $this->gstID,

                    // empty value KB
                    "CDDStatus" => ''
        );
        return $this->Entity['Tax'];

    }

    /**
     * @param $orgID
     * @return mixed
     */
    public function entityCredit($orgID)
    {
        $getEquifaxData = new PayLoadEquifaxData($this->entity);
        $equifaxMap = $getEquifaxData->equifaxRawJson($orgID);

        $this->Entity['Credit'] = array(
                    //"SectorPolicy" => 'SME',
                    // equifax json
                    // reports/v1/organisationDetails/{id}
                   // "Ageofbusiness" => "decision->companyDetails->ageOfCompany",
                    // no value -- kb
                    ///"Experienceinrunningabusiness" => '',
                   // "Timeinthistypeofbusiness" => '',
                   // "LengthofrelationshipwithKiwibank" => '',
                    //equifax json
                    // creditActivitySummary->data->body->ageOfCreditFile
                    //"AgeofBureauFile" => "creditActivitySummary->data->body->ageOfCreditFile",
                    //Equifax json
                    // decision->summary->enquiriesCount
                   // "Inquiriesinlast2years" => "decision->summary->enquiriesCount",
                    //Equifax json
                    // need to confirm we have 3 years of counts on this one?
                    // equiFax -> db -> decision->summary->judgementsCount
                   // "Bureaudefaultsinlast3years" => "decision->summary->judgementsCount",
                    //Last part of credit in HTMl
                   // "NumberOfDishonoursInLastSixMonths" => '',
                    "CharacterStabilityTimeInBusiness" => $equifaxMap['equiFax']['db']['companyDetails']['data']['body']['businessDetails']['registrationPeriod'] ?? NULL,
                   // "CapacityDebtServicingAbilityIO" => '',
                   // "CapacityDebtServicingAbilityPandI" => '',
                   // "CapacityGroupFinancialPositionEquity" => ''
        );
        return $this->Entity['Credit'];
    }

    /**
     * @param $orgID
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */

    public function entityFinancialDetails($orgID)
    {

        $results = "SELECT * FROM finance_month WHERE organisation_id='".$orgID."' ORDER BY period ASC";
        $financeMonth = $this->entity->getConnection()->query($results);
        $totalAssetsCurrentInventory= $financeMonth->fetchAll();

        foreach ($totalAssetsCurrentInventory as $i => $aci) {
            if (($i == 23)) {
                $this->EntityFinancialDetailsBalanceDate = date('d-m-Y', strtotime($totalAssetsCurrentInventory[$i]['period']));
            }
        }

        $this->Entity['FinancialDetail'] = array(
                    "NewStartUp" => 'Choose Yes or No',
                    "DataSupplied" => 'Y',
                    "BalanceDate" => $this->EntityFinancialDetailsBalanceDate,
                  //"typeofFinancialInformationSupplied" => 'Other'
        );
        return $this->Entity['FinancialDetail'];
    }

    /**
     * @return mixed
     */

    public function entityDebtServicing()
    {

        $this->Entity['DebtServicing'] = array(
                    "BBRHLoansLimit" => '',
                    "BBRHLoanterm" => '',
                    "BBRHLoansMonthlyPandI" => '',
                    "BBRHLoansAnnualPandI" => '',
                    "MonthlyInterest" => '',
                    "AccruedInterest" => '',
                    "TermLoanLimits" => '',
                    "TermLoanterm" => '',
                    "TermLoanbaserate" => '',
                    "TermLoanMonthlyPandL" => '',
                    "TermLoanAnnualPandI" => '',
                    "TermLoanMonthlyInterest" => '',
                    "TermLoanAnnualInterest" => '',
                    "ODividedDorRvlgLimits" => '',
                    "ODividedDorRvlgBaseRate" => '',
                    "ODividedDorRvlgMonthlyPandI" => '',
                    "ODividedDorRvlgAnnualPandI" => '',
                    "ODividedDorRvlgMonthlyInterest" => '',
                    "ODividedDorRvlgAnnualInterest" => '',
                    "CreditCardLimits" => '',
                    "CreditCardMonthlyPandI" => '',
                    "CreditCardAnnualPandI" => '',
        );
        return $this->Entity['DebtServicing'];

    }


   public function entityDrawings()
    {

        $this->Entity['Drawings'] = array(
                    "Drawings" => '',
                    "CashAvailableforDistribution" => ''
        );
        return $this->Entity['Drawings'];

    }

}