<?php


namespace App\PayLoad;

use App\Service\S3RanqxClient;
use Psr\Log\LoggerInterface;
use DataDog\DogStatsd;
use App\Entity\Applicant;
use App\PayLoad\PayLoadFileCreation;
use Aws\Ses\SesClient;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Environment;
use PHPMailer\PHPMailer\PHPMailer;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity\Organisation;
use App\PayLoad\PayLoadApplicationDetails;
use App\PayLoad\PayLoadBusinessLoanApplication;
use App\PayLoad\PayLoadDecisionData;
use App\PayLoad\PayLoadEntity;
use App\PayLoad\PayLoadEntityBalanceSheet;
use App\PayLoad\PayLoadEntityFinancialRatios;
use App\PayLoad\PayLoadEntityProfitLoss;
use App\PayLoad\PayLoadEquifaxData;
use App\PayLoad\PayLoadIndividual;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Response;

class PayLoadInitiate
{
    /**
     * @var int
     */
    private $orgID;

    /**
     * @var EntityManagerInterface
     */
    private $objectManager;

    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var S3RanqxClient
     */
    private $client;


    /**
     * PayLoadInitiate constructor.
     * @param EntityManagerInterface $objectManager
     * @param ParameterBagInterface $params
     * @param Environment $twig
     * @param LoggerInterface $logger
     * @param S3RanqxClient $client
     */
    //public function __construct(EntityManagerInterface $objectManager, ParameterBagInterface $params, Environment $twig, LoggerInterface $logger) {
    public function __construct(EntityManagerInterface $objectManager, ParameterBagInterface $params, Environment $twig, LoggerInterface $logger, S3RanqxClient $client) {
        $this->objectManager = $objectManager;
        $this->params = $params;
        $this->twig = $twig;
        $this->statsd = new DogStatsd();
        $this->logger = $logger;
        $this->client = $client;
    }

    //public function startPayload(EntityManagerInterface $entity,LoggerInterface $logger, Environment $twig, $orgID) {
    public function startPayload($orgID) {

        $this->orgID = $orgID;
        ///////////////////////////////////////////////////////////
        // ScoreCard and Decision Data
        $decisionData = new PayLoadDecisionData($this->objectManager);
        $dataDecision = $decisionData->decisionData($this->orgID);
        ////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////
        $payLoadBusinessLoanApplication = new PayLoadBusinessLoanApplication($this->objectManager);
        $businessLoanApplication = $payLoadBusinessLoanApplication->payLoadBusinessData($this->orgID);
        $groupMasterClientDetails = $payLoadBusinessLoanApplication->payLoadGroupMasterClient();
        $groupMasterClientPayLoadDecisionDetails = $payLoadBusinessLoanApplication->payLoadDecision();
        /////////////////////////////////////////////////////////////
        // entityFinancialRatios
        $payLoadFinancialRatios = new PayLoadEntityFinancialRatios();
        $entityFinancialRatios = $payLoadFinancialRatios->entityFinancialRatios();
        /////////////////////////////////////////////////////////////
        //ENTIY CLASS
        $payLoadEntity = new PayLoadEntity($this->objectManager);

        $this->payLoadEntity =
            array(
                "entityDetails" => $payLoadEntity->entityDetails($this->orgID),
                "entityAddress" => $payLoadEntity->entityAddress($this->orgID),
                "entityContact" => $payLoadEntity->entityContact($this->orgID),
                "entityTax" => $payLoadEntity->entityTax($this->orgID),
                "entityFinancialDetails" => $payLoadEntity->entityFinancialDetails($this->orgID),
            );
        $entityDetails = $payLoadEntity->entityDetails($this->orgID);
        /////////////////////////////////////////////////////////////
        // entityAddress
        $entityAddress = $payLoadEntity->entityAddress($this->orgID);
        /////////////////////////////////////////////////////////////
        // entityContact
        $entityContact = $payLoadEntity->entityContact($this->orgID);
        /////////////////////////////////////////////////////////////
        // entityTax
        $entityTax = $payLoadEntity->entityTax($this->orgID);
        /////////////////////////////////////////////////////////////
        // entityCredit
        $entityCredit = $payLoadEntity->entityCredit($this->orgID);
        /////////////////////////////////////////////////////////////
        // entityFinancialDetails
        $entityFinancialDetails = $payLoadEntity->entityFinancialDetails($this->orgID);
        /////////////////////////////////////////////////////////////
        // entityDebtServicing
        // $entityDebtServicing = $payLoadEntity->entityDebtServicing($this->orgID);
        /////////////////////////////////////////////////////////////
        // entityDrawings
        // $entityDrawings = $payLoadEntity->entityDrawings($this->orgID);
        /////////////////////////////////////////////////////////////
        //Profit Loss
        $profitLoss = new PayLoadEntityProfitLoss($this->objectManager);
        $payloadProfitLoss = $profitLoss->entityProfitLoss($this->orgID);
        /////////////////////////////////////////////////////////////
        //Balance Sheet
        $balanceSheet = new PayLoadEntityBalanceSheet($this->objectManager);
        $paylaodBalaceSheet = $balanceSheet->entityBalanceSheet($this->orgID);
        /////////////////////////////////////////////////////////////
        //PayLoadIndividual
        $individual = new PayLoadIndividual($this->objectManager);
        $payloadIndividual = $individual->individualsDetails($this->orgID);
        /////////////////////////////////////////////////////////////
        // EQUIFAXDATA from Json
        // combine into array
        $equiFax = new PayLoadEquifaxData($this->objectManager);
        $equiFaxDataEntity = $equiFax->equifaxJsonMapEntity($this->orgID);
        /////////////////////////////////////////////////////////////
        // Application Details
        $appDetails = new PayLoadApplicationDetails($this->objectManager);
        $applicationDetails = $appDetails->applicationDetails($this->orgID);
        /////////////////////////////////////////////////////////////
        // Make a Paylaod JSON from all the arrays
        $buildPayloadKey = array('applicationDetails','entityFinancialRatios','payLoadEntity','payloadProfitLoss','paylaodBalaceSheet','payloadIndividual','payLoadDecisionData');

        $buildayLoadData = array($applicationDetails,$entityFinancialRatios,$this->payLoadEntity,$payloadProfitLoss,$paylaodBalaceSheet,$payloadIndividual,$dataDecision['payLoadDecisionData']);

        $buildayLoadData = json_encode(array_combine($buildPayloadKey, $buildayLoadData));
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $payLoadHtml = $this->twig->render('\payload\forPayLoad_v2.html.twig',[
            'applicationDetails' => $applicationDetails,
            'businessLoanApplication' => $businessLoanApplication['business']['loanApplication'],
            'groupMasterClient' => $groupMasterClientDetails['groupMasterClient']['details'],
            'groupMasterClientDetailsPayLoadDecisionDetails' => $groupMasterClientPayLoadDecisionDetails['groupMasterClient']['payLoadDecisionDetails'],
            //'entityFinancialRatios' => $entityFinancialRatios[0]['financialRatios'],
            'entityDetails' => $entityDetails,
            // 'entityDetails' => $this->payLoadEntity['entityDetails'],
            'entityRegisteredAddress' => $entityAddress['RegisteredAddress'],
            'entityBusinessAddress' => $entityAddress['BusinessAddress'],
            'entityPostalAddress' => $entityAddress['PostalAddress'],
            'entityThirdPartyAddress' => $entityAddress['ThirdPartyAddress'],
            'entityContactDetails' => $entityContact,
            'entityTax' => $entityTax,
            'entityCredit' => $entityCredit,
            'entityFinancialDetail' => $entityFinancialDetails,
            'payloadProfitLoss' => $payloadProfitLoss,
            'paylaodBalaceSheet' => $paylaodBalaceSheet[0]['BalanceSheet'],
            'payloadIndividualDetails' => $payloadIndividual,
            'equifaxDataEntity' => $equiFaxDataEntity,
            'payLoadDecisionData' => $dataDecision['payLoadDecisionData']
        ]);

        ////////////////////////////////////////////////////////////////////////
        // build up some files for the PAYLOAD ZIP
        $payloadZIP = new PayLoadFileCreation( $this->objectManager,$this->client,$this->logger);
        $payloadZIP->createPayLoadFiles($dataDecision,$payLoadHtml,$applicationDetails['LendingDecision'],$this->orgID);
        ///////////////////////////////////////////////////////////////////////

        return true;
    }


    private function orgApplicantID(int $organisationId) {
        $org = "SELECT applicant_id FROM organisation WHERE id = " . $organisationId." LIMIT 1";
        $statement = $this->objectManager->getConnection()->query($org);
        $orgQuerry = $statement->fetchAll();
        return($orgQuerry[0]['applicant_id']);
    }

    private function decisionJson(int $organisationId) {
        $appID=$this->orgApplicantID($organisationId);
        $dj= "SELECT pdf FROM Applicant WHERE id = '". $appID ."' LIMIT 1";
        $statement = $this->objectManager->getConnection()->query($dj);
        $djQuerry = $statement->fetchAll();
        return($djQuerry[0]['pdf']);
    }
}