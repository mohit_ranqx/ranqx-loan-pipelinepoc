<?php declare(strict_types=1);

namespace App\PayLoad;

use App\Entity\Applicant;
use App\Entity\DecisionPolicyResult;
use App\Service\EquiFaxDataQuery;
use Doctrine\ORM\EntityManagerInterface;
use App\TypeTrait\Utc2NZ;

class PayLoadApplicationDetails
{
    use Utc2NZ;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    private $applicant;
    private $equiDataQuery;

    public function __construct(EntityManagerInterface $entity, Applicant $applicant, EquiFaxDataQuery $equiDataQuery)
    {
        $this->entityManager = $entity;
        $this->applicant = $applicant;
        $this->equiDataQuery = $equiDataQuery;
    }

    public function toArray() : array
    {
        $d = $this->convert2NZ(new \DateTime());
        return [
            'EquifaxCompanyName' => $this->equiDataQuery->companyName(),
            'XeroCompanyName' => $this->companyName(),
            'LineOfBusiness' => $this->lineOfBusiness(),
            'ApplicantName' => $this->applicant->getFirstName() . ' ' . $this->applicant->getLastName(),
            'Individuals' => $this->equiDataQuery->directoryNames(),
            'ApplicationID' => $this->applicant->getReferenceNumber(),
            'ApplicationDateTime' => $this->convert2NZ($this->applicant->getCreated())->format('Y-m-d H:i:s'),
            'ApplicantIPAddress' => $this->applicant->getIpAddress(),
            'LendingDecision' => $this->lendingDecision(),
            'DecisionEmailDateTime' => $d->format('Y-m-d H:i:s'),
            'PayloadCreatedDateTime' => $d->format('Y-m-d H:i:s'),
            'LoanAmount' => \number_format( (double)$this->applicant->getLoanAmount(), 2),
            'standardScore' => $this->equiDataQuery->standardScore(),
            'advancedScore' => $this->equiDataQuery->advanceScore()
        ];
    }

    public function phone() : string
    {
        return $this->applicant->getPhone();
    }

    private function companyName(): string
    {
        if ( $this->applicant->getOrganisation())  {
            return $this->applicant->getOrganisation()->getName();
        }
        return '';
    }

    private function lineOfBusiness(): string
    {
        if ( $this->applicant->getOrganisation())  {
            return $this->applicant->getOrganisation()->getLineOfBusiness();
        }
        return '';
    }

    private function lendingDecision(): string
    {
        $decision = $this->entityManager->getRepository(DecisionPolicyResult::class)
            ->findOneBy(['applicantId'=> $this->applicant->getId()]);
        /** @var DecisionPolicyResult $decision ***/
        if ($decision) {
            return $decision->getFinalDecision();
        }
        return '';
    }
}