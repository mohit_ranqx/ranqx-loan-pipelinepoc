<?php declare(strict_types = 1);

namespace App\PayLoad;

/**
 * Class payLodEntityFinancialRatios
 * @package App\payLod
 */

class PayLoadEntityFinancialRatios
{

    /**
     * @return mixed
     */
    public function entityFinancialRatios() {
        $this->Entity[] = array(
            "financialRatios" =>
                array(
                    "TangibleEquityPercentageCurrent" => '',
                    "TangibleEquityPercentagePrevious" => '',
                    "DebtCoverRatio" => '',
                    "InterestCoverRatio" => '',
                    "EBITPercentageCurrent" => '',
                    "EBITPercentagePrevious" => '',
                    "CurrentRatioCurrent" => '',
                    "CurrentRationPrevious" => '',
                    "GrossProfitPercentageCurrent" => '',
                    "GrossProfitPercentagePrevious" => '',
                    "NPBTCurrent" => '',
                    "NBPTPrevious" => '',
                    "WorkingCapitalRatioCurrent" => '',
                    "WorkingCapitalRatioPrevious" => '',
                    "QuasiEquity" => '',
                    "SalesDiveidedByRevenueGrowth" => '',
                    "PercentageChangeinGrossMargin" => '',
                    "PercentageChangeinCurrentRatio" => ''
                )
        );
        return $this->Entity;

    }
}