<?php declare(strict_types = 1);

namespace App\PayLoad;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PayLoadEquifaxData
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var array
     */
    private $equifaxMapedData;

    /**
     * @var string
     */
    private $registeredAddress;

    /**
     * @var string
     */
    private $businessAddress;

    /**
     * @var string
     */
    private $PostalAddress;

    /**
     * @var string
     */
    private $ThirdParty;


    /**
     * RanqxDataHandler constructor.
     * @param ObjectManager $entityManager
     */

    public function __construct(EntityManagerInterface $entity)
    {
        $this->entityManager=$entity;
        $this->equifaxMapedData=array();
        $this->registeredAddress='';
        $this->businessAddress='';
        $this->PostalAddress='';
        $this->ThirdParty='';

    }

    /**
     * @param $orgID
     * @return array
     */
    public function equifaxJsonMapEntity($orgID) {
        $equifaxMap = $this->equifaxRawJson($orgID);
        $equifaxJsonData=$this->equifaxJsonData($orgID);
        $eqJsonData=$equifaxMap['equiFax']['db']['companyDetails']['data']['body']['addressDetails'] ?? NULL;

        if ($eqJsonData!=NULL) {
            foreach ($eqJsonData as $key => $address) {
                if ($address['typeName'] == 'Registered Address') {
                    $this->registeredAddress = $address['address'];
                }
                if ($address['typeName'] == 'Address for Service') {
                    $this->businessAddress = $address['address'];
                }
                if ($address['typeName'] == 'Address for Postal') {
                    $this->PostalAddress = $address['address'];
                }
                if ($address['typeName'] == 'Address for Third Party') {
                    $this->ThirdParty = $address['address'];
                }
            }
        }

        if (isset($equifaxMap['equiFax']['decision']['companyDetails']['ageOfCompany'])) {
            $entityCreditAgeOfBusiness=date("d-m-Y", strtotime($equifaxMap['equiFax']['decision']['companyDetails']['ageOfCompany']));
        }else{
            $entityCreditAgeOfBusiness=NULL;
        }
        $this->equifaxMapedData = array (
            "EntityDetailsNZCN" => $equifaxMap['equiFax']['db']['companyDetails']['data']['body']['businessDetails']['organisationNumber'] ?? NULL,
            "EntityDetailsANZSICCode" => $equifaxMap['equiFax']['db']['companyDetails']['data']['body']['businessDetails']['industry']['code'] ?? NULL,
            "EntityDetailsANZSICDescription" => $equifaxMap['equiFax']['db']['companyDetails']['data']['body']['businessDetails']['industry']['description'] ?? NULL,
            "EntityAddressRegisteredAddress" => $this->registeredAddress ?? NULL,
            "EntityAddressBusinessAddress" => $this->businessAddress ?? NULL,
            "EntityAddressPostalAddress" => $this->PostalAddress ?? NULL,
            "EntityAddressThirdPartyAddress" => $this->ThirdParty ?? NULL,
            "EntityCreditAgeOfBusiness" => $entityCreditAgeOfBusiness,
            "EntityAgeOfBureauFile" => $equifaxMap['equiFax']['db']['creditActivitySummary']['data']['body']['ageOfCreditFile'] ?? NULL,
            "EntityInquiriesinlast2years" => $equifaxMap['equiFax']['decision']['enquiryHistory']['credit_enquiries_24M'] ?? NULL,
            "EntityBureaudefaultsinlast3years" => $equifaxMap['equiFax']['decision']['summary']['judgementsCount'] ?? NULL,
        );

        return $this->equifaxMapedData;
    }

    /**
     * @param int $organisationId
     * @return mixed
     */
    public function equifaxRawJson(int $organisationId) {
        $eqJson=$this->decisionJson($organisationId);
        return (json_decode($eqJson, true));

    }

    /**
     * @param int $organisationId
     * @return mixed
     */
    public function equifaxJsonData(int $organisationId) {
        return ($this->decisionJson($organisationId));
    }

    /**
     * @param int $organisationId
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    private function orgApplicantID(int $organisationId) {
        $org = "SELECT applicant_id FROM organisation WHERE id = " . $organisationId." LIMIT 1";
        $statement = $this->entityManager->getConnection()->query($org);
        $orgQuerry = $statement->fetchAll();
        return($orgQuerry[0]['applicant_id']);
    }

    /**
     * @param int $organisationId
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    private function decisionJson(int $organisationId) {
        $appID=$this->orgApplicantID($organisationId);
        $dj= "SELECT equi_fax_raw FROM Applicant WHERE id = '". $appID ."' LIMIT 1";
        $statement = $this->entityManager->getConnection()->query($dj);
        $djQuerry = $statement->fetchAll();
        return($djQuerry[0]['equi_fax_raw']);
    }
}