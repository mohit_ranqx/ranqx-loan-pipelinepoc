<?php

namespace App\Security;

use App\Entity\User;
use DataDog\DogStatsd;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use App\Security\SecurityTrait;

class TokenAuthenticator extends AbstractGuardAuthenticator
{
    use SecurityTrait;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * TokenAuthenticator constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->statsd = new DogStatsd();
        $this->logger = $logger;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning false will cause this authenticator
     * to be skipped.
     *
     * @param Request $request
     * @return bool
     */
    public function supports(Request $request)
    {
        $part = parse_url($request->getUri());
        if (strpos($part['path'], '/api/doc') === 0) {
            return false;
        }

        if (strpos($part['path'], '/xero/success') === 0) {
            return false;
        }

        if (strpos($part['path'], '/health/check') === 0) {
            return false;
        }
        //
        if (strpos($part['path'], '/v1/weblet/downloader') === 0) {
            return false;
        }
        //
        if (strpos($part['path'], '/v1/weblet/download') === 0) {
            return false;
        }

        return true;
    }

    /**
     * Called on every request. Return whatever credentials you want to
     *
     * @param Request $request
     * @return array|mixed
     */
    public function getCredentials(Request $request)
    {
        return $request;
    }

    /**
     * @param mixed $request
     * @param UserProviderInterface $userProvider
     * @return User|bool|UserInterface
     */
    public function getUser($request, UserProviderInterface $userProvider)
    {
        $hash = $request->headers->get('hash');
        $token = $request->headers->get('token');
        if (!$request || !$token || !$hash) {
            return null;
        }

        if (
            !$this->verifyFileHash($hash) ||
            !$this->verifyIdentity($request) ||
            !$this->verifyToken($token)
        ) {
            return null;
        }

        return new User();
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        // check credentials - e.g. make sure the password is valid
        // no credential check is needed in this case

        // return true to cause authentication success
        return true;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return JsonResponse|Response|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    /**
     * Called when authentication is needed, but it's not sent
     *
     * @param Request $request
     * @param AuthenticationException|null $authException
     * @return JsonResponse|Response
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [
            // you might translate this message
            'message' => 'Authentication Required'
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @return bool
     */
    public function supportsRememberMe()
    {
        return false;
    }
}