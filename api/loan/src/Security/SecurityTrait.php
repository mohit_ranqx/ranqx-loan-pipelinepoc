<?php

namespace App\Security;

use Auth0\SDK\Exception\CoreException;
use Auth0\SDK\Exception\InvalidTokenException;
use Auth0\SDK\JWTVerifier;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Client as GuzzleClient;
use DataDog\DogStatsd;

trait SecurityTrait {
    /**
     * @var string
     */
    protected $hmacAlgorithm = 'sha3-512';
    /**
     * Audience used in the OAuth process
     *
     * @var string
     */
    protected $audience = 'https://ranqx.auth0.com/api/v2/';

    /**
     * Location of the compiled React JS app
     * @var string
     */
    protected $jsPackage = '/opt/ranqx-lo-ui/public/LoanOrigination.js';

    /**
     * Datadog Object
     *
     * @var DogStatsd
     */
    protected $statsd;

    /**
     * Generate identity hash used across the loan origination process.
     * The hash is a combination of client information, concatenated and
     * hmac together.
     * This hash is checked against one in a session to confirm there has
     * been no suspicious identify change.
     *
     * @param Request $request
     * @return boolean
     */
    public function verifyIdentity(Request $request)
    {
        $headers = $request->server->getHeaders();

        $identityString = sprintf('%-%-%-%-%',
            $headers['ACCEPT_LANGUAGE'],
            $headers['ACCEPT_ENCODING'],
            $headers['ACCEPT'],
            $headers['USER_AGENT'],
            $request->getClientIp()
        );

        $identityHash = hash_hmac($this->hmacAlgorithm, $identityString, '');

        if (!$request->getSession()->get('identityHash')) {
            $request->getSession()->set('identityHash', $identityHash);
        }

        $sessionHash = $request->getSession()->get('identityHash');

        if ($identityHash !== $sessionHash) {
            $identityFailedID = 'identity_check_failed';
            $hashFailedMessage = 'Failed to match identity hash';
            $this->statsd->increment($identityFailedID);
            $this->statsd->event($identityFailedID,
                array(
                    'title' => $hashFailedMessage,
                    'text' => json_encode([
                        'stored_hash' => $sessionHash,
                        'provided_hash' => $identityHash,
                    ]),
                    'alert_type' => 'error'
                )
            );
            $this->logger->error($hashFailedMessage);

            return false;
        }

        return true;
    }
    /**
     * Checks that the file hash in request matches our file on server
     * @param $fileHash
     * @return bool
     */
    public function verifyFileHash($fileHash)
    {
        $storedFileHash = hash_file($this->hmacAlgorithm, $this->jsPackage);

        if ($storedFileHash !== $fileHash) {
            $hashFailedID = 'file_check_failed';
            $fileCheckFailedMessage = 'Failed to match file hash';
            $this->statsd->increment($hashFailedID);
            $this->statsd->event($hashFailedID,
                array(
                    'title' => $fileCheckFailedMessage,
                    'text' => json_encode([
                        'stored_hash' => $storedFileHash,
                        'provided_hash' => $fileHash,
                    ]),
                    'alert_type' => 'error'
                )
            );
            $this->logger->error($fileCheckFailedMessage);

            return false;
        }

        return true;
    }

    /**
     * Verify the Auth0 Token
     * @param $token
     * @return bool|mixed
     */
    public function verifyToken($token)
    {
        try {
            $verifier = new JWTVerifier([
                'supported_algs' => ['RS256'],
                'valid_audiences' => [$this->audience],
                'authorized_iss' => [($this->getAuthEnv())['url']]
            ]);

            $tokenInfo = $verifier->verifyAndDecode($token);

            // Check if token expired
            $machineTime = time();
            if ($tokenInfo->exp < $machineTime) {
                throw new \Exception(sprintf('The token has expired. Expires: %, Machine Time: %',
                    $tokenInfo->exp,
                    $machineTime
                ));
            }

            // Token grant incorrect
            if ($tokenInfo->scope != 'read:client_grants' &&
                $tokenInfo->gty != 'client-credentials'
            ) {
                throw new \Exception(sprintf('Incorrect token grants. Scope: %, GTY: %',
                    $tokenInfo->scope,
                    $tokenInfo->gty
                ));
            }

            return $tokenInfo;
        } catch (InvalidTokenException | CoreException | \Exception $error) {
            $validationFailedID = 'token_validation_failed';
            $validationFailedMessage = 'Failed to validate an Auth0 token';
            $this->statsd->increment($validationFailedID);
            $this->statsd->event($validationFailedID,
                array(
                    'title' => $validationFailedMessage,
                    'text' => json_encode($error),
                    'alert_type' => 'error'
                )
            );
            $this->logger->critical($validationFailedMessage, [
                'cause' => json_encode($error),
            ]);
        }

        return false;
    }

    public function getAuthEnv(): array
    {
        $url = $_ENV['AUTH0_URL'] ?? ( $_SERVER['AUTH0_URL'] ?? getenv('AUTH0_URL'));
        $clientId = $_ENV['AUTH0_CLIENT_ID'] ?? ( $_SERVER['AUTH0_CLIENT_ID'] ?? getenv('AUTH0_CLIENT_ID'));
        $clientSecret = $_ENV['AUTH0_CLIENT_SECRET'] ?? ( $_SERVER['AUTH0_CLIENT_SECRET'] ?? getenv('AUTH0_CLIENT_SECRET'));

        return [
            'url' => $url,
            'clientId' =>$clientId,
            'clientSecret' => $clientSecret
        ];
    }

    private function getAccessToken()
    {
        $authFields = [
            'client_id' => ($this->getAuthEnv())['clientId'],
            'client_secret' => ($this->getAuthEnv())['clientSecret'],
            'audience' => $this->audience,
            'grant_type' => 'client_credentials',
        ];

        $client = new GuzzleClient();

        try {
            $response = $client->request('POST', ($this->getAuthEnv())['url'] . 'oauth/token', [
                'headers' => [
                    'content-type' => 'application/json'
                ],
                'json' => $authFields,
            ]);

            if ($response->getStatusCode() !== 200) {
                $statusFailedID = 'auth_token_status_failed';
                $this->statsd->increment($statusFailedID);
                $this->statsd->event($statusFailedID,
                    array(
                        'title' => 'Auth0 returned a response code other than 200. Token failed to generate',
                        'text' => json_encode($response),
                        'alert_type' => 'error'
                    )
                );

                return false;
            }

            $this->statsd->increment('auth_token_generated');

            return $response->getBody();
        } catch (GuzzleException $event) {
            $callFailedID = 'auth_token_call_failed';
            $this->statsd->increment($callFailedID);
            $this->statsd->event($callFailedID,
                array(
                    'title' => 'Failed to call Auth0 API to generate token',
                    'text' => json_encode($event),
                    'priority' => 'normal',
                    'alert_type' => 'error',
                )
            );
        }

        return false;
    }
}