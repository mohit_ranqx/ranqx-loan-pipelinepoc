<?php


namespace App\Xero;

use App\OAuth\NonceTime;
use App\OAuth\OAuth1ConfigInterface;
use App\OAuth\XeroAccessToken;
use function strtolower;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class XeroAPIConfig
 * @package App\Xero
 */
class XeroAPIConfig implements OAuth1ConfigInterface, SignaturePayloadInterface
{
    /**
    * @var ParameterBagInterface
    */
    private $parames;
    /**
     * @var NonceTime
     */
    private $nonceTime;

    /**
     * @var array
     */
    private $extra;

    /**
     * @var XeroAccessToken
     */
    private $token;


    /**
     * XeroAPIConfig constructor.
     * @param ParameterBagInterface $params
     * @param NonceTime $nonceTime
     * @param XeroAccessToken $token
     */
    public function __construct(ParameterBagInterface $params, NonceTime $nonceTime, XeroAccessToken $token)
    {
        $this->parames = $params;
        $this->nonceTime = $nonceTime;
        $this->token = $token;
        $this->extra = [];
    }

    /**
     * @return array
     */
    public function payload(): array
    {
        $temp['oauth_consumer_key'] = $this->consumerKey();
        $temp['oauth_nonce'] = $this->getNonce();
        $temp['oauth_signature_method'] = $this->algo();
        $temp['oauth_timestamp'] = $this->getTime();
        $temp['oauth_token'] = $this->token->token();
        $temp['oauth_version'] = $this->oauthVersion();
        $temp = array_merge($temp, $this->extra);
        ksort($temp);
        return $temp;
    }


    /**
     * @return array
     */
    public function parameters() :array
    {
        $temp = $this->payload();
        $result = array_merge($temp, $this->extra);

        ksort($result);
        return $result;
    }


    /**
     * @param string $key
     * @param string $value
     */
    public function addExtra(string $key, string $value): void
    {
        $this->extra[$key] = $value;
    }

    /**
     * @return string
     */
    public function callback(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getNonce(): string
    {
        return $this->nonceTime->getNonce();
    }

    /**
     * @return int
     */
    public function getTime(): int
    {
        return $this->nonceTime->getTime();
    }

    /**
     * @return string
     */
    public function algo() : string
    {
        $type = $this->parames->get('xero.application_type');
        return $this->parames->get('xero.signature.method.' . strtolower($type));
    }

    /**
     * @return string
     */
    public function consumerKey(): string
    {
        return $this->parames->get('xero.consumer_key');
    }

    /**
     * @return string
     */
    public function oauthVersion(): string
    {
        return $this->parames->get('xero.oauth.version');
    }

    /**
     * @return string
     */
    public function requestTokenUri(): string
    {
        return $this->parames->get('xero.request_token_url');
    }

    /**
     * @return string
     */
    public function authorizationUri(): string
    {
        return $this->parames->get('xero.authorization_url');
    }

    /**
     * @return string
     */
    public function accessTokenUri(): string
    {
        return $this->parames->get('xero.access_token_url');
    }

    /**
     * @return string
     */
    public function getPublicKey(): string
    {
        return $this->parames->get('xero.rsa_public_key');
    }

    /**
     * @return string
     */
    public function getPrivateKey(): string
    {
        return $this->parames->get('xero.rsa_private_key');
    }

    /**
     * @return string
     */
    public function apiUri(): string
    {
        return $this->parames->get('xero.api_uri');
    }
}
