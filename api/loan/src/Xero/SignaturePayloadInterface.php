<?php


namespace App\Xero;

interface SignaturePayloadInterface
{
    public function payload(): array;
}
