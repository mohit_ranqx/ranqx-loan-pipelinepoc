<?php declare(strict_types=1);

namespace App\Xero;

use App\Entity\FinanceInvoice;
use App\Entity\Organisation;
use App\OAuth\OAuth1ConfigInterface;
use App\OAuth\XeroOAuthSignRSASHA1;
use App\OAuth\XeroRequestTokenEncodeInterface;
use Nette\Utils\DateTime;

use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use function GuzzleHttp\Psr7\str;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class XeroPartnerInvoice
 * @package App\Xero
 */
class XeroPartnerInvoice
{

    /**
     * @var OAuth1ConfigInterface
     */
    private $para;
    /**
     * @var XeroRequestTokenEncodeInterface
     */
    private $encode;

    /**
     * XeroPartnerInvoice constructor.
     * @param XeroRequestTokenEncodeInterface $encode
     * @param OAuth1ConfigInterface $para
     */
    public function __construct(XeroRequestTokenEncodeInterface $encode, OAuth1ConfigInterface $para)
    {
        $this->encode = $encode;
        $this->para = $para;
    }

    /**
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function run() : ResponseInterface
    {

        $sign = new XeroOAuthSignRSASHA1($this->para, $this->encode);
        $query = $sign->sign();
        $client = new Client();
        return $client->request('GET', $this->para->apiUri().'Invoices'.'?'.$query);
    }
}
