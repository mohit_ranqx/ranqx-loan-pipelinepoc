<?php


namespace App\Xero;

use App\Map\RanqxMapBalanceSheet;

/**
 * Class BalanceData
 * @package App\Xero
 */
class BalanceData
{
    /**
     * @var array
     */
    private $account;
    /**
     * @var RanqxMapBalanceSheet
     */
    private $balance;

    private $monthIndex;

    /**
     * BalanceData constructor.
     * @param array $map
     * @param RanqxMapBalanceSheet $balance
     */
    public function __construct(array $map, RanqxMapBalanceSheet $balance, int $monthIndex)
    {
        $this->account = $map;
        $this->balance = $balance;
        $this->monthIndex = $monthIndex;
    }

    /**
     * @return array
     */
    public function run(): array
    {
        $balance = $this->balance->toArray();
        return $this->map($balance, $this->account);
    }
    /**
     * @param array $balanceSheetsRaw
     * @param array $accounts
     * @return array
     */
    private function map(array $balanceSheetsRaw, array $accounts) : array
    {
        $lines = [];
        foreach (@$balanceSheetsRaw['Rows']['Row'] as $balanceSheetSection) {
            if (isset($balanceSheetSection['RowType']) &&
                $balanceSheetSection['RowType'] === 'Section' &&
                isset($balanceSheetSection['Rows'])) {
                foreach (@$balanceSheetSection['Rows']['Row'] as $balanceSheetRow) {
                    if (isset($balanceSheetRow['RowType']) &&
                        @$balanceSheetRow['RowType'] === 'Row' && ($cells = @$balanceSheetRow['Cells']['Cell'])) {
                        $lines[] = [
                            'id'       => @$cells[0]['Attributes']['Attribute']['Value'],
                            'name'     => @$cells[0]['Value'],
                            'category' => 'bs',
                            'value'    => (float)@$cells[$this->monthIndex]['Value'],
                            'account'  => $accounts[@$cells[0]['Attributes']['Attribute']['Value']] ?? []
                        ];
                    }
                }
            }
        }
        return $lines;
    }
}
