<?php


namespace App\Xero;

use App\OAuth\OAuth1ConfigInterface;
use App\OAuth\XeroRequestTokenEncodeInterface;

class XeroAPIEncode implements XeroRequestTokenEncodeInterface
{
    private $para;
    private $payload;
    private $endPoint;

    public function __construct(OAuth1ConfigInterface $parames, SignaturePayloadInterface $payload, $endPoint)
    {
        $this->para = $parames;
        $this->payload = $payload;
        $this->endPoint = $endPoint;
    }

    public function encodeFirst(): string
    {
        return \rawurlencode('GET') . '&';
    }

    public function encodeSecond(): string
    {
        return \rawurlencode($this->para->apiUri() . $this->endPoint). '&';
    }

    public function encodeBase(): string
    {
        $result = $this->payload->payload();
        $temp = [];
        foreach ($result as $key => $value) {
            $temp[] = \rawurlencode($key) . '=' . \rawurlencode((string)$value);
        }
        return implode('&', $temp);
    }

}