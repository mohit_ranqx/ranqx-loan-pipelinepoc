<?php


namespace App\Xero;

use App\OAuth\OAuth1ConfigInterface;
use App\OAuth\XeroOAuthSignRSASHA1;
use App\OAuth\XeroRequestTokenEncodeInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class XeroPartnerProfitAndLoss
 * @package App\Xero
 */
class XeroPartnerProfitAndLoss
{
    /**
     * @var OAuth1ConfigInterface
     */
    private $para;
    /**
     * @var XeroRequestTokenEncodeInterface
     */
    private $encode;

    /**
     * XeroPartnerProfitAndLoss constructor.
     * @param XeroRequestTokenEncodeInterface $encode
     * @param OAuth1ConfigInterface $para
     */
    public function __construct(XeroRequestTokenEncodeInterface $encode, OAuth1ConfigInterface $para)
    {
        $this->encode = $encode;
        $this->para = $para;
    }


    /**
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function run() : ResponseInterface
    {
        $sign = new XeroOAuthSignRSASHA1($this->para, $this->encode);
        $query = $sign->sign();
        $client = new Client();
        $res = $client->request('GET', $this->para->apiUri().'Reports/ProfitAndLoss'.'?'.$query);
        return $res;
    }
}
