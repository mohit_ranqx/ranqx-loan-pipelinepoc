<?php


namespace App\Xero;

use App\Map\RanqxMapBalanceSheet;

/**
 * Class MergeData
 * @package App\Xero
 */
class MergeData
{
    /**
     * @var array
     */
    private $account;
    /**
     * @var RanqxMapBalanceSheet
     */
    private $balance;
    /**
     * @var RanqxMapBalanceSheet
     */
    private $profit;

    /**
     * MergeData constructor.
     * @param array $acc
     * @param RanqxMapBalanceSheet $balance
     * @param RanqxMapBalanceSheet $profit
     */
    public function __construct(array $acc, RanqxMapBalanceSheet $balance, RanqxMapBalanceSheet $profit)
    {
        $this->account = $acc;
        $this->balance = $balance;
        $this->profit = $profit;
    }

    /**
     * @return array
     */
    public function merge(int $monthIndex)
    {
        $data = new BalanceData(
            $this->account,
            $this->balance,
            $monthIndex
        );
        $bs = $data->run();

        $profit = new ProfitAndLossData(
            $this->account,
            $this->profit,
            $monthIndex
        );

        $income = $profit->run();


        $lines = array_merge($bs, $income);
        return  [
            [
                'Lines' => $lines,
            ],
        ];
    }
}
