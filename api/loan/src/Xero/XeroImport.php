<?php


namespace App\Xero;

use App\Entity\Organisation;
use App\OAuth\OAuthFixedNonceTime;
use App\OAuth\XeroAccessToken;
use DateTime;
//use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use \Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class XeroImport
 * @package App\Xero
 */
class XeroImport
{
    /**
     * @var ParameterBagInterface
     */
    private $parames;

    /**
     * XeroImport constructor.
     * @param ParameterBagInterface $params
     */
    public function __construct(ParameterBagInterface $params)
    {
        $this->parames = $params;
    }

    /**
     * @param XeroAccessToken $token
     * @return ResponseInterface
     */
    public function organisation(XeroAccessToken $token) : ResponseInterface
    {
        $apiPara = new XeroAPIConfig($this->parames, new OAuthFixedNonceTime(), $token);
        $encode = new XeroAPIEncode($apiPara, $apiPara, 'Organisations');
        $org = new XeroPartnerOganisation($encode, $apiPara);
        return $org->run();
    }

    /**
     * @param XeroAccessToken $token
     * @return ResponseInterface
     */
    public function accounts(XeroAccessToken $token) : ResponseInterface
    {
        $apiPara = new XeroAPIConfig($this->parames, new OAuthFixedNonceTime(), $token);
        $encode = new XeroAPIEncode($apiPara, $apiPara, 'Accounts');
        $account = new XeroPartnerAccount($encode, $apiPara);
        return $account->run();
    }

    /**
     * @param DateTime $fromDate
     * @param DateTime $toDate
     * @param XeroAccessToken $token
     * @return ResponseInterface
     */
    public function loadBalanceSheets(?DateTime $fromDate, DateTime $toDate, XeroAccessToken $token) : ResponseInterface
    {
        $apiPara = new XeroAPIConfig($this->parames, new OAuthFixedNonceTime(), $token);
        $apiPara->addExtra('date', $toDate->format('Y-m-d'));
        $apiPara->addExtra('periods', '11');
        $apiPara->addExtra('timeframe', 'MONTH');
        $apiPara->addExtra('standardLayout', 'true');
        $encode = new XeroAPIEncode($apiPara, $apiPara, 'Reports/BalanceSheet');
        $bs = new XeroPartnerBalanceSheet($encode, $apiPara);
        return $bs->run();
    }

    /**
     * @param DateTime $fromDate
     * @param DateTime $toDate
     * @param XeroAccessToken $token
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function loadProfitAndLoss(DateTime $fromDate, DateTime $toDate, XeroAccessToken $token) : ResponseInterface
    {
        $apiPara = new XeroAPIConfig($this->parames, new OAuthFixedNonceTime(), $token);
        $apiPara->addExtra('fromDate', $fromDate->format('Y-m-d'));
        $apiPara->addExtra('toDate', $toDate->format('Y-m-d'));
        $apiPara->addExtra('periods', '11');
        $apiPara->addExtra('timeframe', 'MONTH');
        $apiPara->addExtra('standardLayout', 'true');

        $encode = new XeroAPIEncode($apiPara, $apiPara, 'Reports/ProfitAndLoss');
        $pl = new XeroPartnerProfitAndLoss($encode, $apiPara);
        return $pl->run();
    }


    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param XeroAccessToken $token
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function loadInvoice(DateTime $from, DateTime $to, XeroAccessToken $token) : ResponseInterface
    {
        $apiPara = new XeroAPIConfig($this->parames, new OAuthFixedNonceTime(), $token);
        $apiPara->addExtra('standardLayout', 'true');

        $whereStr = 'Date >= DateTime(' . $from->format('Y');
        $whereStr .= ', '. $from->format('m') . ','  . $from->format('d') . ')';
        $whereStr .= '&&Date <= DateTime(' . $to->format('Y');
        $whereStr .= ', '. $to->format('m') . ','  . $to->format('d') . ')';
        $whereStr .= '&&type=="ACCREC"';
        $apiPara->addExtra('where', $whereStr);

        $encode = new XeroAPIEncode($apiPara, $apiPara, 'Invoices');
        $pl = new XeroPartnerInvoice($encode, $apiPara);
        return $pl->run();
    }

}