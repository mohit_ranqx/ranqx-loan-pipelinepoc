<?php


namespace App\Xero;

use App\OAuth\XeroAccessToken;
use App\OAuth\OAuth1ConfigInterface;
use App\OAuth\XeroOAuthSignRSASHA1;
use App\OAuth\XeroRequestTokenEncodeInterface;
use GuzzleHttp\Client;
use \Psr\Http\Message\ResponseInterface;

class XeroPartnerAccount
{
    private $para;
    private $encode;
    public function __construct(XeroRequestTokenEncodeInterface $encode, OAuth1ConfigInterface $para)
    {
        $this->encode = $encode;
        $this->para = $para;
    }

    public function run() : ResponseInterface
    {
        $sign = new XeroOAuthSignRSASHA1($this->para, $this->encode);
        $query = $sign->sign();

        $client = new Client();
        $res = $client->request("GET", $this->para->apiUri().'Accounts'.'?'.$query);
        return $res;
    }
}