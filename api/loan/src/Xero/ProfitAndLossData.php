<?php


namespace App\Xero;

use App\Map\RanqxMapBalanceSheet;

/**
 * Class ProfitAndLossData
 * @package App\Xero
 */
class ProfitAndLossData
{
    /**
     * @var array
     */
    private $account;
    /**
     * @var RanqxMapBalanceSheet
     */
    private $profit;

    private $monthIndex;

    /**
     * ProfitAndLossData constructor.
     * @param array $map
     * @param RanqxMapBalanceSheet $balance
     */
    public function __construct(array $map, RanqxMapBalanceSheet $balance, int $monthIndex)
    {
        ////profile and loss format is same as balance sheet.
        $this->account = $map;
        $this->profit = $balance;
        $this->monthIndex = $monthIndex;
    }

    /**
     * @return array
     */
    public function run()
    {
        $balance = $this->profit->toArray();
        return $this->map($balance, $this->account);
    }

    /**
     * @param array $incomeStatementsRaw
     * @param array $accounts
     * @return array
     */
    private function map(array $incomeStatementsRaw, array $accounts): array
    {
        $lines = [];
        foreach (@$incomeStatementsRaw['Rows']['Row'] as $incomeStatementSection) {
            if (isset($incomeStatementSection['RowType']) &&
                @$incomeStatementSection['RowType'] === 'Section' &&
                isset($incomeStatementSection['Rows'])) {
                foreach (@$incomeStatementSection['Rows']['Row'] as $incomeStatementRow) {
                    if (isset($incomeStatementRow['RowType']) &&
                        $incomeStatementRow['RowType'] === 'Row' && ($cells = @$incomeStatementRow['Cells']['Cell'])) {
                        $lines[] = [
                            'id'       => @$cells[0]['Attributes']['Attribute']['Value'],
                            'name'     => @$cells[0]['Value'],
                            'category' => 'is',
                            'value'    => (float)@$cells[$this->monthIndex]['Value'],
                            'account'  => @$accounts[@$cells[0]['Attributes']['Attribute']['Value']],
                        ];
                    }
                }
            }
        }

        return $lines;
    }
}
