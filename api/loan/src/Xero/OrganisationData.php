<?php


namespace App\Xero;

use App\Entity\Address;
use DateTime;
use Exception;
use Psr\Http\Message\ResponseInterface;
use App\Entity\Organisation;
use function simplexml_load_string;

/**
 * Class OrganisationData
 * @package App\Xero
 */
class OrganisationData
{
    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * OrganisationData constructor.
     * @param ResponseInterface $response
     */
    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    /**
     * @return Organisation
     * @throws Exception
     */
    public function toEntity() : Organisation
    {
        $org = new Organisation();
        $org->setCreated(new \DateTime());
        $data = $this->response->getBody()->getContents();
        $xml = simplexml_load_string($data);
        if ($xml->Organisations->count() > 0) {
            $org->setProviderName($xml->ProviderName->__toString());

            $xmlOrg = ($xml->Organisations[0])->Organisation;

            $org->setName($xmlOrg->Name ? $xmlOrg->Name->__toString() : '');
            $org->setLegalName($xmlOrg->LegalName ? $xmlOrg->LegalName->__toString() : '');
            $tax = $xmlOrg->PaysTax ? $xmlOrg->PaysTax->__toString() : '';
            $org->setPaysTax(strtolower($tax) === 'true');
            $org->setVersion($xmlOrg->Version ? $xmlOrg->Version->__toString() : '');
            $org->setOrganisationType($xmlOrg->OrganisationType ? $xmlOrg->OrganisationType->__toString() : '');
            $org->setBaseCurrency($xmlOrg->BaseCurrency ? $xmlOrg->BaseCurrency->__toString() : '');
            $org->setCountryCode($xmlOrg->CountryCode ? $xmlOrg->CountryCode->__toString() : '');
            $demo = $xmlOrg->IsDemoCompany ? $xmlOrg->IsDemoCompany->__toString() : '';
            $org->setIsDemoCompany(strtolower($demo) === 'true');
            $org->setOrganisationStatus($xmlOrg->OrganisationStatus ? $xmlOrg->OrganisationStatus->__toString() : '');
            $org->setFinancialYearEndDay($xmlOrg->FinancialYearEndDay ? (int)$xmlOrg->FinancialYearEndDay->__toString() : 0);
            $org->setFinancialYearEndMonth($xmlOrg->FinancialYearEndMonth ? (int)$xmlOrg->FinancialYearEndMonth->__toString() : 0);
            $org->setSalesTaxBasis($xmlOrg->SalesTaxBasis ? $xmlOrg->SalesTaxBasis->__toString() : '');
            $org->setDefaultSalesTax($xmlOrg->DefaultSalesTax ? $xmlOrg->DefaultSalesTax->__toString() : '');
            $org->setDefaultPurchasesTax($xmlOrg->DefaultPurchasesTax ? $xmlOrg->DefaultPurchasesTax->__toString() : '');
            $org->setCreatedInXero($xmlOrg->CreatedDateUTC ? new DateTime($xmlOrg->CreatedDateUTC->__toString()) : new \DateTime());
            $org->setOrganisationEntityType($xmlOrg->OrganisationEntityType ? $xmlOrg->OrganisationEntityType->__toString() : '');
            //OrganisationID
            $org->setOrganisationID($xmlOrg->OrganisationID ? $xmlOrg->OrganisationID->__toString() : '');
            $org->setEdition($xmlOrg->Edition ? $xmlOrg->Edition->__toString() : '');
            $org->setXeroClass($xmlOrg->Edition ? $xmlOrg->Class->__toString() : '');
            $org->setTaxNumber($xmlOrg->TaxNumber ? $xmlOrg->TaxNumber->__toString() : '');
            $org->setLineOfBusiness($xmlOrg->LineOfBusiness ? (string)$xmlOrg->LineOfBusiness : '');

            $address = new Address();
            $address->setAddressLine1('');
            $address->setAddressLine2('');
            $address->setAddressType('');
            $address->setCity('');
            $address->setCountry('');
            $address->setPostalCode('');


            if ($xmlOrg->Addresses) {
                $add = $xmlOrg->Addresses->Address;
                if ($add) {
                    $address->setAddressLine1($add->AddressLine1 ? $add->AddressLine1->__toString() : '');
                    $address->setAddressLine2($add->AddressLine2 ? $add->AddressLine2->__toString() : '');
                    $address->setAddressType($add->AddressType ? $add->AddressType->__toString() : '');
                    $address->setCity($add->City ? $add->City->__toString() : '');
                    $address->setCountry($add->Country ? $add->Country->__toString() : '');
                    $address->setPostalCode($add->PostalCode ? $add->PostalCode->__toString() : '');
                }
            }

            $org->setAddress($address);
        }

        return $org;
    }
}
