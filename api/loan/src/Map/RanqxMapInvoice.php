<?php declare(strict_types = 1);


namespace App\Map;

use Psr\Http\Message\ResponseInterface;

/**
 * Class RanqxMapInvoice
 * @package App\Map
 */
class RanqxMapInvoice
{
    private $res;
    public function __construct(ResponseInterface $response)
    {
        $this->res = $response;
    }

    public function toArray() : array
    {
        $invoices = XeroRanqxMap::parseToArray($this->res);

        $results = [];
        $invoices = $invoices['Invoices']['Invoice'] ?? [];
        foreach ($invoices as $invoice) {
            $results[$invoice['InvoiceID']] = $invoice;
        }

        return $results;
    }
}