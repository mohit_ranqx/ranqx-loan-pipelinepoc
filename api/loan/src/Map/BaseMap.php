<?php

namespace App\Map;

use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\AbstractFinancePeriod;
use App\Entity\LedgerAccount;
use App\Enumerable\BalanceSheetAccountTypes;
use App\Enumerable\IncomeStatementAccountTypes;
use Exception;
use ReflectionClass;
use ReflectionException;

/**
 * Class BaseMap
 *
 * @package AppMap
 */
abstract class BaseMap
{

    /**
     * @param AbstractFinancePeriod $finance
     * @param ArrayCollection $newLedgerAccounts
     * @param AbstractFinancePeriod|null $templateFinanceMap
     * @return AbstractFinancePeriod
     * @throws Exception
     */
    public static function mapFinance(
        AbstractFinancePeriod $finance,
        ArrayCollection $newLedgerAccounts,
        AbstractFinancePeriod $templateFinanceMap = null
    ): AbstractFinancePeriod {

        $data = self::getAccountTypesArray();
        $combinedLedgerAccounts = [];
        $localLedgerAccounts = $finance->getLedgerAccounts();
        $matchedAccounts = [];
        $baseLedgerAccounts = [];
        $otherLabour = null;

        if (!$newLedgerAccounts->isEmpty()) {
            //Add the default accounts
            $otherLabour = new LedgerAccount();
            $otherLabour
                ->setType(IncomeStatementAccountTypes::PEOPLE_OTHER)
                ->setTitle('Adjustments')
                ->setPrice(0)
                ->setId('is-pplo')
                ->setCode('IS-PPLO');

            $newLedgerAccounts[] = $otherLabour;
        }

        if ($templateFinanceMap) {
            $baseLedgerAccountsTemp = $templateFinanceMap->getLedgerAccounts();

            // index template accounts by ID
            foreach ($baseLedgerAccountsTemp as $account) {
                $baseLedgerAccounts[$account->getId()] = $account;
            }
        }

        if (!$localLedgerAccounts->isEmpty() && !$newLedgerAccounts->isEmpty()) {
            //Existing accounts with new remote accounts; combine
            foreach ($newLedgerAccounts as $newLedgerAccount) {
                foreach ($localLedgerAccounts as $index => $ledgerAccount) {
                    if ($newLedgerAccount->getId() === 'is-pplo' && $ledgerAccount->getId() === 'is-pplo') {
                        //In the case of Other people costs, use what's already in Ranqx
                        $newLedgerAccount->setPrice($ledgerAccount->getPrice());
                    }

                    if ($templateFinanceMap && ($baseAccount = @$baseLedgerAccounts[$newLedgerAccount->getId()])) {
                        // just use template mapping
                        $newLedgerAccount
                            ->setType($baseAccount->getType())
                            ->setOldType($baseAccount->getOldType());
                    } else {
                        $existingId = $ledgerAccount->getId() ?: $ledgerAccount->getTitle();
                        $newId = $newLedgerAccount->getId() ?: $newLedgerAccount->getTitle();
                        //Matching item; update new status and use saved type
                        if ($existingId === $newId) {
                            $matchedAccounts[] = $newId;

                            if (!$newLedgerAccount->getOldType()) {
                                //Default account old type to current type
                                $newLedgerAccount->setOldType($newLedgerAccount->getType());
                            }

                            //Ensure only current account type values are used
                            if (array_key_exists($ledgerAccount->getType(), $data)) {
                                //Use local account's type in-case it has moved
                                $newLedgerAccount->setType($ledgerAccount->getType());
                            }

                            unset($localLedgerAccounts[$index]);

                            break;
                        }
                    }
                }

                if (!$newLedgerAccount->getOldType()) {
                    //Default account old type to current type
                    $newLedgerAccount->setOldType($newLedgerAccount->getType());
                }

                if (@array_key_exists($newLedgerAccount->getType(), $data)) {
                    $data[$newLedgerAccount->getType()] += $newLedgerAccount->getPrice();
                }

                $combinedLedgerAccounts[] = $newLedgerAccount;
            }

            foreach ($combinedLedgerAccounts as &$account) {
                if (!\in_array($account->getId(), $matchedAccounts)) {
                    $account = self::assetSignFlip($account);
                } else {
                    $account = self::assetSignFlip($account, false);
                }
            }
        } elseif (!$newLedgerAccounts->isEmpty()) {
            //No existing accounts and new remote accounts; calculate
            foreach ($newLedgerAccounts as $newLedgerAccount) {
                if ($templateFinanceMap && ($baseAccount = @$baseLedgerAccounts[$newLedgerAccount->getId()])) {
                    // just use template mapping
                    $newLedgerAccount
                        ->setType($baseAccount->getType())
                        ->setOldType($baseAccount->getOldType());
                } else {
                    $newLedgerAccount->setOldType($newLedgerAccount->getType());
                }

                $newLedgerAccount = self::assetSignFlip($newLedgerAccount);
                $data[$newLedgerAccount->getType()] += $newLedgerAccount->getPrice();

                $combinedLedgerAccounts[] = $newLedgerAccount;
            }

            $combinedLedgerAccounts[] = $otherLabour;
        } else {
            //No new accounts, just return as-is
            return $finance;
        }

        //Add Current Year Earnings to balance sheet
        $cye = new LedgerAccount();
        $cye
            ->setId('CYE')
            ->setType(BalanceSheetAccountTypes::EQUITY_EARNINGS)
            ->setOldType(BalanceSheetAccountTypes::EQUITY_EARNINGS)
            ->setTitle('Current Year Earnings')
            ->setPrice(
                $data[IncomeStatementAccountTypes::REVENUE] -
                ($data[IncomeStatementAccountTypes::COSTOFGOODS]
                    + $data[IncomeStatementAccountTypes::RENT]
                    + $data[IncomeStatementAccountTypes::PEOPLE]
                    + $data[IncomeStatementAccountTypes::PEOPLE_OTHER]
                    + $data[IncomeStatementAccountTypes::OPERATINGCOSTS]
                    + $data[IncomeStatementAccountTypes::OTHER]
                    + $data[IncomeStatementAccountTypes::OTHER_DEPRECIATION]
                    + $data[IncomeStatementAccountTypes::OTHER_INTEREST]
                + $data[IncomeStatementAccountTypes::OTHER_TAX])
            );
        $combinedLedgerAccounts[] = $cye;

        $data[BalanceSheetAccountTypes::EQUITY_EARNINGS] += $cye->getPrice();

        if ($data[IncomeStatementAccountTypes::REVENUE]
            + $data[IncomeStatementAccountTypes::COSTOFGOODS]
            + $data[IncomeStatementAccountTypes::PEOPLE]
            + $data[IncomeStatementAccountTypes::RENT]
            + $data[IncomeStatementAccountTypes::OPERATINGCOSTS] == 0
        ) {
            foreach ($localLedgerAccounts as $account) {
                if ($account->isIncomeStatement()) {
                    if ($account->isRevenue()) {
                        $account->setTitle('Revenue');
                    } elseif ($account->isCog()) {
                        $account->setTitle('Cost of Goods');
                    } elseif ($account->isPeople()) {
                        $account->setTitle('People/Wages');
                    } elseif ($account->isRent()) {
                        $account->setTitle('Rent/Rates');
                    } elseif ($account->isOperatingCost()) {
                        $account->setTitle('Operating Costs');
                    } else {
                        $account->setTitle('Other');
                    }
                    $combinedLedgerAccounts[] = $account;
                }
            }
        }

        self::setFinanceTotalsFromArray($finance, $data);
        $finance->setLedgerAccounts(new ArrayCollection($combinedLedgerAccounts));
        //get interest
        $price = self::getValueOrNull(IncomeStatementAccountTypes::OTHER_INTEREST, $finance);
        $finance->setInterest($price);
        //get Depreciation
        //setDepreciation
        $value = self::getValueOrNull(IncomeStatementAccountTypes::OTHER_DEPRECIATION, $finance);
        $finance->setDepreciation($value);

        $value = self::getValueOrNull(IncomeStatementAccountTypes::OTHER_TAX, $finance);
        $finance->setTax($value);
        return $finance;
    }

    public static function getPrice($type, AbstractFinancePeriod $finance): float
    {
        $data = $finance->getRawData();
        $map = \json_decode($data,true);

        return array_reduce($map, function ($carry, $item) use ($type) {
            if ($item['type'] === $type) {
                $carry += $item['price'];
            }
            return $carry;
        },0.0);
    }

    public static function isNull($type, AbstractFinancePeriod $finance): bool
    {
        $data = $finance->getRawData();
        $map = \json_decode($data,true);

        return array_reduce($map, function ($carry, $item) use ($type) {
            if ($item['type'] === $type) {
                $carry &= false;
            }
            return $carry;
        },true);
    }

    public static function getValueOrNull($type, AbstractFinancePeriod $finance): ?float
    {
        $isNull = self::isNull($type, $finance);
        if ($isNull) return null;

        return self::getPrice($type, $finance);
    }

    /**
     * @param array $ledgerAccountBalances
     *
     * @return ArrayCollection | LedgerAccount[]
     */
    public static function getFinancialDetails(array $ledgerAccountBalances)
    {
        return static::mapAccountsToItems($ledgerAccountBalances);
    }


    /**
     * @param float|int $code
     * @param array $map
     * @return null|string
     */
    protected static function findTypeByNominalCode($code, $map) :? string
    {
        $nominalCodes = array_keys($map);
        $code = (int)$code;
        //Search for category
        $type = $map[reset($nominalCodes)];
        while (($next = next($nominalCodes)) && $code >= $next) {
            $type = $map[$next];
        }

        return $type;
    }

    /**
     * NOTE : TO BE OVERRIDDEN IN THE CHILD CLASS
     *
     * @param  array $ledgerAccountBalances
     *
     * @return LedgerAccount[]
     */
    protected static function mapAccountsToItems(array $ledgerAccountBalances)
    {
    }

    /**
     * @param string $classification
     * @param string $type
     * @param string $name
     * @return string|null
     */
    protected static function findAccountType($classification, $type, $name) :? string
    {
        return null;
    }


    /**
     * @param LedgerAccount $account
     * @param bool $moveAccount
     * @return LedgerAccount
     * @throws Exception
     */
    private static function assetSignFlip(LedgerAccount $account, $moveAccount = true)
    {
        if ($moveAccount) {
            //Move any negative asset bank accounts to liability
            if (($account->getPrice() < 0) && BalanceSheetAccountTypes::ASSETS_CURRENT_BANK == $account->getType()) {
                $account->setOldType(BalanceSheetAccountTypes::ASSETS_CURRENT_BANK);
                $account->setType(BalanceSheetAccountTypes::LIABILITIES_CURRENT_BANK);
            }
        }

        if (BalanceSheetAccountTypes::isBalanceSheet($account->getType())) {
            //Balance sheet rules
            if (BalanceSheetAccountTypes::isAsset($account->getType()) !== BalanceSheetAccountTypes::isAsset($account->getOldType())) {
                //Account moved between Asset and Liability/Equity; inverse the sign
                $account->setPrice($account->getPrice() * (-1));
            }
        } else {
            //Income statement rules
            if (IncomeStatementAccountTypes::isRevenue($account->getType()) !== IncomeStatementAccountTypes::isRevenue($account->getOldType())) {
                //Account moved between Revenue and Others; inverse the sign
                $account->setPrice($account->getPrice() * (-1));
            }
        }

        return $account;
    }


    /**
     * @return array
     * @throws ReflectionException
     */
    private static function getAccountTypesArray(): array
    {
        $accountTypes = (new ReflectionClass(new BalanceSheetAccountTypes()))->getConstants()
            + (new ReflectionClass(new IncomeStatementAccountTypes()))->getConstants();

        $data = [];

        foreach ($accountTypes as $accountType => $value) {
            if (strpos($accountType, 'LEGACY_') !== 0) {
                $data[$value] = 0;
            }
        }

        return $data;
    }

    /**
     * @param AbstractFinancePeriod $finance
     * @param array                 $accountTypesArray
     */
    private static function setFinanceTotalsFromArray(AbstractFinancePeriod $finance, $accountTypesArray)
    {
        $finance
            ->setTotalRevenue($accountTypesArray[IncomeStatementAccountTypes::REVENUE])
            ->setTotalCog($accountTypesArray[IncomeStatementAccountTypes::COSTOFGOODS])
            ->setTotalPeople($accountTypesArray[IncomeStatementAccountTypes::PEOPLE])
            ->setTotalRent($accountTypesArray[IncomeStatementAccountTypes::RENT])
            ->setTotalOperating($accountTypesArray[IncomeStatementAccountTypes::OPERATINGCOSTS])
            ->setTotalOther(
                $accountTypesArray[IncomeStatementAccountTypes::OTHER]
                + $accountTypesArray[IncomeStatementAccountTypes::OTHER_DEPRECIATION]
                + $accountTypesArray[IncomeStatementAccountTypes::OTHER_INTEREST]
                + $accountTypesArray[IncomeStatementAccountTypes::OTHER_TAX]
            )
            ->setTotalAssetsCurrentBank($accountTypesArray[BalanceSheetAccountTypes::ASSETS_CURRENT_BANK])
            ->setTotalAssetsCurrentDebtors($accountTypesArray[BalanceSheetAccountTypes::ASSETS_CURRENT_DEBTORS])
            ->setTotalAssetsCurrentInventory($accountTypesArray[BalanceSheetAccountTypes::ASSETS_CURRENT_STOCK])
            ->setTotalAssetsCurrentOther($accountTypesArray[BalanceSheetAccountTypes::ASSETS_CURRENT_OTHER])
            ->setTotalAssetsFixed($accountTypesArray[BalanceSheetAccountTypes::ASSETS_FIXED])
            ->setTotalAssetsOther($accountTypesArray[BalanceSheetAccountTypes::ASSETS_OTHER])
            ->setTotalLiabilitiesCurrentBank($accountTypesArray[BalanceSheetAccountTypes::LIABILITIES_CURRENT_BANK])
            ->setTotalLiabilitiesCurrentCreditors($accountTypesArray[BalanceSheetAccountTypes::LIABILITIES_CURRENT_CREDITORS])
            ->setTotalLiabilitiesCurrentOther($accountTypesArray[BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER])
            ->setTotalLiabilitiesOther($accountTypesArray[BalanceSheetAccountTypes::LIABILITIES_OTHER])
            ->setTotalEquityCapital($accountTypesArray[BalanceSheetAccountTypes::EQUITY_CAPITAL])
            ->setTotalEquityEarnings($accountTypesArray[BalanceSheetAccountTypes::EQUITY_EARNINGS]);
    }
}
