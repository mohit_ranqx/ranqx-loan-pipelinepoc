<?php

namespace App\Map;

use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\LedgerAccount;
use App\Enumerable\BalanceSheetAccountTypes;
use App\Enumerable\IncomeStatementAccountTypes;
use Psr\Http\Message\ResponseInterface;

/**
 * Class XeroRanqxMap
 *
 * @package AppMap
 */
abstract class XeroRanqxMap extends BaseMap
{
    /**
     * @var array $REPORT_CODE_MAP
     */
    private static $REPORT_CODE_MAP = [
        'ASS' => [
            0     => BalanceSheetAccountTypes::ASSETS_CURRENT_OTHER,
            'CUR' => [
                0     => BalanceSheetAccountTypes::ASSETS_CURRENT_OTHER,
                'BAN' => BalanceSheetAccountTypes::ASSETS_CURRENT_BANK,
                'CON' => BalanceSheetAccountTypes::ASSETS_CURRENT_OTHER,
                'INY' => BalanceSheetAccountTypes::ASSETS_CURRENT_STOCK,
                'INV' => BalanceSheetAccountTypes::ASSETS_CURRENT_OTHER,
                'DIR' => BalanceSheetAccountTypes::ASSETS_CURRENT_OTHER,
                'REL' => BalanceSheetAccountTypes::ASSETS_CURRENT_OTHER,
                'CAS' => [
                    0 => BalanceSheetAccountTypes::ASSETS_CURRENT_BANK,
                ],
                'REC' => [
                    0     => BalanceSheetAccountTypes::ASSETS_CURRENT_DEBTORS,
                    'PRE' => BalanceSheetAccountTypes::ASSETS_CURRENT_OTHER,
                    'TRA' => BalanceSheetAccountTypes::ASSETS_CURRENT_DEBTORS,
                ],
            ],
            'NCA' => [
                0     => BalanceSheetAccountTypes::ASSETS_FIXED,
                'FIX' => BalanceSheetAccountTypes::ASSETS_FIXED,
                'INT' => BalanceSheetAccountTypes::ASSETS_OTHER,
            ],
        ],
        'EQU' => [
            0     => BalanceSheetAccountTypes::EQUITY_CAPITAL,
            'GIF' => BalanceSheetAccountTypes::EQUITY_CAPITAL,
            'OWN' => BalanceSheetAccountTypes::EQUITY_CAPITAL,
            'PRE' => BalanceSheetAccountTypes::EQUITY_CAPITAL,
            'RES' => BalanceSheetAccountTypes::EQUITY_EARNINGS,
            'RET' => BalanceSheetAccountTypes::EQUITY_EARNINGS,
            'SHA' => BalanceSheetAccountTypes::EQUITY_CAPITAL,
        ],
        'LIA' => [
            0     => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
            'CUR' => [
                0     => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
                'FIN' => BalanceSheetAccountTypes::LIABILITIES_CURRENT_CREDITORS,
                'LOA' => BalanceSheetAccountTypes::LIABILITIES_CURRENT_CREDITORS,
                'PAY' => [
                    0     => BalanceSheetAccountTypes::LIABILITIES_CURRENT_CREDITORS,
                    'DIV' => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
                    'EMP' => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
                    'FBT' => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
                    'PAY' => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
                    'TRA' => BalanceSheetAccountTypes::LIABILITIES_CURRENT_CREDITORS,
                    'UNE' => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
                ],
            ],
            'NCL' => [
                0     => BalanceSheetAccountTypes::LIABILITIES_OTHER,
                'ADV' => [
                    0     => BalanceSheetAccountTypes::EQUITY_CAPITAL,
                    'DRA' => [
                        0 => BalanceSheetAccountTypes::EQUITY_EARNINGS,
                    ],
                ],
            ],
        ],
        'EXP' => [
            0     => IncomeStatementAccountTypes::OPERATINGCOSTS,
            'AMO' => IncomeStatementAccountTypes::OTHER_DEPRECIATION,
            'COM' => IncomeStatementAccountTypes::PEOPLE,
            'COS' => IncomeStatementAccountTypes::COSTOFGOODS,
            'DEP' => IncomeStatementAccountTypes::OTHER_DEPRECIATION,
            'DIR' => IncomeStatementAccountTypes::PEOPLE,
            'FOR' => IncomeStatementAccountTypes::OPERATINGCOSTS,
            'TAX' => IncomeStatementAccountTypes::OTHER_TAX,
            'INT' => IncomeStatementAccountTypes::OTHER_INTEREST,
            'LEG' => IncomeStatementAccountTypes::OPERATINGCOSTS,
            'RAT' => IncomeStatementAccountTypes::RENT,
            'REN' => IncomeStatementAccountTypes::RENT,
            'TRA' => IncomeStatementAccountTypes::OPERATINGCOSTS,
            'WAG' => IncomeStatementAccountTypes::PEOPLE,
            'WAH' => IncomeStatementAccountTypes::PEOPLE,
        ],
        'REV' => [
            0 => IncomeStatementAccountTypes::REVENUE,
        ],
    ];

    /**
     * @var array $ACCOUNT_TYPE_MAP
     */
    private static $ACCOUNT_TYPE_MAP = [
        'BANK'                    => [
            0 => BalanceSheetAccountTypes::ASSETS_CURRENT_BANK,
        ],
        'CURRENT'                 => [
            0                  => BalanceSheetAccountTypes::ASSETS_CURRENT_OTHER,
            'INVENTORY'        => BalanceSheetAccountTypes::ASSETS_CURRENT_STOCK,
            'STOCK'            => BalanceSheetAccountTypes::ASSETS_CURRENT_STOCK,
            'WORK IN PROGRESS' => BalanceSheetAccountTypes::ASSETS_CURRENT_STOCK,
            'WIP'              => BalanceSheetAccountTypes::ASSETS_CURRENT_STOCK,
        ],
        'PREPAYMENT'              => [
            0 => BalanceSheetAccountTypes::ASSETS_CURRENT_OTHER,
        ],
        'INVENTORY'               => [
            0 => BalanceSheetAccountTypes::ASSETS_CURRENT_STOCK,
        ],
        'FIXED'                   => [
            0 => BalanceSheetAccountTypes::ASSETS_FIXED,
        ],
        'NONCURRENT'              => [
            0 => BalanceSheetAccountTypes::ASSETS_OTHER,
        ],
        'EQUITY'                  => [
            0          => BalanceSheetAccountTypes::EQUITY_CAPITAL,
            'EARNINGS' => BalanceSheetAccountTypes::EQUITY_EARNINGS,
            'RETAINED' => BalanceSheetAccountTypes::EQUITY_EARNINGS,
            'DRAWING'  => BalanceSheetAccountTypes::EQUITY_EARNINGS,
        ],
        'CURRLIAB'                => [
            0 => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
        ],
        'PAYGLIABILITY'           => [
            0 => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
        ],
        'LIABILITY'               => [
            0 => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
        ],
        'TERMLIAB'                => [
            0 => BalanceSheetAccountTypes::LIABILITIES_OTHER,
        ],
        'WAGESPAYABLELIABILITY'   => [
            0 => BalanceSheetAccountTypes::LIABILITIES_OTHER,
        ],
        'SUPERANNUATIONLIABILITY' => [
            0 => BalanceSheetAccountTypes::LIABILITIES_OTHER,
        ],
        'REVENUE'                 => [
            0 => IncomeStatementAccountTypes::REVENUE,
        ],
        'OTHERINCOME'             => [
            0 => IncomeStatementAccountTypes::REVENUE,
        ],
        'SALES'                   => [
            0 => IncomeStatementAccountTypes::REVENUE,
        ],
        'DIRECTCOSTS'             => [
            0 => IncomeStatementAccountTypes::COSTOFGOODS,
        ],
        'SUPERANNUATIONEXPENSE'   => [
            0 => IncomeStatementAccountTypes::PEOPLE,
        ],
        'WAGESEXPENSE'            => [
            0 => IncomeStatementAccountTypes::PEOPLE,
        ],
        'EXPENSE'                 => [
            0                => IncomeStatementAccountTypes::OPERATINGCOSTS,
            'WAGES'          => IncomeStatementAccountTypes::PEOPLE,
            'PAYROLL'        => IncomeStatementAccountTypes::PEOPLE,
            'SALARIES'       => IncomeStatementAccountTypes::PEOPLE,
            'SUPERANNUATION' => IncomeStatementAccountTypes::PEOPLE,
            'PENSION'        => IncomeStatementAccountTypes::PEOPLE,
            'ACC'            => IncomeStatementAccountTypes::PEOPLE,
            'TRAINING'       => IncomeStatementAccountTypes::PEOPLE,
            'KIWISAVER'      => IncomeStatementAccountTypes::PEOPLE,
            'SUPER'          => IncomeStatementAccountTypes::PEOPLE,
            'RSA'            => IncomeStatementAccountTypes::PEOPLE,
            'RENT'           => IncomeStatementAccountTypes::RENT,
            'RATES'          => IncomeStatementAccountTypes::RENT,
            'COUNCIL'        => IncomeStatementAccountTypes::RENT,
            'INTEREST'       => IncomeStatementAccountTypes::OTHER_INTEREST,
            'TAX'            => IncomeStatementAccountTypes::OTHER_TAX,
            'DEPRECIATION'   => IncomeStatementAccountTypes::OTHER_DEPRECIATION,
            'AMORTISATION'   => IncomeStatementAccountTypes::OTHER_DEPRECIATION,
            'AMORTIZATION'   => IncomeStatementAccountTypes::OTHER_DEPRECIATION,
        ],
        'OVERHEADS'               => [
            0                => IncomeStatementAccountTypes::OPERATINGCOSTS,
            'WAGES'          => IncomeStatementAccountTypes::PEOPLE,
            'PAYROLL'        => IncomeStatementAccountTypes::PEOPLE,
            'SALARIES'       => IncomeStatementAccountTypes::PEOPLE,
            'SUPERANNUATION' => IncomeStatementAccountTypes::PEOPLE,
            'PENSION'        => IncomeStatementAccountTypes::PEOPLE,
            'ACC'            => IncomeStatementAccountTypes::PEOPLE,
            'TRAINING'       => IncomeStatementAccountTypes::PEOPLE,
            'KIWISAVER'      => IncomeStatementAccountTypes::PEOPLE,
            'SUPER'          => IncomeStatementAccountTypes::PEOPLE,
            'RSA'            => IncomeStatementAccountTypes::PEOPLE,
            'RENT'           => IncomeStatementAccountTypes::RENT,
            'RATES'          => IncomeStatementAccountTypes::RENT,
            'COUNCIL'        => IncomeStatementAccountTypes::RENT,
            'INTEREST'       => IncomeStatementAccountTypes::OTHER_INTEREST,
            'TAX'            => IncomeStatementAccountTypes::OTHER_TAX,
            'DEPRECIATION'   => IncomeStatementAccountTypes::OTHER_DEPRECIATION,
            'AMORTISATION'   => IncomeStatementAccountTypes::OTHER_DEPRECIATION,
            'AMORTIZATION'   => IncomeStatementAccountTypes::OTHER_DEPRECIATION,
        ],
    ];

    /**
     * @var array $SYSTEM_TYPE_MAP
     */
    private static $SYSTEM_TYPE_MAP = [
        'DEBTORS'                => [
            0 => BalanceSheetAccountTypes::ASSETS_CURRENT_DEBTORS,
        ],
        'RETAINEDEARNINGS'       => [
            0 => BalanceSheetAccountTypes::EQUITY_EARNINGS,
        ],
        'CREDITORS'              => [
            0 => BalanceSheetAccountTypes::LIABILITIES_CURRENT_CREDITORS,
        ],
        'WAGEPAYABLES'           => [
            0 => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
        ],
        'GST'                    => [
            0 => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
        ],
        'GSTONIMPORTS'           => [
            0 => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
        ],
        'HISTORICAL'             => [
            0 => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
        ],
        'ROUNDING'               => [
            0 => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
        ],
        'TRACKINGTRANSFERS'      => [
            0 => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
        ],
        'UNPAIDEXPCLM'           => [
            0 => BalanceSheetAccountTypes::LIABILITIES_CURRENT_OTHER,
        ],
        'BANKCURRENCYGAIN'       => [
            0 => IncomeStatementAccountTypes::REVENUE,
        ],
        'UNREALISEDCURRENCYGAIN' => [
            0 => IncomeStatementAccountTypes::REVENUE,
        ],
        'REALISEDCURRENCYGAIN'   => [
            0 => IncomeStatementAccountTypes::REVENUE,
        ],
    ];

    /**
     * @param ResponseInterface $response
     *
     * @return mixed
     */
    public static function parseToArray(ResponseInterface $response)
    {
        $theResponse = null;
        $headers = $response->getHeader('Content-Type');
        $type = $headers[0] ?? '';

        if (\stripos($type, 'xml') !== false) {
            $theResponse = simplexml_load_string($response->getBody()->getContents());
        } else if(stripos($type, 'pdf') !== false) {
            $theResponse = $response->getBody()->getContents();
        } else if (stripos( $type, 'json') !== false) {
            $theResponse = json_decode($response->getBody()->getContents());
        }

        return \json_decode(\json_encode($theResponse), true);
    }

    /**
     * @param array      $ledgerAccountBalances
     * @param array|null $accountDetails
     *
     * @return ArrayCollection|LedgerAccount[]
     */
    public static function getFinancialDetails(array $ledgerAccountBalances, $accountDetails = null)
    {
        return self::mapAccountsToItems($ledgerAccountBalances, $accountDetails);
    }

    /**
     * @param array      $ledgerAccountBalances
     * @param array|null $accountDetails
     *
     * @return ArrayCollection|LedgerAccount[]
     */
    protected static function mapAccountsToItems(array $ledgerAccountBalances, $accountDetails = null)
    {
        $accounts = [];
        $ledgerAccounts = new ArrayCollection();

        foreach ($ledgerAccountBalances as $key => $accountBalance) {
            foreach ($accountBalance['Lines'] as $line) {
                if ($line['account']) {
                    $ledgerAccount = new LedgerAccount();

                    if (!isset($accounts[$line['account']['AccountID']])) {
                        //New account found
                        $ledgerAccount->setId($line['account']['AccountID']);
                        $ledgerAccount->setTitle($line['name']);
                        $ledgerAccount->setCode($line['account']['ReportingCode'].':'.$line['account']['Type']);
                        $ledgerAccount->setPrice(0);
                        $ledgerAccount->setNew(true);
                        $ledgerAccount->setType(
                            self::findAccountType(
                                $line['category'],
                                $line['name'],
                                @$line['account']['Type'],
                                $line['account']['SystemAccount'] ?? null,
                                $line['account']['ReportingCode'] ?? null
                            )
                        );

                        $accounts[$line['account']['AccountID']] = $ledgerAccount;
                    }

                    //Special rule for creditcard accounts
                    $creditType = $line['account']['BankAccountType'] ?? null;
                    if ('CREDITCARD' == $creditType) {
                        $line['value'] *= (-1);
                    }

                    //Add to total account amount
                    $accounts[$line['account']['AccountID']]->setPrice(
                        $accounts[$line['account']['AccountID']]->getPrice() + $line['value']
                    );
                } elseif ('Foreign Currency Gains and Losses' == $line['name']) {
                    $forexAccount = new LedgerAccount();
                    $forexAccount
                        ->setId('groupId')
                        ->setTitle('FOREX gains/losses')
                        ->setCode('EXP.FOR')
                        ->setType($line['value'] < 0 ? IncomeStatementAccountTypes::REVENUE : IncomeStatementAccountTypes::OPERATINGCOSTS)
                        ->setPrice(abs($line['value']));

                    $accounts['FOREX'] = $forexAccount;
                }
            }
            unset($ledgerAccountBalances[$key]);
        }

        //Group new accounts into Ranqx columns
        foreach ($accounts as $key => $ledgerAccount) {
            if ($ledgerAccount->getPrice() < 0 && BalanceSheetAccountTypes::ASSETS_CURRENT_BANK == $ledgerAccount->getType()) {
                //Negative bank accounts become liability
                $ledgerAccount->setPrice(abs($ledgerAccount->getPrice()));
                $ledgerAccount->setType(BalanceSheetAccountTypes::LIABILITIES_CURRENT_BANK);
            }

            $ledgerAccounts[] = $ledgerAccount;
            unset($accounts[$key]);
        }

        return $ledgerAccounts;
    }

    /**
     * @param string $category
     * @param string $name
     * @param string $accountType
     * @param string $systemType
     * @param string $reportCode
     * @return null|string
     */
    protected static function findAccountType($category, $name, $accountType, $systemType = null, $reportCode = null) :? string
    {
        //Normalise the strings
        $category = preg_replace('/\s+/', '', strtoupper($category));
        $accountType = preg_replace('/\s+/', '', strtoupper($accountType));
        $name = preg_replace('/\s+/', '', $name);

        $reportCodeResult = self::findTypeByReportCode($reportCode, self::$REPORT_CODE_MAP);
        $systemTypeResult = self::findTypeByAccountType($systemType, $name, self::$SYSTEM_TYPE_MAP);
        $accountTypeResult = self::findTypeByAccountType($accountType, $name, self::$ACCOUNT_TYPE_MAP);

        //Count the results, give more weight to report code result
        $resultCounts = array_count_values(array_filter([
            $reportCodeResult,
            $systemTypeResult,
            $accountTypeResult,
        ]));

        //Weight the results
        if ($reportCodeResult) {
            // Report code score weight depends on it's depth, indicated by number of periods "."
            $resultCounts[$reportCodeResult] *= (1 + substr_count($reportCode, '.'));
        }

        if ($systemTypeResult) {
            $resultCounts[$systemTypeResult] *= 1.5;
        }

        if ($accountTypeResult) {
            $resultCounts[$accountTypeResult] *= 2;
        }

        //Return highest scoring result
        if (!empty($resultCounts) && $result = array_search(max($resultCounts), $resultCounts)) {
            return $result;
        }

        //Default value if nothing found
        if ('BS' == $category) {
            return BalanceSheetAccountTypes::ASSETS_OTHER;
        }

        return IncomeStatementAccountTypes::OPERATINGCOSTS;

    }

    /**
     * @param $code
     * @param $name
     * @param $types
     * @return string|null
     */
    private static function findTypeByAccountType($code, $name, $types)
    {
        if (!empty($code)) {
            $ranqxType = null;

            if (($nameArray = @$types[$code]) || ($nameArray = @$types[0])) {
                if (is_array($nameArray)) {
                    //Type level found
                    foreach ($nameArray as $nameMap => $accountType) {
                        if (strlen($nameMap) > 3) {
                            //Long name; case insensitive
                            if (stripos($name, $nameMap) !== false) {
                                return $accountType;
                            }
                        } else {
                            //Short name; case sensitive
                            if (strpos($name, $nameMap) !== false) {
                                return $accountType;
                            }
                        }
                    }

                    return $nameArray[0];
                }

                return $nameArray;
            }
        }

        return null;
    }

    /**
     * @param $code
     * @param $types
     * @return string|null
     */
    private static function findTypeByReportCode($code, $types)
    {
        if (!empty($code)) {
            $codeParts = explode('.', $code);
            $ranqxType = null;

            if (isset($types[reset($codeParts)]) && $subTypes = $types[array_shift($codeParts)]) {
                //Map is defined, return or dig deeper
                if (is_array($subTypes)) {

                    if (empty($codeParts)) {
                        $codeParts[] = $code;
                    }

                    $ranqxType = self::findTypeByReportCode(
                        join('.', $codeParts),
                        $subTypes
                    );
                } else {
                    //End of search
                    $ranqxType = $subTypes;
                }
            } elseif (count($codeParts) == 1 && isset($types[0])) {
                //Use default report code
                return $types[0];
            } else {
                //Undefined map, use default if available
                if (isset($types[0])) {
                    $ranqxType = $types[0];
                }
            }

            return $ranqxType;
        }

        return null;
    }
}
