<?php declare(strict_types = 1);


namespace App\Map;

use Psr\Http\Message\ResponseInterface;

class RanqxMapBalanceSheet
{
    private $res;
    private $data;
    public function __construct(ResponseInterface $response)
    {
        $this->res = $response;
        $this->data = [];
    }

    public function toArray() : array
    {
        if (empty($this->data)) {
            $temp = XeroRanqxMap::parseToArray($this->res);
            $this->data = $temp['Reports']['Report'] ?? [];
            return $this->data;
        }
        return $this->data;
    }

    public function months(): array
    {
        $data = $this->toArray();
        return $this->getMonths($data);
    }

    private function getMonths(array $balance): array
    {
        if (isset($balance['Rows']['Row'])) {
            //find header row
            foreach($balance['Rows']['Row'] as $row) {
                if ($row['RowType'] === 'Header') {
                    if (isset($row['Cells']['Cell'][0])) {
                        unset($row['Cells']['Cell'][0]);
                        return $row['Cells']['Cell'];
                    }
                }
            }
        }
        return [];
    }
}
