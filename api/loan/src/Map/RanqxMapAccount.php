<?php declare(strict_types = 1);


namespace App\Map;

use Psr\Http\Message\ResponseInterface;

/**
 * Class RanqxMapAccount
 * @package App\Map
 */
class RanqxMapAccount
{
    /**
     * @var ResponseInterface
     */
    private $res;

    /**
     * RanqxMapAccount constructor.
     * @param ResponseInterface $response
     */
    public function __construct(ResponseInterface $response)
    {
        $this->res = $response;
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        $accounts = XeroRanqxMap::parseToArray($this->res);
        $results = [];
        $accounts = $accounts['Accounts']['Account'] ?? [];
        foreach ($accounts as $account) {
            $results[$account['AccountID']] = $account;
        }

        return $results;
    }
}
