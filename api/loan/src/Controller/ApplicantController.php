<?php declare(strict_types = 1);


namespace App\Controller;

use App\Message\EmailForApplicantMessage;
use App\Service\LoadApplicant;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Messenger\MessageBusInterface;
use function json_decode;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;

/**
 * @RouteResource("/", pluralize=false)
 * ****/
class ApplicantController extends AbstractFOSRestController
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    private $params;

    /**
     * ApplicantController constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator, ParameterBagInterface $params)
    {
        $this->validator = $validator;
        $this->params  = $params;
    }


    /**
     *
     * @Rest\Post("/v1/kb/save/applicant")
     * @param Request $request
     * @return Response
     * @SWG\Post(
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     tags={"KB Applicant"},
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="firstName", type="string", example="Bob"),
     *              @SWG\Property(property="lastName", type="string", example="Jones"),
     *              @SWG\Property(property="phone", type="string", example="123456567"),
     *              @SWG\Property(property="email", type="string", example="example@example.com"),
     *              @SWG\Property(property="businessName", type="string", example="Business name"),
     *              @SWG\Property(property="loanAmount", type="number", example="50000"),
     *              @SWG\Property(property="loanReason", type="string", example="some reason"),
     *              @SWG\Property(property="isCustomer", type="string", example="no"),
     *              @SWG\Property(property="nzbnNumber", type="string", example="9429032028089"),
     *              @SWG\Property(property="accepted",   type="boolean", example=false),
     *              @SWG\Property(property="loanTerm",   type="integer", example=0),
     *              @SWG\Property(property="salutation", type="string", example="Mr"),
     *              @SWG\Property(property="tenant", type="string", example="https://local.api.ranqx.io:44433")
     *          )
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="save an applicant",
     *         @SWG\Schema (
     *             @SWG\Property(property="http_code",type="string", example="200"),
     *             @SWG\Property(property="data", type="object",
     *                 @SWG\Property(property="id", type="boolean", example="Va5HgyVYixOOCZmdHACGwuUTUHee6xwm-Iy"),
     *                 @SWG\Property(property="firstName", type="string", example="Bob"),
     *                 @SWG\Property(property="lastName", type="string", example="Jones"),
     *                 @SWG\Property(property="phone", type="string", example="123456567"),
     *                 @SWG\Property(property="email", type="string", example="example@example.com"),
     *                 @SWG\Property(property="businessName", type="string", example="Business name"),
     *                 @SWG\Property(property="loanAmount", type="number", example="50000"),
     *                 @SWG\Property(property="loanReason", type="string", example="some reason"),
     *                 @SWG\Property(property="isCustomer", type="string", example="no"),
     *                 @SWG\Property(property="nzbnNumber", type="string", example="9429032028089"),
     *                 @SWG\Property(property="accepted",   type="boolean", example=false),
     *                 @SWG\Property(property="loanTerm",   type="integer", example=0),
     *                 @SWG\Property(property="salutation",   type="string", example="Mr")
     *             ),
     *         )
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="save an applicant and error happened",
     *         @SWG\Schema (
     *             @SWG\Property(property="http_code",type="string", example="400"),
     *             @SWG\Property(property="data", type="object",
     *                 @SWG\Property(property="id", type="boolean", example="null"),
     *                 @SWG\Property(property="firstName", type="string", example="Bob"),
     *                 @SWG\Property(property="lastName", type="string", example="Jones"),
     *                 @SWG\Property(property="phone", type="string", example="123456567"),
     *                 @SWG\Property(property="email", type="string", example="example@example.com"),
     *                 @SWG\Property(property="businessName", type="string", example="Business name"),
     *                 @SWG\Property(property="loanAmount", type="number", example="50000"),
     *                 @SWG\Property(property="loanReason", type="string", example="some reason"),
     *                 @SWG\Property(property="isCustomer", type="string", example="no"),
     *                 @SWG\Property(property="nzbnNumber", type="string", example="9429032028089"),
     *                 @SWG\Property(property="accepted",   type="boolean", example=false),
     *                 @SWG\Property(property="loanTerm",   type="integer", example=0),
     *                 @SWG\Property(property="salutation",   type="string", example="Mr")
     *             ),
     *              @SWG\Property(property="errors", type="object",ref="#/definitions/ErrorModel")
     *         )
     *     )
     * )
     *
     */
    public function save(Request $request)
    {
        $data = json_decode((string)$request->getContent(), true);
        if ($data) {
            $app = new LoadApplicant($data, $this->validator);
            $result = $app->load($this->getDoctrine()->getManager());
            $view = $this->view($result, $result['http_code']);
            return $this->handleView($view);
        }

        return new JsonResponse(['response' => 'Invalid payload.'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Options("/v1/kb/save/applicant")
     * @return Response
     * @throws \Exception
     */
    public function preflight()
    {
        return  new Response('Test');
    }
}
