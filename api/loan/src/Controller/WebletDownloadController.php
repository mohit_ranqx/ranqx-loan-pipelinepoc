<?php

namespace App\Controller;

use App\Entity\BlockIps;
use App\Entity\User;
use DataDog\DogStatsd;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\StreamedResponse;
use App\Security\SecurityTrait;

/**
 * Class WebletDownloadController
 * @package App\Controller
 * @RouteResource("/", pluralize=false)
 */
class WebletDownloadController extends AbstractFOSRestController
{
    use SecurityTrait;

    /**
     * @var string
     */
    private $origin;

    /**
     * Location of the compiled React JS downloader
     * @var string
     */
    protected $jsDownloader =  '/opt/ranqx-lo-ui/public/LoanOriginationDownloader.js';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * WebletDownloadController constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->statsd = new DogStatsd();
        $this->logger = $logger;
    }

    /**
     * Delivers React JS package - Loan Origination Overlay
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param ParameterBagInterface $bag
     * @return JsonResponse|StreamedResponse
     * @Rest\Get("/v1/weblet/download")
     */
    public function download(Request $request, EntityManagerInterface $entityManager, ParameterBagInterface $bag)
    {
        $authFailedMessage = ['response' => 'Authentication failed.'];
        $authFailedStatus = Response::HTTP_UNAUTHORIZED;

        if (!$this->validateClient($request, $entityManager, true)) {
            return new JsonResponse($authFailedMessage, $authFailedStatus);
        }

        $token = $this->getAccessToken();
        if (!$token) {
            // Already doing DataDog log in geAccessToken, not required here
            return new JsonResponse($authFailedMessage, $authFailedStatus);
        }

        $response = $this->buildJsResponse($request, $this->jsPackage, $bag);

        // If the response is a JsonResponse it means something went wrong and we should not continue
        // with the package delivery
        if (is_a($response, 'JsonResponse')) {
            return $response;
        }

        $token = json_decode($token);
        // Generate file has for checking through the application process
        $fileHash = hash_file($this->hmacAlgorithm, $this->jsPackage);

        $response->headers->add([
           'token' => $token->access_token,
           'hash' => $fileHash
        ]);

        return $response->send();
    }

    /**
     * Delivers JS downloader script
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param ParameterBagInterface $bag
     * @return JsonResponse|StreamedResponse
     * @Rest\Get("/v1/weblet/downloader")
     */
    public function downloader(Request $request, EntityManagerInterface $entityManager, ParameterBagInterface $bag) {
        $authFailedMessage = ['response' => 'Authentication failed.'];
        $authFailedStatus = Response::HTTP_BAD_REQUEST;

        if (!$this->validateClient($request, $entityManager, false)) {
            return new JsonResponse($authFailedMessage, $authFailedStatus);
        }

        $response = $this->buildJsResponse($request, $this->jsDownloader, $bag);

        // If the response is a JsonResponse it means something went wrong and we should not continue
        // with the package delivery
        if (is_a($response, 'JsonResponse')) {
            return $response;
        }

        return $response->send();
    }

    /**
     * Build response from JS file
     *
     * @param Request $request
     * @param string $fileLocation
     * @param ParameterBagInterface $bag
     * @return StreamedResponse|JsonResponse
     */
    private function buildJsResponse(Request $request, string $fileLocation, ParameterBagInterface $bag) {
        if (!file_exists($fileLocation)) {
            $fileFailure = 'failed_to_find_js_package';
            $fileFailureMessage = 'Failed to find the js package: ' . $fileLocation;
            $this->statsd->increment($fileFailure);
            $this->statsd->event($fileFailure,
                array(
                    'title' => $fileFailureMessage,
                    'text' => json_encode([
                        'request' => $request,
                        'fileLocation' => $fileLocation
                    ]),
                    'alert_type' => 'error'
                )
            );
            $this->logger->critical($fileFailureMessage, [
                'cause' => json_encode([
                    'request' => $request,
                    'fileLocation' => $fileLocation
                ]),
            ]);

            return new JsonResponse(['response' => 'The file could not be delivered. Please contact support.'], Response::HTTP_NO_CONTENT);
        }

        $jsFileData = file_get_contents($fileLocation);

        // Build and insert API URL into React JS payload
        $jsFileData = str_replace("{{{API_URL}}}", $bag->get('ui.api_url'), $jsFileData);

        $response = new StreamedResponse();
        $response->setCallback(function () use ($jsFileData) {
            echo $jsFileData;
        });

        $response->headers->set('Access-Control-Allow-Origin', $request->headers->get('origin'));
        $response->headers->set('Access-Control-Allow-Credentials', 'false');
        $response->headers->set('Access-Control-Expose-Headers', 'token, hash');

        return $response;
    }

    /**
     * Validates detail of client request
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param bool $validateOrigin
     * @return bool
     */
    private function validateClient(Request $request, EntityManagerInterface $entityManager, bool $validateOrigin) {
        if (!$this->verifyIdentity($request)) {
            // Already doing DataDog log in verifyIdentity, not required here
            return false;
        }

        // Check the IP is not reported
        $blockIps = $entityManager->getRepository(BlockIps::class);
        if ($blockIps->findBy(['address' => $request->getClientIp()])) {
            $blockedIpMessage = 'blocked_ip_access_attempt';
            $this->statsd->increment($blockedIpMessage);
            $this->logger->error($blockedIpMessage);

            return false;
        }

        // Check origin && referer combination
        $referer = $request->headers->get('referer');
        $origin = $request->headers->get('origin');

        // Strip query if in referer
        $referer = strtok($referer, "?");

        $tenant = $entityManager->getRepository(User::class);

        // Origin is not sent in the initial downloader request so should not be checked
        // Query tenants
        if ($validateOrigin) {
            $currentTenant = $tenant->findBy(['origin' => $origin, 'referer' => $referer, 'active' => 1]);
        } else {
            $currentTenant = $tenant->findBy(['referer' => $referer, 'active' => 1]);
        }

        if (isset($currentTenant[0])) {
            return true;
        }

        $tenantFailedID = 'tenant_check_failed';
        $tenantFailedMessage = 'Failed to find valid tenant with origin and referer';
        $this->statsd->increment($tenantFailedID);

        $this->statsd->event($tenantFailedID,
            array(
                'title' => $tenantFailedMessage,
                'text' => json_encode([
                    'origin' => $this->origin,
                    'referer' => $referer,
                    'validate_origin' => $validateOrigin,
                ]),
                'alert_type' => 'error'
            )
        );

        $this->logger->critical($tenantFailedMessage, [
            'cause' => json_encode([
                'origin' => $this->origin,
                'referer' => $referer,
                'validate_origin' => $validateOrigin,
            ]),
        ]);

        return false;
    }
}
