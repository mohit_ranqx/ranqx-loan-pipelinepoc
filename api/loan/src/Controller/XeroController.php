<?php declare(strict_types=1);


namespace App\Controller;

use App\Entity\User;
use App\Message\ApplicantExceptionMessage;
use App\Message\DecisionRequestMessage;
use App\Message\RanqxExceptionMessage;
use App\Message\SetDecisionExceptionMessage;
use App\OAuth\Consumer\Xero;
use App\Service\PrivateKeyCrypt;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Message\XeroAccessTokenMessage;
use \Symfony\Component\HttpFoundation\RedirectResponse;
use \GuzzleHttp\Exception\GuzzleException;
use \Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use \Psr\Log\LoggerInterface;
use App\TypeTrait\DataDogMetric;
use App\Message\EncryptedApplicantIdExceptionLoggingMessage;
use App\TypeTrait\XeroKey;
use App\Entity\Applicant;

/**
 * Class XeroController
 * @package App\Controller
 * @RouteResource("/", pluralize=false)
 */
class XeroController extends AbstractFOSRestController
{
    use XeroKey;
    use DataDogMetric;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * XeroController constructor.
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(LoggerInterface $logger, EntityManagerInterface $entityManager)
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @param Xero $xero
     * @param MessageBusInterface $messageBus
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws Exception;
     * @throws GuzzleException
     * @Rest\Get("/xero/success")
     */
    public function success(
        Request $request,
        Xero $xero,
        MessageBusInterface $messageBus,
        EntityManagerInterface $entityManager
    ): Response {

        $appId = $request->get('appId');
        // 1
        if (!$appId) {
            $messageBus->dispatch(new RanqxExceptionMessage('Applicant id not returned from xero.'));
            return $this->uiRedirect($request);
        }

        // 2
        try {
            $crypt = new PrivateKeyCrypt(
                $this->privateKey(),
                $this->publicKey()
            );

            $idJSON = json_decode($crypt->decode($request->get('appId')));

            $appId = (int) $idJSON->appId;
        } catch (\Exception $e) {
            $messageBus->dispatch(new RanqxExceptionMessage($e->getTraceAsString()));

            return $this->uiRedirect($request);
        }

        // 3
        //decode was successful.
        //oauth third leg here
        if ($request->query->get('error') !== null) {
            $msg = $request->query->get('error_description');
            $messageBus->dispatch(new ApplicantExceptionMessage($appId, $msg));
            //do not put setDecision to true into ApplicantExceptionMessage,
            //the ApplicantExceptionMessage is an exception used by many different function.
            //if put setDecision to true into ApplicantExceptionMessage will causing decision queue to run
            //twice.
            $messageBus->dispatch(new SetDecisionExceptionMessage($appId));
            return $this->uiRedirect($request);
        }
        // 4
        if ($request->query->get('oauth_token') === null || $request->query->get('oauth_verifier') === null) {
            $msg = 'Missing oauth_token or oauth_verifier token.';
            $messageBus->dispatch(new ApplicantExceptionMessage($appId, $msg));
            $messageBus->dispatch(new SetDecisionExceptionMessage($appId));

            return $this->uiRedirect($request);
        }

        // 5
        try {
            $token = $xero->accessToken($request->query->get('oauth_token'), $request->query->get('oauth_verifier'), $request->get('appId'));
            $messageBus->dispatch(new XeroAccessTokenMessage($token, $appId));
            return $this->uiRedirect($request, false, true);
        } catch (Exception $e) {
            $messageBus->dispatch(new ApplicantExceptionMessage($appId, $e->getMessage()));
            $messageBus->dispatch(new SetDecisionExceptionMessage($appId));

            return $this->uiRedirect($request);
        }
    }


    /**
     * @param Xero $xero
     * @param Request $request
     * @param LoggerInterface $logger
     * @param MessageBusInterface $bus
     * @return JsonResponse|Response
     * @Rest\Get("/xero/connect")
     */
    public function connect(Xero $xero, Request $request, LoggerInterface $logger, MessageBusInterface $bus)
    {
        $id = $request->query->get('id');

        $errorResponse = [
            "error" => "Xero connection failure",
            "error_code" => "xero_connection_exception",
        ];

        if (!$id) {
            $bus->dispatch(new RanqxExceptionMessage('missing applicant id'));
            $this->logger->critical('Missing applicant id');
            $this->metric('xero_missing_app_id');

            return $this->uiRedirect($request, true, false, $errorResponse);
        }

        try {
            $token = $xero->requestToken($id);
            $uri = $xero->authorization($token);
            return new JsonResponse(['redirect' => $uri]);
        } catch (GuzzleException $exception) {
            $bus->dispatch(new EncryptedApplicantIdExceptionLoggingMessage($id, $exception->getTraceAsString()));
            $this->logger->critical($exception->getMessage());
            $this->metric('xero_connection_exception');

            return $this->uiRedirect($request, true, false, $errorResponse);
        } catch (\Exception $exception) {
            $bus->dispatch(new EncryptedApplicantIdExceptionLoggingMessage($id, $exception->getTraceAsString()));
            $this->logger->critical($exception->getMessage());
            $this->metric('xero_connection_exception');

            return $this->uiRedirect($request, true, false, $errorResponse);
        }
    }

    /**
     * @Rest\Options("/xero/connect")
     * @return Response
     */
    public function preflight()
    {
        return new Response('Test');
    }

    /**
     * Returns the url where the UI is located for this request
     * currently we store the referrer and use that for the redirect url
     *
     * @param Request $request
     * @param bool $json
     * @param bool $success
     * @param array $errors
     * @return RedirectResponse|JsonResponse
     */
    private function uiRedirect(Request $request, $json = false, $success = false, $errors = [])
    {
        $crypt = new PrivateKeyCrypt(
            $this->privateKey(),
            $this->publicKey()
        );

        $idJSON = json_decode($crypt->decode($request->get('appId')));
        $tenant = $idJSON->tenant;

        $redirect = $this->getParameter('xero.back.to');

        if (!$tenant) {
            $this->logger->critical('Failed to get tenant through Xero redirect');
            $this->metric('tenant_passthrough_failed');
        }

        $tenantEntity = $this->entityManager->getRepository(User::class);
        $tenantRecord = $tenantEntity->findBy(['name' => $tenant, 'active' => 1]);

        if (isset($tenantRecord[0])) {
            $redirect = $tenantRecord[0]->getReferer();
        }

        if (!isset($tenantRecord[0])) {
            $this->logger->critical('Failed to find a tenant to redirect to');
            $this->metric('xero_tenant_redirect_failed');
            $this->event('Failed to find a tenant to redirect to');

            $redirect = $this->getParameter('xero.back.to');
        }

        if ($json) {
            $response = new JsonResponse(array_merge([
                'redirect' => $redirect,
                'xeroSuccess' => $success,
            ], $errors));
        } else {
            $response = new RedirectResponse($redirect . (parse_url($redirect, PHP_URL_QUERY) ? '&' : '?') . 'loXeroSuccess=' . $success);
        }

        return $response;
    }
}
