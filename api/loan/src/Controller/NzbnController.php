<?php declare(strict_types = 1);


namespace App\Controller;


use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\NzbnSearch;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;
use App\Service\NzbnDirectors;

/**
 * @RouteResource("/", pluralize=false)
 * ****/
class NzbnController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/v1/nzbn/search")
     * @return Response
     * @throws \Exception
     * @param Request $request
     * @param NzbnSearch $handler
     *
     * @SWG\Get(
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     tags={"NZBN search"},
     *     @SWG\Parameter(
     *          name="term",
     *          in="query",
     *          type="string",
     *          description="company name"
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="company list",
     *         @SWG\Schema (
     *             @SWG\Property(property="http_code",type="string", example="200"),
     *             @SWG\Property(property="data", type="array",
     *                 @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="name", type="string", example="BAY HORSE WISDOM LIMITED"),
     *                      @SWG\Property(property="nzbn", type="string", example="9429047374171"),
     *                  ),
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="name", type="string", example="LION-HEART WISDOM LIMITED"),
     *                      @SWG\Property(property="nzbn", type="string", example="9429047299016"),
     *                  )
     *             ),
     *             @SWG\Property(property="errors",type="object"),
     *         )
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="search term can not be blank, and min length is 3",
     *         @SWG\Schema (
     *             @SWG\Property(property="http_code",type="string", example="422"),
     *             @SWG\Property(property="data", type="object"),
     *             @SWG\Property(property="errors", type="object",ref="#/definitions/ErrorModel")
     *         )
     *     ),
     *
     * )
     *
     ***/
    public function search(Request $request, NzbnSearch $handler): Response
    {
        $result = $handler->search($request->query->get('term', ''));
        $view = $this->view($result, $result['http_code']);
        return $this->handleView($view);
    }

    /**
     * @Rest\Get("/v1/nzbn/{number}/directors")
     * @return Response
     * @param int $number
     * @param NzbnDirectors $nzbnDirectors
     * @SWG\Get(
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     tags={"Get directors by nzbn number"},
     *     @SWG\Parameter(
     *         name="number",
     *         in="path",
     *         type="integer",
     *         required=true,
     *         maxLength=13
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="director list",
     *         @SWG\Schema(
     *             @SWG\Property(property="http_code",type="string", example="200"),
     *             @SWG\Property(property="data", type="array",
     *                 @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="firstName", type="string", example="John"),
     *                      @SWG\Property(property="lastName", type="string", example="SIMPSON"),
     *                      @SWG\Property(property="middleNames", type="string", example="Graeme"),
     *                      @SWG\Property(property="title", type="string", example="")
     *                  ),
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="firstName", type="string", example="Deborah"),
     *                      @SWG\Property(property="lastName", type="string", example="SIMPSON"),
     *                      @SWG\Property(property="middleNames", type="string", example="Jane"),
     *                      @SWG\Property(property="title", type="string", example="")
     *                  ),
     *             ),
     *             @SWG\Property(property="errors", type="object",ref="#/definitions/ErrorModel")
     *         )
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="director search errors",
     *         @SWG\Schema (
     *             @SWG\Property(property="http_code",type="string", example="422"),
     *             @SWG\Property(property="data", type="object"),
     *             @SWG\Property(property="errors", type="object",ref="#/definitions/ErrorModel")
     *         )
     *     ),
     * )
     * ***/
    public function directors(int $number, NzbnDirectors $nzbnDirectors): Response
    {
        $result = $nzbnDirectors->search($number);
        $view = $this->view($result, $result['http_code']);
        return $this->handleView($view);
    }
}