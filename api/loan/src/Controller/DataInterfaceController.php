<?php declare(strict_types = 1);


namespace App\Controller;

use App\DataInterface\Equifax;
use App\DataInterface\RanqxDataHandler;
use App\Entity\FinanceMonth;
use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;

/**
 * @RouteResource("/", pluralize=false)
 * ****/

class DataInterfaceController extends AbstractFOSRestController
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * @var ObjectManager
     */
    private $objectManager;


    /**
     * DataInterfaceController constructor.
     * @param ValidatorInterface $validator
     * @param ParameterBagInterface $params
     * @param ObjectManager $objectManager
     */
    public function __construct(
        ValidatorInterface $validator,
        ParameterBagInterface $params,
        ObjectManager $objectManager
    ) {
        $this->validator = $validator;
        $this->params  = $params;
        $this->objectManager = $objectManager;
    }

    public function numberOfNzbn(Request $request): Response
    {
        $date = new DateTime('-6 month');
        $handler = new RanqxDataHandler($this->objectManager, new Equifax($this->params), $this->params);
        $count = $handler->numberOf($request->query->get('nzbn', ''), $date);

        $temp = ['http_code' => 200 , 'data' => ['nzbn' => $count]];
        $view = $this->view($temp, $temp['http_code']);
        return $this->handleView($view);
    }

    public function oldestPeriod(Request $request): Response
    {
        $number = (int)$request->query->get('number', '');
        $handler = new RanqxDataHandler($this->objectManager, new Equifax($this->params), $this->params);
        $data = $handler->oldestData($number);

        if (empty($data)) {
            $data = json_encode(new \stdClass);
        }

        $temp = ['http_code' => 200 , 'data' => $data];
        $view = $this->view($temp, $temp['http_code']);
        return $this->handleView($view);
    }

    public function mostRecentCalculationValue(Request $request): Response
    {
        $number = (int)$request->query->get('org', '');
        $handler = new RanqxDataHandler($this->objectManager, new Equifax($this->params), $this->params);
        $data = $handler->mostRecentMonthlyCalulationValue($number);

        if (empty($data)) {
            $data = json_encode(new \stdClass);
        }

        $temp = ['http_code' => 200 , 'data' => $data];
        $view = $this->view($temp, $temp['http_code']);
        return $this->handleView($view);
    }
}
