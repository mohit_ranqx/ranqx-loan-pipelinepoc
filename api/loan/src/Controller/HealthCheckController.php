<?php declare(strict_types=1);

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * @RouteResource("/", pluralize=false)
 */
class HealthCheckController extends AbstractFOSRestController
{
    /**
     * @var mixed
     */
    private $redisHost;
    /**
     * @var int
     */
    private $redisPort;

    /**
     * @var EntityManagerInterface
     */
    private $entityManger;

    /**
     * HealthCheckController constructor.
     * @param ParameterBagInterface $bag
     */
    public function __construct(ParameterBagInterface $bag, EntityManagerInterface $entityManager)
    {
        $this->redisHost = $bag->get('health_redis_host');
        $this->redisPort = (int)$bag->get('health_redis_port');
        $this->entityManger = $entityManager;
    }

    /**
     *
     * @Rest\Get("/health/check")
     * ****/
    public function check(): JsonResponse
    {
        try {
            $this->redis();
            $this->doctrine();
            return new JsonResponse('OK', Response::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_SERVICE_UNAVAILABLE);
        }
    }

    /**
     * @return string
     * @throws \RedisException
     */
    public function redis(): string
    {
        $redis = new \Redis();
        $redis->connect($this->redisHost, $this->redisPort);
        return $redis->ping();
    }

    /**
     * @return bool
     */
    public function doctrine(): bool
    {
        $b = $this->entityManger->getConnection()->ping();
        if ($b === false) {
            throw new \RuntimeException('DB service is down.');
        }
        return $b;
    }
}