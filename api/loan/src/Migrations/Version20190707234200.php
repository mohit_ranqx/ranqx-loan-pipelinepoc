<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190707234200 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE calculate_finance_month CHANGE orginisation_id organisation_id INT NOT NULL, CHANGE mrginal_cashflow_rolling marginal_cashflow_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE period period_start DATETIME NOT NULL');
        $this->addSql('ALTER TABLE calculate_finance_yearly CHANGE orginisation_id organisation_id INT DEFAULT NULL, CHANGE mrginal_cashflow_rolling_yearly marginal_cashflow_rolling_yearly DOUBLE PRECISION DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE calculate_finance_month CHANGE organisation_id orginisation_id INT NOT NULL, CHANGE marginal_cashflow_rolling mrginal_cashflow_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE period_start period DATETIME NOT NULL');
        $this->addSql('ALTER TABLE calculate_finance_yearly CHANGE organisation_id orginisation_id INT DEFAULT NULL, CHANGE marginal_cashflow_rolling_yearly mrginal_cashflow_rolling_yearly DOUBLE PRECISION DEFAULT NULL');
    }
}
