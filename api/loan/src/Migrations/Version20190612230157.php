<?php

    declare(strict_types=1);

    namespace DoctrineMigrations;

    use Doctrine\DBAL\Schema\Schema;
    use Doctrine\Migrations\AbstractMigration;

    /**
     * Auto-generated Migration: Please modify to your needs!
     */
    final class Version20190612230157 extends AbstractMigration
    {
        public function getDescription() : string
        {
            return '';
        }

        public function up(Schema $schema) : void
        {
            // this up() migration is auto-generated, please modify it to your needs
            $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

            $this->addSql('CREATE TABLE Applicant (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(64) NOT NULL, last_name VARCHAR(64) NOT NULL, email VARCHAR(256) NOT NULL, phone VARCHAR(32) NOT NULL, business_name VARCHAR(256) NOT NULL, loan_amount NUMERIC(19, 4) NOT NULL, created DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
            $this->addSql('ALTER TABLE organisation ADD applicant_id INT DEFAULT NULL');
            $this->addSql('ALTER TABLE organisation ADD CONSTRAINT FK_E6E132B497139001 FOREIGN KEY (applicant_id) REFERENCES Applicant (id) ON DELETE CASCADE');
            $this->addSql('CREATE UNIQUE INDEX UNIQ_E6E132B497139001 ON organisation (applicant_id)');
        }

        public function down(Schema $schema) : void
        {
            // this down() migration is auto-generated, please modify it to your needs
            $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

            $this->addSql('ALTER TABLE organisation DROP FOREIGN KEY FK_E6E132B497139001');
            $this->addSql('DROP TABLE Applicant');
            $this->addSql('DROP INDEX UNIQ_E6E132B497139001 ON organisation');
            $this->addSql('ALTER TABLE organisation DROP applicant_id');
        }
    }
