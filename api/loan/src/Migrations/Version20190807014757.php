<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!!!!
 */
final class Version20190807014757 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE calculate_finance_month_current CHANGE revenue_current revenue_current NUMERIC(19, 7) DEFAULT NULL, CHANGE gross_margin_value_current gross_margin_value_current NUMERIC(19, 7) DEFAULT NULL, CHANGE gross_margin_percentage_current gross_margin_percentage_current NUMERIC(19, 7) DEFAULT NULL, CHANGE ebitda_value_current ebitda_value_current NUMERIC(19, 7) DEFAULT NULL, CHANGE ebitda_margin_current ebitda_margin_current NUMERIC(19, 7) DEFAULT NULL, CHANGE interest_cover_current interest_cover_current NUMERIC(19, 7) DEFAULT NULL, CHANGE marginal_cashflow_current marginal_cashflow_current NUMERIC(19, 7) DEFAULT NULL, CHANGE working_capital_ratio_current working_capital_ratio_current NUMERIC(19, 7) DEFAULT NULL, CHANGE quick_ratio_current quick_ratio_current NUMERIC(19, 7) DEFAULT NULL, CHANGE cash_ratio_current cash_ratio_current NUMERIC(19, 7) DEFAULT NULL, CHANGE net_cash_balance_current net_cash_balance_current NUMERIC(19, 7) DEFAULT NULL, CHANGE debt_ratio_current debt_ratio_current NUMERIC(19, 7) DEFAULT NULL, CHANGE equity_ratio_current equity_ratio_current NUMERIC(19, 7) DEFAULT NULL, CHANGE people_cost_ratio_current people_cost_ratio_current NUMERIC(19, 7) DEFAULT NULL, CHANGE operating_cost_ratio_current operating_cost_ratio_current NUMERIC(19, 7) DEFAULT NULL');
        $this->addSql('ALTER TABLE calculate_finance_month_rolling CHANGE revenue_rolling revenue_rolling NUMERIC(19, 7) DEFAULT NULL, CHANGE gross_margin_value_rolling gross_margin_value_rolling NUMERIC(19, 7) DEFAULT NULL, CHANGE gross_margin_percentage_rolling gross_margin_percentage_rolling NUMERIC(19, 7) DEFAULT NULL, CHANGE ebitda_value_rolling ebitda_value_rolling NUMERIC(19, 7) DEFAULT NULL, CHANGE ebitda_value_percentage_rolling ebitda_value_percentage_rolling NUMERIC(19, 7) DEFAULT NULL, CHANGE interest_cover_rolling interest_cover_rolling NUMERIC(19, 7) DEFAULT NULL, CHANGE marginal_cashflow_rolling marginal_cashflow_rolling NUMERIC(19, 7) DEFAULT NULL, CHANGE debt_leverage_rolling debt_leverage_rolling NUMERIC(19, 7) DEFAULT NULL, CHANGE debitor_days_rolling debitor_days_rolling NUMERIC(19, 7) DEFAULT NULL, CHANGE creditor_days_rolling creditor_days_rolling NUMERIC(19, 7) DEFAULT NULL, CHANGE defence_internal_ratio_rolling defence_internal_ratio_rolling NUMERIC(19, 7) DEFAULT NULL, CHANGE expense_cover_ratio_rolling expense_cover_ratio_rolling NUMERIC(19, 7) DEFAULT NULL, CHANGE operating_cost_ratio_rolling operating_cost_ratio_rolling NUMERIC(19, 7) DEFAULT NULL, CHANGE people_cost_ratio_rolling people_cost_ratio_rolling NUMERIC(19, 7) DEFAULT NULL');
        $this->addSql('ALTER TABLE calculate_finance_yearly CHANGE revenue_growth_yearly revenue_growth_yearly NUMERIC(19, 7) DEFAULT NULL, CHANGE stock_days_yearly stock_days_yearly NUMERIC(19, 7) DEFAULT NULL');
    }


    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE calculate_finance_month_current CHANGE revenue_current revenue_current NUMERIC(24, 12) DEFAULT NULL, CHANGE gross_margin_value_current gross_margin_value_current NUMERIC(24, 12) DEFAULT NULL, CHANGE gross_margin_percentage_current gross_margin_percentage_current NUMERIC(24, 12) DEFAULT NULL, CHANGE ebitda_value_current ebitda_value_current NUMERIC(24, 12) DEFAULT NULL, CHANGE ebitda_margin_current ebitda_margin_current NUMERIC(24, 12) DEFAULT NULL, CHANGE interest_cover_current interest_cover_current NUMERIC(24, 12) DEFAULT NULL, CHANGE marginal_cashflow_current marginal_cashflow_current NUMERIC(24, 12) DEFAULT NULL, CHANGE working_capital_ratio_current working_capital_ratio_current NUMERIC(24, 12) DEFAULT NULL, CHANGE quick_ratio_current quick_ratio_current NUMERIC(24, 12) DEFAULT NULL, CHANGE cash_ratio_current cash_ratio_current NUMERIC(24, 12) DEFAULT NULL, CHANGE net_cash_balance_current net_cash_balance_current NUMERIC(24, 12) DEFAULT NULL, CHANGE debt_ratio_current debt_ratio_current NUMERIC(24, 12) DEFAULT NULL, CHANGE equity_ratio_current equity_ratio_current NUMERIC(24, 12) DEFAULT NULL, CHANGE people_cost_ratio_current people_cost_ratio_current NUMERIC(24, 12) DEFAULT NULL, CHANGE operating_cost_ratio_current operating_cost_ratio_current NUMERIC(24, 12) DEFAULT NULL');
        $this->addSql('ALTER TABLE calculate_finance_month_rolling CHANGE revenue_rolling revenue_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE gross_margin_value_rolling gross_margin_value_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE gross_margin_percentage_rolling gross_margin_percentage_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE ebitda_value_rolling ebitda_value_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE ebitda_value_percentage_rolling ebitda_value_percentage_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE interest_cover_rolling interest_cover_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE marginal_cashflow_rolling marginal_cashflow_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE debt_leverage_rolling debt_leverage_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE debitor_days_rolling debitor_days_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE creditor_days_rolling creditor_days_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE defence_internal_ratio_rolling defence_internal_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE expense_cover_ratio_rolling expense_cover_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE operating_cost_ratio_rolling operating_cost_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE people_cost_ratio_rolling people_cost_ratio_rolling NUMERIC(24, 12) DEFAULT NULL');
        $this->addSql('ALTER TABLE calculate_finance_yearly CHANGE revenue_growth_yearly revenue_growth_yearly NUMERIC(24, 12) DEFAULT NULL, CHANGE stock_days_yearly stock_days_yearly NUMERIC(24, 12) DEFAULT NULL');
    }
}
