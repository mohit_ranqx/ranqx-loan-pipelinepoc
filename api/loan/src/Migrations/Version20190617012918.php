<?php

    declare(strict_types=1);

    namespace DoctrineMigrations;

    use Doctrine\DBAL\Schema\Schema;
    use Doctrine\Migrations\AbstractMigration;

    /**
     * Auto-generated Migration: Please modify to your needs!
     */
    final class Version20190617012918 extends AbstractMigration
    {
        public function getDescription() : string
        {
            return '';
        }

        public function up(Schema $schema) : void
        {
            // this up() migration is auto-generated, please modify it to your needs
            $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

            $this->addSql('CREATE TABLE calculate_finance_yearly (id INT AUTO_INCREMENT NOT NULL, orginisation_id INT DEFAULT NULL, revenue_rolling_yearly INT DEFAULT NULL, revenue_growth_rolling_yearly INT DEFAULT NULL, gross_margin_value_rolling_yearly INT DEFAULT NULL, gross_margin_percentage_rolling_yearly INT DEFAULT NULL, ebitda_value_rolling_yearly INT DEFAULT NULL, ebitda_value_percentage_rolling_yearly INT DEFAULT NULL, interest_cover_rolling_yearly INT DEFAULT NULL, mrginal_cashflow_rolling_yearly INT DEFAULT NULL, trans_journal_completeness_rolling_yearly INT DEFAULT NULL, last_gst_rolling_yearly INT DEFAULT NULL, working_capital_ratio_rolling_yearly INT DEFAULT NULL, quick_ratio_rolling_yearly INT DEFAULT NULL, cash_ratio_rolling_yearly INT DEFAULT NULL, net_cash_balance_rolling_yearly INT DEFAULT NULL, debit_ratio_rolling_yearly INT DEFAULT NULL, debit_ratio_rolling_yearly_integer INT DEFAULT NULL, debt_leverage_rolling_yearly INT DEFAULT NULL, equity_ratio_rolling_yearly INT DEFAULT NULL, debitor_days_rolling_yearly INT DEFAULT NULL, creditor_days_rolling_yearly INT DEFAULT NULL, stock_days_rolling_yearly INT DEFAULT NULL, defence_internal_ratio_rolling_yearly INT DEFAULT NULL, expense_cover_ratio_rolling_yearly INT DEFAULT NULL, profit_ebitda_to_cl_rolling_yearly INT DEFAULT NULL, operating_cost_ratio_rolling_yearly INT DEFAULT NULL, people_cost_ratio_rolling_yearly INT DEFAULT NULL, outstanding_debtors_rolling_yearly INT DEFAULT NULL, debtor_concentration_rolling_yearly INT DEFAULT NULL, late_creditors_proportion_rolling_yearly INT DEFAULT NULL, late_creditors_value_rolling_yearly INT DEFAULT NULL, period_start DATETIME NOT NULL, period_end DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        }

        public function down(Schema $schema) : void
        {
            // this down() migration is auto-generated, please modify it to your needs
            $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

            $this->addSql('DROP TABLE calculate_finance_yearly');
        }
    }
