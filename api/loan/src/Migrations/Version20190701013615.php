<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190701013615 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE calculate_finance_month CHANGE revenue_rolling revenue_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE revenue_growth_rolling revenue_growth_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE gross_margin_value_rolling gross_margin_value_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE gross_margin_percentage_rolling gross_margin_percentage_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE ebitda_value_rolling ebitda_value_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE ebitda_value_percentage_rolling ebitda_value_percentage_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE interest_cover_rolling interest_cover_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE mrginal_cashflow_rolling mrginal_cashflow_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE trans_journal_completeness_rolling trans_journal_completeness_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE last_gst_rolling last_gst_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE working_capital_ratio_rolling working_capital_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE quick_ratio_rolling quick_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE cash_ratio_rolling cash_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE net_cash_balance_rolling net_cash_balance_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE debit_ratio_rolling debit_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE debt_leverage_rolling debt_leverage_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE equity_ratio_rolling equity_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE debitor_days_rolling debitor_days_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE creditor_days_rolling creditor_days_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE stock_days_rolling stock_days_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE defence_internal_ratio_rolling defence_internal_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE expense_cover_ratio_rolling expense_cover_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE profit_ebitda_to_cl_rolling profit_ebitda_to_cl_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE operating_cost_ratio_rolling operating_cost_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE people_cost_ratio_rolling people_cost_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE outstanding_debtors_rolling outstanding_debtors_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE debtor_concentration_rolling debtor_concentration_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE late_creditors_proportion_rolling late_creditors_proportion_rolling NUMERIC(24, 12) DEFAULT NULL, CHANGE late_creditors_value_rolling late_creditors_value_rolling NUMERIC(24, 12) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE calculate_finance_month CHANGE revenue_rolling revenue_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE revenue_growth_rolling revenue_growth_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE gross_margin_value_rolling gross_margin_value_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE gross_margin_percentage_rolling gross_margin_percentage_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE ebitda_value_rolling ebitda_value_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE ebitda_value_percentage_rolling ebitda_value_percentage_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE interest_cover_rolling interest_cover_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE mrginal_cashflow_rolling mrginal_cashflow_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE trans_journal_completeness_rolling trans_journal_completeness_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE last_gst_rolling last_gst_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE working_capital_ratio_rolling working_capital_ratio_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE quick_ratio_rolling quick_ratio_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE cash_ratio_rolling cash_ratio_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE net_cash_balance_rolling net_cash_balance_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE debit_ratio_rolling debit_ratio_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE debt_leverage_rolling debt_leverage_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE equity_ratio_rolling equity_ratio_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE debitor_days_rolling debitor_days_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE creditor_days_rolling creditor_days_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE stock_days_rolling stock_days_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE defence_internal_ratio_rolling defence_internal_ratio_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE expense_cover_ratio_rolling expense_cover_ratio_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE profit_ebitda_to_cl_rolling profit_ebitda_to_cl_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE operating_cost_ratio_rolling operating_cost_ratio_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE people_cost_ratio_rolling people_cost_ratio_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE outstanding_debtors_rolling outstanding_debtors_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE debtor_concentration_rolling debtor_concentration_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE late_creditors_proportion_rolling late_creditors_proportion_rolling NUMERIC(12, 2) DEFAULT NULL, CHANGE late_creditors_value_rolling late_creditors_value_rolling NUMERIC(12, 2) DEFAULT NULL');
    }
}
