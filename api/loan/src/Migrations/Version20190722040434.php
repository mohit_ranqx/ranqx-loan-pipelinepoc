<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190722040434 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE calculate_finance_month DROP last_gst_rolling, DROP working_capital_ratio_rolling, DROP quick_ratio_rolling, DROP cash_ratio_rolling, DROP net_cash_balance_rolling, DROP debit_ratio_rolling, DROP equity_ratio_rolling');
        $this->addSql('ALTER TABLE calculate_finance_yearly ADD revenue_growth_yearly DOUBLE PRECISION DEFAULT NULL, ADD stock_days_yearly DOUBLE PRECISION DEFAULT NULL, DROP revenue_rolling_yearly, DROP revenue_growth_rolling_yearly, DROP gross_margin_value_rolling_yearly, DROP gross_margin_percentage_rolling_yearly, DROP ebitda_value_rolling_yearly, DROP ebitda_value_percentage_rolling_yearly, DROP interest_cover_rolling_yearly, DROP marginal_cashflow_rolling_yearly, DROP trans_journal_completeness_rolling_yearly, DROP last_gst_rolling_yearly, DROP working_capital_ratio_rolling_yearly, DROP quick_ratio_rolling_yearly, DROP cash_ratio_rolling_yearly, DROP net_cash_balance_rolling_yearly, DROP debit_ratio_rolling_yearly, DROP debit_ratio_rolling_yearly_integer, DROP debt_leverage_rolling_yearly, DROP equity_ratio_rolling_yearly, DROP debitor_days_rolling_yearly, DROP creditor_days_rolling_yearly, DROP stock_days_rolling_yearly, DROP defence_internal_ratio_rolling_yearly, DROP expense_cover_ratio_rolling_yearly, DROP profit_ebitda_to_cl_rolling_yearly, DROP operating_cost_ratio_rolling_yearly, DROP people_cost_ratio_rolling_yearly, DROP outstanding_debtors_rolling_yearly, DROP debtor_concentration_rolling_yearly, DROP late_creditors_proportion_rolling_yearly, DROP late_creditors_value_rolling_yearly');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE calculate_finance_month ADD last_gst_rolling NUMERIC(24, 12) DEFAULT NULL, ADD working_capital_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, ADD quick_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, ADD cash_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, ADD net_cash_balance_rolling NUMERIC(24, 12) DEFAULT NULL, ADD debit_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, ADD equity_ratio_rolling NUMERIC(24, 12) DEFAULT NULL');
        $this->addSql('ALTER TABLE calculate_finance_yearly ADD revenue_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD revenue_growth_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD gross_margin_value_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD gross_margin_percentage_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD ebitda_value_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD ebitda_value_percentage_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD interest_cover_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD marginal_cashflow_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD trans_journal_completeness_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD last_gst_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD working_capital_ratio_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD quick_ratio_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD cash_ratio_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD net_cash_balance_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD debit_ratio_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD debit_ratio_rolling_yearly_integer DOUBLE PRECISION DEFAULT NULL, ADD debt_leverage_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD equity_ratio_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD debitor_days_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD creditor_days_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD stock_days_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD defence_internal_ratio_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD expense_cover_ratio_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD profit_ebitda_to_cl_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD operating_cost_ratio_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD people_cost_ratio_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD outstanding_debtors_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD debtor_concentration_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD late_creditors_proportion_rolling_yearly DOUBLE PRECISION DEFAULT NULL, ADD late_creditors_value_rolling_yearly DOUBLE PRECISION DEFAULT NULL, DROP revenue_growth_yearly, DROP stock_days_yearly');
    }
}
