<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190729005732 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE calculate_finance_month_current (id INT AUTO_INCREMENT NOT NULL, organisation_id INT NOT NULL, revenue_current NUMERIC(24, 12) DEFAULT NULL, gross_margin_value_current NUMERIC(24, 12) DEFAULT NULL, gross_margin_percentage_current NUMERIC(24, 12) DEFAULT NULL, ebitda_value_current NUMERIC(24, 12) DEFAULT NULL, ebitda_margin_current NUMERIC(24, 12) DEFAULT NULL, interest_cover_current NUMERIC(24, 12) DEFAULT NULL, marginal_cashflow_current NUMERIC(24, 12) DEFAULT NULL, working_capital_ratio_current NUMERIC(24, 12) DEFAULT NULL, quick_ratio_current NUMERIC(24, 12) DEFAULT NULL, cash_ratio_current NUMERIC(24, 12) DEFAULT NULL, net_cash_balance_current NUMERIC(24, 12) DEFAULT NULL, debt_ratio_current NUMERIC(24, 12) DEFAULT NULL, equity_ratio_current NUMERIC(24, 12) DEFAULT NULL, people_cost_ratio_current NUMERIC(24, 12) DEFAULT NULL, operating_cost_ratio_current NUMERIC(24, 12) DEFAULT NULL, period_start DATETIME NOT NULL, period_end DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE calculate_finance_current_month');
        $this->addSql('DROP TABLE calculate_finance_month');
        $this->addSql('ALTER TABLE calculate_finance_yearly CHANGE revenue_growth_yearly revenue_growth_yearly NUMERIC(24, 12) DEFAULT NULL, CHANGE stock_days_yearly stock_days_yearly NUMERIC(24, 12) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE calculate_finance_current_month (id INT AUTO_INCREMENT NOT NULL, organisation_id INT NOT NULL, revenue_current INT DEFAULT NULL, gross_margin_value_current NUMERIC(24, 12) DEFAULT NULL, gross_margin_percentage_current NUMERIC(24, 12) DEFAULT NULL, ebitda_value_current NUMERIC(24, 12) DEFAULT NULL, ebitda_margin_current NUMERIC(24, 12) DEFAULT NULL, interest_cover_current NUMERIC(24, 12) DEFAULT NULL, marginal_cashflow_current NUMERIC(24, 12) DEFAULT NULL, working_capital_ratio_current NUMERIC(24, 12) DEFAULT NULL, quick_ratio_current NUMERIC(24, 12) DEFAULT NULL, cash_ratio_current NUMERIC(24, 12) DEFAULT NULL, net_cash_balance_current NUMERIC(24, 12) DEFAULT NULL, debt_ratio_current NUMERIC(24, 12) DEFAULT NULL, people_cost_ratio_current NUMERIC(24, 12) DEFAULT NULL, period_start DATETIME NOT NULL, period_end DATETIME NOT NULL, equity_ratio_current NUMERIC(24, 12) DEFAULT NULL, operating_cost_ratio_current NUMERIC(24, 12) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE calculate_finance_month (id INT AUTO_INCREMENT NOT NULL, organisation_id INT NOT NULL, revenue_rolling NUMERIC(24, 12) DEFAULT NULL, gross_margin_value_rolling NUMERIC(24, 12) DEFAULT NULL, gross_margin_percentage_rolling NUMERIC(24, 12) DEFAULT NULL, ebitda_value_rolling NUMERIC(24, 12) DEFAULT NULL, ebitda_value_percentage_rolling NUMERIC(24, 12) DEFAULT NULL, interest_cover_rolling NUMERIC(24, 12) DEFAULT NULL, marginal_cashflow_rolling NUMERIC(24, 12) DEFAULT NULL, debt_leverage_rolling NUMERIC(24, 12) DEFAULT NULL, debitor_days_rolling NUMERIC(24, 12) DEFAULT NULL, creditor_days_rolling NUMERIC(24, 12) DEFAULT NULL, defence_internal_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, expense_cover_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, operating_cost_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, people_cost_ratio_rolling NUMERIC(24, 12) DEFAULT NULL, period_start DATETIME NOT NULL, period_end DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE calculate_finance_month_current');
        $this->addSql('ALTER TABLE calculate_finance_yearly CHANGE revenue_growth_yearly revenue_growth_yearly DOUBLE PRECISION DEFAULT NULL, CHANGE stock_days_yearly stock_days_yearly DOUBLE PRECISION DEFAULT NULL');
    }
}
