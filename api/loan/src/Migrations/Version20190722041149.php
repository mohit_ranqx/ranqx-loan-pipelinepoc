<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190722041149 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE calculate_finance_month DROP revenue_growth_rolling, DROP trans_journal_completeness_rolling, DROP stock_days_rolling, DROP profit_ebitda_to_cl_rolling, DROP outstanding_debtors_rolling, DROP debtor_concentration_rolling, DROP late_creditors_proportion_rolling, DROP late_creditors_value_rolling');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE calculate_finance_month ADD revenue_growth_rolling NUMERIC(24, 12) DEFAULT NULL, ADD trans_journal_completeness_rolling NUMERIC(24, 12) DEFAULT NULL, ADD stock_days_rolling NUMERIC(24, 12) DEFAULT NULL, ADD profit_ebitda_to_cl_rolling NUMERIC(24, 12) DEFAULT NULL, ADD outstanding_debtors_rolling NUMERIC(24, 12) DEFAULT NULL, ADD debtor_concentration_rolling NUMERIC(24, 12) DEFAULT NULL, ADD late_creditors_proportion_rolling NUMERIC(24, 12) DEFAULT NULL, ADD late_creditors_value_rolling NUMERIC(24, 12) DEFAULT NULL');
    }
}
