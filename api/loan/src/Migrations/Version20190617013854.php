<?php

    declare(strict_types=1);

    namespace DoctrineMigrations;

    use Doctrine\DBAL\Schema\Schema;
    use Doctrine\Migrations\AbstractMigration;

    /**
     * Auto-generated Migration: Please modify to your needs!
     */
    final class Version20190617013854 extends AbstractMigration
    {
        public function getDescription() : string
        {
            return '';
        }

        public function up(Schema $schema) : void
        {
            // this up() migration is auto-generated, please modify it to your needs
            $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

            $this->addSql('CREATE TABLE calculation_errors_monthly (id INT AUTO_INCREMENT NOT NULL, calulate_finance_month_id INT NOT NULL, orginisation_id INT NOT NULL, calculation_name VARCHAR(255) DEFAULT NULL, calculation_value INT DEFAULT NULL, error_message VARCHAR(255) DEFAULT NULL, created DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
            $this->addSql('CREATE TABLE calculation_errors_yearly (id INT AUTO_INCREMENT NOT NULL, calulate_finance_yearly_id INT NOT NULL, orginisation_id INT NOT NULL, calculation_name VARCHAR(255) DEFAULT NULL, calculation_value VARCHAR(255) DEFAULT NULL, error_message VARCHAR(255) DEFAULT NULL, created DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        }

        public function down(Schema $schema) : void
        {
            // this down() migration is auto-generated, please modify it to your needs
            $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

            $this->addSql('DROP TABLE calculation_errors_monthly');
            $this->addSql('DROP TABLE calculation_errors_yearly');
        }
    }
