<?php

    declare(strict_types=1);

    namespace DoctrineMigrations;

    use Doctrine\DBAL\Schema\Schema;
    use Doctrine\Migrations\AbstractMigration;

    /**
     * Auto-generated Migration: Please modify to your needs!
     */
    final class Version20190606050019 extends AbstractMigration
    {
        public function getDescription() : string
        {
            return '';
        }

        public function up(Schema $schema) : void
        {
            // this up() migration is auto-generated, please modify it to your needs
            $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

            $this->addSql('CREATE TABLE finance (id INT AUTO_INCREMENT NOT NULL, organisation_id INT DEFAULT NULL, period DATETIME NOT NULL, raw_data LONGTEXT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, currency VARCHAR(255) NOT NULL, total_revenue DOUBLE PRECISION NOT NULL, total_cog DOUBLE PRECISION NOT NULL, total_people DOUBLE PRECISION NOT NULL, total_rent DOUBLE PRECISION NOT NULL, total_operating DOUBLE PRECISION NOT NULL, total_other DOUBLE PRECISION NOT NULL, total_assets_current_bank DOUBLE PRECISION NOT NULL, total_assets_current_debtors DOUBLE PRECISION NOT NULL, total_assets_current_inventory DOUBLE PRECISION NOT NULL, total_assets_current_other DOUBLE PRECISION NOT NULL, total_assets_fixed DOUBLE PRECISION NOT NULL, total_assets_other DOUBLE PRECISION NOT NULL, total_liabilities_current_bank DOUBLE PRECISION NOT NULL, total_liabilities_current_creditors DOUBLE PRECISION NOT NULL, total_liabilities_current_other DOUBLE PRECISION NOT NULL, total_liabilities_other DOUBLE PRECISION NOT NULL, total_equity_capital DOUBLE PRECISION NOT NULL, total_equity_earnings DOUBLE PRECISION NOT NULL, staff_number DOUBLE PRECISION NOT NULL, INDEX IDX_CE28EAE09E6B1585 (organisation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
            $this->addSql('CREATE TABLE finance_month (id INT AUTO_INCREMENT NOT NULL, organisation_id INT DEFAULT NULL, period DATETIME NOT NULL, raw_data LONGTEXT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, currency VARCHAR(255) NOT NULL, total_revenue DOUBLE PRECISION NOT NULL, total_cog DOUBLE PRECISION NOT NULL, total_people DOUBLE PRECISION NOT NULL, total_rent DOUBLE PRECISION NOT NULL, total_operating DOUBLE PRECISION NOT NULL, total_other DOUBLE PRECISION NOT NULL, total_assets_current_bank DOUBLE PRECISION NOT NULL, total_assets_current_debtors DOUBLE PRECISION NOT NULL, total_assets_current_inventory DOUBLE PRECISION NOT NULL, total_assets_current_other DOUBLE PRECISION NOT NULL, total_assets_fixed DOUBLE PRECISION NOT NULL, total_assets_other DOUBLE PRECISION NOT NULL, total_liabilities_current_bank DOUBLE PRECISION NOT NULL, total_liabilities_current_creditors DOUBLE PRECISION NOT NULL, total_liabilities_current_other DOUBLE PRECISION NOT NULL, total_liabilities_other DOUBLE PRECISION NOT NULL, total_equity_capital DOUBLE PRECISION NOT NULL, total_equity_earnings DOUBLE PRECISION NOT NULL, staff_number DOUBLE PRECISION NOT NULL, INDEX IDX_8E1904FD9E6B1585 (organisation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
            $this->addSql('CREATE TABLE organisation (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, legal_name VARCHAR(255) NOT NULL, pays_tax TINYINT(1) NOT NULL, provider_name VARCHAR(255) NOT NULL, organisation_type VARCHAR(32) NOT NULL, version VARCHAR(8) NOT NULL, base_currency VARCHAR(4) NOT NULL, country_code VARCHAR(2) NOT NULL, is_demo_company TINYINT(1) NOT NULL, organisation_status VARCHAR(8) NOT NULL, financial_year_end_day INT NOT NULL, financial_year_end_month INT NOT NULL, sales_tax_basis VARCHAR(32) NOT NULL, default_sales_tax VARCHAR(32) NOT NULL, default_purchases_tax VARCHAR(32) NOT NULL, created_in_xero DATETIME NOT NULL, organisation_entity_type VARCHAR(16) NOT NULL, organisation_id VARCHAR(64) NOT NULL, edition VARCHAR(16) NOT NULL, xero_class VARCHAR(16) NOT NULL, created DATETIME NOT NULL, address_address_line1 VARCHAR(255) NOT NULL, address_address_line2 VARCHAR(255) NOT NULL, address_city VARCHAR(255) NOT NULL, address_postal_code VARCHAR(16) NOT NULL, address_country VARCHAR(255) NOT NULL, address_address_type VARCHAR(16) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
            $this->addSql('ALTER TABLE finance ADD CONSTRAINT FK_CE28EAE09E6B1585 FOREIGN KEY (organisation_id) REFERENCES organisation (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE finance_month ADD CONSTRAINT FK_8E1904FD9E6B1585 FOREIGN KEY (organisation_id) REFERENCES organisation (id) ON DELETE CASCADE');
        }

        public function down(Schema $schema) : void
        {
            // this down() migration is auto-generated, please modify it to your needs
            $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

            $this->addSql('ALTER TABLE finance DROP FOREIGN KEY FK_CE28EAE09E6B1585');
            $this->addSql('ALTER TABLE finance_month DROP FOREIGN KEY FK_8E1904FD9E6B1585');
            $this->addSql('DROP TABLE finance');
            $this->addSql('DROP TABLE finance_month');
            $this->addSql('DROP TABLE organisation');
        }
    }
