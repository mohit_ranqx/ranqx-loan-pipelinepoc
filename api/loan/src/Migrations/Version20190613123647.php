<?php

    declare(strict_types=1);

    namespace DoctrineMigrations;

    use Doctrine\DBAL\Schema\Schema;
    use Doctrine\Migrations\AbstractMigration;

    /**
     * Auto-generated Migration: Please modify to your needs!
     */
    final class Version20190613123647 extends AbstractMigration
    {
        public function getDescription() : string
        {
            return '';
        }

        public function up(Schema $schema) : void
        {
            // this up() migration is auto-generated, please modify it to your needs
            $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

            $this->addSql('CREATE TABLE calculate_finance_month (id INT AUTO_INCREMENT NOT NULL, orginisation_id INT NOT NULL, revenue_rolling INT DEFAULT NULL, revenue_growth_rolling INT DEFAULT NULL, gross_margin_value_rolling INT DEFAULT NULL, gross_margin_percentage_rolling INT DEFAULT NULL, ebitda_value_rolling INT DEFAULT NULL, ebitda_value_percentage_rolling INT DEFAULT NULL, interest_cover_rolling INT DEFAULT NULL, mrginal_cashflow_rolling INT DEFAULT NULL, trans_journal_completeness_rolling INT DEFAULT NULL, last_gst_rolling INT DEFAULT NULL, working_capital_ratio_rolling INT DEFAULT NULL, quick_ratio_rolling INT DEFAULT NULL, cash_ratio_rolling INT DEFAULT NULL, net_cash_balance_rolling INT DEFAULT NULL, debit_ratio_rolling INT DEFAULT NULL, debt_leverage_rolling INT DEFAULT NULL, equity_ratio_rolling INT DEFAULT NULL, debitor_days_rolling INT DEFAULT NULL, creditor_days_rolling INT DEFAULT NULL, stock_days_rolling INT DEFAULT NULL, defence_internal_ratio_rolling INT DEFAULT NULL, expense_cover_ratio_rolling INT DEFAULT NULL, profit_ebitda_to_cl_rolling INT DEFAULT NULL, operating_cost_ratio_rolling INT DEFAULT NULL, people_cost_ratio_rolling INT DEFAULT NULL, outstanding_debtors_rolling INT DEFAULT NULL, debtor_concentration_rolling INT DEFAULT NULL, late_creditors_proportion_rolling INT DEFAULT NULL, late_creditors_value_rolling INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        }

        public function down(Schema $schema) : void
        {
            // this down() migration is auto-generated, please modify it to your needs
            $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

            $this->addSql('DROP TABLE calculate_finance_month');
        }
    }
