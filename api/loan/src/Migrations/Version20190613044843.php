<?php

    declare(strict_types=1);

    namespace DoctrineMigrations;

    use Doctrine\DBAL\Schema\Schema;
    use Doctrine\Migrations\AbstractMigration;

    /**
     * Auto-generated Migration: Please modify to your needs!
     */
    final class Version20190613044843 extends AbstractMigration
    {
        public function getDescription() : string
        {
            return '';
        }

        public function up(Schema $schema) : void
        {
            // this up() migration is auto-generated, please modify it to your needs
            $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

            $this->addSql('ALTER TABLE Applicant ADD loan_reason LONGTEXT DEFAULT NULL, ADD nzbn_number VARCHAR(64) DEFAULT NULL, ADD is_customer TINYINT(1) NOT NULL, ADD accepted TINYINT(1) NOT NULL, CHANGE business_name business_name VARCHAR(256) DEFAULT NULL, CHANGE loan_amount loan_amount NUMERIC(19, 4) DEFAULT NULL');
        }

        public function down(Schema $schema) : void
        {
            // this down() migration is auto-generated, please modify it to your needs
            $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

            $this->addSql('ALTER TABLE Applicant DROP loan_reason, DROP nzbn_number, DROP is_customer, DROP accepted, CHANGE business_name business_name VARCHAR(256) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE loan_amount loan_amount NUMERIC(19, 4) NOT NULL');
        }
    }
