<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190723010259 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE calculate_finance_current_month ADD equity_ratio_current NUMERIC(24, 12) DEFAULT NULL, ADD operating_cost_ratio_current NUMERIC(24, 12) DEFAULT NULL, DROP debt_leverage_current, DROP debitor_days_current');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE calculate_finance_current_month ADD debt_leverage_current NUMERIC(24, 12) DEFAULT NULL, ADD debitor_days_current NUMERIC(24, 12) DEFAULT NULL, DROP equity_ratio_current, DROP operating_cost_ratio_current');
    }
}
