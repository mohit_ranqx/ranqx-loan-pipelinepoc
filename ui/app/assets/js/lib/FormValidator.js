import validator from "react-validation";

class FormValidator {
    state = {};

    validation(state, inputs) {
        this.state = state;

        // Reset validation state
        this.state.isInvalid = false;

        // Check if only validating one input
        // If only one input put into array to
        // validate in next step
        if (inputs && !Array.isArray(inputs)) {
            inputs = [inputs];
        }

        // Validate multiple inputs
        if (inputs) {
            inputs.forEach(input => {
                let member = this.state[input];

                if (!member.validations) {
                    return;
                }

                this.state[input] = this.validate(member);
            });

            return this.state;
        }

        // Validate all inputs
        Object.keys(this.state).forEach((index) => {
            let member = this.state[index];

            if (!member || !member.validations) {
                return;
            }

            this.state[index] = this.validate(member);
        });

        return this.state;
    }

    validate(member) {
        // Reset member validation state
        member.isStarted = true;
        member.isInvalid = false;
        member.message = "";

        member.validations.forEach(validation => {
            // if the field isn't already marked invalid by an earlier rule
            if (!member.isInvalid) {
                const allowedFieldTypes = ["string", "number"];
                const fieldValue = allowedFieldTypes.includes(typeof member.value) ? member.value.toString() : "";
                const args = validation.args || [];
                const validation_method = typeof validation.method === "string" ?
                    validator[validation.method] :
                    validation.method;

                // Decode optional arguments
                let decodedArgs = [];
                args.forEach(arg => {
                    if (typeof arg === "function") {
                        decodedArgs.push(arg(this.state));

                        return;
                    }

                    decodedArgs.push(arg);
                });

                // call the validation_method with the current field value
                // as the first argument, any additional arguments, and the
                // whole state as a final argument.  If the result doesn't
                // match the rule.validWhen property, then modify the
                // validation object for the field and set the isValid
                // field to false
                if (validation_method(fieldValue, ...decodedArgs, this.state) !== validation.validWhen) {
                    member.isInvalid = true;
                    member.message = validation.message;
                    this.state.isInvalid = true;
                }
            }
        });

        return member;
    }
}
export default FormValidator;