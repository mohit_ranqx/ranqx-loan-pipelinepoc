import React  from "react";
import {Box, Typography} from "@material-ui/core";
import LoanOriginationModal from "../components/LoanOrigination/LoanOriginationModal";
import StylesProvider from "@material-ui/styles/StylesProvider";

import styles from "./errorBoundary.less";
import Cookies from "universal-cookie";

class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    componentDidCatch(error, info) {
        this.setState({
            hasError: true,
            error: error,
            info: info,
        });

        console.log(error + " " + info)
    }

    render() {
        const {
            hasError
        } = this.state;

        if (hasError) {
            return <LoanOriginationModal>
                <StylesProvider injectFirst>
                    <Box className={styles.loanOriginationContainer__titleContainer}>
                        <Typography
                            className={`${styles.formTitle} ${styles.loanOriginationContainer__titleContainer_FormTitle}`}
                            variant="h1">
                            <span>Opps something went wrong</span>
                        </Typography>
                    </Box>

                    <Box className={styles.loanOriginationContainer__content} display="flex" alignItems="center">
                        <Typography
                            className={styles.typography}
                            variant="body1"
                        >
                            <span>Looks like something went wrong, try reloading the page or loading it in another browser. Or contact Kiwibank by calling <a href="tel:0800 601 601">0800 601 601</a>.</span>
                        </Typography>
                    </Box>
                </StylesProvider>
            </LoanOriginationModal>
        } else {
            return this.props.children;
        }
    }
}

export default ErrorBoundary;