import React, { Component } from "react";
import ReactDom from "react-dom";
import { MemoryRouter, Route } from "react-router";
import LoanTerms from "./components/LoanOrigination/LoanTerms";
import LoanForm from "./components/LoanOrigination/LoanForm";
import LoanIntegration from "./components/LoanOrigination/LoanIntegration";
import { StylesProvider } from "@material-ui/styles";
import ErrorBoundary from "./lib/ErrorBoundary";
import LoanOriginationModal from "./components/LoanOrigination/LoanOriginationModal";
import LoanNotification from "./components/LoanOrigination/LoanNotification";

class LoanOrigination extends Component {
    targetRef = React.createRef();
    targetElement = null;
    containerElement = null;
    apiURL = "{{{API_URL}}}";

    constructor(props) {
        super(props);

        this.state = {
            showForm: false,
            isComplete: false,
        };
    }

    displayForm() {
        // disabled website scroll.footerMiddleLine
        document.body.style.cssText = document.documentElement.style.cssText = "overflow: hidden; height: 100%; margin: 0; padding: 0;";
        disableBodyScroll(this.targetElement);

        this.setState({
            showForm: true,
        });
    }

    componentDidMount() {
        this.containerElement = document.getElementById(this.props.config.id);
        this.targetElement = this.targetRef.current;
    }

    forceTop() {
        document.getElementsByClassName("loanOriginationContainer__modalScroll")[0].scroll(0, 0);
    }

    render() {
        return <ErrorBoundary>
                <LoanOriginationModal targetElement={this.targetElement} containerElement={this.containerElement}>
                    <StylesProvider injectFirst>
                        <MemoryRouter onChange={this.forceTop}>
                            <Route exact path="/" component={LoanTerms} />
                            <Route path="/applicant" render={(props) => <LoanForm token={this.props.config.token} hash={this.props.config.hash} apiUrl={this.apiURL} {...props} /> } />
                            <Route path="/integration" render={(props) => <LoanIntegration token={this.props.config.token} hash={this.props.config.hash} apiUrl={this.apiURL} {...props} /> } />
                            <Route path="/notification" render={(props) => <LoanNotification {...props} /> } />
                        </MemoryRouter>
                    </StylesProvider>
                </LoanOriginationModal>
            </ErrorBoundary>
    }
}

export const init = (loanOriginationConfig) => {
    ReactDom.render(<LoanOrigination config={loanOriginationConfig} />, document.getElementById(loanOriginationConfig.id));
};