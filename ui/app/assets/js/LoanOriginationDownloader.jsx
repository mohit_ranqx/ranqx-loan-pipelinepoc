
import React, {Component} from "react";
import ReactDom from "react-dom";
import Cookies from "universal-cookie";
import LoanOriginationNotificationModal from "./components/LoanOrigination/LoanOriginationNotificationModal";

class LoanOriginationDownloader {
    downloadURL = "{{{API_URL}}}" + "v1/weblet/download?v=";
    id = "";
    isSuccess = false;
    token = null;
    hash = null;
    lazyLoad = false;

    init(params) {
        const loadDownloader = this;

        if (typeof params !== "object") {
            return false;
        }

        loadDownloader.id = params.id;
        loadDownloader.isSuccess = params.isSuccess;

        if (!loadDownloader.lazyLoad) {
            this.load(function (token, hash) {
                loadDownloader.token = token;
                loadDownloader.hash = hash;
            });
        }

        return loadDownloader;
    }

    load(callback) {
        const xmlHttpRequest = new XMLHttpRequest();
        xmlHttpRequest.withCredentials = false;
        xmlHttpRequest.open("GET", this.downloadURL + process.env.APPLICATION_VERSION, true);

        xmlHttpRequest.onloadstart = function () {
            xmlHttpRequest.responseType = "text";
        };

        xmlHttpRequest.onload = function () {
            if(xmlHttpRequest.status === 200) {
                const token = xmlHttpRequest.getResponseHeader("token");
                const hash = xmlHttpRequest.getResponseHeader("hash");

                let script = document.createElement("script");
                script.type = "text/javascript";
                script.text = xmlHttpRequest.responseText;

                document.head.appendChild(script);

                if (typeof callback === "function") {
                    callback(token, hash);
                }
            }
        };

        xmlHttpRequest.send();

        return true;
    }

    show() {
        const id = this.id;

        if (this.lazyLoad) {
            return this.load(function (token, hash) {
                LoanOrigination.init({
                    id: id,
                    token: token,
                    hash: hash,
                });
            });
        }

        LoanOrigination.init({
            id: id,
            token: this.token,
            hash: this.hash,
        });
    }
}

export const init = (params) => {
    const loanOriginationDownloader = new LoanOriginationDownloader();

    // Check if this is a redirect back from Xero
    const cookies = new Cookies();
    const id = cookies.get("lo_id");

    if (id !== undefined) {
        let now = new Date();
        let time = now.getTime();
        let expireTime = time + 1800;
        now.setTime(expireTime);

        const cookieOptions = {
            maxAge: 1800,
            domain: window.location.hostname,
            expireTime: now.toUTCString(),
            secure: true,
            httpOnly: false,
            sameSite: "lax"
        };

        const ieCompatability = [
            "Netscape",
            "Microsoft Internet Explorer",
        ];

        if (ieCompatability.indexOf(navigator.appName) === -1) {
            cookieOptions.path = window.location.pathname;
        }

        cookies.remove("lo_id", cookieOptions);

        params.isSuccess = true;

        const webletContainer =  document.getElementById(params.id);

        if (!webletContainer) {
            console.log("The specified weblet is not found");
        }

        ReactDom.render(<LoanOriginationNotificationModal/>, webletContainer);
    }

    return loanOriginationDownloader.init(params);
};