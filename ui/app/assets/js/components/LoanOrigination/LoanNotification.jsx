
import React from "react";
import {CircularProgress, Typography} from "@material-ui/core";
import "typeface-roboto";
import Box from "@material-ui/core/Box";
import {Help, CheckCircleOutline} from "@material-ui/icons";
import Cookies from "universal-cookie";

/** Styles **/
import styles from "./LoanNotification.less";

class LoanNotification extends React.Component {
    integrationSuccess = false;

    constructor(props) {
        super(props);

        const cookies = new Cookies();
        const referenceNumber = cookies.get("lo_referenceNumber");


        this.integrationSuccess = Boolean(this.getParameterByName("loXeroSuccess"));

        if (referenceNumber !== undefined) {
            let now = new Date();
            let time = now.getTime();
            let expireTime = time + 1800;
            now.setTime(expireTime);

            const cookieOptions = {
                maxAge: 1800,
                domain: window.location.hostname,
                expireTime: now.toUTCString(),
                secure: true,
                httpOnly: false,
                sameSite: "lax"
            };

            const ieCompatability = [
                "Netscape",
                "Microsoft Internet Explorer",
            ];

            if (ieCompatability.indexOf(navigator.appName) === -1) {
                cookieOptions.path = window.location.pathname;
            }

            cookies.remove("lo_referenceNumber", cookieOptions);
        }

        this.state = {
            "referenceNumber": referenceNumber,
        }
    }

    getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return "";
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    render() {
        return (
            <Box className={`${styles.successContainer} ${styles.loanOriginationContainer__content}`}>
                { this.integrationSuccess ? (
                    <CheckCircleOutline className={styles.successContainer_successIcon} />
                ) : (
                    <Help className={styles.successContainer_lightFailIcon} />
                )}

                <br />
                <br />

                <Typography
                    className={styles.formTitle}
                    variant="h4">
                    { this.integrationSuccess ? (
                        <span>Great, we're done.</span>
                    ) : (
                        <span>Oops we missed something...</span>
                    )}
                </Typography>

                <br />
                <br />


                <Typography
                    className={styles.typography}
                    variant="body1">
                    { this.integrationSuccess ? (
                        <span>
                            Your application is now being processed.
                            <br />
                            We'll email you with more information shortly, so please check your inbox (and spam folder).
                        </span>

                    ) : (
                        <span>
                            We might not have been able to get all the information we need to finalise your application but don’t worry, we’ll be in touch with you soon.
                        </span>
                    )}
                </Typography>

                <br />

                <Typography
                    className={styles.typography}
                    variant="body1">
                    { this.state.referenceNumber ? (
                        <span>Your application number is {this.state.referenceNumber}</span>
                    ) : (
                        <span>Please check your inbox for your application number.</span>
                    )}
                </Typography>

                <br />

                <Typography
                    className={styles.typography}
                    variant="body1"
                >
                    <a className={styles.loanOrigination__link} href="https://www.kiwibank.co.nz/business-banking/">Back to Kiwibank business banking</a>
                </Typography>

                <br />
                <br />
            </Box>
        );
    }
}

export default LoanNotification;