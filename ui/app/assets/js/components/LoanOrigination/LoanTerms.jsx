
import React from "react";
import ArrowRight from "@material-ui/icons/ArrowRight";
import { Divider, Typography, Grid, Checkbox, Box, Button, Tooltip, } from "@material-ui/core";
import "typeface-roboto";
import { withRouter } from "react-router-dom";
import HelpOutline from "@material-ui/icons/HelpOutline";

/** Styles **/
import styles from "./loanTerms.less";

class LoanTerms extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            authorisedIdentityAndCompany: false,
            authorisedThirdParties: false,
            authorisedAccountingService: false,
            invalidAuthorisation: false,
            authorisedXeroPermissions: false,
        };

        this.changeAuthorisedIdentityAndCompany = this.changeAuthorisedIdentityAndCompany.bind(this);
        this.changeAuthorisedThirdParties = this.changeAuthorisedThirdParties.bind(this);
        this.changeAuthorisedAccountingService = this.changeAuthorisedAccountingService.bind(this);
        this.changeAuthorisedXeroPermissions = this.changeAuthorisedXeroPermissions.bind(this);
    }

    componentDidMount() {
        //styles.use();
    }

    componentWillUnmount() {
        //styles.unuse();
    }

    changeAuthorisedIdentityAndCompany(event, authorised) {
        this.setState({
            authorisedIdentityAndCompany: authorised,
        });
    }

    changeAuthorisedThirdParties(event, authorised) {
        this.setState({
            authorisedThirdParties: authorised,
        });
    }

    changeAuthorisedAccountingService(event, authorised) {
        this.setState({
            authorisedAccountingService: authorised,
        });
    }

    changeAuthorisedXeroPermissions(event, authorised) {
        this.setState({
            authorisedXeroPermissions: authorised,
        });
    }

    isInvalidAuthorisation() {
        return this.state.invalidAuthorisation ? "authorisationCheckboxShake" : "";
    }

    isAuthorised() {
        const {
            authorisedIdentityAndCompany,
            authorisedThirdParties,
            authorisedAccountingService,
            authorisedXeroPermissions,
        } = this.state;

        return !!(authorisedIdentityAndCompany &&
            authorisedThirdParties &&
            authorisedAccountingService &&
            authorisedXeroPermissions
        );
    }

    continue() {
        if (this.isAuthorised()) {
            this.props.history.push("applicant");

            return;
        }

        this.setState({
            invalidAuthorisation: true,
        });
    }

    render() {
        return (
            <Box>
                <Box className={`${styles.loanTermsForm} ${styles.loanOriginationContainer__content}`} display="flex" alignItems="center">
                    <form noValidate autoComplete="off">
                        <Typography
                            className={styles.typography}
                            variant="body1"
                        >
                            We're on a mission to help Kiwi businesses grow - whether you want more working capital,
                            to invest in inventory, fund receivables or to hire more staff.
                            The application process is simpler than ever and takes just a few minutes.
                        </Typography>

                        <br />

                        <Typography
                            className={styles.formSubject}
                            variant="h6">
                            First we need to know:
                        </Typography>

                        <Grid
                            className={styles.loanOrigination__container_bottomMargin}
                            container
                            component="div"
                        >
                            <Grid xl={1} lg={1} md={1} sm={2} xs={2} component="div" item={true}>
                                <Checkbox
                                    className={styles.loanOrgination__checkbox}
                                    value="authorise"
                                    onChange={this.changeAuthorisedIdentityAndCompany}
                                />
                            </Grid>

                            <Grid xl={11} lg={11} md={11} sm={10} xs={10} component="div" item={true}>
                                <Typography
                                    className={styles.typography}
                                    variant="body1"
                                >
                                    You're a director of the New Zealand registered company that would like a Business Overdraft from us,
                                    you're acting on behalf of that company and are authorised to do so.
                                </Typography>
                            </Grid>
                        </Grid>

                        <Grid
                            className={styles.loanOrigination__container_bottomMargin}
                            container
                            component="div"
                        >
                            <Grid xl={1} lg={1} md={1} sm={2} xs={2} component="div" item={true}>
                                <Checkbox
                                    className={styles.loanOrgination__checkbox}
                                    value="authorise"
                                    onChange={this.changeAuthorisedThirdParties}
                                />
                            </Grid>

                            <Grid xl={11} lg={11} md={11} sm={10} xs={10} component="div" item={true}>
                                <Typography
                                    className={styles.typography}
                                    variant="body1"
                                >
                                    You authorise Kiwibank and our third-party providers to collect information about your company from sources we will use,
                                    including Xero, to process your application
                                </Typography>
                            </Grid>
                        </Grid>

                        <Grid
                            className={styles.loanOrigination__container_bottomMargin}
                            container
                            component="div"
                        >
                            <Grid xl={1} lg={1} md={1} sm={2} xs={2} component="div" item={true}>
                                <Checkbox
                                    className={styles.loanOrgination__checkbox}
                                    value="authorise"
                                    onChange={this.changeAuthorisedAccountingService}
                                />
                            </Grid>

                            <Grid xl={11} lg={11} md={11} sm={10} xs={10} component="div" item={true}>
                                <Typography
                                    className={styles.typography}
                                    variant="body1"
                                >
                                    Your company uses the Xero online accounting system and all of your company's data is accurate,
                                    complete and up-to-date.
                                </Typography>
                            </Grid>
                        </Grid>

                        <Grid
                            className={styles.loanOrigination__container_bottomMargin}
                            container
                            component="div"
                        >
                            <Grid xl={1} lg={1} md={1} sm={2} xs={2} component="div" item={true}>
                                <Checkbox
                                    className={styles.loanOrgination__checkbox}
                                    value="authorise"
                                    onChange={this.changeAuthorisedXeroPermissions}
                                />
                            </Grid>

                            <Grid xl={11} lg={11} md={11} sm={10} xs={10} component="div" item={true}>
                                <Typography
                                    className={styles.typography}
                                    variant="body1"
                                >
                                    You have the Standard Subscriber or Adviser role in Xero that allows you to manage connected apps.
                                    <Tooltip
                                        leaveTouchDelay={1500}
                                        interactive={true}
                                        title="The Xero Standard Subscriber and Adviser roles are assigned the permission to &quot;set up or disconnect connected apps&quot; by default, and you must have either to continue with the application.  If you're not sure which role you have ask your Xero Administrator. or visit MyXero and check the Access column for each Organisation you have access to.">
                                        <HelpOutline className={styles.tooltipIcon} />
                                    </Tooltip>
                                </Typography>
                            </Grid>
                        </Grid>

                        <Divider component="hr" />

                        <Button
                            className={styles.loanOrigination__button_primary}
                            variant="contained"
                            color="primary"
                            disabled={!this.isAuthorised()}
                            onClick={() => this.continue()}
                        >
                            Let's get started <ArrowRight />
                        </Button>

                        <br />
                    </form>
                </Box>
            </Box>
        );
    }
}

export default withRouter(LoanTerms);