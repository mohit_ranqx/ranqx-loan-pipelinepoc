import React, {Component} from "react";
import ErrorBoundary from "../../lib/ErrorBoundary";
import {StylesProvider} from "@material-ui/styles";
import LoanNotification from "./LoanNotification";
import LoanOriginationModal from "./LoanOriginationModal";

class LoanOriginationNotificationModal extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ErrorBoundary>
                <LoanOriginationModal>
                    <StylesProvider injectFirst>
                        <LoanNotification />
                    </StylesProvider>
                </LoanOriginationModal>
            </ErrorBoundary>
        );
    }
}

export default LoanOriginationNotificationModal;