
import React from "react";
import {
    TextField,
    Select,
    MenuItem,
    Button,
    FormControl,
    Typography,
    Grid,
    CircularProgress,
    Box,
    Slider,
    Modal,
} from "@material-ui/core";
import Cookies from "universal-cookie";
import FormValidator from "../../lib/FormValidator";
import "typeface-roboto";
import { withRouter } from "react-router-dom";
import validator from "validator"
import axios from "axios";
import AutoSuggest from "react-autosuggest";

// Icons
import ArrowRight from "@material-ui/icons/ArrowRight";
import ValidationIcon from "../Icons/ValidationIcon";
import ValidationMessage from "../Icons/ValidationMessage";

/** Styles **/
import styles from "./loanForm.less";

class LoanForm extends React.Component {
    loanEndpoint = this.props.apiUrl + "v1/kb/save/applicant";
    minimumLoanAmount = 0;
    maximumLoanAmount = 100000;
    stepsLoanAmount = 5000;
    loanReasonList = [
        "Working Capital",
        "Help Cash Flow",
        "Buy Inventory",
        "Add More Staff",
        "Short Term Overdraft",
        "Pay Creditors",
    ];
    salutationList = [
        "Mr",
        "Miss",
        "Mrs",
        "Ms",
        "Dr",
    ];
    businessRecordsEndpoint = this.props.apiUrl + "v1/nzbn/search";
    businessSearchTimer = null;
    businessSearchTypingInterval = 1000;
    businessSearchMinimumLength = 2;
    loanAmountChange = null;
    loading = null;
    requestConfig = {
        headers: {
            hash: this.props.hash,
            token: this.props.token,
        },
    };

    constructor(props) {
        super(props);

        const form = this;

        form.state = {
            loanId: null,
            formStarted: false,
            formLoading: false,
            validation: [],
            salutation: {
                value: "",
                isStarted: false,
                isInvalid: false,
                validations: [
                    {
                        method: validator.isEmpty,
                        validWhen: false,
                        message: "Please select a title.",
                    },
                ]
            },
            firstName: {
                value: "",
                isStarted: false,
                isInvalid: false,
                validations: [
                    {
                        method: validator.isEmpty,
                        validWhen: false,
                        message: "Please provide a first name.",
                    },
                ]
            },
            lastName: {
                value: "",
                isStarted: false,
                isInvalid: false,
                validations: [
                    {
                        method: validator.isEmpty,
                        validWhen: false,
                        message: "Please provide a last name.",
                    },
                ]
            },
            email: {
                value: "",
                isStarted: false,
                isInvalid: false,
                validations: [
                    {
                        method: validator.isEmpty,
                        validWhen: false,
                        message: "Please provide an email address.",
                    },
                    {
                        method: validator.isEmail,
                        validWhen: true,
                        message: "Please provide a valid email.",
                    },
                ]
            },
            confirmEmail: {
                value: "",
                isStarted: false,
                isInvalid: false,
                validations: [
                    {
                        method: validator.isEmpty,
                        validWhen: false,
                        message: "Please provide an email address.",
                    },
                    {
                        method: validator.isEmail,
                        validWhen: true,
                        message: "Please provide a valid email.",
                    },
                    {
                        method: validator.equals,
                        validWhen: true,
                        message: "Email addresses do not match.",
                        args: [
                            function (state) { return state.email.value },
                        ],
                    },
                ]
            },
            phone: {
                value: "",
                isStarted: false,
                isInvalid: false,
                validations: [
                    {
                        method: validator.isEmpty,
                        validWhen: false,
                        message: "Please provide a phone number.",
                    },
                ]
            },
            businessName: {
                value: "",
                isStarted: false,
                isInvalid: false,
                validations: [
                    {
                        method: validator.isEmpty,
                        validWhen: false,
                        message: "Please provide a company name.",
                    },
                ]
            },
            loanAmount: {
                value: 0,
                isStarted: false,
                isInvalid: false,
                validations: [
                    {
                        method: validator.isEmpty,
                        validWhen: false,
                        message: "Please select a loan amount.",
                    },
                    {
                        method: function(field_value) { return field_value < 5000; },
                        validWhen: false,
                        message: "Loan must be a minimum of $5000.",
                    },
                ]
            },
            loanReason: {
                value: "",
                isStarted: false,
                isInvalid: false,
                validations: [
                    {
                        method: validator.isEmpty,
                        validWhen: false,
                        message: "Please select a loan reason.",
                    },
                ]
            },
            businessRecords: [],
            nzbnNumber: {
                value: "",
                validations: [
                    {
                        method: validator.isEmpty,
                        validWhen: false,
                        message: "Please select a company from the company name search dropdown."
                    },
                ]
            },
            businessSearchModalOpen: false,
            businessSearchOpen: false,
            businessResultsLoading: false,
        };

        form.formValidator = new FormValidator();

        this.changeLoanAmount = this.changeLoanAmount.bind(this);
        this.changeLoanReason = this.changeLoanReason.bind(this);
        this.changeFirstName = this.changeFirstName.bind(this);
        this.changeLastName = this.changeLastName.bind(this);
        this.changeEmail = this.changeEmail.bind(this);
        this.changePhone = this.changePhone.bind(this);
        this.changeBusinessName = this.changeBusinessName.bind(this);
        this.interactionEnd = this.interactionEnd.bind(this);
        this.validate = this.validate.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.changeSalutation = this.changeSalutation.bind(this);
        this.changeConfirmEmail = this.changeConfirmEmail.bind(this);
        this.openBusinessSearchModal = this.openBusinessSearchModal.bind(this);
        this.closeBusinessSearchModal = this.closeBusinessSearchModal.bind(this);
        this.changeBusinessSearchModal = this.changeBusinessSearchModal.bind(this);
        this.blurBusinessRecord = this.blurBusinessRecord.bind(this);
        this.focusBusinessRecord = this.focusBusinessRecord.bind(this);
    }

    openBusinessSearchModal() {
        this.changeBusinessSearchModal(true);
    }

    closeBusinessSearchModal() {
        this.changeBusinessSearchModal(false);
    }

    changeBusinessSearchModal(open) {
        this.setState({
            businessSearchModalOpen: open
        });
    }

    changeLoanAmount(event, loanAmount) {
        let loanForm = this;

        this.setState({
            loanAmount: {
                ...this.state.loanAmount,
                isStarted: true,
                value: loanAmount,
            },
        }, () => {
            clearTimeout(this.loanAmountChange);

            this.loanAmountChange = setTimeout(function () {
                loanForm.validate("loanAmount");
            }, 1000);
        });
    }

    changeLoanReason(event) {
        this.setState({
            loanReason: {
                ...this.state.loanReason,
                value: event.target.value,
            },
        });
    }

    changeFirstName(event) {
        this.setState({
            firstName: {
                ...this.state.firstName,
                value: event.target.value,
            }
        });
    }

    changeLastName(event) {
        this.setState({
            lastName: {
                ...this.state.lastName,
                value: event.target.value,
            },
        });
    }

    changeEmail(event) {
        this.setState({
            email: {
                ...this.state.email,
                value: event.target.value,
            },
        });
    }

    changePhone(event) {
        this.setState({
            phone: {
                ...this.state.phone,
                value: event.target.value,
            },
        });
    }

    changeBusinessName(event, { newValue }) {
        let businessName = event.target.value;
        let businessNameState = this.state.businessName;

        if(event.keyCode === 13) {
            event.preventDefault();
            return false;
        }

        this.setState({
            businessName: {
                ...businessNameState,
                value: this.state.businessSearchOpen ? newValue : businessName,
            },
        });
    }

    changeSalutation(event) {
        this.setState({
            salutation: {
                ...this.state.salutation,
                value: event.target.value,
            },
        });
    }

    changeConfirmEmail(event) {
        this.setState({
            confirmEmail: {
                ...this.state.confirmEmail,
                value: event.target.value,
            },
        });
    }

    interactionEnd(event) {
        let loanForm = this;

        clearTimeout(this.loading);

        this.loading = setTimeout(function () {
            loanForm.sendForm();
        }, 1000);
    }

    getLoanAmountFormatted() {
        return this.state.loanAmount.value.toLocaleString(navigator.language, { minimumFractionDigits: 0 })
    }

    getLoanMinimumFormatted() {
        return this.minimumLoanAmount.toLocaleString(navigator.language, { minimumFractionDigits: 0 })
    }

    getLoanMaximumFormatted() {
        return this.maximumLoanAmount.toLocaleString(navigator.language, { minimumFractionDigits: 0 })
    }

    validate(inputs) {
        this.setState(
            {
                ...this.formValidator.validation(this.state, inputs),
                formStarted: true,
            }
        );

        return this.state.isInvalid;
    }

    sendForm() {
        const {
            salutation,
            firstName,
            lastName,
            email,
            phone,
            businessName,
            loanAmount,
            loanReason,
            nzbnNumber,
        } = this.state;


        const path = window.location.href;

        let loanForm = this;
        let loanData = {
            salutation: salutation.value,
            firstName: firstName.value,
            lastName: lastName.value,
            email: email.value,
            phone: phone.value,
            businessName: businessName.value,
            loanAmount: loanAmount.value,
            loanReason: loanReason.value,
            nzbnNumber: nzbnNumber.value,
            tenant: path,
        };

        if (this.state.loanId !== null) {
            loanData.id = this.state.loanId;
        }

        loanForm.setState({
            formLoading: true,
        });

        axios.post(this.loanEndpoint,
            loanData,
            this.requestConfig
        ).then(function (response) {
            if (response.status !== 200) {
                return;
            }

            let loanId = response.data.data.id;
            let referenceNumber = response.data.data.referenceNumber;

            // Successful response return an entry id which will be used to track the application process
            if (loanId) {
                if (loanForm.state.finalSubmission) {
                    const cookies = new Cookies();

                    let now = new Date();
                    let time = now.getTime();
                    let expireTime = time + 1800;
                    now.setTime(expireTime);

                    const cookieOptions = {
                        maxAge: 1800,
                        domain: window.location.hostname,
                        expireTime: now.toUTCString(),
                        secure: true,
                        httpOnly: false,
                        sameSite: "lax"
                    };

                    const ieCompatability = [
                        "Netscape",
                        "Microsoft Internet Explorer",
                    ];

                    if (ieCompatability.indexOf(navigator.appName) === -1) {
                        cookieOptions.path = window.location.pathname;
                    }

                    cookies.set("lo_id", loanId, cookieOptions);
                    cookies.set("lo_referenceNumber", referenceNumber, cookieOptions);

                    loanForm.props.history.push("integration");

                    return;
                }

                loanForm.setState({
                    loanId: loanId,
                    formLoading: false,
                });
            }
        }).catch(function (error) {
            console.log("Form submission error: " + error);
        });
    }

    submitForm(event) {
        event.preventDefault();
        event.stopPropagation();

        if (this.state.formLoading === true || this.loading) {
            return false;
        }

        if (this.validate(null)) {
            return false;
        }

        this.setState({
            finalSubmission: true,
        });

        this.sendForm();
    }

    // Autosuggest will call this function every time you need to update suggestions.
    onBusinessRecordsFetchRequested = ({ value }) => {
        const form = this;

        this.clearBusinessSearch();

        // Don't continue for empty or undefined values
        if (value === "" || value === undefined) {
            return false;
        }

        // We need a minimum of three characters before we start search
        if (value.length < form.businessSearchMinimumLength) {
            return false;
        }

        this.businessSearchTimer = setTimeout(function () { form.fetchBusinessResults(value) }, this.businessSearchTypingInterval);

    };

    focusBusinessRecord() {
        this.setState({
            businessSearchOpen: true,
            nzbnNumber: {
                ...this.state.nzbnNumber,
                value: "",
            },
        });
    }

    blurBusinessRecord() {
        this.clearBusinessSearch();

        this.setState({
            businessSearchOpen: false
        }, () => {
            this.validate(["businessName", "nzbnNumber"]);
        });
    }

    clearBusinessSearch() {
        this.setState({
            businessResultsLoading: false,
        });

        clearTimeout(this.businessSearchTimer);
    }

    fetchBusinessResults(value) {
        const form = this;

        form.setState({
            businessResultsLoading: true,
        });

        const companySearchConfig = {
            params: {
                term: value
            }
        };

        axios.get(this.businessRecordsEndpoint, { ...this.requestConfig,
            ...companySearchConfig
        }).then(function (response) {
            form.setState({
                businessResultsLoading: false,
            });

            if (response.status !== 200) {
                return false;
            }

            form.setState({
                businessRecords: response.data.data.slice(0,10),
            });
        });
    }

    // Autosuggest will call this function every time you need to clear suggestions.
    onBusinessRecordsClearRequested = () => {
        this.setState({
            businessRecords: []
        });
    };

    // When suggestion is clicked, Autosuggest needs to populate the input
    // based on the clicked suggestion. Teach Autosuggest how to calculate the
    // input value for every given suggestion.
    onBusinessRecordClicked = (businessRecord) => {
        // Todo: using component did update for validation in later versions
        this.setState({
            businessSearchOpen: false,
            nzbnNumber: {
                ...this.state.nzbnNumber,
                value: businessRecord.nzbn
            },
            businessName: {
                ...this.state.businessName,
                value: businessRecord.name,
            },
        }, () => {
            this.validate(["businessName", "nzbnNumber"]);
        });

        return businessRecord.name;
    };

    render() {
        const {
            salutation,
            loanAmount,
            firstName,
            lastName,
            email,
            confirmEmail,
            phone,
            loanReason,
            businessName,
            businessRecords,
            businessSearchModalOpen,
            formLoading,
            finalSubmission,
            businessSearchOpen,
            nzbnNumber,
            businessResultsLoading,
        } = this.state;

        const businessNameLabel = "Company name";

        const inputProps = {
            id: "applicants-business",
            label: businessNameLabel,
            onChange: this.changeBusinessName,
            value: businessName.value,
            autoFocus: true,
            className: "react-autosuggest__input",
            onBlur: this.blurBusinessRecord,
            onFocus: this.focusBusinessRecord,
            autoComplete: "off",
            "aria-label": businessNameLabel,
            "aria-required": true,
            "aria-autocomplete": "list",
        };

        const businessSearchModal = (
            <Modal
                className={styles.businessSearchDescription__modal}
                aria-describedby="businessSearchDescription"
                open={businessSearchModalOpen}
                onClose={this.closeBusinessSearchModal}
            >
                <div className={styles.modal}>
                    <button className={`${styles.loanOrigination__button} ${styles.modalClose}`} onClick={this.closeBusinessSearchModal} type="button">&#215;</button>
                    <p id="businessSearchDescription">
                        Kiwibank operates within a highly regulated environment and
                        reserves the right to exclude certain industries and
                        companies from using their products for various reasons.
                        The list of companies presented here is compiled from various external sources and is updated regularly,
                        but may not contain recently registered companies;
                        please get in touch with us so we can help you with your application
                        by calling <a href="tel:0800 601 601">0800 601 601</a>.
                    </p>
                </div>
            </Modal>
        );

        // Renders each element in autocomplete dropdown
        const renderBusinessRecord = businessRecord => (
            <div>
                {businessRecord.name}
            </div>
        );

        // Renders autocomplete container and appends info box if query returns 3 or less business results
        const renderBusinessRecordContainer = ({ containerProps , children, query }) => {
            let businessInfo = "";

            if (
                businessName.isStarted &&
                businessName.value !== "" &&
                businessSearchOpen === true &&
                nzbnNumber.value === "" &&
                businessResultsLoading === false &&
                (children === null || children.props.items.length <= 3)
            ) {
                businessInfo =
                    <div className={styles.reactAutosuggest__suggestionsInfo}>
                        If you can't find your company refine your search or <a className={styles.loanOrigination__link} onClick={this.openBusinessSearchModal}>click here</a>
                    </div>
            }

            return (
                <div {...containerProps}>
                    <div>
                        {children}

                        {businessInfo}

                        {businessSearchModal}
                    </div>
                </div>
            );
        };

        const renderAutoComplete = inputProps => (
            <div>
                <TextField
                    {...inputProps}
                />

                {businessResultsLoading ? (
                        <CircularProgress className={styles.reactAutosuggest__container__suggestionsLoader} />
                    ) : ""}
            </div>
        );

        let loanAmountClassName = loanAmount.isStarted ? styles.loanSliderContainer__subtitle_bold : styles.loanSliderContainer__subtitle_light;

        return (
            <Box>
                <Box className={`${styles.loanOriginationForm} ${styles.loanOriginationContainer__content}`} display="flex" alignItems="center">
                    <form noValidate method="post" autoComplete="on" onSubmit={this.submitForm}>
                        {/* STEP 1: Business Name autocomplete from NZBN */}
                        <FormControl className={styles.formControl_container}>
                            <AutoSuggest
                                suggestions={businessRecords}
                                onSuggestionsFetchRequested={this.onBusinessRecordsFetchRequested}
                                onSuggestionsClearRequested={this.onBusinessRecordsClearRequested}
                                getSuggestionValue={this.onBusinessRecordClicked}
                                renderSuggestion={renderBusinessRecord}
                                inputProps={inputProps}
                                renderInputComponent={renderAutoComplete}
                                renderSuggestionsContainer={renderBusinessRecordContainer}
                            />

                            {businessName.isInvalid ? (
                                <ValidationMessage
                                    visible={businessName.isStarted}
                                    validated={false}
                                    message={businessName.message}
                                />
                            ) : (
                                <ValidationMessage
                                    visible={businessName.isStarted}
                                    validated={!nzbnNumber.isInvalid}
                                    message={nzbnNumber.message}
                                />
                            )}

                            <ValidationIcon
                                visible={businessName.isStarted && !businessResultsLoading && !businessSearchOpen}
                                validated={!businessName.isInvalid && !nzbnNumber.isInvalid}
                                hideIfInvalid={true}
                            />
                        </FormControl>

                        {/* STEP 2: Loan amount */}
                        <FormControl className={`${styles.loanSlider__container} ${styles.formControl_container}`}>
                            <Typography
                                className={loanAmountClassName}
                                variant="h6">
                                How much money does your company need
                                {!loanAmount.isStarted ? (
                                    <span>?</span>
                                ) : (
                                    <span>: <span className={styles.sliderAmount}>${this.getLoanAmountFormatted()}</span></span>
                                )}

                            </Typography>

                            <Slider
                                className={styles.loanSlider__container_inner}
                                value={loanAmount.value}
                                min={this.minimumLoanAmount}
                                max={this.maximumLoanAmount}
                                step={this.stepsLoanAmount}
                                onChange={this.changeLoanAmount}
                            />

                            <Grid
                                container
                                className={styles.sliderAmountsContainer}
                            >
                                <Grid item xs={6}>
                                    <Typography
                                        className={styles.loanMinimum}
                                        variant="body1"
                                    >
                                        ${this.getLoanMinimumFormatted()}
                                    </Typography>
                                </Grid>

                                <Grid item xs={6}>

                                    <Typography
                                        className={styles.loanMaximum}
                                        variant="body1"
                                    >
                                        ${this.getLoanMaximumFormatted()}
                                    </Typography>
                                </Grid>
                            </Grid>

                            <ValidationMessage
                                visible={loanAmount.isStarted}
                                validated={!loanAmount.isInvalid}
                                message={loanAmount.message}
                            />
                        </FormControl>

                        {/* STEP 3: Loan reason */}
                        <FormControl className={styles.formControl_container}>
                            <Select
                                value={loanReason.value}
                                displayEmpty={true}
                                onChange={this.changeLoanReason}
                                className={loanReason.value === "" ? "selectPlaceholder__default" : "primarySelect"}
                                required
                                onBlur={() => this.validate("loanReason")}
                                inputProps={{
                                    name: "loanReason",
                                }}
                            >
                                <MenuItem value="" selected={true}>What does your company need it for?</MenuItem>
                                {this.loanReasonList.map((reason, index) => <MenuItem key={index} value={reason}>{reason}</MenuItem>)}
                            </Select>

                            <ValidationMessage
                                visible={loanReason.isStarted}
                                validated={!loanReason.isInvalid}
                                message={loanReason.message}
                            />
                        </FormControl>

                        {/* STEP 5: Salutation */}
                        <FormControl className={styles.formControl_container}>
                            <Select
                                value={salutation.value}
                                displayEmpty={true}
                                onChange={this.changeSalutation}
                                className={salutation.value === "" ? "selectPlaceholder__default" : "primarySelect"}
                                required
                                autoComplete="honorific-prefix"
                                onBlur={() => this.validate("salutation")}
                                inputProps={{
                                    name: "salutation",
                                }}
                            >
                                <MenuItem value="" selected={true}>Title</MenuItem>
                                {this.salutationList.map((salutation, index) => <MenuItem key={index} value={salutation}>{salutation}</MenuItem>)}
                            </Select>

                            <ValidationMessage
                                visible={salutation.isStarted}
                                validated={!salutation.isInvalid}
                                message={salutation.message}
                            />
                        </FormControl>

                        {/* STEP 6: First Name */}
                        <FormControl className={styles.formControl_container}>
                            <TextField
                                id="applicants-first-name"
                                label="First name"
                                onChange={this.changeFirstName}
                                autoComplete="given-name"
                                required
                                onBlur={() => this.validate("firstName")}
                                value={firstName.value}
                            />

                            <ValidationIcon
                                visible={firstName.isStarted}
                                validated={!firstName.isInvalid}
                                hideIfInvalid={true}
                            />

                            <ValidationMessage
                                visible={firstName.isStarted}
                                validated={!firstName.isInvalid}
                                message={firstName.message}
                            />
                        </FormControl>

                        {/* STEP 7: Last Name */}
                        <FormControl className={styles.formControl_container}>
                            <TextField
                                id="applicants-last-name"
                                label="Last name"
                                onChange={this.changeLastName}
                                autoComplete="family-name"
                                required
                                onBlur={() => this.validate("lastName")}
                                value={lastName.value}
                            />

                            <ValidationIcon
                                visible={lastName.isStarted}
                                validated={!lastName.isInvalid}
                                hideIfInvalid={true}
                            />

                            <ValidationMessage
                                visible={lastName.isStarted}
                                validated={!lastName.isInvalid}
                                message={lastName.message}
                            />
                        </FormControl>

                        {/* STEP 8: Email Name */}
                        <FormControl className={styles.formControl_container}>
                            <TextField
                                id="applicants-email"
                                label="Email address"
                                type="email"
                                onChange={this.changeEmail}
                                autoComplete="email"
                                required
                                onBlur={() => this.validate("email")}
                                value={email.value}
                            />

                            <ValidationIcon
                                visible={email.isStarted}
                                validated={!email.isInvalid}
                                hideIfInvalid={true}
                            />

                            <ValidationMessage
                                visible={email.isStarted}
                                validated={!email.isInvalid}
                                message={email.message}
                            />
                        </FormControl>

                        {/* STEP 9: Confirm Email Name */}
                        <FormControl className={styles.formControl_container}>
                            <TextField
                                id="applicants-email-confirm"
                                label="Confirm email address"
                                type="email"
                                onChange={this.changeConfirmEmail}
                                required
                                onBlur={() => this.validate("confirmEmail")}
                                value={confirmEmail.value}
                            />

                            <ValidationIcon
                                visible={confirmEmail.isStarted}
                                validated={!confirmEmail.isInvalid}
                                hideIfInvalid={true}
                            />

                            <ValidationMessage
                                visible={confirmEmail.isStarted}
                                validated={!confirmEmail.isInvalid}
                                message={confirmEmail.message}
                            />
                        </FormControl>

                        {/* STEP 10: Phone Number */}
                        <FormControl className={styles.formControl_container}>
                            <TextField
                                id="applicants-phone"
                                label="Phone number"
                                type="tel"
                                onChange={this.changePhone}
                                autoComplete="tel"
                                required
                                onBlur={() => this.validate("phone")}
                                value={phone.value}
                            />

                            <ValidationIcon
                                visible={phone.isStarted}
                                validated={!phone.isInvalid}
                                hideIfInvalid={true}
                            />

                            <ValidationMessage
                                visible={phone.isStarted}
                                validated={!phone.isInvalid}
                                message={phone.message}
                            />
                        </FormControl>

                        <Button
                            className={styles.loanOrigination__button_primary}
                            variant="contained"
                            color="primary"
                            type="submit"
                        >
                            {finalSubmission && formLoading ? (
                                <CircularProgress />
                            ) : (
                                <div className={styles.loanOrigination__button_primary_align}>
                                    Next <ArrowRight />
                                </div>
                            )}

                        </Button>
                    </form>
                </Box>
            </Box>
        );
    }
}

export default withRouter(LoanForm);