
import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import "typeface-roboto";
import { withRouter } from "react-router-dom";
import Cookies from "universal-cookie";
import {Button, CircularProgress} from "@material-ui/core";
import HelpOutline from "@material-ui/icons/HelpOutline";

/** Styles **/
import styles from "./loanIntegration.less";
import axios from "axios";

class LoanIntegration extends React.Component {
    requestConfig = {
        headers: {
            hash: this.props.hash,
            token: this.props.token,
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            formLoading: false,
        };

        this.openXero = this.openXero.bind(this);
    }

    openXero() {
        const integration = this;
        const cookies = new Cookies();

        const xeroParams = {
            params: {
                id: cookies.get("lo_id"),
            }
        };

        this.setState({
            formLoading: true,
        });

        axios.get(this.props.apiUrl + "xero/connect", {
            ...this.requestConfig,
            ...xeroParams
        }).then(function (response) {
            if (response.data.error_code || response.status !== 200 || !integration.validURL(response.data.redirect)) {
                integration.setState({
                    formLoading: false,
                });

                 integration.props.history.push("notification");

                 return;
            }

            window.location.href = response.data.redirect;
        });
    }

    validURL(urlString) {
        const pattern = new RegExp("^(https?:\\/\\/)?" + // protocol
            "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
            "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
            "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
            "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
            "(\\#[-a-z\\d_]*)?$","i"); // fragment locator
        return !!pattern.test(urlString);
    }

    render() {
        return (
            <Box className={`${styles.loanIntegrationForm} ${styles.loanOriginationContainer__content}`}>
                <Typography
                    className={`${styles.formTitle} ${styles.integrationTitle}`}
                    variant="h1">
                    Connect to Xero
                </Typography>

                <img className={styles.integrationLogo} src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgdmlld0JveD0iMCAwIDExMzMuODU4NiAxMTMzLjg1ODYiCiAgIGhlaWdodD0iMTEzMy44NTg2IgogICB3aWR0aD0iMTEzMy44NTg2IgogICB4bWw6c3BhY2U9InByZXNlcnZlIgogICBpZD0ic3ZnMiIKICAgdmVyc2lvbj0iMS4xIj48bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGE4Ij48cmRmOlJERj48Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPjwvY2M6V29yaz48L3JkZjpSREY+PC9tZXRhZGF0YT48ZGVmcwogICAgIGlkPSJkZWZzNiI+PGNsaXBQYXRoCiAgICAgICBpZD0iY2xpcFBhdGgxOCIKICAgICAgIGNsaXBQYXRoVW5pdHM9InVzZXJTcGFjZU9uVXNlIj48cGF0aAogICAgICAgICBpZD0icGF0aDE2IgogICAgICAgICBkPSJNIDAsODUwLjM5NCBIIDg1MC4zOTQgViAwIEggMCBaIiAvPjwvY2xpcFBhdGg+PC9kZWZzPjxnCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMS4zMzMzMzMzLDAsMCwtMS4zMzMzMzMzLDAsMTEzMy44NTg3KSIKICAgICBpZD0iZzEwIj48ZwogICAgICAgaWQ9ImcxMiI+PGcKICAgICAgICAgY2xpcC1wYXRoPSJ1cmwoI2NsaXBQYXRoMTgpIgogICAgICAgICBpZD0iZzE0Ij48cGF0aAogICAgICAgICAgIGlkPSJwYXRoMjAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzFhYjRkNztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZSIKICAgICAgICAgICBkPSJtIDQyNC45MzMsMTMwLjg0MyBjIDE2Mi4zMzUsMCAyOTMuOTMyLDEzMS41OTcgMjkzLjkzMiwyOTMuOTMzIDAsMTYyLjMzNCAtMTMxLjU5NywyOTMuOTMyIC0yOTMuOTMyLDI5My45MzIgQyAyNjIuNTk4LDcxOC43MDggMTMxLDU4Ny4xMSAxMzEsNDI0Ljc3NiAxMzEsMjYyLjQ0IDI2Mi41OTgsMTMwLjg0MyA0MjQuOTMzLDEzMC44NDMiIC8+PHBhdGgKICAgICAgICAgICBpZD0icGF0aDIyIgogICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmUiCiAgICAgICAgICAgZD0ibSAyNzEuNzAxLDQyNS42MjUgNTAuMTYyLDUwLjI4NiBjIDEuNjYzLDEuNjk1IDIuNTkxLDMuOTM5IDIuNTkxLDYuMzE5IDAsNC45NiAtNC4wMzIsOC45ODggLTguOTkyLDguOTg4IC0yLjQxNiwwIC00LjY4MSwtMC45NDcgLTYuMzg5LC0yLjY3NiAtMC4wMDUsLTAuMDEgLTUwLjExNywtNTAuMDkzIC01MC4xMTcsLTUwLjA5MyBsIC01MC4zNDEsNTAuMTY4IGMgLTEuNzAxLDEuNjc3IC0zLjk1NSwyLjYwMSAtNi4zNDUsMi42MDEgLTQuOTU0LDAgLTguOTg3LC00LjAyNiAtOC45ODcsLTguOTgxIDAsLTIuNDE3IDAuOTY1LC00LjY4NSAyLjY5NywtNi4zOTQgbCA1MC4xNTUsLTUwLjE0OCAtNTAuMTI5LC01MC4yMjMgYyAtMS43NjIsLTEuNzM1IC0yLjcyMywtNC4wMDggLTIuNzIzLC02LjQzNiAwLC00Ljk1OSA0LjAzMywtOC45ODIgOC45ODcsLTguOTgyIDIuMzk0LDAgNC42NSwwLjkyOSA2LjM0NSwyLjYyNSBsIDUwLjI2Myw1MC4xOTUgNTAuMDcyLC01MC4wMTIgYyAxLjc3NiwtMS44MzYgNC4wNjcsLTIuODE0IDYuNTEyLC0yLjgxNCA0Ljk1NSwwIDguOTg3LDQuMDI5IDguOTg3LDguOTg4IDAsMi4zOTMgLTAuOTI3LDQuNjQxIC0yLjYxNCw2LjMzNiB6IiAvPjxwYXRoCiAgICAgICAgICAgaWQ9InBhdGgyNCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lIgogICAgICAgICAgIGQ9Im0gNTY5LjkwNiw0MjUuNjQgYyAwLC05LjAwNiA3LjMyMywtMTYuMzMyIDE2LjM0LC0xNi4zMzIgOC45OTYsMCAxNi4zMjIsNy4zMjYgMTYuMzIyLDE2LjMzMiAwLDkuMDA4IC03LjMyNiwxNi4zMzMgLTE2LjMyMiwxNi4zMzMgLTkuMDE3LDAgLTE2LjM0LC03LjMyNSAtMTYuMzQsLTE2LjMzMyIgLz48cGF0aAogICAgICAgICAgIGlkPSJwYXRoMjYiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZSIKICAgICAgICAgICBkPSJtIDUzOC45Myw0MjUuNjMyIGMgMCwyNi4wOCAyMS4yMTYsNDcuMyA0Ny4yOTksNDcuMyAyNi4wNywwIDQ3LjI5MSwtMjEuMjIgNDcuMjkxLC00Ny4zIDAsLTI2LjA3NCAtMjEuMjIxLC00Ny4yODUgLTQ3LjI5MSwtNDcuMjg1IC0yNi4wODMsMCAtNDcuMjk5LDIxLjIxMSAtNDcuMjk5LDQ3LjI4NSBtIC0xOC42MDYsMCBjIDAsLTM2LjMzNCAyOS41NjUsLTY1Ljg5NSA2NS45MDUsLTY1Ljg5NSAzNi4zMzksMCA2NS45MTksMjkuNTYxIDY1LjkxOSw2NS44OTUgMCwzNi4zMzkgLTI5LjU4LDY1LjkwOCAtNjUuOTE5LDY1LjkwOCAtMzYuMzQsMCAtNjUuOTA1LC0yOS41NjkgLTY1LjkwNSwtNjUuOTA4IiAvPjxwYXRoCiAgICAgICAgICAgaWQ9InBhdGgyOCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lIgogICAgICAgICAgIGQ9Im0gNTE1LjY0Niw0OTAuNDEzIC0yLjc2NSwwLjAwOCBjIC04LjMwMSwwIC0xNi4zMDcsLTIuNjE4IC0yMi45OTQsLTcuNzcxIC0wLjg4MSw0LjAzMiAtNC40ODUsNy4wNjYgLTguNzc4LDcuMDY2IC00Ljk0MSwwIC04LjksLTMuOTU5IC04LjkxMiwtOC45MSAwLC0wLjAxOCAwLjAzLC0xMTEgMC4wMywtMTExIDAuMDEzLC00Ljk0MiA0LjA0NCwtOC45NTkgOC45ODgsLTguOTU5IDQuOTQzLDAgOC45NzMsNC4wMTcgOC45ODYsOC45NyAwLDAuMDIyIDAuMDA0LDY4LjI1MSAwLjAwNCw2OC4yNTEgMCwyMi43NDggMi4wOCwzMS45MzUgMjEuNTY2LDM0LjM3IDEuODAxLDAuMjI0IDMuNzYsMC4xODkgMy43NjgsMC4xODkgNS4zMzIsMC4xODMgOS4xMjEsMy44NDcgOS4xMjEsOC43OTkgMCw0Ljk1NSAtNC4wNDUsOC45ODcgLTkuMDE0LDguOTg3IiAvPjxwYXRoCiAgICAgICAgICAgaWQ9InBhdGgzMCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lIgogICAgICAgICAgIGQ9Im0gMzQzLjA3Miw0MzYuNDgyIGMgMCwwLjI0NiAwLjAxOSwwLjUwMiAwLjAzMSwwLjc1NSA1LjIxOCwyMC42MjcgMjMuODk5LDM1Ljg5MSA0Ni4xNDcsMzUuODkxIDIyLjUxNywwIDQxLjM2OSwtMTUuNjM5IDQ2LjMyLC0zNi42NDYgeiBtIDExMC45MDMsMS42OTcgYyAtMy44NzMsMTguMzM3IC0xMy45MTIsMzMuMzk5IC0yOS4xOTksNDMuMDcyIC0yMi4zNDYsMTQuMTg3IC01MS44NTQsMTMuNDAyIC03My40MzgsLTEuOTQ3IC0xNy42MDYsLTEyLjUyNSAtMjcuNzY5LC0zMy4wMTQgLTI3Ljc2OSwtNTQuMTIgMCwtNS4yOTIgMC42MzgsLTEwLjYzNCAxLjk2NiwtMTUuODkyIDYuNjQ4LC0yNi4xNTIgMjkuMTMsLTQ1Ljk0OSA1NS45MzQsLTQ5LjIzIDcuOTU0LC0wLjk2MyAxNS42OTYsLTAuNTAyIDIzLjcxMywxLjU3NCA2Ljg4OSwxLjY3NiAxMy41NTUsNC40NyAxOS42OTcsOC40MDYgNi4zNzUsNC4wOTggMTEuNzAxLDkuNTAyIDE2Ljg1OSwxNS45NzEgMC4xMDQsMC4xMTkgMC4yMDksMC4yMjQgMC4zMTMsMC4zNDkgMy41OCw0LjQ0IDIuOTE2LDEwLjc1NCAtMS4wMTgsMTMuNzY4IC0zLjMxOCwyLjU0MyAtOC44ODgsMy41NzQgLTEzLjI3MSwtMi4wMzkgLTAuOTQyLC0xLjM0MiAtMS45OTMsLTIuNzIzIC0zLjE1LC00LjEwNCAtMy40ODUsLTMuODUxIC03LjgwOSwtNy41ODIgLTEyLjk5LC0xMC40NzYgLTYuNTkzLC0zLjUyMiAtMTQuMTA2LC01LjUzNSAtMjIuMDkzLC01LjU4IC0yNi4xNDIsMC4yOTEgLTQwLjEzMSwxOC41MzkgLTQ1LjEwNywzMS41NjQgLTAuODY5LDIuNDMyIC0xLjUzOSw0Ljk1NSAtMi4wMDYsNy41NTMgLTAuMDYxLDAuNDg2IC0wLjEwNiwwLjk1NSAtMC4xMjQsMS4zODcgNS40MDUsMCA5My44MTIsMC4wMTUgOTMuODEyLDAuMDE1IDEyLjg2MSwwLjI3IDE5Ljc4Myw5LjM0OCAxNy44NzEsMTkuNzI5IiAvPjwvZz48L2c+PC9nPjwvc3ZnPg==" />

                <Typography
                    className={`${styles.typography} ${styles.xeroBody}`}
                    variant="body1"
                >
                    Log into Xero and we'll take a look at your company's financial data,
                    helping us to make a quick decision about your application.
                </Typography>

                <br />
                <br />

                <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    className={styles.xeroButton}
                    onClick={this.openXero}
                >
                    {this.state.formLoading ? (
                        <CircularProgress />
                    ) : (
                        <Box>
                            Log in to Xero
                        </Box>
                    )}
                </Button>

                <br />
                <br />

                <Typography
                    className={`${styles.typography} ${styles.xeroFooter}`}
                    variant="body1"
                >
                    <HelpOutline className={styles.errorOutline} /> Our assessment of your application will review selected monthly balances from your company's financial statements.
                </Typography>

                <br />
                <br />
            </Box>
        );
    }
}

export default withRouter(LoanIntegration);