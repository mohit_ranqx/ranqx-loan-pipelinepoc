import React from "react";
import {Box, Typography} from "@material-ui/core";
import CloseModalIcon from "../Icons/CloseModalIcon";
import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from "body-scroll-lock";
import Footer from "../Footer/Footer";
import Cookies from "universal-cookie";

import styles from "./loanOrigination.less";

class LoanOriginationModal extends React.Component {
    originalBodyCss = "";
    originalDocumentCss = "";
    modalDocumentCss = "overflow: hidden; height: 100%; margin: 0; padding: 0;";
    oldHTML = "";

    static defaultProps = {
        targetElement: false,
        containerElement: false,
    };

    constructor(props) {
        super(props);

        this.state = {
            showForm: false,
            isComplete: false,
        };

        this.closeForm = this.closeForm.bind(this);
    }

    displayForm() {
        // disabled website scroll.footerMiddleLine
        this.originalBodyCss = document.body.style.cssText;
        this.originalDocumentCss = document.documentElement.style.cssText;
        document.body.style.cssText = this.originalBodyCss + " " + this.modalDocumentCss;
        document.documentElement.style.cssText = this.originalDocumentCss + " " + this.modalDocumentCss;
        disableBodyScroll(this.targetElement);

        this.setState({
            showForm: true,
        });
    }

    closeForm() {
        const cookies = new Cookies();
        const cookiePath = window.location.pathname;
        cookies.remove("lo_id", { path: cookiePath });
        cookies.remove("lo_referenceNumber", { path: cookiePath });

        this.setState({
            showForm: false,
        });

        location.reload();
    }

    componentDidMount() {
        this.displayForm();
    }

    componentWillUnmount() {
        clearAllBodyScrollLocks();
    }

    render() {
        const { targetElement, children } = this.props;
        const { showForm } = this.state;

        if (showForm) {
            return <Box id="loanOriginationPackage" className={styles.loanOriginationContainer__modal}>
                <Box
                    className={styles.loanOrigination__form_container}
                    ref={targetElement}>
                    <Box className={styles.loanOriginationContainer}>
                        <CloseModalIcon className={styles.loanOrigination__closeModal} onPress={this.closeForm}/>

                        <Box className={styles.loanOriginationContainer__titleContainer}>
                            <Typography
                                className={`${styles.formTitle} ${styles.loanOriginationContainer__titleContainer_FormTitle}`}
                                variant="h1">
                                Business Overdraft application form
                            </Typography>
                        </Box>

                        <Box className={styles.loanOriginationContainer__modalScroll}>
                            {children}

                            <Footer />
                        </Box>
                    </Box>
                </Box>
            </Box>;
        }

        return "";
    }
}

export default LoanOriginationModal;