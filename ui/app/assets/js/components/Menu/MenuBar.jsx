
import React from 'react';
import injectSheet from 'react-jss';
import PropTypes from 'prop-types';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Box from "@material-ui/core/Box";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/core/Menu";

const styles = {
    appBar: {
        backgroundColor: '#69a41e',
    },
};

function MenuBar(props) {
    return (
        <AppBar className={props.classes.appBar} position="static">
            <Toolbar>
                <Box justifyContent="flex-end">
                    <IconButton href="/" edge="end" aria-label="Menu">
                        <MenuIcon />
                    </IconButton>
                </Box>
            </Toolbar>
        </AppBar>
    );
}

MenuBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default injectSheet(styles)(MenuBar);