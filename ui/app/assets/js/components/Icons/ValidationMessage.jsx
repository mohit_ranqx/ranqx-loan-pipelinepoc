
import React from "react";
import Warning from "@material-ui/icons/Warning"
import {
    Box,
    FormHelperText,
} from "@material-ui/core";

import styles from "./icons.less";

class ValidationMessage extends React.Component {
    static defaultProps = {
        validated: false,
        visible: false,
        message: null,
    };

    render() {
        let icon = "";

        if (this.props.visible) {
            icon = <Box className={styles.validationMessage}>
                {this.props.validated ? null : (
                    <FormHelperText className={styles.validationMessage__container}>
                        <Warning className={styles.validationMessage__warningIcon} />

                        <span className={styles.validationMessage__warningMessage}>{this.props.message}</span>
                    </FormHelperText>
                )}
            </Box>;
        }

        return icon;
    }
}

export default ValidationMessage;