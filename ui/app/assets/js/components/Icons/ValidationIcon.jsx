import React from "react";
import CheckCircle from "@material-ui/icons/CheckCircle";
import ErrorIcon from "@material-ui/icons/Error";
import Box from "@material-ui/core/Box";

import styles from "./icons.less";

class ValidationIcon extends React.Component {
    static defaultProps = {
        validated: false,
        hideIfInvalid: true,
        visible: false,
    };

    render() {
        let icon = "";

        if (this.props.visible) {
            icon = <Box className={styles.formControl_container_validationIconContainer}>
                {this.props.validated ? (
                    <CheckCircle className={styles.successIcon} />
                ) : (!this.props.hideIfInvalid) && (
                    <ErrorIcon className={styles.errorIcon} />
                )}
            </Box>;
        }

        return icon;
    }
}

export default ValidationIcon;