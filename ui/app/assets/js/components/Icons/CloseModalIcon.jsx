import React from "react";
import { Box } from "@material-ui/core";
import styles from "./icons.less";

class CloseModalIcon extends React.Component {
    static defaultProps = {
        onPress: () => {},
    };

    constructor(props) {
        super(props);
    }

    render() {
        const { onPress } = this.props;

        return <button className={styles.closeModalIcon} onClick={onPress}>
            <span className={styles.uAccessibleText}>Show/hide menu</span>
            <Box className={styles.closeModalIcon__container}>
                <Box className={`${styles.closeModalIcon__cross} ${styles.closeModalIcon__crossForward}`}></Box>
                <Box className={`${styles.closeModalIcon__cross} ${styles.closeModalIcon__crossBackward}`}></Box>
            </Box>
        </button>
    }
}

export default CloseModalIcon;