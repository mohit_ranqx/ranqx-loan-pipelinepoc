
import React from "react";
import {Divider, Grid, Typography, Box} from "@material-ui/core";
import Lock from "@material-ui/icons/Lock";
import Phone from "@material-ui/icons/Phone";
import "typeface-roboto";

import styles from "./footer.less";

class Footer extends React.Component {
    render() {
        return (
            <Box className={`${styles.footerContainer} ${styles.loanOrigination__container_content}`}>
                <Divider className={styles.LoanOrigination__divider} />

                <Grid container className={styles.footerDetails}>
                    <Grid item xs={6} className={styles.footerMiddleLine}>
                        <Lock className={styles.footerDetails__svg} />

                        <Typography
                            className={`${styles.typography} ${styles.footerDetail__typography}`}
                            variant="body1"
                        >
                            This site uses<br />
                            256-bit encryption
                        </Typography>
                    </Grid>

                    <Grid item xs={6}>
                        <Phone className={styles.footerDetails__svg} />

                        <Typography
                            className={`${styles.typography} ${styles.footerDetail__typography}`}
                            variant="body1"
                        >
                            Any questions<br />
                            Ph: <a href="tel:0800 601 601">0800 601 601</a>
                        </Typography>
                    </Grid>
                </Grid>

                <Divider className={styles.LoanOrigination__divider} />

                <br />

        <Typography
            className={styles.footerDetail__typography}
            variant="body1"
        >
            This service is provided by Kiwibank.
        </Typography>

        <br />

        <Typography
            className={styles.footerDetail__typography}
            variant="body1"
                >
                <a className={styles.footerDetail__typography_a} href="https://www.kiwibank.co.nz/about-us/governance/legal-documents-and-information/legal-documents/" target="_blank">Terms & Conditions</a> | <a className={styles.footerDetail__typography_a} href="https://www.kiwibank.co.nz/about-us/governance/legal-documents-and-information/website-privacy-policy/" target="_blank">Privacy Policy</a>
        </Typography>
    </Box>
    );
    }
}

export default Footer;