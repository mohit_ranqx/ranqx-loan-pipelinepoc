const webpack = require("webpack");
const TerserPlugin = require("terser-webpack-plugin")
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const path = require("path");

module.exports = () => {
    let version = process.env.APPLICATION_VERSION;
    if (!version) {
        version = Math.floor((Math.random() * 100000) + 1);
    }

    let plugins = [
        new CleanWebpackPlugin(),
        new HtmlWebPackPlugin({
            inject: false,
            title: "Example Bank",
            api_url: process.env.API_URL,
            filename: "index.html",
            template: "assets/views/index.ejs",
        }),
        // For cookie tests
        new HtmlWebPackPlugin({
            inject: false,
            title: "Example Bank",
            api_url: process.env.API_URL,
            filename: "envs/new-systest.html",
            template: "assets/views/index.ejs",
        }),
        new webpack.ExtendedAPIPlugin(),
        new webpack.DefinePlugin({
            "process.env.APPLICATION_VERSION": JSON.stringify(version)
        }),
    ];


    let mode = "development";

    if (["prod"].indexOf(process.env.APP_ENV) !== -1) {
        mode = "production";

        plugins.push(new TerserPlugin({
            parallel: true,
            terserOptions: {
                ecma: 6,
                output: {
                    comments: false,
                },
                mangle: true,
            },
        }));
    }

    let cssInjector = function insertAtTop(element) {
        let parent = document.getElementById("loanOriginationPackage");
        if (!parent) {
            console.log("failed to find element to insert styles");
        }

        element.setAttribute("scoped", true);
        parent.appendChild(element);
    };

    return {
        mode: mode,
        entry: {
            LoanOrigination: [
                "core-js/stable",
                "regenerator-runtime/runtime",
                __dirname + "/assets/js/LoanOrigination.jsx",
            ],
            LoanOriginationDownloader: [
                "core-js/stable",
                "regenerator-runtime/runtime",
                __dirname + "/assets/js/LoanOriginationDownloader.jsx",
            ],
        },
        devtool: "source-map",
        output: {
            path: __dirname + "/public",
            filename: "[name].js",
            library: "[name]",
            libraryTarget: "umd",
            umdNamedDefine: true,
        },
        resolve: {
            extensions: ["*", ".js", ".jsx"],
            alias: {
                "@material-ui/styles": path.resolve(__dirname, "node_modules", "@material-ui/styles"),
            },
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: "babel-loader",
                            options: {
                                cacheDirectory: true,
                                babelrc: true,
                            },
                        },
                        {
                            loader: "eslint-loader"
                        },
                    ],
                },
                {
                    test: /\.html$/,
                    use: [
                        {
                            loader: "html-loader"
                        },
                    ]
                },
                { test: /\.(png|jpe?g|svg)$/, use: {loader: "file-loader", options: {outputPath: "img",}}},
                { test: /\.(woff|woff2|eot|ttf)$/, use: {loader: "file-loader", options: {outputPath: "fonts",}}},
                {
                    test: /\.(css|less)$/,
                    use: [
                        {
                            loader: "style-loader",
                            // options: {
                            //     injectType: "lazySingletonStyleTag",
                            //     insert: cssInjector,
                            // },
                        },
                        {
                            loader: "css-loader",
                            options: {
                                importLoaders: 1,
                                modules: {
                                    mode: "local",
                                    localIdentName: "[path]___[name]__[local]___[hash:base64:5]" //babel-plugin-css-module format
                                },
                                sourceMap: false,
                            }
                        },
                        {
                            loader: "less-loader",
                        },
                        {
                            loader: "postcss-loader",
                        },
                    ]
                }
            ]
        },
        plugins: plugins
    }
};