#!/usr/bin/env bash

yarn ngrok http localhost:8844 -region=au -hostname=ranqx-ui.au.ngrok.io
yarn ngrok http https://localhost:4433 -region=au -hostname=ranqx-api.au.ngrok.io
yarn webpack-dev-server --port 8844 --env.ENVIRONMENT=development --mode development