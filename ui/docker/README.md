# Docker build for UI

Supports two modes, with their own docker-compose files.
* *devtest* mode where the application is run using docker-compose. The code is
  then bind-mounted from the local filesystem.
* *prod* mode where the application is run in ECS container orchestration. The
  code is baked into the container itself.

These are the docker containers defined here:

* *ui-builder* is the container which contains the tools necessary to
  build the UI:
    * Stock Ubuntu image
    * Additional build tools installed
    * Script to perform the build
* *ui-devtest* does the build over a bind-mount when it is run.
* *ui-prod* does the build into a container when it is built.

The ui-builder container is duplicated between the docker-compose files.

container_inventory.json lists the containers produced by this build.
