# Minimal Docker image for PHP source code
ARG UI_BUILDER

FROM $UI_BUILDER as builder

ARG UI_SOURCE_FOLDER_LOCAL
ENV UI_SOURCE_FOLDER_LOCAL=${UI_SOURCE_FOLDER_LOCAL}

COPY --chown=www-data:www-data ${UI_SOURCE_FOLDER_LOCAL} /var/www/

RUN /usr/local/bin/compile-ui.sh

# ###### MULTI-STAGE BUILD - stage 2 #######
FROM busybox

COPY --from=builder --chown=www-data:www-data /var/www /opt/ranqx-lo-ui

# If used with Docker Compose and a local volume, the contents
# of the container directory will be copied to the volume if it is empty.
# See the README for how to empty a volume if-needs-be.
VOLUME /opt/ranqx-lo-ui

# Labels at the end of the file prevent unnecessary layer rebuilds
LABEL maintainer="admin@deref.nz"

# Labels.
LABEL org.label-schema.schema-version="1.0" \
  org.label-schema.build-date=$BUILD_DATE \
  org.label-schema.name="$ORGANISATION/example-application" \
  org.label-schema.description="Example Application data container" \
  org.label-schema.url="https://www.deref.nz/" \
  org.label-schema.vcs-url="https://gitlab.com/deref" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.version=$VERSION \
  org.label-schema.docker.cmd="docker run --name example-application -d -t $ORGANISATION/example-application"

ENTRYPOINT ["tail"]
CMD ["-f", "/dev/null"]
