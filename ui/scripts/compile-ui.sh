#!/usr/bin/env bash

set -euxo pipefail

# NOTE: the latest version of the code is assumed to already be in the folder

# The user is usually www-data but it needs to be able to be overridden for local builds
# e.g. setting the user to ubuntu allows the application_builder_run to update bind mount data

USER=${1-"www-data"}
FOLDER=/var/www

pushd $FOLDER

echo "=== Compile ${0} ==="

echo "Running as ${USER}"

# NOTE: workaround for UNABLE_TO_GET_ISSUER_CERT_LOCALLY error.
# TODO: fix this so that https can be used
su --preserve-environment -l $USER -s /bin/bash -c "HOME=/tmp; cd $FOLDER && yarn setup && yarn build"

# su -l $USER -s /bin/bash -c "cd $FOLDER &&
#     npm config set registry http://registry.npmjs.org/ &&
#     npm install && npm run build"

echo "Status: "$?

ls -al $FOLDER

echo "======================="
