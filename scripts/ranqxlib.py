import boto3
import json
import os
import re
import subprocess
import sys

debug=False

# Root of the code hierarchy
base_dir=os.path.dirname(os.path.dirname(sys.argv[0]))
versions_file=base_dir + "/.versions"

########################################
# Sub process functions

# Call a command return its output as a list of lines
def subp_capture(command, subdir="", env=None):
    if debug:
        print("------------------------")
        print("--Calling shell command: " + command)
    res = subprocess.run(command, cwd=base_dir+subdir, env=env, shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if debug:
        print(output)
        print("------------------------")

    if res.returncode != 0:
        print(res.stdout.decode())
        print(res.stderr.decode())
        raise Exception("shell command failed ({code}): {command}".format(
            code=res.returncode, command=command))

    return res.stdout.decode().strip().splitlines()

# Call a command and don't capture its output
def subp_run(command, subdir="", env=None):
    if debug:
        print("------------------------")
        print("--Calling shell command: " + command)
    res = subprocess.run(command, cwd=base_dir+subdir, env=env, shell=True)
    if debug:
        print("------------------------")

    if res.returncode != 0:
        raise Exception("shell command failed ({code}): {command}".format(
            code=res.returncode, command=command))

    return

# Call a command and return a boolean indicating success or failure
def subp_test(command, subdir="", env=None):
    if debug:
        print("------------------------")
        print("--Calling shell command: " + command)
    res = subprocess.run(command, cwd=base_dir+subdir, env=env, shell=True)
    if debug:
        print("------------------------")

    if res.returncode != 0:
        return False

    return True

########################################
# Project component map

# A map of everything related to each project in the build hierarchy
# Need to load some of this from container_inventory.json in each project dir

project_inventory = [
    {
        'name' : 'API',
        'directory' : 'api',
        'version_names' : {'APPLICATION_VERSION', 'BUILDER_VERSION'},
        'components' : [
            {
                'name' : 'api-code',
                'legacy-image-name' : 'loan-origination',
            },
            {
                'name' : 'api-init',
                'legacy-image-name' : 'loan-origination-init',
            },
        ]
    },
    {
        'name' : 'UI',
        'directory' : 'ui',
        'version_names' : {'UI_VERSION', 'UI_BUILDER_VERSION'},
        'components' : [
            {
                'name' : 'ui-code',
                'legacy-image-name' : 'loan-origination-ui',
            },
        ]
    },
    {
        'name' : 'Infrastructure',
        'directory' : 'infrastructure/ranqx-ecs-docker',
        'version_names' : { 'NGINX_VERSION',
                            'NGINX_UI_VERSION',
                            'PHPFPM_VERSION',
                            'PHP_ORGSQS_VERSION',
                            'PHP_XEROSQS_VERSION' },
        'components' : [
            {
                'name' : 'api-nginx',
                'legacy-image-name' : 'nginx',
            },
            {
                'name' : 'api-phpfpm',
                'legacy-image-name' : 'phpfpm',
            },
            {
                'name' : 'jobs-calc',
                'legacy-image-name' : 'php-orgsqs',
            },
            {
                'name' : 'jobs-xero',
                'legacy-image-name' : 'php-xerosqs',
            },
            {
                'name' : 'ui-nginx',
                'legacy-image-name' : 'nginx-ui',
            },
        ]
    },
]

def get_project_names():
    return [x['name'] for x in project_inventory]

# Return a list of container dicts for the named project.
def get_project_containers(project_name):
    for project in project_inventory:
        if project['name'] == project_name:
            coinvfn = project['directory'] + "/docker/container_inventory.json"
            with open(coinvfn) as coinvf:
                return json.load(coinvf)['containers']

########################################
# Version Management

# Run this once to fetch remote tags
def init_versions():
    print("Fetching tags...")
    subp_capture("git fetch --tags")

# Return true if a string is a version tag
def is_version_tag(tagstring):
    return re.fullmatch("v([0-9]+.[0-9]+.[0-9]+)", tagstring)

# Convert a version from tagstring format to list of integers format
def tag_str_to_int(tagstring):
    return list(map(int, tagstring[1:].split('.')))

# Convert a version from list of integers format to tagstring format
def tag_int_to_str(tagint):
    return "v" + ".".join(map(str, tagint))

# Return true if a tag already exists
def does_version_exist(version):
    return version in subp_capture("git tag")

# Fetch repository tags, filter out just the version tags, and return the
# highest one in list of integers format
def get_max_version():
    git_tags=subp_capture("git tag")

    int_tags=[tag_str_to_int(tag) for tag in git_tags if is_version_tag(tag)]

    return max(int_tags)

########################################
# AWS Account stuff

aws_accounts = [
    {
        'name' : 'master',
        'id' : '219405760663',
        'canonical_id' : '',
    },
    {
        'name' : 'prod',
        'id' : '547366802934',
        'canonical_id' : 'ff9f53067bb75f5aec438d9f3b73babfd8cb481b097fc88d1a9826f17dd02f81',
    },
    {
        'name' : 'build',
        'id' : '727040197320',
        'canonical_id' : '70874d37515d142f637b2351edfe18f2ca20f8854d297baff74302d77280b3fe',
    },
    {
        'name' : 'staging',
        'id' : '790674549577',
        'canonical_id' : '5ce13e73dbc3e27463027981f0f7e1ce9431d06da1ca75264646e1af48a353ee',
    },
    {
        'name' : 'systest',
        'id' : '739151534789',
        'canonical_id' : 'fb0a12e4bdee7020ac7c1db538a26b7a58d42f3a1a59cbd81bae4bae33947373',
    },
    {
        'name' : 'sectest',
        'id' : '664072489434',
        'canonical_id' : 'eb588e1c788b8165a3c84947d634f7e32df9ca4480c3b43172dc297dd606a0ea',
    },
    {
        'name' : 'sandbox',
        'id' : '944440317645',
        'canonical_id' : 'f490e0b2dd5cf4bdb9d846273e2d8d720bea271345abc5159ff78c57b0abb02a',
    },
]

# Fetch the AWS account we're currently running in.
def get_aws_account():
    account_id=boto3.client('sts').get_caller_identity().get('Account')
    account_infos=[x for x in aws_accounts if x['id'] == account_id]
    if len(account_infos) == 1:
        return account_infos[0]
    raise Exception("Unrecocgnised account " +  account_id)

# The ECR root for the given AWS account
def get_ecr_root(aws_account):
    return "{}.dkr.ecr.ap-southeast-2.amazonaws.com/ranqx/lo".format(
        aws_account['id'])

########################################
# Process Environment

def create_env_vars(ecr_acct_name=None):
    # Return the environment variables to be used by build and deploy processes
    # All of these environment variables are referenced by scripts
    ret_env=dict()

    # PATH
    ret_env['PATH'] = os.environ['PATH']

    # The AWS account for ECR access
    if ecr_acct_name is not None:
        ecr_account=[x for x in aws_accounts if x['name'] == ecr_acct_name][0]
    else:
        ecr_account=get_aws_account()

    # Load .versions
    with open(versions_file) as vsf:
        for line in vsf.readlines():
            if len(line.strip()) > 0 and not line.startswith("#"):
                (vname, vval) = line.strip().split("=")
                # For the build account use the real versions value, for
                # everything else use latest.
                if ecr_account['name'] == "build":
                    ret_env[vname] = vval
                else:
                    ret_env[vname] = 'latest'

    # Set Image variables referenced by Cloud Formation templates. e.g.
    # IMAGE_API_CODE=
    # 727040197.dkr.ecr.ap-southeast-2.amazonaws.com/ranqx/lo/api-code:v1.0.5
    for project_name in get_project_names():
        for container in get_project_containers(project_name):
            image_ref="{image_root}/{container_name}:{version}".format(
                image_root=get_ecr_root(ecr_account),
                container_name=container['name'],
                version=ret_env[container['version']])
            image_env="IMAGE_" + container['name'].upper().replace("-","_")
            ret_env[image_env]=image_ref

    return ret_env
