#!/usr/bin/env python3

import fileinput
import os
import re
import sys

import ranqxlib

def usage():
    print(
"""
Assign a new version of Ranqx LO software.

    {script_name} [--prev <version>] [--major] [--minor]

Ranqx LO versions have 3 numeric values: major, minor and patch. They are
written in a dot separated form, e.g. v1.0.8.

This script tags the current code in the repository from which it is run with a
new version tag, and updates the component versions within it as necessary. By
default it will discover the newest version tag in the repository, and
increment its patch value to create the newly assigned version.  To increment
the major or minor values, use the corresponding parameter. To work from a
version other than the newest in the repository, use the --prev parameter with
a version argument, such as v0.1.35.

A git comparison is done to determine which components have changed, and a
change to the .versions file is committed to increase their versions.

Finally a git tag is applied for the overall software version, and everything
is pushed to the repository.

Repository credentials are required to push the changes.
""".format(script_name=os.path.basename(sys.argv[0])))

################################################################################
# Support functions

# Modify the ".versions" file to increment the version of a specific container
def increment_container_version(varname, major, minor):
    print("  Incrementing {}", varname)
    with fileinput.FileInput(files=[ranqxlib.versions_file],inplace=True) as vs:
        for line in vs:
            if not line.startswith(varname):
                print(line, end='')
            else:
                keyval=line.strip().split("=v")
                verbits=keyval[1].split(".")
                if major:
                    verbits[0] = str(int(verbits[0]) + 1)
                    verbits[1] = '0'
                    verbits[2] = '0'
                elif minor:
                    verbits[1] = str(int(verbits[1]) + 1)
                    verbits[2] = '0'
                else:
                    verbits[2] = str(int(verbits[2]) + 1)
                print(keyval[0] + "=v" + ".".join(verbits))

def update_component_versions(project, major, minor):
    print("Updating versions for {}".format(project['name']))
    for vname in project['version_names']:
        increment_container_version(vname, major, minor)

################################################################################
# Main execution

try:
    print("Starting tagging of new Ranqx LO software version")

    print("Checking for local changes")
    status=ranqxlib.subp_capture("git status --short")
    if len(status) > 0:
        raise Exception("Refusing to build while there are local changes")

    ########################################
    # Find the next version which we're tagging
    ranqxlib.init_versions()
    prev_tag_int=ranqxlib.get_max_version()
    prev_tag=ranqxlib.tag_int_to_str(prev_tag_int)

    if '--prev' in sys.argv:
        if sys.argv.index('--prev') + 1 >= len(sys.argv):
            raise Exception("--prev needs an argument")
        prev_tag=sys.argv[sys.argv.index('--prev') + 1]
        if not ranqxlib.is_version_tag(prev_tag):
            raise Exception("--prev argument must be a version tag")
        prev_tag_int=ranqxlib.tag_str_to_int(prev_tag)

    major = '--major' in sys.argv
    minor = '--minor' in sys.argv
    next_tag_int=list(prev_tag_int)
    if major:
        next_tag_int[0]+=1
        next_tag_int[1]=0
        next_tag_int[2]=0
    elif minor:
        next_tag_int[1]+=1
        next_tag_int[2]=0
    else:
        next_tag_int[2]+=1

    next_tag=ranqxlib.tag_int_to_str(next_tag_int)
    print("Assigning version following {}".format(prev_tag))
    if ranqxlib.does_version_exist(next_tag):
        raise Exception("Next version already exists: {}".format(next_tag))
    print("Assigning new version {}".format(next_tag))

    ########################################
    # Increment versions for modified components
    print("Updating component versions for changes since previous version")

    if next_tag_int[2] == 0:
        # On a minor or major change update every component version
        for project in ranqxlib.project_inventory:
            update_component_versions(project, major, minor)
    else:
        changed_files=ranqxlib.subp_capture("git diff --name-only " + prev_tag)
        for project in ranqxlib.project_inventory:
            for changed_file in changed_files:
                if changed_file.startswith(project['directory']):
                    update_component_versions(project, major, minor)
                    break

    ########################################
    # Commit the version changes and apply the new tag.

    if ranqxlib.subp_test("git diff --quiet .versions"):
        print(".versions file is unchanged")
    else:
        print("Committing changes to .versions files")
        commit_message="'Updating component versions for {}'".format(next_tag)
        ranqxlib.subp_run("git commit -m {} .versions".format(commit_message))

    print("Tagging {}".format(next_tag))
    tag_message="'Tagging {}'".format(next_tag)
    ranqxlib.subp_run("git tag -a {} -m {}".format(next_tag, tag_message))

    print("Pushing changes upstream")
    ranqxlib.subp_run("git push")
    ranqxlib.subp_run("git push --tags")

except Exception as e:
    print("***Error:")
    print(e)
    sys.exit(1)
