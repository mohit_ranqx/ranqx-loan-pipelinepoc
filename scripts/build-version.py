#!/usr/bin/env python3

import os
import re
import sys
import traceback

import ranqxlib

def usage():
    print(
"""
Build a new version of Ranqx LO software.

    {script_name} [<version>]

Fetches and builds the specified version, or default to the highest tag.

Subsequent containers are pushed to the Ranqx container repository.

This script must be run on the build server, which is the only location with
push access to the container repository.
""".format(script_name=os.path.basename(sys.argv[0])))

################################################################################
# Support functions

def build_project(project):
    print("Building {}".format(project['name']))

    env_vars = ranqxlib.create_env_vars()

    ranqxlib.subp_run("./scripts/env_and_build.sh {}".format(project['directory']),
                      env=env_vars)

    image_root = ranqxlib.get_ecr_root(aws_account)
    print("Uploading images to {}".format(image_root))
    for continfo in ranqxlib.get_project_containers(project['name']):
        if not continfo['publish']:
            continue
        local_image = 'ranqx/ranqx-loan/{}:{}'.format(
            continfo['legacy-name'], env_vars[continfo['version']])

        remote_image = '{}/{}:{}'.format(
            image_root, continfo['name'], env_vars[continfo['version']])

        print("pushing {} to {}".format(local_image, remote_image))
        ranqxlib.subp_run("docker tag {} {}".format(local_image, remote_image))
        ranqxlib.subp_run("docker push {}".format(remote_image))


################################################################################
# Build Tagged Version

def build_tagged_version():
    print("Checking for local changes")
    status=ranqxlib.subp_capture("git status --short")
    if len(status) > 0:
        raise Exception("Refusing to build while there are local changes")

    ########################################
    # Checkout the tag to build
    ranqxlib.init_versions()
    if len(sys.argv) > 1:
        to_build_str=sys.argv[1]
        if not ranqxlib.is_version_tag(to_build_str):
            raise Exception("--prev parameter must be a version tag")
        to_build_ints=ranqxlib.tag_str_to_int(to_build_str)
    else:
        to_build_ints=ranqxlib.get_max_version()
        to_build_str=ranqxlib.tag_int_to_str(to_build_ints)
    print("Building version {}".format(to_build_str))
    ranqxlib.subp_run("git checkout {}".format(to_build_str))

    ########################################
    # Build changed projects
    if to_build_ints[2] == 0:
        # This is a major or minor version change because final field is 0.
        # So build every project
        for project in ranqxlib.project_inventory:
            build_project(project)
    else:
        # This is a patch change, so detect which projects to build
        # by comparing the .versions file with the previously tagged .versions file
        prev_ints=list(to_build_ints)
        prev_ints[2]-=1
        diffcmd="git diff {} -- .versions".format(ranqxlib.tag_int_to_str(prev_ints))
        # Diff output is a git diff of the .versions file, so look for lines
        # starting with + and extract the component name from them.
        modified_components=set()
        for line in ranqxlib.subp_capture(diffcmd):
            match = re.match("^\+([^=]+)=", line)
            if match is not None:
                modified_components.add(match.group(1))

        print("Building modified components: " + " ".join(modified_components))
        for project in ranqxlib.project_inventory:
            # if any of the version names changed, build the project
            if len(modified_components & project['version_names']) > 0:
                build_project(project)

        print("Successfully built all modified components")

################################################################################
# Build Untagged Version

def build_untagged_version():
    for project in ranqxlib.project_inventory:
        build_project(project)

################################################################################
# Main execution

try:
    print("Starting build and publish of Ranqx LO software")

    aws_account = ranqxlib.get_aws_account()
    if aws_account['name'] in ("prod", "staging"):
        print("Builds cannot be run in this environment, use the build account")
        raise Exception("Refusing to build in this environment")

    ########################################
    # Login to docker repository to be certain of ability to push
    docker_login_generator="aws ecr get-login --no-include-email --region ap-southeast-2"
    docker_login_command=ranqxlib.subp_capture(docker_login_generator)
    ranqxlib.subp_run(docker_login_command)

    ########################################
    # Run a suitable build for this AWS account
    if aws_account['name'] == "build":
        print("Running in build account, building the latest tag")
        build_tagged_version()
    else:
        print("Running in a dev account, building current head")
        build_untagged_version()

except Exception as e:
    print("***Error:")
    traceback.print_exc()
    sys.exit(1)
