#!/usr/bin/env python3

import boto3
import os
import re
import sys

import ranqxlib

def usage():
    print(
"""
Deploy Ranqx LO software to a production-style environment.

    {script_name} [<version>]

If <version> is a version tag in git such as v0.2.13, then that code base is
used for deployment, and the corresponding container images are pulled from the
build account ECR repositories.

If <version> is not specified, then the highest version tagged in git will be
used.

If <version> is "latest", then deployment will be done from the local version
of the code, and container images will be pulled from the local AWS ECR
account. This mode is not supported in staging or production.

This must be executed from a deploy machine with sufficient AWS privileges
running in the target account.
""".format(script_name=os.path.basename(sys.argv[0])))

try:
    print("Starting deployment of Ranqx LO software")

    ########################################
    # Determine environment this is running in
    env = ranqxlib.get_aws_account()['name']
    print("Target Environment: {}".format(env))

    if len(sys.argv) > 1 and sys.argv[1] == 'latest':
        if env in ('staging', 'prod'):
            raise Exception("Cannot deploy latest to staging or prod")

        ########################################
        # Stop all tasks in services, because container tags haven't changed,
        # so service deployment won't trigger them to restart.
        print("Stopping tasks in services")
        ecs_client=boto3.client('ecs', region_name="ap-southeast-2")

        # Find the cluster they are in
        clusters = ecs_client.list_clusters()['clusterArns']
        if len(clusters) > 1:
            raise Exception("Too many clusters")
        cluster_arn = clusters[0]

        services = ecs_client.list_services(cluster=cluster_arn)
        sys.stdout.write("  ")
        for service in services['serviceArns']:
            tasks = ecs_client.list_tasks(cluster=cluster_arn, serviceName=service)
            for task in tasks['taskArns']:
                sys.stdout.write(".")
                ecs_client.stop_task(cluster=cluster_arn, task=task)
        print()

        env_vars = ranqxlib.create_env_vars()
        to_deploy_str="latest"

    else:
        # Deploying a tagged version
        print("Checking for local changes")
        status=ranqxlib.subp_capture("git status --short")
        if len(status) > 0:
            raise Exception("Refusing to deploy while there are local changes")

        ########################################
        # Determine and checkout the tag to build
        ranqxlib.init_versions()
        if len(sys.argv) < 1:
            to_deploy_ints=ranqxlib.get_max_version()
            to_deploy_str=ranqxlib.tag_int_to_str(to_deploy_ints)
        else:
            to_deploy_str = sys.argv[1]

        ranqxlib.subp_run("git checkout {}".format(to_deploy_str))

        ########################################
        # Create environment variables for deployment scripts
        env_vars = ranqxlib.create_env_vars(ecr_acct_name="build")

    print("Deploying version {}".format(to_deploy_str))

    for deploy_target in ["loanapi", "phporgsqs", "phpxerosqs"]:
        print("--------------------------")
        print("Deploying target {}".format(deploy_target))
        deploy_cmd="ENV={} scripts/services/deploy-{}.sh".format(env, deploy_target)
        ranqxlib.subp_run(deploy_cmd, subdir="/infrastructure/ranqx-ecs",
                          env=env_vars)

    print("--------------------------")

    ########################################
    # Run the api init task
    print("Running api init job")
    ecs_client=boto3.client('ecs', region_name="ap-southeast-2")

    # Find the task definition
    family='ranqxloan-ranqxloanapi-init'
    task_defs = ecs_client.list_task_definitions(familyPrefix=family)
    if len(task_defs['taskDefinitionArns']) != 1:
        raise Exception("Must have exactly 1 init task")
    task_def_arn=task_defs['taskDefinitionArns'][0]

    # Find the cluster to run as
    clusters = ecs_client.list_clusters()['clusterArns']
    if len(clusters) > 1:
        raise Exception("Too many clusters")
    cluster_arn = clusters[0]

    # Find networking details
    ec2_client=boto3.client('ec2', region_name="ap-southeast-2")

    # Find vpc
    vpcs=ec2_client.describe_vpcs(
        Filters=[ { 'Name': 'cidr', 'Values': [ '10.0.0.0/16', ] }, ],
        )['Vpcs']
    if len(vpcs) > 1:
        raise Exception("Too many vpcs")
    vpc_id = vpcs[0]['VpcId']

    # Find the subnet to run on
    subnets = ec2_client.describe_subnets(
        Filters=[
            { 'Name': 'vpc-id', 'Values': [ vpc_id ] },
            { 'Name': 'tag:Name', 'Values': [ 'PrivateSubnetA' ] },
        ],
    )['Subnets']
    if len(subnets) != 1:
        raise Exception("Did not find exactly 1 subnet")
    subnet_name=[x['Value'] for x in subnets[0]['Tags'] if x['Key'] == 'Name'][0]
    subnet_id=subnets[0]['SubnetId']

    # Find the security group to assign
    secgrps = ec2_client.describe_security_groups(
        Filters=[
            { 'Name': 'vpc-id', 'Values': [ vpc_id ] },
        ],
    )['SecurityGroups']
    secgrps = [x for x in secgrps if "WebServiceSecurityGroup" in x['GroupName']]
    if len(secgrps) != 1:
        raise Exception("Did not find exactly 1 security group")
    sec_group_id=secgrps[0]['GroupId']
    sec_group_name=secgrps[0]['GroupName']

    print("Running Task with config")
    print("    Task Definition:  {}".format(task_def_arn))
    print("    Cluster:          {}".format(cluster_arn))
    print("    VPC:              {}".format(vpc_id))
    print("    Subnet:           {} ({})".format(subnet_name, subnet_id))
    print("    Security Group:   {} ({})".format(sec_group_name, sec_group_id))

    response = ecs_client.run_task(
        cluster=cluster_arn,
        launchType = 'FARGATE',
        taskDefinition=task_def_arn,
        count = 1,
        platformVersion='LATEST',
        networkConfiguration={
            'awsvpcConfiguration': {
                'subnets': [ subnet_id ],
                'securityGroups': [ sec_group_id ],
                'assignPublicIp': 'DISABLED'
            }
        })

    if len(response['tasks']) != 1 or len(response['failures']) > 0:
        print(str(response))
        raise Exception("Returned too many tasks or failures")

    print("Launched Task: {}".format(response['tasks'][0]['taskArn']))

    print("--------------------------")

    print("Succesfully deployed version {} to {}".format(to_deploy_str, env))

except Exception as e:
    print("***Error:")
    print(e)
    sys.exit(1)
