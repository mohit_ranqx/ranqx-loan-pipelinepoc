#!/bin/bash

# A very minimal script called by build-version.py which runs existing environment
# setting shell scripts in order to do the docker-compose

set -euxo pipefail

script_dir=$(dirname $0)
BASE_DIR=$( cd $script_dir/..; echo $PWD )
cd $BASE_DIR

set -a
source .env/_common

VCS_BRANCH_ENV_FILE=$BASE_DIR/.versions

cd $1
docker-compose -f docker/docker-compose.yaml --project-directory . build
