#!/usr/bin/env bash

# A lot of this overlaps with the script test-server-run-branch.sh,
# and the duplication should be removed.

set -euxo pipefail
set -a

fail() {
    echo "$@"
    exit 1
}

# Make sure script is being executed in the correct directory.
cd $(dirname $0)/..
[[ -d ".git" ]] || fail "This must be run from a git working directory"
[[ -f ".env/_common" ]] || fail "This doesn't look like a Ranqx LO code base"
ROOT_DIR=$PWD

#cp .env/dev api/loan/.env
#cp .env/dev ui/app/.env

test -f .env/_common && source .env/_common || true

if [[ -f $VCS_BRANCH_ENV_FILE ]]; then
    echo "VCS_BRANCH_ENV_FILE ($VCS_BRANCH_ENV_FILE) already exists"
else
    echo "Creating VCS_BRANCH_ENV_FILE ($VCS_BRANCH_ENV_FILE)"
    touch $VCS_BRANCH_ENV_FILE

    if [[ -f ".env/dev"  ]]; then
        cat ".env/dev" > $VCS_BRANCH_ENV_FILE
    fi

    # Hack an issue specific queue name into the environment file
    QNAME_XERO=$VCS_BRANCH_LO"_test.fifo"
    QNAME_CAL=$VCS_BRANCH_LO"_test_cal.fifo"

    sed -i 's|XERO_QUEUE_URL=.*|XERO_QUEUE_URL='$QNAME_XERO'|g' $VCS_BRANCH_ENV_FILE
    sed -i 's|XERO_QUEUE_URL_CAL=.*|XERO_QUEUE_URL_CAL='$QNAME_CAL'|g' $VCS_BRANCH_ENV_FILE
fi

# Add local folder overrides
if [[ -e .env.local ]] ; then
    echo "Local config file .env.local exists - preserving it"
else
    echo "Creating local config file .env.local"
    echo "APPLICATION_SOURCE_FOLDER=$ROOT_DIR/api/loan" >> .env.local
    echo "UI_SOURCE_FOLDER=$ROOT_DIR/ui/app" >> .env.local
fi

cat $VCS_BRANCH_ENV_FILE;

source $VCS_BRANCH_ENV_FILE

#cp $VCS_BRANCH_ENV_FILE api/loan/.env
#cat $VCS_BRANCH_ENV_FILE > ui/app/.env.dev
#sed -i 's|_=.*||g' api/loan/.env

test -f ./.env.local && source ./.env.local || true

# Clean cached data where necessary
rm -rf api/loan/var/
rm -rf ui/app/node_modules/

# Make sure the docker process can read everything it needs :sledgehammer:
chmod -R go+rX api infrastructure scripts ui || true

docker_compose="docker-compose -f docker/docker-compose.yaml -f docker/docker-compose-devtest.yaml --project-directory ."

cat <<EOF
--------------------------------------------------------------------------------
Build the UI application
EOF
pushd ui > /dev/null
$docker_compose build
$docker_compose run ui_code
popd > /dev/null

cat <<EOF
--------------------------------------------------------------------------------
Build the API and migrate the database
EOF
pushd api > /dev/null
$docker_compose build
$docker_compose run api_code
popd > /dev/null

cat <<EOF
--------------------------------------------------------------------------------
Build and start the infrastructure
EOF
pushd infrastructure/ranqx-ecs-docker > /dev/null
$docker_compose build
$docker_compose up --detach
popd > /dev/null

cat <<EOF
--------------------------------------------------------------------------------
Local environment startup complete

EOF
