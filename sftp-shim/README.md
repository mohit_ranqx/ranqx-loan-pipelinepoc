# SFTP Shim for Ranqx-LO
> Bridge from AWS S3 to Kiwibank

It Allows encrypted files with payload data to be available for Kiwibank. 
The files are fetched from AWS S3, encrypted and transfered to a secure location in the SFTP Shim server.

The FTP is enabled as a SFTP subsystem from SSHD.
Cron invokes the bridge script every 60 seconds.
The bridge script encrypts the newest files, moves the file to the Chroot jail of user `sftpuser` and deletes temporary non-encrypted files.

## Installing / Getting Started

To access the files:
* IP from kiwibank must be listed in the AWS VPC security group
* Access to the gpg key for decryption

## Folder Contents

1. SFTP configuration file. 
2. Script to fetch from S3 and ecnrypt files.

### Debugging Instructions

Run the following to restart and validate status of involved services.

```shell
sudo systemctl status ssh
sudo systemctl restart ssh
sudo systemctl status cron
sudo systemctl restart cron
```