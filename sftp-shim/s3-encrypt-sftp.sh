#!/usr/bin/env bash
#   This script checks the S3 bucket $BUCKET_NAME for new files.
#   It has a directory $S3_ENCRYPTED containing empty files mirroring the S3 bucket
#   each $NEW_FILE not present in the local directory is fetched 
#   to $S3_DIR, gpg encrypted for $RECIPIENT, and moved to $GPG_ENCRYPTED
#   directory, where it's accessible to SFTP download.

set -euxo pipefail

RECIPIENT=
BUCKET_NAME=
S3_DIR=/home/ubuntu/s3-bucket
S3_ENCRYPTED=/home/ubuntu/s3-encrypted
GPG_ENCRYPTED=/home/sftpdir/payload

[[ -n "$RECIPIENT" ]] || { echo 'SFTP Shim recipient must be configured' && exit 1; }
[[ -n "$BUCKET_NAME" ]] || { echo 'SFTP Shim bucket must be configured' && exit 1; }

# 1. get a list of files in S3
NEW_FILES=$(aws s3 ls $BUCKET_NAME | awk '{print $4}')

# 2. for each file in the listing
COUNT=1
for NEW_FILE in $NEW_FILES; do
    # 3. If that file exists locally: Do nothing
    if [[ -f "$S3_ENCRYPTED/$NEW_FILE.gpg" ]]; then
        # File already encrypted
        :
    else
        # 4. Else: action: Download, encrypt and save
        printf "$(date +%F_%X) Encrypting file $NEW_FILE \n"
        cd $S3_DIR
        aws s3api get-object --bucket $BUCKET_NAME --key $NEW_FILE $NEW_FILE
        gpg --trust-model direct --recipient "$RECIPIENT" --encrypt $NEW_FILE
        sudo mv "$NEW_FILE.gpg" $GPG_ENCRYPTED
        touch $S3_ENCRYPTED/"$NEW_FILE.gpg"
        echo "$(date +%F_%X) File $NEW_FILE.gpg --> encrypted"
        shred --remove --zero "$NEW_FILE"
    fi
    COUNT=$((COUNT + 1))
done
