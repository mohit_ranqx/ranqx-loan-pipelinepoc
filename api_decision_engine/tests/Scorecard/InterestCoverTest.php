<?php declare(strict_types = 1);


namespace App\Tests\Scorecard;


use App\Scorecard\InterestCover;
use PHPUnit\Framework\TestCase;

class InterestCoverTest extends TestCase
{
    //Value is missing.	+27
    //interest_cover < 0.000001	+8
    //0.000001 <= interest_cover < 50	+20
    //50 <= interest_cover	+57

    public function testValueIsMission()
    {
        $obj = new InterestCover(null);
        $this->assertEquals(27, $obj->evaluate());
    }

    public function testLessThan()
    {
        $obj = new InterestCover(0.0000001);
        $this->assertEquals(8, $obj->evaluate());
    }

    public function testInBetween()
    {
        $obj = new InterestCover(0.000001);
        $expected = 20;
        $this->assertEquals($expected, $obj->evaluate());
        $obj = new InterestCover(0.1);
        $this->assertEquals($expected, $obj->evaluate());
        $obj = new InterestCover(49.999);
        $this->assertEquals($expected, $obj->evaluate());
    }

    public function testMore()
    {
        $obj = new InterestCover(50.00);
        $this->assertEquals(57, $obj->evaluate());

        $obj = new InterestCover(50.009);
        $this->assertEquals(57, $obj->evaluate());
    }
}