<?php declare(strict_types = 1);


namespace App\Tests\Scorecard;


use App\Scorecard\RevenueGrowth;
use PHPUnit\Framework\TestCase;

class RevenueGrowthTest extends TestCase
{
    public function testEvaluateNull()
    {
        $obj = new RevenueGrowth(null);
        $expected = 12;
        $this->assertEquals($expected, $obj->evaluate());
    }

    public function testEvaluateNeg()
    {
        $obj = new RevenueGrowth(-9.087);
        $expected = 21;
        $this->assertEquals($expected, $obj->evaluate());
    }
    //0<= revenue_growth < 0.25
    public function testEvaluate1()
    {
        $obj = new RevenueGrowth(0.00);
        $expected = 32;
        $this->assertEquals($expected, $obj->evaluate());

        $obj = new RevenueGrowth(0.15);
        $expected = 32;
        $this->assertEquals($expected, $obj->evaluate());

        $obj = new RevenueGrowth(0.248);
        $expected = 32;
        $this->assertEquals($expected, $obj->evaluate());
    }

    //0.25 <= revenue_growth < 0.55
    public function testEvaluate2()
    {
        $obj = new RevenueGrowth(0.25);
        $expected = 51;
        $this->assertEquals($expected, $obj->evaluate());

        $obj = new RevenueGrowth(0.26);
        $this->assertEquals($expected, $obj->evaluate());

        $obj = new RevenueGrowth(0.54);
        $this->assertEquals($expected, $obj->evaluate());
    }
    //0.55 <= revenue_growth
    public function testEvaluate3()
    {
        $obj = new RevenueGrowth(0.55);
        $expected = 31;
        $this->assertEquals($expected, $obj->evaluate());

        $obj = new RevenueGrowth(0.56);
        $this->assertEquals($expected, $obj->evaluate());

        $obj = new RevenueGrowth(2.45);
        $this->assertEquals($expected, $obj->evaluate());
    }

}