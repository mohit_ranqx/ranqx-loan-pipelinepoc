<?php declare(strict_types = 1);


namespace App\Tests\Decision;

use App\Decision\OldestPeriod;
use App\Decision\Policy;
use PHPUnit\Framework\TestCase;

class OldestPeriodTest extends TestCase
{
    /**
     * @dataProvider provider
     */
    public function testPolicyRule($data):void
    {
        $obj = new OldestPeriod($data['xeroData']);

        $this->assertEquals(Policy::PASS, $obj->policyRule());
    }
    //providerZero
    /**
     * @dataProvider providerZero
     */
    public function testPolicyRule1($data): void
    {
        $obj = new OldestPeriod($data['xeroData']);

        $this->assertEquals(Policy::DECLINE, $obj->policyRule());
    }

    public function provider(): array
    {
        $data = [
            'currentMonth' => [
                0 => [
                    'id' => 3,
                    'organisation_id' => 49,
                    'revenue_current' => '137482.8300000',
                    'gross_margin_value_current' => '61359.6600000',
                    'gross_margin_percentage_current' => '0.4463078',
                    'ebitda_value_current' => '1366.6600000',
                    'ebitda_margin_current' => '0.0099406',
                    'interest_cover_current' => '2.1287539',
                    'marginal_cashflow_current' => '-1.0612962',
                    'working_capital_ratio_current' => '1.2850511',
                    'quick_ratio_current' => '0.4169007',
                    'cash_ratio_current' => '0.1446260',
                    'net_cash_balance_current' => '40801.9500000',
                    'debt_ratio_current' => '0.6528806',
                    'equity_ratio_current' => '0.2862172',
                    'people_cost_ratio_current' => '0.3077839',
                    'operating_cost_ratio_current' => '0.0884334',
                    'period_start' => '2019-07-01T00:00:00+00:00',
                    'period_end' => '2019-07-31T23:59:59+00:00',
                ],
            ],
            'nzbn' => 2,
            'xeroData' => [
                'total' => 24,
                'oldest' =>  [
                    'totalRevenue' => 179940.83,
                    'totalCog' => 78812.17,
                    'totalPeople' => 36315,
                    'totalRent' => 5699.92,
                    'totalOperating' => 12158.08,
                    'totalOther' => 2723.58,
                    'totalAssetsCurrentBank' => 54750,
                    'totalAssetsCurrentDebtors' => 76416.68,
                    'totalAssetsCurrentInventory' => 225864,
                    'totalAssetsCurrentOther' => 184238,
                    'totalAssetsFixed' => 548752.17,
                    'totalAssetsOther' => 0,
                    'totalLiabilitiesCurrentBank' => 14000,
                    'totalLiabilitiesCurrentCreditors' => 20411.57,
                    'totalLiabilitiesCurrentOther' => 184196.85,
                    'totalLiabilitiesOther' => 405670,
                    'totalEquityCapital' => 243115,
                    'totalEquityEarnings' => 119629.11,
                ],
            ]
        ];

        return array(
            [$data]
        );
    }

    public function providerZero(): array
    {
        $data = [
            'currentMonth' => [
                0 => [
                    'id' => 3,
                    'organisation_id' => 49,
                    'revenue_current' => '137482.8300000',
                    'gross_margin_value_current' => '61359.6600000',
                    'gross_margin_percentage_current' => '0.4463078',
                    'ebitda_value_current' => '1366.6600000',
                    'ebitda_margin_current' => '0.0099406',
                    'interest_cover_current' => '2.1287539',
                    'marginal_cashflow_current' => '-1.0612962',
                    'working_capital_ratio_current' => '1.2850511',
                    'quick_ratio_current' => '0.4169007',
                    'cash_ratio_current' => '0.1446260',
                    'net_cash_balance_current' => '40801.9500000',
                    'debt_ratio_current' => '0.6528806',
                    'equity_ratio_current' => '0.2862172',
                    'people_cost_ratio_current' => '0.3077839',
                    'operating_cost_ratio_current' => '0.0884334',
                    'period_start' => '2019-07-01T00:00:00+00:00',
                    'period_end' => '2019-07-31T23:59:59+00:00',
                ],
            ],
            'nzbn' => 2,
            'xeroData' => [
                'total' => 24,
                'oldest' =>  [
                    'totalRevenue' => 0,
                    'totalCog' => 0,
                    'totalPeople' => 0,
                    'totalRent' => 0,
                    'totalOperating' => 0,
                    'totalOther' => 0,
                    'totalAssetsCurrentBank' => 0,
                    'totalAssetsCurrentDebtors' => 0,
                    'totalAssetsCurrentInventory' => 0,
                    'totalAssetsCurrentOther' => 0,
                    'totalAssetsFixed' => 0,
                    'totalAssetsOther' => 0,
                    'totalLiabilitiesCurrentBank' => 0,
                    'totalLiabilitiesCurrentCreditors' => 0,
                    'totalLiabilitiesCurrentOther' => 0,
                    'totalLiabilitiesOther' => 0,
                    'totalEquityCapital' => 0,
                    'totalEquityEarnings' => 0,
                ],
            ]
        ];

        return array(
            [$data]
        );
    }
}