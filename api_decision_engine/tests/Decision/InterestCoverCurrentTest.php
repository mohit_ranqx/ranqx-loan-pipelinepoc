<?php declare(strict_types = 1);


namespace App\Tests\Decision;


use App\Decision\InterestCoverCurrent;
use App\Decision\Policy;
use PHPUnit\Framework\TestCase;

class InterestCoverCurrentTest extends TestCase
{
    public function testPolicyRule()
    {
        $obj = new InterestCoverCurrent(1.0);
        $this->assertEquals(Policy::REFER, $obj->policyRule());
    }

    public function testPolicyRule2()
    {
        $obj = new InterestCoverCurrent(3.0);
        $this->assertEquals(Policy::PASS, $obj->policyRule());
    }

    public function testPolicyRule3()
    {
        $obj = new InterestCoverCurrent(2.0);
        $this->assertEquals(Policy::PASS, $obj->policyRule());
    }
}