<?php declare(strict_types = 1);


namespace App\Tests\Decision;


use App\Decision\NumberOfNzbn;
use App\Decision\Policy;
use PHPUnit\Framework\TestCase;

class NumberOfNzbnTest extends TestCase
{
    public function testPolicyRule()
    {
        $obj = new NumberOfNzbn(2);
        $this->assertEquals(Policy::REFER, $obj->policyRule());
    }

    public function testPolicyRule1()
    {
        $obj = new NumberOfNzbn(3);
        $this->assertEquals(Policy::REFER, $obj->policyRule());
    }

    public function testPolicyRule2()
    {
        $obj = new NumberOfNzbn(1);
        $this->assertEquals(Policy::PASS, $obj->policyRule());
    }
}