<?php declare(strict_types = 1);


namespace App\Tests\Decision;

use App\Decision\DebtRatioCurrent;
use App\Decision\Policy;
use PHPUnit\Framework\TestCase;

class DebtRationCurrentTest extends TestCase
{
    public function testPolicyRule()
    {
        $obj = new DebtRatioCurrent(1.0);
        $this->assertEquals(Policy::REFER, $obj->policyRule());
    }

    public function testPolicyRule2()
    {
        $obj = new DebtRatioCurrent(2.0);
        $this->assertEquals(Policy::PASS, $obj->policyRule());
    }

    public function testPolicyRule3()
    {
        $obj = new DebtRatioCurrent(0.0);
        $this->assertEquals(Policy::REFER, $obj->policyRule());
    }
}