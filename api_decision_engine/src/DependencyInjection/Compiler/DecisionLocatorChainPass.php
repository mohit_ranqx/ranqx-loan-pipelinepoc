<?php declare(strict_types = 1);


namespace App\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use App\DecisionHandler\DecisionDataLocatorChain;

class DecisionLocatorChainPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {

        if (!$container->has(DecisionDataLocatorChain::class)) {
            return;
        }

        $definition = $container->findDefinition(DecisionDataLocatorChain::class);
        $taggedServices = $container->findTaggedServiceIds('app.decision.locator');
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addLocator', [new Reference($id)]);
        }
    }
}