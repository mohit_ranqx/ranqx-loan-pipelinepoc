<?php declare(strict_types = 1);


namespace App\DependencyInjection\Compiler;


use App\Scorecard\ScorecardDataLocatorChain;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ScorecardLocatorChainPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {

        if (!$container->has(ScorecardDataLocatorChain::class)) {
            return;
        }

        $definition = $container->findDefinition(ScorecardDataLocatorChain::class);
        $taggedServices = $container->findTaggedServiceIds('app.scorecard.locator');
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addLocator', [new Reference($id)]);
        }
    }
}