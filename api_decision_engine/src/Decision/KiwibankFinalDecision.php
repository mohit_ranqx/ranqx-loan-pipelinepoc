<?php declare(strict_types = 1);


namespace App\Decision;


use App\Decision\Exception\KiwibankFinalResultNoExistException;

class KiwibankFinalDecision
{
    use PolicyResult;

    private $cumulativePolicyDecision;
    private $cumulativeScorecardDecision;
    public function __construct(int $cumulativePolicyDecision, int $cumulativeScorecardDecision)
    {
        $this->cumulativePolicyDecision = $cumulativePolicyDecision;
        $this->cumulativeScorecardDecision = $cumulativeScorecardDecision;
    }

    public function make(): int
    {
        //Cumulative Policy Decision Cumulative Scorecard Decision final
        //DECLINE	DECLINE	DECLINE
        //DECLINE	REFER	DECLINE
        //DECLINE	APPROVE	DECLINE
        //REFER	DECLINE	DECLINE
        //REFER	REFER	REFER
        //REFER	APPROVE	REFER
        //PASS	DECLINE	DECLINE
        //PASS	REFER	REFER
        //PASS	APPROVE	APPROVE

        $table[] = [Policy::DECLINE,Policy::DECLINE,Policy::DECLINE];
        $table[] = [Policy::DECLINE,Policy::REFER,Policy::DECLINE];
        $table[] = [Policy::DECLINE,Policy::APPROVE,Policy::DECLINE];
        $table[] = [Policy::REFER,Policy::DECLINE,Policy::DECLINE];
        $table[] = [Policy::REFER,Policy::REFER,Policy::REFER];
        $table[] = [Policy::REFER,Policy::APPROVE,Policy::REFER];
        $table[] = [Policy::PASS,Policy::DECLINE,Policy::DECLINE];
        $table[] = [Policy::PASS,Policy::REFER,Policy::REFER];
        $table[] = [Policy::PASS,Policy::APPROVE,Policy::APPROVE];

        foreach ($table as $decsion) {
            if ($decsion[0] === $this->cumulativePolicyDecision &&
            $decsion[1] === $this->cumulativeScorecardDecision) {
                return $decsion[2];
            }
        }

        $str = 'final result not found. cumulative Policy Decision '.$this->cumulativePolicyDecision;
        $str .= ' cumulative Scorecard Decision '.$this->cumulativeScorecardDecision;
        throw new KiwibankFinalResultNoExistException($str);
    }
}