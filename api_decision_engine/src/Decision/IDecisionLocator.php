<?php declare(strict_types = 1);


namespace App\Decision;


/**
 * Interface IDecisionLocator
 * @package App\Decision
 */
interface IDecisionLocator
{
    /**
     * @param $dataSrc
     * @return IRule|Policy
     */
    public function findData($dataSrc) : IRule;
    public function getDecisionClass(): string;
}