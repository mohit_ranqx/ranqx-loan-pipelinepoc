<?php declare(strict_types = 1);


namespace App\Decision;


/**
 * Class InterestCoverCurrent
 * @package App\Decision
 * KB_US_PR_006
 */
class InterestCoverCurrent extends Policy implements IRule
{
    /**
     * @var float
     */
    private $data;

    /**
     * InterestCoverCurrent constructor.
     * @param float $data
     */
    public function __construct(float $data)
    {
        $this->data = $data;
    }

    /**
     * @return bool
     *
     *
     */
    public function satisfied(): bool
    {
        return ((2.0 - $this->data) > 0.001);
    }
    /*
     * For every Policy Rule, record Policy Rule Results as follows:
     * ****/
    public function policyRule(): int
    {
        if ($this->satisfied()) {
            return self::REFER;
        }
        return self::PASS;
    }
}