<?php declare(strict_types = 1);


namespace App\Decision;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class InterestCoverCurrentLocator
 * @package App\Decision
 */
class InterestCoverCurrentLocator implements IDecisionLocator
{
    /**
     * @param $dataSrc
     * @return IRule
     * @throws \DomainException
     */
    public function findData($dataSrc): IRule
    {
        //calculate_finance_month_current.interest_cover_current column
        if (isset($dataSrc['currentMonth'][0]['interest_cover_current'])) {
            return new InterestCoverCurrent((float)$dataSrc['currentMonth'][0]['interest_cover_current']);
        }
        throw new KiwiBankDataSrcNoExistException('interest_cover_current field is not been found.');
    }

    public function getDecisionClass(): string
    {
        return "Interest Cover Current";
    }

}