<?php declare(strict_types = 1);


namespace App\Decision;


/**
 * Class NumberOfNzbn
 * @package App\Decision
 *
 * KB_US_PR_002
 * https://ranqxltd.atlassian.net/wiki/spaces/LO/pages/651886596/Ranqx+Decision+Engine
 */
class NumberOfNzbn extends Policy implements IRule
{
    /**
     * @var int
     */
    private $counter;

    /**
     * NumberOfNzbn constructor.
     * @param int $counter
     */
    public function __construct(int $counter)
    {
        $this->counter = $counter;
    }

    /**
     * @return bool
     */
    public function satisfied(): bool
    {
        return $this->counter >= 2;
    }

    public function policyRule(): int
    {
        if ($this->satisfied()) {
            return self::REFER;
        }

        return self::PASS;
    }
}