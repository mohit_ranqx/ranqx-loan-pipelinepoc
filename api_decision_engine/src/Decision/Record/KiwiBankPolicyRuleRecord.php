<?php declare(strict_types = 1);


namespace App\Decision\Record;


use App\Decision\IDecisionLocator;
use App\Decision\Policy;
use App\DecisionHandler\DecisionDataLocatorChain;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use App\Decision\PolicyResult;

class KiwiBankPolicyRuleRecord
{
    use PolicyResult;

    private $orgId;
    private $dataSrc;
    private $data;
    private $locators;

    public function __construct(int $orgId, array $dataSrc, DecisionDataLocatorChain $locatorChain)
    {
        $this->orgId = $orgId;
        $this->dataSrc = $dataSrc;
        $this->locators = $locatorChain;
    }

    public function policyResult(): array
    {
        $records = [];

        /** @var IDecisionLocator $locator ***/
        foreach ($this->locators->getLocators() as $locator) {
            try {
                $decision = $locator->findData($this->dataSrc);
                $result = $decision->policyRule();
                $src = $locator->getDecisionClass();
                $records[$src] = $this->resultToString($result);
            } catch (KiwiBankDataSrcNoExistException $e) {
                $result = Policy::NODATA;
                $src = $locator->getDecisionClass();
                $records[$src] = $this->resultToString($result);
            }
        }

        return $records;
    }

    public function setPolicyResult(array $data)
    {
        $this->data = $data;
    }

    public function getJsonData()
    {
        $combined['org'] = $this->orgId;
        $combined['policyResults'] = $this->policyResult();
    }
}