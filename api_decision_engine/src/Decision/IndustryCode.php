<?php declare(strict_types = 1);


namespace App\Decision;


class IndustryCode extends Policy implements IRule
{

    private $data;

    /**
     * DebtRatioCurrent constructor.
     * @param float $data
     */
    public function __construct(string $data)
    {
        $this->data = $data;
    }

    /**
     * @return bool
     */
    public function satisfied(): bool
    {
        //KB_US_PR_001
        //Company's Debt Ratio <1.5
        return $this->data !== '';
    }

    public function policyRule(): int
    {
        if ($this->satisfied()) {
            return self::REFER;
        }
        return self::PASS;
    }
}