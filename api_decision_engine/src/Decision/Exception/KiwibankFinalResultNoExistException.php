<?php declare(strict_types = 1);


namespace App\Decision\Exception;


use Symfony\Component\Messenger\Exception\UnrecoverableMessageHandlingException;

class KiwibankFinalResultNoExistException extends UnrecoverableMessageHandlingException
{

}