<?php declare(strict_types = 1);


namespace App\Decision;


/**
 * Class OldestPeriod
 * @package App\Decision
 *
 * KB_US_PR_003
 * Check the oldest period in the finance_month table for the current application.
   If there are zeros in ALL the columns for the period, this means we don't have 24 months worth of data.
 */
class OldestPeriod extends Policy implements IRule
{

    /**
     * @var array
     */
    private $data;

    /**
     * OldestPeriod constructor.
     * @param array $oldestFinanceMonth
     */
    public function __construct(array $oldestFinanceMonth)
    {
        $this->data = $oldestFinanceMonth;
    }

    /**
     * @return bool
     * If there are zeros in ALL the columns for the period,
     * satisfied returns true, means
     *
     */
    public function satisfied(): bool
    {
        if (isset($this->data['oldest']['period'])) {
            unset($this->data['oldest']['period']);
        }
        if (isset($this->data['oldest']['staffNumber'])) {
            unset($this->data['oldest']['staffNumber']);
        }

        $allZero = (array_sum($this->data['oldest']) - 0 <= 0.001);
        $number = $this->data['total'];
        return $number >= 24 && $allZero;
    }

    public function policyRule(): int
    {
        if ($this->satisfied()) {
            return self::DECLINE;
        }
        return self::PASS;
    }
}

/* `finance_month`.`total_revenue`,
    `finance_month`.`total_cog`,
    `finance_month`.`total_people`,
    `finance_month`.`total_rent`,
    `finance_month`.`total_operating`,
    `finance_month`.`total_other`,
    `finance_month`.`total_assets_current_bank`,
    `finance_month`.`total_assets_current_debtors`,
    `finance_month`.`total_assets_current_inventory`,
    `finance_month`.`total_assets_current_other`,
    `finance_month`.`total_assets_fixed`,
    `finance_month`.`total_assets_other`,
    `finance_month`.`total_liabilities_current_bank`,
    `finance_month`.`total_liabilities_current_creditors`,
    `finance_month`.`total_liabilities_current_other`,
    `finance_month`.`total_liabilities_other`,
    `finance_month`.`total_equity_capital`,
    `finance_month`.`total_equity_earnings`,
    `finance_month`.`interest` */