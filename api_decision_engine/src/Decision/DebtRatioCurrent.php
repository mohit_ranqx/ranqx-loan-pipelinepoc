<?php declare(strict_types = 1);


namespace App\Decision;


/**
 * Class DebtRatioCurrent
 * @package App\Decision
 * KB_US_PR_005
 *
 */
class DebtRatioCurrent extends Policy implements IRule
{
    /**
     * @var float
     */
    private $data;

    /**
     * DebtRatioCurrent constructor.
     * @param float $data
     */
    public function __construct(float $data)
    {
        $this->data = $data;
    }

    /**
     * @return bool
     */
    public function satisfied(): bool
    {
        //Company's Debt Ratio <1.5
        return ((1.5 - $this->data) > 0.001);
    }

    public function policyRule(): int
    {
        if ($this->satisfied()) {
            return self::REFER;
        }
        return self::PASS;
    }
}