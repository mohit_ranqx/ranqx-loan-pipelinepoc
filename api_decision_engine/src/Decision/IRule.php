<?php declare(strict_types = 1);


namespace App\Decision;


interface IRule
{
    public function policyRule() : int;
}