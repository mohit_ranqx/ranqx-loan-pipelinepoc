<?php declare(strict_types = 1);


namespace App\Decision;


use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

/**
 * Class NumberOfNzbnLocator
 * @package App\Decision
 */
class NumberOfNzbnLocator implements IDecisionLocator
{
    /**
     * @param $dataSrc
     * @return IRule
     * @throws \DomainException
     */
    public function findData($dataSrc): IRule
    {
        if (isset($dataSrc['nzbn'])) return new NumberOfNzbn((int)$dataSrc['nzbn']);
        throw new KiwiBankDataSrcNoExistException('nzbn field is not been found.');
    }

    public function getDecisionClass(): string
    {
        return 'Multiple applications';
    }
}