<?php declare(strict_types = 1);


namespace App\Decision;


use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class OldestPeriodLocator
 * @package App\Decision
 */
class OldestPeriodLocator implements IDecisionLocator
{
    /**
     * @param $dataSrc
     * @return IRule
     * @throws \DomainException
     */
    public function findData($dataSrc): IRule
    {
        if (isset($dataSrc['xeroData'])) return new OldestPeriod($dataSrc['xeroData']);
        throw new KiwiBankDataSrcNoExistException('Oldest Period (xeroData) field is not been found.');
    }

    public function getDecisionClass(): string
    {
        return 'Age of Xero data';
    }
}