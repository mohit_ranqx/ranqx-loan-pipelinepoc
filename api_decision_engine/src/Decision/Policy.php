<?php declare(strict_types = 1);


namespace App\Decision;


abstract class Policy
{
    public const PASS = 1;
    public const REFER = 2;
    public const DECLINE = 3;
    public const NODATA = 4;
    public const APPROVE = 5;

    /*
    A Policy Rule Check involves evaluating a pre-defined expression against
    the relevant data to achieve a TRUE or FALSE outcome
    *****/
    abstract public function satisfied(): bool;
}