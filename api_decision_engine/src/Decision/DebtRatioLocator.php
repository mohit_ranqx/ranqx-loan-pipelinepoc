<?php declare(strict_types = 1);


namespace App\Decision;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;


class DebtRatioLocator implements IDecisionLocator
{
    public function findData($dataSrc): IRule
    {
        //calculate_finance_month_current.debt_ratio_current column
        if (isset($dataSrc['currentMonth'][0]['debt_ratio_current'])) {
            return new DebtRatioCurrent((float)$dataSrc['currentMonth'][0]['debt_ratio_current']);
        }
        throw new KiwiBankDataSrcNoExistException('debt_ratio_current field is not been found.');
    }

    public function getDecisionClass(): string
    {
        return "Debt Ratio Current";
    }
}