<?php declare(strict_types = 1);


namespace App\Decision;


trait PolicyResult
{
    public function resultToString(int $result): string
    {
        switch ($result) {
            case Policy::PASS : return 'PASS';
            case Policy::REFER : return 'REFER';
            case Policy::DECLINE : return 'DECLINE';
            case Policy::NODATA : return 'No Data';
            case Policy::APPROVE: return 'APPROVE';
        }
        return '';
    }
}