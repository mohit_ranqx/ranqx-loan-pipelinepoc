<?php declare(strict_types = 1);


namespace App\SQS;

use Aws\Sqs\SqsClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\TransportException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\RedeliveryStamp;
use Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface;
use App\Message\DecisionQueueMessage;
use Symfony\Component\Messenger\Stamp\TransportMessageIdStamp;

class DecisionQueueReceiver implements ReceiverInterface
{
    private $params;
    private $options;
    private $sqs;
    private $counter = 0;

    public function __construct(SqsClient $client,
        ParameterBagInterface $params,
        array $options = [])
    {
        $this->params = $params;
        $this->options  = $options;
        $this->sqs = $client;
    }

    public function get(): iterable
    {
        $info = new DecisionReceiveFIFOParameter($this->params);
        $obj = new DecisionFIFOQueueReceive($info, $this->sqs);
        $data = '';
        $obj->receive(function($message) use (&$data) {
            $data = $message;
        });
        $dm = new DecisionQueueMessage($data);
        $envelope = new Envelope($dm);
        $envelope = $envelope->with(new RedeliveryStamp($this->counter, 'decision'));
        return [$envelope];
    }

    public function ack(Envelope $envelope): void
    {
        // TODO: Implement ack() method.
    }

    public function reject(Envelope $envelope): void
    {
        // TODO: Implement reject() method.
    }

    public function retryCounter(int $retry): void
    {
        $this->counter = $retry;
    }

}