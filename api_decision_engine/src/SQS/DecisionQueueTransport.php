<?php declare(strict_types = 1);


namespace App\SQS;

use Aws\Sqs\SqsClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\TransportException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;
use App\Message\DecisionQueueMessage;
use Symfony\Component\Messenger\Exception;

class DecisionQueueTransport implements TransportInterface
{
    private $params;
    private $options;
    private $sqs;
    private $receiver;
    private $counter = 1;

    public function __construct(SqsClient $client,
        ParameterBagInterface $params,
        array $options = [])
    {
        $this->params = $params;
        $this->options  = $options;
        $this->sqs = $client;
    }

    public function get(): iterable
    {
        if ($this->receiver === null) {
            $this->receiver = new DecisionQueueReceiver($this->sqs, $this->params, $this->options);
        } else {
            ++$this->counter;
            $this->receiver->retryCounter($this->counter);
        }
        return $this->receiver->get();
    }

    public function ack(Envelope $envelope): void
    {
        // TODO: Implement ack() method.
        // dump($envelope);
    }

    public function reject(Envelope $envelope): void
    {
    }

    public function send(Envelope $envelope): Envelope
    {
         return new Envelope(new \StdClass());
    }

}