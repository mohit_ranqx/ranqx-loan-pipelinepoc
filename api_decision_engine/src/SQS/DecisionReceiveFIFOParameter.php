<?php


namespace App\SQS;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class DecisionReceiveFIFOParameter implements FIFOParameterInterface
{
    private $parameter;
    public function __construct(ParameterBagInterface $params)
    {
        $this->parameter = $params;
    }

    public function parameters(): array
    {
	    $ranqx = $this->parameter->get('ranqx');
	    $paras = [
            'QueueUrl' => $ranqx['aws']['queue']['decision']['dsn'],
            'AttributeNames' => ['All'],
            'MessageAttributeNames' => ['All'],
            'MaxNumberOfMessages' => 1,
            'VisibilityTimeout' =>  30,
            'WaitTimeSeconds' =>  20
        ];
        return $paras;
    }
}
