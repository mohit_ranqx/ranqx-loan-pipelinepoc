<?php declare(strict_types = 1);


namespace App\SQS;

interface FIFOReceiverInterface
{
    public function receive(callable $handler): void;
}
