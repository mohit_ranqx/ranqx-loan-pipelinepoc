<?php declare(strict_types = 1);


namespace App\SQS;

use Aws\Result;
use Aws\Sqs\SqsClient;
use Symfony\Component\Messenger\Envelope;

class DecisionFIFOQueueReceive implements FIFOReceiverInterface
{
    /**
     * @var FIFOParameterInterface
     */
    private $params;
    /**
     * @var SqsClient
     */
    private $client;

    public function __construct(FIFOParameterInterface $parameter, SqsClient $client)
    {
        $this->params = $parameter;
        $this->client = $client;
    }

    public function receive(callable $handler): void
    {
        $params = $this->params->parameters();
        /** @var Result $result **/
        $result = $this->client->receiveMessage($params);

        $messages = $result->get('Messages') ?? [];

        foreach ($messages as $message) {

            $messageHandler = $message['ReceiptHandle'] ?? [];
            if ($messageHandler) {
                //ReceiptHandle is invalid
                //This is the expected behavior of SQS given a visibility timeout of 0 seconds
                //When working with a FIFO queue, DeleteMessage operations will fail
                // if the request is received outside of the visibility timeout window/
                //I have a visibility timeout 30s and same error which can only
                // mean I tried to delete a message after 30s am I right?
                // Yes, DeleteMessage will fail with an error if the visibility window has already expired
                /*$this->client->deleteMessage([
                    'QueueUrl' => $params['QueueUrl'],
                    'ReceiptHandle' => $messageHandler
                ]);*/
            }

            $data = trim($message['Body'],'""');
            // this handler should run after deleteMessage, reasons see above comments.
            $handler($data);
        }
    }
}
