<?php declare(strict_types = 1);


namespace App\SQS;

use Aws\Sqs\SqsClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\TransportFactoryInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

class DecisionQueueTransportFactory implements TransportFactoryInterface
{
    private $sqs;
    private $params;

    public function __construct(SqsClient $sqs, ParameterBagInterface $params)
    {
        $this->sqs = $sqs;
        $this->params = $params;
    }

    public function createTransport(string $dsn, array $options, SerializerInterface $serializer): TransportInterface
    {
        return new DecisionQueueTransport($this->sqs, $this->params,$options);
    }

    public function supports(string $dsn, array $options): bool
    {
        return 0 === strpos($dsn, 'decision://');
    }
}