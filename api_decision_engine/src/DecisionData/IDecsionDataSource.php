<?php declare(strict_types = 1);


namespace App\DecisionData;


interface IDecsionDataSource
{
    public function getData(): string ;
}