<?php declare(strict_types = 1);


namespace App\DecisionData;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class DebtRationCurrentDataSource implements IDecsionDataSource
{
    private $parameterBag;
    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    public function getData(): string
    {
        $url = $this->parameterBag->get('decision.engine.data.url');
        //TODO: get data;
        //TODO: set up decision chain
    }
}