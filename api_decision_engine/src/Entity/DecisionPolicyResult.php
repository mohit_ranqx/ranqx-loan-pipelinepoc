<?php declare(strict_types = 1);


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DecisionPolicyResultRepository")
 * @ORM\Table(name="decision_policy_result")
 */
class DecisionPolicyResult
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\Column(type="integer") ***/
    private $applicantId;

    /** @ORM\Column(type="json") ***/
    private $result;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $cumulativePolicyDecision;

    /** @ORM\Column(type="json") ***/
    private $scorecard;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $cumulativeScorecard;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $finalDecision;

    /** @ORM\Column(type="datetime") ****/
    protected $created;

    public function __construct()
    {
        $this->created  = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created): void
    {
        $this->created = $created;
    }



    /**
     * @return mixed
     */
    public function getCumulativeScorecard()
    {
        return $this->cumulativeScorecard;
    }

    /**
     * @param mixed $cumulativeScorecard
     */
    public function setCumulativeScorecard($cumulativeScorecard): void
    {
        $this->cumulativeScorecard = $cumulativeScorecard;
    }

    /**
     * @return mixed
     */
    public function getFinalDecision()
    {
        return $this->finalDecision;
    }

    /**
     * @param mixed $finalDecision
     */
    public function setFinalDecision($finalDecision): void
    {
        $this->finalDecision = $finalDecision;
    }
    /**
     * @return mixed
     */
    public function getScorecard()
    {
        return $this->scorecard;
    }

    /**
     * @param mixed $scorecard
     */
    public function setScorecard($scorecard): void
    {
        $this->scorecard = $scorecard;
    }

    /**
     * @return mixed
     */
    public function getCumulativePolicyDecision()
    {
        return $this->cumulativePolicyDecision;
    }

    /**
     * @param mixed $cumulativePolicyDecision
     */
    public function setCumulativePolicyDecision($cumulativePolicyDecision): void
    {
        $this->cumulativePolicyDecision = $cumulativePolicyDecision;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getApplicantId()
    {
        return $this->applicantId;
    }

    /**
     * @param mixed $applicantId
     */
    public function setApplicantId($applicantId): void
    {
        $this->applicantId = $applicantId;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result): void
    {
        $this->result = $result;
    }
}