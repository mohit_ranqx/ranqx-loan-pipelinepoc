<?php declare(strict_types = 1);


namespace App\DecisionHandler;



use App\Decision\IDecisionLocator;

/**
 * Class DecisionDataLocatorChain
 * @package App\DecisionHandler
 */
class DecisionDataLocatorChain
{
    /**
     * @var $locators IDecisionLocator[]
     */
    private $locators;

    /**
     * DecisionDataLocatorChain constructor.
     */
    public function __construct()
    {
        $this->locators = [];
    }

    /**
     * @param IDecisionLocator $locator
     */
    public function addLocator(IDecisionLocator $locator): void
    {
        $this->locators[] = $locator;
    }


    /**
     * @return IDecisionLocator[]
     */
    public function getLocators(): array
    {
        return $this->locators;
    }
}