<?php declare(strict_types = 1);


namespace App\DecisionHandler;

use App\Decision\IRule;
use App\DecisionData\IDecsionDataSource;

class DecisionChain
{
    private $rules;

    public function __construct()
    {
        $this->rules = [];
    }

    public function addRule(IDecsionDataSource $rule)
    {
        $this->rules[] = $rule;
    }

    public function getRule()
    {
        return $this->rules;
    }
}