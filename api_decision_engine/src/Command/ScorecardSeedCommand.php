<?php declare(strict_types = 1);


namespace App\Command;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\ScorecardSeed;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ScorecardSeedCommand extends Command
{
    protected static $defaultName = 'app:ranqx:scorecard:seed';
    private $orm;
    private $para;

    public function __construct(EntityManagerInterface $orm, ParameterBagInterface $bag)
    {
        $this->orm = $orm;
        $this->para = $bag;
        parent::__construct(null);
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('seeding bank score card')
            ->setHelp('seeding bank score card');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $decision = $this->para->get('decision');
        if (!isset($decision['seeds'])) {
            throw new \DomainException('seeds value not exists. check your decision.yaml file.');
        }
        $seeds = $decision['seeds'];

        foreach ($seeds as $bank => $value) {
            $seed = $this->orm->getRepository(ScorecardSeed::class)->findOneBy(['name' => $bank]);
            if ($seed) {
                $seed->setValue($value);
            } else {
                $seed = new ScorecardSeed();
                $seed->setName($bank);
                $seed->setValue($value);
            }
            $this->orm->persist($seed);
        }

        $this->orm->flush();
    }
}