<?php declare(strict_types = 1);


namespace App\Scorecard;

use App\Scorecard\IScorecardLocator;

/**
 * Class ScorecardDataLocatorChain
 * @package App\Scorecard
 */
class ScorecardDataLocatorChain
{

    /**
     * @var $locators IScorecardLocator[]
     */
    private $locators;

    /**
     * ScorecardDataLocatorChain constructor.
     */
    public function __construct()
    {
        $this->locators = [];
    }

    /**
     * @param IScorecardLocator $locator
     */
    public function addLocator(IScorecardLocator $locator): void
    {
        $this->locators[] = $locator;
    }


    /**
     * @return IScorecardLocator[]
     */
    public function getLocators(): array
    {
        return $this->locators;
    }
}