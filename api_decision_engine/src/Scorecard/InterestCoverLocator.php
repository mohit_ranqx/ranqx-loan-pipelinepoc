<?php declare(strict_types = 1);


namespace App\Scorecard;


use App\Decision\Exception\KiwiBankDataSrcNoExistException;

class InterestCoverLocator implements IScorecardLocator
{
    public function findData($dataSrc): IScorecardRule
    {
        if (isset($dataSrc['calFinanceCurrentMonth'][0]['interest_cover_current'])) {
            return new InterestCover((float)$dataSrc['calFinanceCurrentMonth'][0]['interest_cover_current']);
        }
        throw new KiwiBankDataSrcNoExistException('interest_cover_current field is not been found.');
    }

    public function getDecisionClass(): string
    {
        return 'Interest Cover';
    }

}