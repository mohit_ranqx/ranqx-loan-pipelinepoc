<?php declare(strict_types = 1);


namespace App\Scorecard;


use App\Decision\Policy;
use App\Decision\PolicyResult;

class KiwibankCumulativeScorecardDecision
{
    use PolicyResult;

    private $data;
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function make(): int
    {
        $total = $this->data['total'];
        if ($total >= 250 && $total <= 449) {
            return Policy::REFER;
        }

        if ($total >= 450) {
            return Policy::APPROVE;
        }

        if ($total < 250 ) {
            return Policy::DECLINE;
        }
    }
}