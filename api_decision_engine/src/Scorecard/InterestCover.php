<?php declare(strict_types = 1);


namespace App\Scorecard;


class InterestCover implements IScorecardRule
{
    private $interestCover;
    public function __construct(?float $interestCover)
    {
        $this->interestCover = $interestCover;
    }

    public function evaluate(): int
    {
        //Value is missing.	+27

        //0.000001 <= interest_cover < 50	+20
        //50 <= interest_cover	+57
        if ($this->interestCover === null) {
            return 27;
        }
        //interest_cover < 0.000001	+8
        if ( ($this->interestCover - 0.000001) < 0) {
            return 8;
        }

        //0.000001 <= interest_cover < 50
        if (($this->interestCover - 0.000001) >= 0 && $this->interestCover < 50) {
            return 20;
        }

        if ($this->interestCover >= 50 ) {
            return 57;
        }
    }
}