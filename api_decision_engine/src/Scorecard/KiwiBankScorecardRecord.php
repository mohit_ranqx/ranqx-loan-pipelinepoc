<?php declare(strict_types = 1);


namespace App\Scorecard;


use App\Decision\Exception\KiwiBankDataSrcNoExistException;
use App\Decision\IDecisionLocator;

class KiwiBankScorecardRecord
{
    private $data;
    private $locators;
    private $seed;

    public function __construct($seed, array $dataSrc, ScorecardDataLocatorChain $locatorChain)
    {
        $this->data = $dataSrc;
        $this->locators = $locatorChain;
        $this->seed = $seed;
    }

    public function result(): array
    {
        $records = [];

        /** @var IDecisionLocator $locator ***/
        foreach ($this->locators->getLocators() as $locator) {
            try {
                $card = $locator->findData($this->data);
                $result = $card->evaluate();
                $src = $locator->getDecisionClass();
                $records[$src] = $result;
            } catch (KiwiBankDataSrcNoExistException $e) {
                $src = $locator->getDecisionClass();
                $records[$src] = 0;
            }
        }

        $records['total'] = $this->seed + array_sum($records);
        $records['seed'] = $this->seed;
        return $records;
    }
}