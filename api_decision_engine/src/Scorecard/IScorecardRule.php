<?php declare(strict_types = 1);


namespace App\Scorecard;


interface IScorecardRule
{
    public function evaluate(): int;
}