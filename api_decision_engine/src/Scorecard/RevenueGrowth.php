<?php declare(strict_types = 1);


namespace App\Scorecard;


class RevenueGrowth implements IScorecardRule
{
    private $revenueGrowthYearly;
    public function __construct(?float $revenueGrowthYearly)
    {
        $this->revenueGrowthYearly = $revenueGrowthYearly;
    }

    public function evaluate(): int
    {
        if ($this->revenueGrowthYearly === null) {
            return 12;
        }
        if ($this->revenueGrowthYearly < 0) {
            return 21;
        }
        //0<= revenue_growth < 0.25
        if ($this->revenueGrowthYearly >= 0 && $this->revenueGrowthYearly < 0.25) {
            return 32;
        }
        //0.25 <= revenue_growth < 0.55
        if ($this->revenueGrowthYearly >= 0.25 && $this->revenueGrowthYearly < 0.55) {
            return 51;
        }

        //0.55 <= revenue_growth
        if ($this->revenueGrowthYearly >= 0.55) {
            return 31;
        }
    }
}