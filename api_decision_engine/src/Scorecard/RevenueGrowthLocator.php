<?php declare(strict_types = 1);


namespace App\Scorecard;


use App\Scorecard\RevenueGrowth;
use App\Decision\Exception\KiwiBankDataSrcNoExistException;

/**
 * Class RevenueGrothLocator
 * @package App\Scorecard
 */
class RevenueGrowthLocator implements IScorecardLocator
{
    /**
     * @param $dataSrc
     * @return IScorecardRule
     */
    public function findData($dataSrc): IScorecardRule
    {
        if (isset($dataSrc['calFinanceYearly'][0]['revenue_growth_yearly'])) {
            return new RevenueGrowth((float)$dataSrc['calFinanceYearly'][0]['revenue_growth_yearly']);
        }
        throw new KiwiBankDataSrcNoExistException('revenue_growth_yearly field is not been found.');
    }

    /**
     * @return string
     */
    public function getDecisionClass(): string
    {
        return 'Revenue Growth Yearly';
    }

}