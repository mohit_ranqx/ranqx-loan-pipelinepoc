<?php declare(strict_types = 1);


namespace App\Scorecard;



interface IScorecardLocator
{
    public function findData($dataSrc) : IScorecardRule;
    public function getDecisionClass(): string;
}