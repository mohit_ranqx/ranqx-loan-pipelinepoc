<?php declare(strict_types = 1);


namespace App\MessageHandler;


use App\Entity\DecisionPolicyResult;
use App\Entity\ScorecardSeed;
use App\Scorecard\ScorecardDataLocatorChain;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\Message\DecisionQueueMessage;
use GuzzleHttp\Client;
use App\DecisionHandler\DecisionDataLocatorChain;
use App\Decision\Record\KiwiBankPolicyRuleRecord;
use App\Decision\KiwiBankCumulativePolicyDecision;
use App\Scorecard\KiwiBankScorecardRecord;
use App\Scorecard\KiwibankCumulativeScorecardDecision;
use App\Decision\KiwibankFinalDecision;
use function json_decode;

/**
 * Class DecisionQueueMessageHandler
 * @package App\MessageHandler
 */
class DecisionQueueMessageHandler implements MessageHandlerInterface
{
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var DecisionDataLocatorChain
     */
    private $locators;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ScorecardDataLocatorChain
     */
    private $scoreLocatorChain;


    /**
     * DecisionQueueMessageHandler constructor.
     * @param ParameterBagInterface $params
     * @param DecisionDataLocatorChain $locatorChain
     * @param ScorecardDataLocatorChain $scoreLocatorChain
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        ParameterBagInterface $params,
        DecisionDataLocatorChain $locatorChain,
        ScorecardDataLocatorChain $scoreLocatorChain,
        EntityManagerInterface $entityManager)
    {
        $this->params = $params;
        $this->locators = $locatorChain;
        $this->scoreLocatorChain = $scoreLocatorChain;
        $this->entityManager = $entityManager;
    }


    /**
     * @param DecisionQueueMessage $message
     * @throws GuzzleException
     */
    public function __invoke(DecisionQueueMessage $message)
    {
        //TODO: need refactor here. different bank need different decsion.
        $content = $message->getContent();
        if ($content !== '') {
            $data = json_decode($content,true);
            //TODO: make decision here
            $dataSrc = $this->getDecisionData($data['orgId']);
            //TODO: Different bank use different policy
            // Policy Rule checks
            // Once all the Policy Rule Checks have been performed,
            // and all of the Policy Rule Results have been recorded, move to the next stage - Checking Scorecard Rules.
            //if ($data['bank'] === 'KiwiBank')
            $record =  new KiwiBankPolicyRuleRecord($data['orgId'], $dataSrc['data'], $this->locators);
            $records = $record->policyResult();

            $log = $this->logging($data['applicantId'],$records);

            //TODO: next make Cumulative Policy Decision
            $cd = new KiwiBankCumulativePolicyDecision($records);
            $cumulativePolicyDecision = $cd->make();
            $log->setCumulativePolicyDecision($cd->resultToString($cumulativePolicyDecision));



            //score card
            //bank name comes from decsion.yaml file.
            $decision = $this->params->get('decision');
            $seed = $this->entityManager->getRepository(ScorecardSeed::class)
                ->findOneBy(['name' => $decision['bank']]);

            if ($seed) {
                $card = new KiwiBankScorecardRecord($seed->getValue(),$dataSrc['data'], $this->scoreLocatorChain);
                $cardResult = $card->result();
                $log->setScorecard($cardResult);

                $cardDecision= new KiwibankCumulativeScorecardDecision($cardResult);
                $cumulativeScorecard = $cardDecision->make();
                $result = $cardDecision->resultToString($cumulativeScorecard);
                $log->setCumulativeScorecard($result);
                //final decision
                $fd = new KiwibankFinalDecision($cumulativePolicyDecision, $cumulativeScorecard);
                $log->setFinalDecision($fd->resultToString($fd->make()));
            }


            $this->entityManager->persist($log);
            $this->entityManager->flush();
        }
    }

    /**
     * @param $appId
     * @param $records
     * @return DecisionPolicyResult
     */
    private function logging($appId,$records): DecisionPolicyResult
    {
        $result = new DecisionPolicyResult();
        $result->setApplicantId($appId);
        $result->setResult($records);
        return $result;
    }

    /**
     * @param int $orgId
     * @return mixed
     * @throws GuzzleException
     */
    public function getDecisionData(int $orgId)
    {
        $url = $this->params->get('decision.engine.data.url');
        $client = new Client();
        $res = $client->request('GET', $url . '?org='.$orgId, ['verify' => false]);
        return json_decode( $res->getBody()->getContents(), true);
    }
}