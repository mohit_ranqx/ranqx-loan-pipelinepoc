# TODO

Build server manual steps

```
Add staff SSH keys

echo "PATH=$PATH:/home/ubuntu/ranqx-loan/scripts" > ~/.bash_profile

git clone git@bitbucket.org:ranqxteam/ranqx-loan.git
cd ranqx-loan
git checkout --track origin/feature/LO-95-Docker-Improvement
scripts/test-server-init.sh
scripts/test-server-run-branch.sh feature/LO-95-Docker-Improvement

# Then database setup...
```

```
Add SSH key
* * * * * [[ -f /home/ubuntu/ranqx-loan/scripts/test-server-track-branch.sh ]] && cd /home/ubuntu/ranqx-loan && scripts/test-server-track-branch.sh feature/LO-95-Docker-Improvement
# OR
* * * * * cd /home/ubuntu/ranqx-loan && scripts/test-server-track-branch.sh feature/LO-95-Docker-Improvement 2>&1 >> /home/ubuntu/test-server-track-branch.log
```

* Write scripts to automate builds
    * Clean old app files and rebuild
    * Clean old ui files and rebuild
    * List branches available
    * List tags available
* Combine .env files in sub directories to a root-level .env folder
* Combine docker configuration from sub directories into containers/ directory

## References

Error: Failed to create the changeset: Waiter ChangeSetCreateComplete failed: Waiter encountered a terminal failure state Status: FAILED. Reason: Transform AWS::Include failed with: The specified S3 object's content should be valid Yaml/JSON

This

```
KeyName: Ref: Ec2SshKe
```

Needs to become this:

```
KeyName:
    Ref: Ec2SshKey
```

Also, !Ref does not work