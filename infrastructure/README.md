# Ranqx Container Service

## TODO

* User SSM encrypted parameter for RDS password

## Running Containers Locally

```
cd ranqx-ecs-docker
```

### Build

```
set -a && source .env/local && docker-compose build
```

NOTE: set -a, export all env vars

### Run

By default, the `./default_site` folder is used as the application source folder. You can set this to another directory in the `.env.local` file by setting the `APPLICATION_SOURCE_FOLDER`

```
set -a && source .env/local && docker-compose up
```

### Test Servers' SSL Setup

On a Ubuntu 18.04 box, run this:

```
add-apt-repository ppa:certbot/certbot
apt update 
apt install -y certbot

certbot certonly --manual -d *.test.ranqx.io --agree-tos --no-bootstrap --manual-public-ip-logging-ok --preferred-challenges dns-01 --server https://acme-v02.api.letsencrypt.org/directory
```

Setup the requested TXT DNS record, then save the new cert files and rebuild the NGINX containers.

### Debugging

```
docker run --entrypoint /bin/bash -it deref/nginx:v1.1.9
```

### Mount A Volume

Deprecated: we use bind mounts now instead of Docker Volumes

```
docker run -v wa-dashboard_application-volume:/app -it ubuntu bash
```
## Remote - Fargate

### Initial Setup

```
AWS_PROFILE=ranqx-prod ENV=prod scripts/custom/prod/deploy-ecr.sh
AWS_PROFILE=ranqx-prod ENV=prod scripts/custom/prod/deploy-ssl.sh
```

```
AWS_PROFILE-ranqx-prod scripts/ecr-publish.sh
```

## References

Bash shell options: https://www.tldp.org/LDP/abs/html/options.html

Symfony queues: https://symfony.com/doc/current/messenger.html

> By default, the command will run forever: looking for new messages on your transport and handling them. This command is called your "worker".

Docker, 1 main process per container: https://docs.docker.com/config/containers/multi-service_container/

> It is generally recommended that you separate areas of concern by using one service per container.

## Gotchas

### On Windows, this error can occur:

`ERROR: for nginx  Cannot start service nginx: driver failed programming external connectivity on endpoint nginx (cfcfd473ef1cbd22086933313eaebb28eadcf6275378c4a9cc8cb7f34cbd8338): Error starting userland proxy: Bind for 0.0.0.0:80: unexpected error Permission denied`

Try running `netcfg -d` to clean-up network devices.

### Nginx request mut be to /status/

/status does a 301 redirect that will cause health check problems

https://serverfault.com/questions/759762/how-to-stop-nginx-301-auto-redirect-when-trailing-slash-is-not-in-uri/812461#812461

## USEFUL COMMANDS TO RUN CONSOLE COMAMNDS 
## How to run command inside Docker containers?
To run the console command inside the Docker execute the following command in your terminal:

```
docker-compose run php composer install
```

The above command will be executed on php container where we have the access to the PHP-FPM service and the Composer.


## To run any other command:

```
docker-compose run php bin/console doctrine:schema:update --force
```
