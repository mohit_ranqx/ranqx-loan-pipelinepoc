#!/usr/bin/env bash

usage() {
  echo
  echo "Usage: $0 browser|config|credentials"
  echo
  exit 2
}

if [[ -z $1 ]] ||
  ( [[ $1 != 'browser' ]] &&
    [[ $1 != 'config' ]] &&
    [[ $1 != 'credentials' ]] ); then
      usage
fi

test -d ./AwsConfigGen && pushd ./AwsConfigGen || \
  test -d scripts/AwsConfigGen && pushd scripts/AwsConfigGen || \
  ( echo "AwsConfigGen does not exist" && exit 2 )

if [[ ! -d node_modules ]]; then
  npm i
fi

npm run $1
