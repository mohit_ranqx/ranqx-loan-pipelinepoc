const YAML = require('yaml')
const fs = require('fs')

const contents = fs.readFileSync('../../../project.yaml', 'utf8')
const project = YAML.parse(contents)

const { master: { accountId: masterAccountId, company }, subAccounts } = project

module.exports = {
  masterAccountId,
  company,
  subAccounts
}
