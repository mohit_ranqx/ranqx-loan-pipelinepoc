const common = require('./common')
const { masterAccountId, subAccounts } = common

config = Object.keys(subAccounts).reduce((acc, x) => (`${acc}
[profile ${x}]
role_arn = arn:aws:iam::${subAccounts[x].accountId}:role/${subAccounts[x].role}
region = ${subAccounts[x].defaultRegion || 'us-east-1'}
color = ${subAccounts[x].color || '#feff00'}
`), '')

console.log(`
# ====== Copy & Paste the following into the aws-extenc-switch-roles plugin ======
${config}
# ================================================================================`.replace(/^\s+|\s+$/g, ''))