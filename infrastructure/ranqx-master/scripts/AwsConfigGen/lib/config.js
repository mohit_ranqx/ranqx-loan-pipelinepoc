const common = require('./common')
const { masterAccountId, company, subAccounts } = common

const region = 'ap-southeast-2'

config = Object.keys(subAccounts).reduce((acc, x) => (`${acc}
[profile ${company}-${x}]
region=${region}
role_arn=arn:aws:iam::${subAccounts[x].accountId}:role/${subAccounts[x].role}
source_profile=${company}-master
mfa_serial=arn:aws:iam::${masterAccountId}:mfa/<REPLACE_THIS_WITH_YOUR_MFA>
#external_id=1
`), '')

console.log(`
[profile ${company}-master]
region = ${region}
${config}
`.replace(/^\s+|\s+$/g, ''))
