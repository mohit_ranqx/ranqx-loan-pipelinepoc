const common = require('./common')
const { masterAccountId, company, subAccounts } = common

console.log(`
[${company}-master]
aws_access_key_id = <REPLACE THIS WITH THE KEY>
aws_secret_access_key = <REPLACE THIS WITH THE SECRET>
`.replace(/^\s+|\s+$/g, ''))
