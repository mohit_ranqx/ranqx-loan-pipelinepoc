# aws-master Ranqx Resources

The master account is used for billing aggregation and user access only.

* Groups and Polices resources are defined here.
* Users should be added via the console.

## Deployment

Deployed as `ranqx-permissions` in aws-master Sydney. Even though it is deployed in a Region, the resource are Global.
