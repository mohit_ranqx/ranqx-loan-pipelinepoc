# Certificate Generation

```
openssl req -x509 -nodes -days 36500 -newkey rsa:2048 -keyout localhost.key -out localhost.crt -config localhost.conf
```

