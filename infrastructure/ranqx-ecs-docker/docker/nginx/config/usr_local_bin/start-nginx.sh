#!/usr/bin/env bash

# PHP_FPM_HOST is set via an env var so that the same configuration
# can be used locally and with AWS Fargate

set -euxo pipefail

if [[ -e /etc/nginx/conf.d/default.template.conf ]]; then
  echo "---> nginx default template exists processing it"
  # NOTE: second argument to envsubst can have multiple vars separated by spaces
  envsubst '${PHP_FPM_HOST}' < /etc/nginx/conf.d/default.template.conf > \
    /etc/nginx/conf.d/default.conf

  rm -f /etc/nginx/conf.d/default.template.conf
fi

exec /usr/sbin/nginx -g "daemon off;"
