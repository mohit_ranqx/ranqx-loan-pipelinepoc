DROP PROCEDURE IF EXISTS BuildDataDog;

DELIMITER $$

CREATE PROCEDURE BuildDataDog()
proc_Exit:BEGIN

    DELETE FROM mysql.user WHERE User = 'datadog' AND Host != '';

    IF (SELECT 1 FROM mysql.user WHERE User = 'datadog') THEN
        LEAVE proc_Exit;
    ELSE
        CREATE USER 'datadog'@'%' IDENTIFIED WITH mysql_native_password by 'asmFrj4NVKdK';
        ALTER USER 'datadog'@'%' WITH MAX_USER_CONNECTIONS 5;
        GRANT PROCESS, REPLICATION CLIENT ON *.* TO 'datadog'@'%';
        GRANT SELECT ON performance_schema.* TO 'datadog'@'%';
    END IF;

END$$

FLUSH PRIVILEGES;

CALL BuildDataDog();

