#!/usr/bin/env bash

source scripts/_lib.sh

set -euxo pipefail

scripts/platform/deploy-ssl.sh || true

echo

scripts/platform/deploy-s3.sh || true

echo

scripts/platform/deploy-network.sh || true

echo

scripts/platform/deploy-ecs.sh || true

echo

scripts/platform/deploy-elasticache-redis.sh || true

echo

scripts/platform/deploy-rds-mysql.sh || true

echo

scripts/platform/deploy-sns.sh || true

echo

scripts/platform/deploy-sqs.sh || true
