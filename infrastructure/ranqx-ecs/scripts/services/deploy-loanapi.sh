#!/usr/bin/env bash

source scripts/_lib.sh

set -euxo pipefail

STACK_NAME=loanapi

CAPABILITIES="CAPABILITY_IAM"

template="templates/services/4-loan-api.yaml"

AWS_DEFAULT_REGION=$AWS_REGION aws cloudformation deploy \
  --no-fail-on-empty-changeset \
  --template-file $template \
  --stack-name ${PRODUCT_NAME}-${ENV}-${STACK_NAME} \
  --capabilities ${CAPABILITIES} \
  --parameter-overrides ProductName=${PRODUCT_NAME} \
    ImageApiCode=${IMAGE_API_CODE} ImageApiNginx=${IMAGE_API_NGINX} \
    ImageUiCode=${IMAGE_UI_CODE} \
    ImageApiPhpfpm=${IMAGE_API_PHPFPM} ImageApiInit=${IMAGE_API_INIT} \
    Env=${ENV} EnvDomain=${ENVDOMAIN} Version=${VERSION} \
    DefaultDesiredCount=${SERVICE_UI_DESIRED_COUNT}
