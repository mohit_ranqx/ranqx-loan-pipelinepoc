#!/usr/bin/env bash

source scripts/_lib.sh

set -euxo pipefail

STACK_NAME=phporgsqs

CAPABILITIES="CAPABILITY_IAM"

template="templates/services/6-php-orgsqs.yaml"

AWS_DEFAULT_REGION=$AWS_REGION aws cloudformation deploy \
  --no-fail-on-empty-changeset \
  --template-file $template \
  --stack-name ${PRODUCT_NAME}-${ENV}-${STACK_NAME} \
  --capabilities ${CAPABILITIES} \
  --parameter-overrides ProductName=${PRODUCT_NAME} \
    ImageApiCode=${IMAGE_API_CODE} ImageJobsCalc=${IMAGE_JOBS_CALC} \
    Env=${ENV} Version=${VERSION} \
    DefaultDesiredCount=1
