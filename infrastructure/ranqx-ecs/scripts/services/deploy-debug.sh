#!/usr/bin/env bash

source scripts/_lib.sh

set -euxo pipefail

STACK_NAME=debug

CAPABILITIES="CAPABILITY_IAM"

template="templates/services/99-debug.yaml"

AWS_DEFAULT_REGION=$AWS_REGION aws cloudformation deploy \
  --template-file $template \
  --stack-name ${PRODUCT_NAME}-${ENV}-${STACK_NAME} \
  --capabilities ${CAPABILITIES} \
  --parameter-overrides ProductName=${PRODUCT_NAME} \
    Env=${ENV} Version=${VERSION} \
    DefaultDesiredCount=1
