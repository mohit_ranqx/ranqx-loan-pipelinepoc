#!/usr/bin/env bash

source scripts/_lib.sh

EXPECTED_ENV=test
requireEnv $EXPECTED_ENV "This should only be deployed to ${EXPECTED_ENV}"

set -euxo pipefail

STACK_NAME=s3

CAPABILITIES="CAPABILITY_IAM"

template="templates/custom/${EXPECTED_ENV}/s3.yaml"

AWS_DEFAULT_REGION=$AWS_REGION aws cloudformation deploy \
  --template-file $template \
  --stack-name ${PRODUCT_NAME}-${ENV}-${STACK_NAME} \
  --capabilities ${CAPABILITIES} \
  --parameter-overrides ProductName=${PRODUCT_NAME} \
    Env=${ENV} Version=${VERSION} \
    S3TempBucket=${S3_TEMP_BUCKET}
