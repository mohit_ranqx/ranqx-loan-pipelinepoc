#!/usr/bin/env bash

source scripts/_lib.sh

EXPECTED_ENV=test
requireEnv $EXPECTED_ENV "This should only be deployed to ${EXPECTED_ENV}"

set -euxo pipefail

STACK_NAME=ec2

CAPABILITIES="CAPABILITY_IAM"

TEMPLATE="templates/custom/${EXPECTED_ENV}/ec2.yaml"
TEMPLATE_BUILD="templates/custom/${EXPECTED_ENV}/build/ec2.yaml"

AWS_DEFAULT_REGION=$AWS_REGION aws cloudformation package \
  --template-file $TEMPLATE \
  --s3-bucket $S3_TEMP_BUCKET \
  --output-template-file $TEMPLATE_BUILD

AWS_DEFAULT_REGION=$AWS_REGION aws cloudformation deploy \
  --template-file $TEMPLATE_BUILD \
  --stack-name ${PRODUCT_NAME}-${ENV}-${STACK_NAME} \
  --capabilities ${CAPABILITIES} \
  --parameter-overrides ProductName=${PRODUCT_NAME} \
    Env=${ENV} Version=${VERSION} \
    Ec2SshKey=${EC2_SSH_KEY} 
