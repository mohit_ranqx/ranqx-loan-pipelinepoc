#!/bin/bash

group=/ecs/ranqxloan-sectest
stream=$1

export LC_ALL='en_US.utf8'

aws --region=ap-southeast-2 logs get-log-events --start-time 1 \
    --log-group-name $group --log-stream-name $stream \
    | sed -rn '/^ *"message"/ {s/^ *"message": *"(.*)",/\1/; p; }' \
    | sed -r 's/\\"/"/g'
