fatalError() {
  echo
  echo "Error: ${1}"
  echo
  exit 2
}

requireEnv() {
  if [[ $ENV != "$1" ]]; then
    fatalError "$2"
  fi
}

if [[ -z $ENV ]]; then
  fatalError "ENV is not set"
fi

if [[ ! -f .env/${ENV} ]]; then
  fatalError ".env/${ENV} file does not exist"
fi

source .env/_common
source .env/${ENV}

for var in AWS_REGION PRODUCT_NAME VERSION VPC_SUBNET ENABLE_NAT_GATEWAY_A ENABLE_NAT_GATEWAY_B; do
  if [[ -z ${!var} ]]; then
    fatalError "${var} is not set"
  fi
done
