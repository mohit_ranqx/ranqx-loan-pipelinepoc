#!/usr/bin/env bash

source scripts/_lib.sh

set -euxo pipefail

STACK_NAME=redis

template="templates/platform/3-elasticache-redis.yaml"

AWS_DEFAULT_REGION=$AWS_REGION aws cloudformation deploy \
  --template-file $template \
  --stack-name ${PRODUCT_NAME}-${ENV}-${STACK_NAME} \
  --parameter-overrides ProductName=${PRODUCT_NAME} \
    Env=${ENV} Version=${VERSION} \
    RedisEnabled=${REDIS_ENABLED} \
    RedisAzMode=${REDIS_AZ_MODE} \
    RedisNodeType=${REDIS_NODE_TYPE} \
    RedisNodeCount=${REDIS_NODE_COUNT}
