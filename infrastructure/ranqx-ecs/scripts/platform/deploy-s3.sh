#!/usr/bin/env bash

source scripts/_lib.sh

set -euxo pipefail

STACK_NAME=s3

template="templates/platform/3-s3.yaml"

AWS_DEFAULT_REGION=$AWS_REGION aws cloudformation deploy \
  --template-file $template \
  --stack-name ${PRODUCT_NAME}-${ENV}-${STACK_NAME} \
  --parameter-overrides ProductName=${PRODUCT_NAME} \
    Env=${ENV} Version=${VERSION}
