#!/usr/bin/env bash

source scripts/_lib.sh

set -euxo pipefail

STACK_NAME=ssl

template="templates/platform/1-ssl.yaml"

AWS_DEFAULT_REGION=$AWS_REGION aws cloudformation deploy \
  --template-file $template \
  --stack-name ${PRODUCT_NAME}-${ENV}-${STACK_NAME} \
  --parameter-overrides ProductName=${PRODUCT_NAME} \
    Env=${ENV} Version=${VERSION} \
    SslDomain=${SSL_DOMAIN}
