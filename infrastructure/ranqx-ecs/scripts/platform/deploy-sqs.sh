#!/usr/bin/env bash

source scripts/_lib.sh

set -euxo pipefail

STACK_NAME=sqs

template="templates/platform/4-sqs.yaml"

AWS_DEFAULT_REGION=$AWS_REGION aws cloudformation deploy \
  --template-file $template \
  --stack-name ${PRODUCT_NAME}-${ENV}-${STACK_NAME} \
  --parameter-overrides ProductName=${PRODUCT_NAME} \
    Env=${ENV} Version=${VERSION} VpcSubnet=${VPC_SUBNET}
