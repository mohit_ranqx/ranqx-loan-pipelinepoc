#!/usr/bin/env bash

source scripts/_lib.sh

set -euxo pipefail

STACK_NAME=mysql

template="templates/platform/3-rds-mysql.yaml"

AWS_DEFAULT_REGION=$AWS_REGION aws cloudformation deploy \
  --template-file $template \
  --stack-name ${PRODUCT_NAME}-${ENV}-${STACK_NAME} \
  --parameter-overrides ProductName=${PRODUCT_NAME} \
    Env=${ENV} Version=${VERSION} \
    MySqlEnabled=${MYSQL_ENABLED} \
    MySqlMultiAz=${MYSQL_MULTI_AZ} \
    MySqlStorage=${MYSQL_STORAGE} \
    MySqlInstanceClass=${MYSQL_INSTANCE_CLASS}
