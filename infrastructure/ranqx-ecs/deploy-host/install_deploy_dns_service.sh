#!/bin/bash

cat <<EOF
Installing deploy-dns service which will set a DNS entry to access a deploy
service each time it is booted, handling dynamic EC2 addresses.
EOF

set -euxo pipefail

cd $(dirname $0)

sudo cp update_deploy_dns.py /usr/local/bin
sudo chown root.root /usr/local/bin/update_deploy_dns.py
sudo chmod 755 /usr/local/bin/update_deploy_dns.py

sudo cp deploy-dns.service /etc/systemd/system/
cd /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable deploy-dns.service
sudo systemctl start deploy-dns.service
