#!/usr/bin/env python3

# Fetch public hostname of this instance and set it into Route53
# as deploy.<hosted-zone>

import boto3
import sys
import urllib.request

# Set to my hostname on the way up, something non-accessible on the way down
if sys.argv[1] == 'down':
    deploy_hostname = 'no-access.ranqx.io'
else:
    me_url="http://169.254.169.254/latest/meta-data/public-hostname"
    deploy_hostname=urllib.request.urlopen(me_url).read().decode()

r53 = boto3.client('route53')
first_hosted_zone = r53.list_hosted_zones()['HostedZones'][0]

resp = r53.change_resource_record_sets(
    HostedZoneId=first_hosted_zone['Id'],
    ChangeBatch={
        'Changes' : [
            {
                'Action': 'UPSERT',
                'ResourceRecordSet': {
                    'Name': 'deploy.' + first_hosted_zone['Name'],
                    'Type': 'CNAME',
                    'TTL' : 30,
                    'ResourceRecords': [
                        {
                            'Value': deploy_hostname
                        },
                    ]
                }
            }
        ]
    }
)
